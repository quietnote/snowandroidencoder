//
//  SensetimeCommon.cpp
//  SnowSenseTime
//
//  Created by heejun on 2018. 12. 26..
//  Copyright © 2018년 SnowUnity. All rights reserved.


// common header for licence check and Iunity interface
#include <vector>
#include <stdio.h>
#include "st_mobile_license.h"
#include "IUnityInterface.h"
#define U_IEXPORT UNITY_INTERFACE_EXPORT
#define U_IAPI UNITY_INTERFACE_API


extern "C" int U_IEXPORT U_IAPI
SnowSenseTime_GenerateActivationCode(const unsigned char* licenseBuffer, int licenseBufferSize, unsigned char* activationCodeBuffer, int* activationCodeBufferSize)
{
    return st_mobile_generate_activecode_from_buffer((char*)licenseBuffer, licenseBufferSize, (char*)activationCodeBuffer, activationCodeBufferSize);
}

extern "C" int U_IEXPORT U_IAPI
SnowSenseTime_CheckActivationCode(const unsigned char* licenseBuffer, int licenseBufferSize, const unsigned char* activationCodeBuffer, int activationCodeBufferSize)
{
    return st_mobile_check_activecode_from_buffer((char*)licenseBuffer, licenseBufferSize, (char*)activationCodeBuffer, activationCodeBufferSize);
}
