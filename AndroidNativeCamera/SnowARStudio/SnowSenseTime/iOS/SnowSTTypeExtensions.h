//
//  SensetimeCommon.cpp
//  SnowSenseTime
//
//  Created by victor on 2018. 12. 26..
//  Copyright © 2018년 SnowUnity. All rights reserved.
//

// extension of sensetime common types, should be sync with SenseTimeLib.cs
#include <vector>
#include <stdio.h>
#include "st_mobile_common.h"

struct st_point4f_t {
    float x;
    float y;
    float z;
    float w;
};