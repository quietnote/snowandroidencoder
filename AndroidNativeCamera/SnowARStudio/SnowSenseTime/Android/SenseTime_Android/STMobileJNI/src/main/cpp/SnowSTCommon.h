//
//  SensetimeCommon.cpp
//  SnowSenseTime
//
//  Created by heejun on 2018. 12. 26..
//  Copyright © 2018년 SnowUnity. All rights reserved.


// common header for licence check and Iunity interface
#include <vector>
#include <stdio.h>
#include "st_mobile_license.h"
#include "IUnityInterface.h"
#define U_IEXPORT UNITY_INTERFACE_EXPORT
#define U_IAPI UNITY_INTERFACE_API


#include <jni.h>
#include <android/log.h>

#define LOG_TAG    "SenseTime_JNI"
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

JNIEnv* g_env;

extern "C" JNIEXPORT jint JNI_OnLoad(JavaVM* vm, void* reserved) {
    vm->AttachCurrentThread(&g_env, 0);
    return JNI_VERSION_1_6;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GenerateActivationCode(jobject context, const unsigned char* licenseBuffer, int licenseBufferSize, unsigned char* activationCodeBuffer, int* activationCodeBufferSize)
{
    return st_mobile_generate_activecode_from_buffer(g_env, context, (char*)licenseBuffer, licenseBufferSize, (char*)activationCodeBuffer, activationCodeBufferSize);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_CheckActivationCode(jobject context, const unsigned char* licenseBuffer, int licenseBufferSize, const unsigned char* activationCodeBuffer, int activationCodeBufferSize)
{
    return st_mobile_check_activecode_from_buffer(g_env, context, (char*)licenseBuffer, licenseBufferSize, (char*)activationCodeBuffer, activationCodeBufferSize);
}
