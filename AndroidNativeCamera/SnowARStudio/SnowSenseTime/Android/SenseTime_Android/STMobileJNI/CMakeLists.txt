cmake_minimum_required(VERSION 3.4.1)

add_library(SnowSenseTime
            SHARED
                 src/main/cpp/SnowSenseTime.cpp
            )

include_directories(./src/main/cpp/include)

find_library(log-lib log)
find_library(dl-lib dl)
find_library(android-lib android)
find_library(stmobile-lib st_mobile PATHS ./src/main/cpp/libs/${ANDROID_ABI}/ NO_CMAKE_FIND_ROOT_PATH)

target_link_libraries( SnowSenseTime ${log-lib} ${dl-lib} ${stmobile-lib} ${android-lib})


