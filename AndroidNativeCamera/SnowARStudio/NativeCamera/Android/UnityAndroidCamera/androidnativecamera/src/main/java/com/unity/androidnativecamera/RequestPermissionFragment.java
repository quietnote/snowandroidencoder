package com.unity.androidnativecamera;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class RequestPermissionFragment extends Fragment {
    public static final String TAG = "Young";

    public static final int REQUEST_PERMISSIONS_CODE = 555;
    RequestPermissionCallback mCallback;

    private static final String[] CAMERA_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
    };

    public void setCallback(RequestPermissionCallback callback){
        mCallback = callback;
    }


    @Override
    public void onCreate( Bundle savedInstanceState ) {

        super.onCreate( savedInstanceState );

        //Bundle bundle = getArguments();
        //CAMERA_PERMISSIONS = bundle.getStringArray("requestPermissionFragment");

        Log.d(TAG, "onCreate -- CAMERA_PERMISSIONS.length = " + CAMERA_PERMISSIONS.length);
//        if(CAMERA_PERMISSIONS.length<=0){
//            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
//        }
        requestCameraPermissions();
    }

    private void requestCameraPermissions() {
        if (shouldShowRationale()) {
            Log.d(TAG, "requestCameraPermissions() -- shouldShowRationale() is true ");
            PermissionConfirmationDialog.newInstance().show(getChildFragmentManager(), "dialog");

            /*AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage(*//*R.string.request_permission*//* "This app needs camera permission.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(CAMERA_PERMISSIONS, REQUEST_PERMISSIONS_CODE);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }
                            })
                    .create().show();
*/

        } else {// ask permission for the first time
            Log.d(TAG, "requestCameraPermissions() --> requestPermissions ");
            requestPermissions(CAMERA_PERMISSIONS, REQUEST_PERMISSIONS_CODE);
        }
    }

    private boolean shouldShowRationale() {
        Activity activity = getActivity();
        for (String permission : CAMERA_PERMISSIONS) {
            if (shouldShowRequestPermissionRationale(permission)) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult -- permissions = " + permissions);
        Log.d(TAG, "onRequestPermissionsResult -- grantResults = " + grantResults);

        if (requestCode == REQUEST_PERMISSIONS_CODE) {

            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    showMissingPermissionError();
                    mCallback.requestPermissionResult(0); //denied
                    return;
                }
            }
            mCallback.requestPermissionResult(1); //granted

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        Log.d(TAG, "onRequestPermissionsResult -- end");
    }

//    private void showMissingPermissionError(){
//        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
//        dialog.setTitle("Ask Permission")
//                .setMessage("If you want to use this app, we need the permissions")
//                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        try {
//                            Log.d(TAG, "onRequestPermissionsResult : onClick Positive");
//                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
//                                    .setData(Uri.parse("package:" + getActivity().getPackageName()));
//                            getActivity().startActivity(intent);
//
//                            //getActivity().finish();
//                            getActivity().getSupportFragmentManager().beginTransaction().remove(RequestPermissionFragment.this).commit();
//
//                        } catch (ActivityNotFoundException e) {
//                            Log.d(TAG, "onRequestPermissionsResult : onClick Positive -- ActivityNotFoundException");
//                            e.printStackTrace();
//
//                            Intent intent = new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
//                            getActivity().startActivity(intent);
//
//                            //getActivity().finish();
//                            getActivity().getSupportFragmentManager().beginTransaction().remove(RequestPermissionFragment.this).commit();
//                        }
//                    }
//                })
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Log.d(TAG, "onRequestPermissionsResult : onClick Positive -- ActivityNotFoundException");
//                        //getFragmentManager().beginTransaction().remove(this).commit(); ---------------------------------------------
//                        getActivity().finish();
//                    }
//                })
//                .create()
//                .show();
//    }



    private void showMissingPermissionError() {
        Activity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, "This app needs camera permission.", Toast.LENGTH_SHORT).show();
            activity.finish();
        }
    }

    public static class PermissionConfirmationDialog extends DialogFragment {

        public static PermissionConfirmationDialog newInstance() {
            return new PermissionConfirmationDialog();
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            dialog.setMessage(/*R.string.request_permission*/ "This app needs camera permission.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(CAMERA_PERMISSIONS, REQUEST_PERMISSIONS_CODE);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }
                            });
            return dialog.create();
        }
    }
}
