package com.unity.androidnativecamera;

public interface IFrameReady
{
    void onFrameUpdate(FrameData frameData, int yStride, int uvStride, int uvStep);
}
