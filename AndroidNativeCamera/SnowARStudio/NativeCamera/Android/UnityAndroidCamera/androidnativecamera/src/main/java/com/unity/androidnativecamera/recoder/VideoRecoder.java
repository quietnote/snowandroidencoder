package com.unity.androidnativecamera.recoder;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.util.Log;

import com.unity.libraryyuv.Libyuv;

public class VideoRecoder {
    private final static String TAG = "Young";
    public static final int AUDIO_FORMAT = AudioFormat.ENCODING_PCM_16BIT;//AudioFormat.ENCODING_PCM_FLOAT;


    private int mVideoWidth = 1280;
    private int mVideoHeight = 760;
    private int mAudioChannels = 1;
    private int mChannelConFig = AudioFormat.CHANNEL_IN_MONO;
    private int mAudioSampleRate = 44100;
    private int mFrameRate = 10;//30;

    private int mVFrameCount = 0;
    private int mAFrameCount = 0;

    private enum RecordState {RECORDING, PAUSED, STOPPED};
    volatile RecordState mRecordState = RecordState.STOPPED;

    //private long mPresentTimeUs;
    MuxingHandlerThread mMuxingThread;
    private long startTime = 0;


    public VideoRecoder(int width, int height, int audioChannels, int audioSampleRate, int frameRate) {
        mVideoWidth = width;
        mVideoHeight = height;
        mAudioChannels = audioChannels;
        mChannelConFig = (mAudioChannels == 2) ? AudioFormat.CHANNEL_IN_STEREO : AudioFormat.CHANNEL_IN_MONO;
        mAudioSampleRate = audioSampleRate;
        mFrameRate = frameRate;
        Libyuv.setEncoderResolution(mVideoWidth, mVideoHeight);
    }


    public void prepare() {
        startMuxingingThread();
    }

    private void startMuxingingThread() {
        if (mMuxingThread == null) {
            mMuxingThread = new MuxingHandlerThread("MuxingHandlerThread");
        }

        mMuxingThread.start();
        mMuxingThread.prepare(mVideoWidth, mVideoHeight, mAudioChannels, mAudioSampleRate, mFrameRate);
    }

    private void quitMuxingThread() {
        Log.e(TAG, "quitMuxingThread()");
        if (mMuxingThread != null && mMuxingThread.isAlive()) {
            mMuxingThread.quit();
        }
        //mMuxingThread = null;
    }

    private void clearQueueMuxingThread() {
        Log.e(TAG, "clearQueueMuxingThread()");
        if (mMuxingThread == null) {
            return;
        }
        mMuxingThread.clearQueue();
    }


    public boolean isRecordingVideo() {
        if (mRecordState == RecordState.RECORDING) {
            return true;
        }
        return false;
    }


    private AudioRecordThread mAudioRecordThread;

    public void startAudioRecordForTest() {
        Log.e(TAG, "startAudioRecordForTest()");
        mAudioRecordThread = new AudioRecordThread();
        mAudioRecordThread.start();
    }

    class RunningThread extends Thread {
        boolean isRunning;

        public boolean isRunning() {
            return isRunning;
        }

        public void stopRunning() {
            Log.e(TAG, "RunningThread: stopRunning()");
            this.isRunning = false;
        }
    }


    class AudioRecordThread extends RunningThread {
        private AudioRecord mAudioRecord;
        private byte[] mAudioDataBuffer;
        //private float[] mAudioDataBuffer;

        public AudioRecordThread() {
//        mBufferSize = AudioRecord.getMinBufferSize(mAudioSampleRate, mChannelConFig, AUDIO_FORMAT);
//        int buffer_size = SAMPLES_PER_FRAME * 10;
//        if (buffer_size < mBufferSize)
//            buffer_size = ((mBufferSize / SAMPLES_PER_FRAME) + 1) * SAMPLES_PER_FRAME * 2;

            Log.w(TAG, "AudioRecordThread(): mAudioSampleRate = " + mAudioSampleRate + ", mChannelConFig = " + mChannelConFig);
            int bufferSize = AudioRecord.getMinBufferSize(mAudioSampleRate, mChannelConFig, AUDIO_FORMAT) + 8191;
            if (bufferSize != AudioRecord.ERROR_BAD_VALUE) {
                bufferSize = bufferSize - (bufferSize % 8192);
            }

            bufferSize = 4096;

            mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, mAudioSampleRate, mChannelConFig, AUDIO_FORMAT, bufferSize);
            Log.w(TAG, "AudioRecordThread(): mAudioRecord.getState() = " + mAudioRecord.getState() + ", bufferSize = " + bufferSize);
            if (mAudioRecord.getState() != AudioRecord.STATE_INITIALIZED || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
                Log.e(TAG, "AudioRecordThread(): initializing failed");
                mAudioRecord = null;
                return;
            }

            mAudioDataBuffer = new byte[bufferSize];
        }

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
            //android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

            Log.e(TAG, "AudioRecordThread run()");
            mAudioRecord.startRecording();
            isRunning = true;
            mRecordState = RecordState.RECORDING;
            /* audio encoding loop */
            while (isRunning) {
                if (mRecordState == RecordState.RECORDING && mAudioRecord != null) {
                    //int bufferReadSize = mAudioRecord.read(mAudioData.array(), 0, mAudioData.capacity());
                    //mAudioData.limit(bufferReadSize);

                    int bufferReadSize = mAudioRecord.read(mAudioDataBuffer, 0, mAudioDataBuffer.length, AudioRecord.READ_BLOCKING);
                    if (bufferReadSize > 0) {
                        Log.d(TAG, "AudioRecordThread run() : read() bufferReadSize = " + bufferReadSize);
                        OnReceiveAudioData(mAudioDataBuffer);
                    }
                }
            }
            Log.d(TAG, "mAudioRecord run() -- stopRecording");
            mAudioRecord.stop();
            mAudioRecord.release();
            mAudioRecord = null;
            Log.d(TAG, "mAudioRecord released");
        }
    }

    public boolean startRecording(boolean bTest) {
        mVFrameCount = 0;
        mAFrameCount = 0;

        if (bTest == true) {
            startAudioRecordForTest();
        }
        else{
            mRecordState = RecordState.RECORDING;
        }
        //mPresentTimeUs = System.currentTimeMillis();
        startTime = System.currentTimeMillis();
        return true;
    }

    public void stopRecording() {

        mRecordState = RecordState.STOPPED;
        long duration = System.currentTimeMillis()- startTime;
        if (mAudioRecordThread != null) {
            if (mAudioRecordThread.isRunning() == true) {
                mAudioRecordThread.stopRunning();
            }
        }

        clearQueueMuxingThread();
        quitMuxingThread();

        if (mMuxingThread != null) {
            mMuxingThread.stopEncording();
        }
        mMuxingThread = null;

        mVFrameCount = 0;
        mAFrameCount = 0;
        startTime = 0;
        Log.e(TAG, "stopRecording(): duration = " + duration + ", mAFrameCount = " + mAFrameCount + ", mVFrameCount = " + mVFrameCount);
    }


    public void releaseResources() {
        Log.d(TAG, "release(): releasing encoder objects");

        try {
            if (mAudioRecordThread != null) {
                mAudioRecordThread.interrupt();
                try {
                    mAudioRecordThread.join();
                } catch (InterruptedException e) {
                    mAudioRecordThread.interrupt();
                } finally {
                    mAudioRecordThread = null;
                    //mRecordState = RecordState.STOPPED;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        clearQueueMuxingThread();
        quitMuxingThread();
        mMuxingThread = null;

    }


    byte[] data_Y = null;
    byte[] data_UV = null;

    public void OnReceiveVideoData(byte[] argbByte) {
        Log.w(TAG, "[VIDEO] OnReceiveVideoData: mRecordState = " + mRecordState + ", argbByte.length = " + argbByte.length+ ", mVFrameCount = " + mVFrameCount);

        byte[] nv21_frameBuffer = Libyuv.ARGBToNV21(argbByte, mVideoWidth, mVideoHeight, false, 90);
        Log.v(TAG, "Camera2: onImageAvailable --> OnReceiveVideoData(): nv21_frameBuffer.length = " + nv21_frameBuffer.length);


        if (mRecordState == RecordState.RECORDING && mMuxingThread != null) {
            //long pts = System.currentTimeMillis() - mPresentTimeUs;

            mMuxingThread.requestEncode(computePresentationTimeVideo(mVFrameCount), nv21_frameBuffer);
            mVFrameCount++;
        }
    }

    //public void OnReceiveAudioData(float[] data) {
    public void OnReceiveAudioData(byte[] data) {
        Log.w(TAG, "[AUDIO] OnReceiveAudioData: mRecordState = " + mRecordState + ", data.length = " + data.length + ", mAFrameCount = " + mAFrameCount);

        if (mRecordState == RecordState.RECORDING && mMuxingThread != null) {
            //long pts = System.currentTimeMillis() - mPresentTimeUs;

            mMuxingThread.requestAudioEncode(computePresentationTimeAudio(mAFrameCount, data.length), data);
            mAFrameCount++;
        }
    }

    private long computePresentationTimeMsec(int frameIndex) {
        final long ONE_BILLION = 1000000000;
        return frameIndex * ONE_BILLION / (long) mFrameRate / 1000;
    }


    private long computePresentationTimeVideo(int frameIndex) {
        final long ONE_MILLION = 1000000;
        return (long)frameIndex * ONE_MILLION / (long) mFrameRate;
    }

    private long computePresentationTimeAudio(int frameIndex, int dataSize) {
        final long BYTE_PER_SEC = mAudioSampleRate * 16/8;   // 44100*16bit / 8bit ==> 88200 byte per sec
        //final long DATA_SIZE = 4096;
        //Log.v(TAG, "[AUDIO] computePresentationTimeAudio: dataSize = " + dataSize + ", frameIndex = " + frameIndex + ", (1000 *1000 * dataSize/BYTE_PER_SEC) = " + (1000 *1000 * (long)dataSize/BYTE_PER_SEC));

        return frameIndex * (1000 *1000 * (long)dataSize/BYTE_PER_SEC);
    }

}
