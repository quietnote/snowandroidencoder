package com.unity.androidnativecamera.recoder;

import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.util.Log;

import com.unity.androidnativecamera.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

class MediaMuxerWrapper {
    private static final  String TAG = "Young";

    private static final int OUTPUT_FORMAT = MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4;
    private static final int TOTAL_TRACK_NUM = 2;
    private File mOutputFile = null;

    MediaMuxer mMuxer;
    boolean mMuxerStarted = false;
    int mAddedTrackNum = 0;
    int mFinishedTrackNum = 0;
    Object sync = new Object();

    public MediaMuxerWrapper() {
        restart();
    }

    public int addTrack(MediaFormat mediaFormat) {
        int trackIndex = mMuxer.addTrack(mediaFormat);
        mAddedTrackNum++;

        if (mAddedTrackNum == TOTAL_TRACK_NUM) {
            mMuxer.start();
            mMuxerStarted = true;
            Log.e(TAG, "MediaMuxerWrapper : addTrack() -- mMuxerStarted = " + mMuxerStarted);
        }
        return trackIndex;
    }

    public void finishTrack() {
        mFinishedTrackNum++;
        Log.e(TAG, "MediaMuxerWrapper : finishTrack() -- mFinishedTrackNum = " + mFinishedTrackNum);
        if (mFinishedTrackNum == TOTAL_TRACK_NUM) {
            stop();
        }
    }

    public boolean isAllTracksAdded() {
        return (mAddedTrackNum == TOTAL_TRACK_NUM);
    }

    public boolean isAllTracksFinished() {
        return (mFinishedTrackNum == TOTAL_TRACK_NUM);
    }


    public void stop() {
        if (mMuxer != null) {
            if (mMuxerStarted == false) {
                Log.e(TAG, "Stopping Muxer before it was started");
                return;
            }

            if (isAllTracksFinished() == false) {
                Log.e(TAG, "Stopping Muxer before all tracks added!");
                return;
            }

            Log.e(TAG, "MediaMuxerWrapper : stop()");
            mMuxer.stop();
            mMuxerStarted = false;
            mMuxer.release();
            mMuxer = null;
            mAddedTrackNum = 0;
            mFinishedTrackNum = 0;
        }
    }

    private void restart() {
        stop();

        String recordedTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        mOutputFile = Utils.getOutputVideoFile(recordedTime, OUTPUT_FORMAT);

        try {
            mMuxer = new MediaMuxer(mOutputFile.toString(), OUTPUT_FORMAT);
        } catch (IOException e) {
            throw new RuntimeException("MediaMuxer creation failed", e);
        }
    }
}
