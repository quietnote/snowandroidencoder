package com.unity.androidnativecamera;

public class FrameData {

    public byte[] mBufferY;
    public byte[] mBufferU;
    public byte[] mBufferV;
    public byte[] mRGB;
    public int mWidth;
    public int mHeight;

    public FrameData(byte[] bufferY, byte[] bufferU, byte[] bufferV, byte[] rgb, int width, int height) {
        mBufferY=bufferY;
        mBufferU=bufferU;
        mBufferV=bufferV;
        mRGB = rgb;

        mWidth = width;
        mHeight = height;
    }
}
