package com.unity.androidnativecamera;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaMuxer;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class Utils {
    private final static String LOGTAG = "Young";
    private static RenderScript mRS = null;
    private static ScriptIntrinsicYuvToRGB mYuvToRgb = null;
    private static Allocation ain = null;
    private static Allocation aOut = null;


    static public Bitmap NV21ToRGBABitmap(byte[] nv21, int width, int height) {
        YuvImage yuvImage = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        yuvImage.compressToJpeg(new Rect(0, 0, width, height), 100, baos);
        byte[] cur = baos.toByteArray();
        return BitmapFactory.decodeByteArray(cur, 0, cur.length);
    }

    public static final int YUV_420 = 1;
    public static final int NV21 = 2;


    public static byte[] YUVToRGBAByte(byte[] frame, int width, int height, Context context, int inputFormat) {

        int frameSize = width * height * 4;
        byte[] rgbaBuffer = new byte[frameSize];


        try {
            if (mRS == null) {
                mRS = RenderScript.create(context);

                switch (inputFormat) {
                    case YUV_420: {
                        mYuvToRgb = ScriptIntrinsicYuvToRGB.create(mRS, Element.RGBA_8888(mRS));

                        Type.Builder yuvTypeBuilder = new Type.Builder(mRS, Element.YUV(mRS));
                        yuvTypeBuilder.setX(width).setY(height);
                        yuvTypeBuilder.setYuvFormat(ImageFormat.YUV_420_888);

                        Type.Builder rgbTypeBuilder = new Type.Builder(mRS, Element.RGBA_8888(mRS));
                        rgbTypeBuilder.setX(width).setY(height);

                        ain = Allocation.createTyped(mRS, yuvTypeBuilder.create(), Allocation.USAGE_SCRIPT);
                        aOut = Allocation.createTyped(mRS, rgbTypeBuilder.create(), Allocation.USAGE_SCRIPT & Allocation.USAGE_SHARED);
                    }
                    break;

                    case NV21: {
                        mYuvToRgb = ScriptIntrinsicYuvToRGB.create(mRS, Element.U8_4(mRS));

                        Type.Builder yuvTypeBuilder = new Type.Builder(mRS, Element.U8(mRS));
                        yuvTypeBuilder.setX(width).setY(height).setMipmaps(false);
                        yuvTypeBuilder.setYuvFormat(ImageFormat.NV21);

                        Type.Builder rgbTypeBuilder = new Type.Builder(mRS, Element.RGBA_8888(mRS));
                        rgbTypeBuilder.setX(width).setY(height).setMipmaps(false);

                        ain = Allocation.createTyped(mRS, yuvTypeBuilder.create(), Allocation.USAGE_SCRIPT);
                        aOut = Allocation.createTyped(mRS, rgbTypeBuilder.create(), Allocation.USAGE_SCRIPT);
                    }
                    break;
                }


                mYuvToRgb = ScriptIntrinsicYuvToRGB.create(mRS, Element.RGBA_8888(mRS));

                Type.Builder yuvTypeBuilder = new Type.Builder(mRS, Element.createPixel(mRS, Element.DataType.UNSIGNED_8, Element.DataKind.PIXEL_YUV));
                yuvTypeBuilder.setX(width).setY(height).setMipmaps(false);
                yuvTypeBuilder.setYuvFormat(ImageFormat.NV21);
                ain = Allocation.createTyped(mRS, yuvTypeBuilder.create(), Allocation.USAGE_SCRIPT);

                Type.Builder rgbTypeBuilder = new Type.Builder(mRS, Element.RGBA_8888(mRS));
                rgbTypeBuilder.setX(width).setY(height).setMipmaps(false);
                aOut = Allocation.createTyped(mRS, rgbTypeBuilder.create(), Allocation.USAGE_SCRIPT);
            }
            ain.copyFrom(frame);
            mYuvToRgb.setInput(ain);
            mYuvToRgb.forEach(aOut);
            aOut.copyTo(rgbaBuffer);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return rgbaBuffer;
    }

    public static boolean isOrientationLandscape(Context context) {
        boolean isOrientationLandscape;
        int orientation = context.getResources().getConfiguration().orientation;
        switch (orientation) {
            case Configuration.ORIENTATION_LANDSCAPE:
                isOrientationLandscape = true;
                break;
            case Configuration.ORIENTATION_PORTRAIT:
            default:
                isOrientationLandscape = false;
        }
        return isOrientationLandscape;
    }

    public static File getOutputVideoFile(String fileName, int type) {
        if (!Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
            return null;
        }

        //File f = new File(Environment.getExternalStorageDirectory(), "Download/video_encoded.264");
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), BuildConfig.APPLICATION_ID);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(LOGTAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        File mediaFile;
        if (type == MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }


    public static int selectColorFormat(MediaCodecInfo codecInfo, String mimeType) {
        MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType(mimeType);
        for (int i = 0; i < capabilities.colorFormats.length; i++) {
            int colorFormat = capabilities.colorFormats[i];
            if (isRecognizedFormat(colorFormat)) {
                return colorFormat;
            }
        }
        Log.e(LOGTAG, "couldn't find a good color format for " + codecInfo.getName() + " / " + mimeType);
        return 0;   // not reached
    }

    /**
     * Returns true if this is a color format that this test code understands (i.e. we know how
     * to read and generate frames in this format).
     */
    public static boolean isRecognizedFormat(int colorFormat) {
        switch (colorFormat) {
            // these are the formats we know how to handle for this test
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
            //case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible:
                return true;
            default:
                return false;
        }
    }


    public static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            if (!codecInfo.isEncoder()) {
                continue;
            }
            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }


}
