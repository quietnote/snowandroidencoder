package com.unity.androidnativecamera.recoder;

import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import com.unity.androidnativecamera.Utils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MuxingHandlerThread extends HandlerThread {
    private final static String TAG = "Young";
    private static final int MSG_SEND_TO_ENCODER = 11;
    private static final int MSG_SEND_TO_AUDIO_ENCODER = 22;

    private enum Type {VIDEO, AUDIO};
    Map<Long, byte[]> requestMap = Collections.synchronizedMap(new HashMap<Long, byte[]>());
    Map<Long, byte[]> requestAudioMap = Collections.synchronizedMap(new HashMap<Long, byte[]>());

    Context mContext;
    Handler mHandler;


    public MuxingHandlerThread(String name) {
        super(name);
    }

    protected void onLooperPrepared() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Log.v(TAG, "handleMessage: msg.what = " + msg.what + ", msg.obj = " + (long) msg.obj);

                switch (msg.what) {
                    case MSG_SEND_TO_ENCODER:
                        sendToEncoder(mVideoEncoder, (long) msg.obj);
                        break;

                    case MSG_SEND_TO_AUDIO_ENCODER:
                        sendToAudioEncoder(mAudioEncoder, (long) msg.obj);
                        break;
                }
            }
        };
    }

    public void clearQueue() {
        mHandler.removeMessages(MSG_SEND_TO_ENCODER);
        mHandler.removeMessages(MSG_SEND_TO_AUDIO_ENCODER);
        if (requestMap != null) {
            requestMap.clear();
        }

        if (requestAudioMap != null) {
            requestAudioMap.clear();
        }
    }

    public void requestEncode(long timestamp, byte[] data) {
        //Log.v(TAG, "requestEncode: timestamp = " + timestamp + ", data.length = " + data.length);
        requestMap.put(timestamp, data);
        mHandler.obtainMessage(MSG_SEND_TO_ENCODER, timestamp).sendToTarget();
    }

    public void requestAudioEncode(long timestamp, byte[] data) {
        Log.v(TAG, "requestAudioEncode: timestamp = " + timestamp + ", data.length = " + data.length);
        requestAudioMap.put(timestamp, data);
        mHandler.obtainMessage(MSG_SEND_TO_AUDIO_ENCODER, timestamp).sendToTarget();
    }


    //private static final String VIDEO_MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding
    private static final int IFRAME_INTERVAL = 1;           // 5 seconds between I-frames


    private MediaMuxerWrapper mMuxerWrapper;
    private MediaCodec mVideoEncoder;
    private MediaCodec mAudioEncoder;
    private MediaCodec.BufferInfo mVideoBufferInfo;
    private MediaCodec.BufferInfo mAudioBufferInfo;
    private int mAudioTrackIndex;
    private int mVideoTrackIndex;

    private long lastEncodedAudioTimeStamp = 0;


    public void sendToEncoder(MediaCodec encoder, long timestamp) {
        byte[] data = requestMap.get(timestamp);

        int inputBufferIndex = encoder.dequeueInputBuffer(-1);

        if (inputBufferIndex >= 0) {

            ByteBuffer inputBuffer = encoder.getInputBuffer(inputBufferIndex);
            Log.i(TAG, "[VIDEO] [IBI = " + inputBufferIndex + "] sendToEncoder() : data.length = " + data.length + ",  inputBuffer.remaining = " + inputBuffer.remaining() + ", timestamp = " + timestamp);
            inputBuffer.clear();
            inputBuffer.put(data);

            encoder.queueInputBuffer(inputBufferIndex, 0, data.length, timestamp, 0);

            //synchronized (mMuxerWrapper.sync) {
                drainEncoder(mVideoEncoder, mVideoBufferInfo, true);
            //}
        } else {
            Log.e(TAG, "[VIDEO] [IBI = " + inputBufferIndex + "] sendToEncoder() : data.length = " + data.length + ", timestamp = " + timestamp);
        }

    }

    public void sendToAudioEncoder(MediaCodec encoder, long timestamp) {
        byte[] data = requestAudioMap.get(timestamp);

        int inputBufferIndex = encoder.dequeueInputBuffer(-1);


        if (inputBufferIndex >= 0) {

//            ByteBuffer inputBuffer = encoder.getInputBuffer(inputBufferIndex);
//            Log.e(TAG, "[AUDIO] sendToEncoder(): data.length = " + data.length + ",  inputBuffer.remaining = " + inputBuffer.remaining() + ", timestamp = " + timestamp);
//            FloatBuffer floatBuffer = inputBuffer.asFloatBuffer();
//            floatBuffer.clear();
//            Log.e(TAG, "[AUDIO] sendToEncoder(): floatBuffer.remaining = " + floatBuffer.remaining());
//            floatBuffer.put(data);


            ByteBuffer inputBuffer = encoder.getInputBuffer(inputBufferIndex);
            Log.i(TAG, "[AUDIO] [IBI = " + inputBufferIndex + "] sendToAudioEncoder() : data.length = " + data.length + ",  inputBuffer.remaining = " + inputBuffer.remaining() + ", timestamp = " + timestamp);
            inputBuffer.clear();
            inputBuffer.put(data);

            encoder.queueInputBuffer(inputBufferIndex, 0, data.length, timestamp, 0);

            //synchronized (mMuxerWrapper.sync) {
                drainEncoder(mAudioEncoder, mAudioBufferInfo, false);
            //}
        }
        else{
            Log.e(TAG, "[AUDIO] [IBI = " + inputBufferIndex + "] sendToAudioEncoder() : data.length = " + data.length + ", timestamp = " + timestamp);
        }

    }


    public void drainEncoder(MediaCodec encoder, MediaCodec.BufferInfo bufferInfo, boolean isVideoFrame) {
        final int TIMEOUT_USEC = 10000;
        Log.w(TAG, ((isVideoFrame == true) ? "[VIDEO]" : "[AUDIO]") + " =============== drainEncoder =============== ");

        while (true) {
            int outputBufferIndex = encoder.dequeueOutputBuffer(bufferInfo, TIMEOUT_USEC);
            Log.w(TAG, ((isVideoFrame == true) ? "[VIDEO]" : "[AUDIO]") + " [OBI = " + outputBufferIndex + "]----->>> drainEncoder()");

            if (outputBufferIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                break;
            } else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                encoder.getOutputBuffers();
                Log.e(TAG, "INFO_OUTPUT_BUFFERS_CHANGED");
            } else if (outputBufferIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                Log.e(TAG, "INFO_OUTPUT_FORMAT_CHANGED");
                if (mMuxerWrapper.mMuxerStarted == false) {
                    if (isVideoFrame == true) {
                        mVideoTrackIndex = mMuxerWrapper.addTrack(encoder.getOutputFormat());
                    } else {
                        mAudioTrackIndex = mMuxerWrapper.addTrack(encoder.getOutputFormat());
                    }

                    if (mMuxerWrapper.isAllTracksAdded() == false) {
                        break;  // Allow both encoders to send output format changed before attempting to write samples
                    }

                } else {
                    Log.e(TAG, "format changed after muxer start!!!!!!");
                }

            } else if (outputBufferIndex >= 0) {

                ByteBuffer encodedData = encoder.getOutputBuffer(outputBufferIndex);
                if (encodedData == null) {
                    throw new RuntimeException("encoderOutputBuffer " + outputBufferIndex + " was null");
                }

                Log.i(TAG, ((isVideoFrame == true) ? "[VIDEO]" : "[AUDIO]") + " === { buffer info : offset = " + bufferInfo.offset + ", size = " + bufferInfo.size + ", flags = " + bufferInfo.flags + ", presentationTimeUs = " + bufferInfo.presentationTimeUs + " }");
                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    // The codec config data was pulled out and fed to the muxer when we got
                    // the INFO_OUTPUT_FORMAT_CHANGED status.  Ignore it.
                    Log.e(TAG, "-------------------------------------ignoring BUFFER_FLAG_CODEC_CONFIG");
                    bufferInfo.size = 0;
                }

                if (bufferInfo.size != 0) {

                    if (mMuxerWrapper.mMuxerStarted == false) {
                        Log.e(TAG, ((isVideoFrame == true) ? "[VIDEO]" : "[AUDIO]") + "Muxer not started. dropping " + ((encoder == mVideoEncoder) ? " video" : " audio") + " frames");
                        //throw new RuntimeException("muxer hasn't started");
                    } else {

                        // adjust the ByteBuffer values to match BufferInfo
                        encodedData.position(bufferInfo.offset);
                        encodedData.limit(bufferInfo.offset + bufferInfo.size);

                        if (isVideoFrame == false) {
                            Log.v(TAG, "[AUDIO]" + " lastEncodedAudioTimeStamp = " + lastEncodedAudioTimeStamp + ", presentationTimeUs = " + bufferInfo.presentationTimeUs);
                            if ((bufferInfo.presentationTimeUs - 23219) == lastEncodedAudioTimeStamp) { // Magical AAC encoded frame time
                                bufferInfo.presentationTimeUs = lastEncodedAudioTimeStamp;
                            } else {
                                lastEncodedAudioTimeStamp = bufferInfo.presentationTimeUs;
                            }
                        }

                        if (bufferInfo.presentationTimeUs < 0) {
                            bufferInfo.presentationTimeUs = 0;
                        }

                        if (mMuxerWrapper != null && mMuxerWrapper.mMuxer != null) {
                            synchronized (mMuxerWrapper.sync) {
                                mMuxerWrapper.mMuxer.writeSampleData((isVideoFrame == true) ? mVideoTrackIndex : mAudioTrackIndex, encodedData, bufferInfo);
                            }
                            Log.w(TAG, ((isVideoFrame == true) ? "[VIDEO]" : "[AUDIO]") + " [OBI = " + outputBufferIndex + "] === { buffer info : offset = " + bufferInfo.offset + ", size = " + bufferInfo.size + ", flags = " + bufferInfo.flags + ", presentationTimeUs = " + bufferInfo.presentationTimeUs + " }");
                        }
                    }

                }

                Log.w(TAG, ((isVideoFrame == true) ? "[VIDEO]" : "[AUDIO]") + " [OBI = " + outputBufferIndex + "]<<<----- releaseOutputBuffer()");
                encoder.releaseOutputBuffer(outputBufferIndex, false);

            } else {
                break;
            }

        }
    }


    public void stopEncording() {
        Log.e(TAG, "MuxingHandlerThread: stopEncording()");

        if (mMuxerWrapper != null) {
            mMuxerWrapper.finishTrack();
            mMuxerWrapper.finishTrack();
        }

        stopAndReleaseVideoEncoder();
        stopAndReleaseAudioEncoder();
    }


    private int mVideoWidth = 1280;
    private int mVideoHeight = 760;

    public int mVideoBitRate = 2*1024*1024;//2Mbps //https://developer.android.com/guide/topics/media/media-formats
    private int mFrameRate = 30;

    private int mAudioChannels = 1;
    public int mAudioSampleRate = 44100;
    private int mAudioBitRate = 192*1024; //192Kbps          1280 x 720 px: 192Kbps   //   480*360: 128Kbps   //   176 x 144 px:24Kbps


    public void prepare(int width, int height, int audioChannels, int audioSampleRate, int frameRate) {
        mVideoWidth = width;
        mVideoHeight = height;
        mAudioChannels = audioChannels;
        mAudioSampleRate = audioSampleRate;
        mFrameRate = frameRate;

        prepareVideoEncoder();
        prepareAudioEncoder();

        if (mMuxerWrapper == null) {
            mMuxerWrapper = new MediaMuxerWrapper();
        }
        mVideoEncoder.start();
        mAudioEncoder.start();

        //mVideoTrackIndex = mMuxerWrapper.addTrack(mVideoEncoder.getOutputFormat());
        //mAudioTrackIndex = mMuxerWrapper.addTrack(mAudioEncoder.getOutputFormat());
    }


    private void prepareVideoEncoder() {
        if (mVideoEncoder != null) {
            stopAndReleaseVideoEncoder();
        }

        mVideoBufferInfo = new MediaCodec.BufferInfo();

        MediaCodecInfo codecInfo = Utils.selectCodec(MediaFormat.MIMETYPE_VIDEO_AVC);
        if (codecInfo == null) {
            // Don't fail CTS if they don't have an AVC codec (not here, anyway).
            Log.e(TAG, "prepareVideoEncoder(): Unable to find an appropriate codec for " + MediaFormat.MIMETYPE_VIDEO_AVC);
            return;
        }
        Log.e(TAG, "prepareVideoEncoder(): found codec: " + codecInfo.getName());

        int colorFormat = Utils.selectColorFormat(codecInfo, MediaFormat.MIMETYPE_VIDEO_AVC);
        Log.e(TAG, "prepareVideoEncoder() : found colorFormat: " + colorFormat);

        MediaFormat videoFormat = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, mVideoWidth, mVideoHeight);
        videoFormat.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat /*CodecCapabilities.COLOR_FormatSurface*/);
//        https://gist.github.com/bryanwang/7380901)
//        COLOR_FormatYUV420Planar (I420)
//        COLOR_FormatYUV420PackedPlanar (also I420)
//        COLOR_FormatYUV420SemiPlanar (NV12)
//        COLOR_FormatYUV420PackedSemiPlanar (also NV12)
//        COLOR_TI_FormatYUV420PackedSemiPlanar (also also NV12)

        videoFormat.setInteger(MediaFormat.KEY_FRAME_RATE, mFrameRate);
        videoFormat.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);
        videoFormat.setInteger(MediaFormat.KEY_BIT_RATE, mVideoBitRate);
        videoFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 0);

        Log.d(TAG, "videoFormat: " + videoFormat);
        try {
            mVideoEncoder = MediaCodec.createEncoderByType(MediaFormat.MIMETYPE_VIDEO_AVC);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mVideoEncoder.configure(videoFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        Log.v(TAG, "encoder is " + mVideoEncoder.getCodecInfo().getName());
    }

    private void prepareAudioEncoder() {
        if (mAudioEncoder != null) {
            stopAndReleaseAudioEncoder();
        }

        mAudioBufferInfo = new MediaCodec.BufferInfo();
        MediaFormat audioFormat = new MediaFormat();
        audioFormat.setString(MediaFormat.KEY_MIME, MediaFormat.MIMETYPE_AUDIO_AAC); // "audio/mp4a-latm"
        audioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);//--------------------------------
        audioFormat.setInteger(MediaFormat.KEY_SAMPLE_RATE, mAudioSampleRate);
        audioFormat.setInteger(MediaFormat.KEY_CHANNEL_COUNT, mAudioChannels);
        audioFormat.setInteger(MediaFormat.KEY_BIT_RATE, mAudioBitRate);
        audioFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 0); //-------------------------

        Log.d(TAG, "audioFormat: " + audioFormat);
        try {
            mAudioEncoder = MediaCodec.createEncoderByType(MediaFormat.MIMETYPE_AUDIO_AAC);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mAudioEncoder.configure(audioFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
    }


    private void stopAndReleaseVideoEncoder() {
        Log.e(TAG, "stopAndReleaseVideoEncoder()");
        if (mVideoEncoder != null) {
            mVideoEncoder.stop();
            mVideoEncoder.release();
            mVideoEncoder = null;
        }
    }

    private void stopAndReleaseAudioEncoder() {
        Log.e(TAG, "stopAndReleaseAudioEncoder()");

        lastEncodedAudioTimeStamp = 0;
        if (mAudioEncoder != null) {
            mAudioEncoder.stop();
            mAudioEncoder.release();
            mAudioEncoder = null;
        }
    }

}
