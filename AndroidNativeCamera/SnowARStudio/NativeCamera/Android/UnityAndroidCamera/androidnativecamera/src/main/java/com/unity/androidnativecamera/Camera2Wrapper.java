package com.unity.androidnativecamera;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.TextureView;

import com.unity.androidnativecamera.recoder.VideoRecoder;


public class Camera2Wrapper /*implements IFrameReady*/ {
    private final static String LOGTAG = "Young";

    private Context mContext;
    private Camera2 mCamera2 = null;

    public Camera2Wrapper(Context context) {
        mContext = context;
    }

    public void destroy() {
        closeCamera2();
    }

    public int getCamera2Count() {
        if (mContext != null) {
            return Camera2.getCamera2Count(mContext);
        }
        return 0;
    }

    public int getCamera2SensorOrientation(int index) {
        if (mContext != null) {
            return Camera2.getCamera2SensorOrientation(mContext, index);
        }
        return 0;
    }

    public boolean isCamera2FrontFacing(int index) {
        if (mContext != null) {
            return Camera2.isCamera2FrontFacing(mContext, index);
        }
        return false;
    }

    public int[] getCamera2Resolutions(int index, int format) {
        if (mContext != null) {
            return Camera2.getCamera2Resolutions(mContext, index, format);
        }
        return null;
    }

    public boolean initializeCamera2(int lensFacing, int requestedWidth, int requestedHeight, int requestedFPS, CameraInterface callback) {
        if (mCamera2 == null && mContext != null) {
            mCamera2 = new Camera2();
            return mCamera2.init(mContext, lensFacing, requestedWidth, requestedHeight, requestedFPS, callback, ImageFormat.YUV_420_888);
        }

        return false;
    }

    public boolean isCamera2AutoFocusPointSupported(int index) {
        return Camera2.isCamera2AutoFocusPointSupported(mContext, index);
    }

    public boolean setAutoFocusPoint(float x, float y) {
        if (mCamera2 != null) {
            return mCamera2.setAutoFocusPoint(x, y);
        }
        return false;
    }


    public Rect getFrameSizeCamera2() {
        return mCamera2 != null ? mCamera2.getFrameSize() : new Rect();
    }

    public int getFrameSizeCamera2Width() {
        if (getFrameSizeCamera2() == null) {
            Log.d(LOGTAG, "Camera2Wrapper:getFrameSizeCamera2Width() -- getFrameSizeCamera2() is null");
            return -1;
        }
        return getFrameSizeCamera2().width();

    }

    public int getFrameSizeCamera2Height() {
        if (getFrameSizeCamera2() == null) {
            Log.d(LOGTAG, "Camera2Wrapper:getFrameSizeCamera2Height() -- getFrameSizeCamera2() is null");
            return -1;
        }
        return getFrameSizeCamera2().height();
    }

    public void closeCamera2() {
        if (mCamera2 != null) {
            mCamera2.close();
        }
        mCamera2 = null;
    }

    public void startCamera2(IFrameReady frameReadyInterface) {
        if (mCamera2 != null) {
            mCamera2.startPreview(frameReadyInterface);
        }
    }

    public void pauseCamera2() {
        if (mCamera2 != null) {
            mCamera2.pausePreview();
        }
    }

    public void stopCamera2() {
        if (mCamera2 != null) {
            mCamera2.stopPreview();
        }
    }

    public String[] getCameraIds() {
        if (mContext != null) {
            return Camera2.getCameraIds(mContext);
        }
        return null;
    }

    public String getCameraId(int index) {
        if (mContext != null) {
            return Camera2.getCameraIds(mContext)[index];
        }
        return null;
    }

    public int[] getOutputFormats(int index) {
        if (mContext != null) {
            return Camera2.getOutputFormats(mContext, index);
        }
        return null;
    }

    // Clamping coordinates so auto focus rectangle is inside focus area to avoid making it too small to focus on
    private final int FOCUS_AREA_HALF_SIZE = 100;

    private int clampToFocusArea(float pos) {
        final int FOCUS_AREA_MIN = -1000;
        final int FOCUS_AREA_MAX = 1000;
        return (int) Math.min(Math.max(pos * (FOCUS_AREA_MAX - FOCUS_AREA_MIN) + FOCUS_AREA_MIN, FOCUS_AREA_MIN + FOCUS_AREA_HALF_SIZE), FOCUS_AREA_MAX - FOCUS_AREA_HALF_SIZE);
    }

    protected Object getCameraFocusArea(float focusX, float focusY) {
        int x = clampToFocusArea(focusX);
        int y = clampToFocusArea(1.0f - focusY);
        Rect meteringArea = new Rect(x - FOCUS_AREA_HALF_SIZE, y - FOCUS_AREA_HALF_SIZE, x + FOCUS_AREA_HALF_SIZE, y + FOCUS_AREA_HALF_SIZE);
        return new Camera.Area(meteringArea, 1000);
    }

//    public void frameReadyCamera2(Object yBuffer, Object uBuffer, Object vBuffer, int yStride, int uvStride, int uvStep)
//    {
//        nativeFrameReady(yBuffer, uBuffer, vBuffer, yStride, uvStride, uvStep);
//    }

//    public void surfaceTextureReadyCamera2(Object surfaceTexture)
//    {
//        nativeSurfaceTextureReady(surfaceTexture);
//    }


    private int DENIED = 0, GRANTED = 1, SHOULD_ASK = 2;

    private boolean hasAllPermissionsGranted(Activity activity, String[] cameraPermissions) {

        for (String permission : cameraPermissions) {

            Log.d(LOGTAG, "hasAllPermissionsGranted() -- permission == " + permission);
            if (activity.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                Log.d(LOGTAG, "hasAllPermissionsGranted -- value == " + activity.checkSelfPermission(permission));
                return false;
            }
        }
        return true;
    }

    public void requestCameraPermissions(Activity activity, String[] CAMERA_PERMISSIONS, RequestPermissionCallback callback) {
        Log.d(LOGTAG, "requestCameraPermissions");

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            callback.requestPermissionResult(GRANTED);
        }

        if (hasAllPermissionsGranted(activity, CAMERA_PERMISSIONS) == true) {
            Log.d(LOGTAG, "requestCameraPermissions -- GRANTED");
            callback.requestPermissionResult(GRANTED);
            return;
        }

        RequestPermissionFragment requestPermissionFragment = new RequestPermissionFragment();
        requestPermissionFragment.setCallback(callback);
        activity.getFragmentManager().beginTransaction().add(0, requestPermissionFragment).commit();
    }


    TextureView mTextureView = null;

    public void setTexture(TextureView textureView) {
        Log.e(LOGTAG, "Camera2Wrapper: setTexture()");

        if (null == textureView) {
            Log.e(LOGTAG, "Camera2Wrapper: setTexture() -- textureView is null, return");
            return;
        }

        if (mCamera2 != null) {
            mTextureView = textureView; //for Test
            mCamera2.setTexture(textureView);
        }
    }


    private VideoRecoder mVideoRecoder;

    public boolean startRecordingVideo(int videoWidth, int videoHeight, int audioChannels, int audioFreq, int frameRate, Activity activity) {
        Log.e(LOGTAG, "Camera2Wrapper: startRecordingVideo()");

        if (mVideoRecoder == null) {
            mVideoRecoder = new VideoRecoder(videoWidth, videoHeight, audioChannels, audioFreq, frameRate);
        }
        mVideoRecoder.prepare();

        boolean bTest = false;
        if (mCamera2 != null && mTextureView != null) {
            mCamera2.helpTestRecording(mVideoRecoder);
            bTest = true;
        }

        return mVideoRecoder.startRecording(bTest);
    }

    public boolean stopRecordingVideo() {
        Log.e(LOGTAG, "Camera2Wrapper: stopRecordingVideo()");
        //boolean result = false;
        if (mVideoRecoder != null) {
            mVideoRecoder.stopRecording();
        }

        if (mCamera2 != null && mTextureView != null) {
            mCamera2.stopHelpTestRecording();
        }
        return true;
    }


    public void OnReceiveVideoData(byte[] data) {
        Log.i(LOGTAG, "Camera2Wrapper: OnReceiveVideoData(): data = " + data + ", data.length" + data.length);

        if (mVideoRecoder != null) {
            mVideoRecoder.OnReceiveVideoData(data);
        }
    }

    public void OnReceiveAudioData(byte[] data) {
        Log.i(LOGTAG, "Camera2Wrapper: OnReceiveAudioData(): data = " + data + ", data.length" + data.length);
        if (mVideoRecoder != null) {
            mVideoRecoder.OnReceiveAudioData(data);
        }
    }

    public boolean isRecordingVideo() {
        if (mVideoRecoder != null) {
            return mVideoRecoder.isRecordingVideo();
        }
        return false;
    }

    public void closeRecordingVideo() {
        if (mVideoRecoder != null) {

            if (mVideoRecoder.isRecordingVideo() == true) {
                mVideoRecoder.stopRecording();
            }
            mVideoRecoder.releaseResources();
            mVideoRecoder = null;
        }

        if (mCamera2 != null && mTextureView != null) {
            mCamera2.stopHelpTestRecording();
        }
    }

}
