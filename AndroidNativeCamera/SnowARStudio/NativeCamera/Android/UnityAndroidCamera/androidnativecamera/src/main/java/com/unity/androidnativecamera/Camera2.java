package com.unity.androidnativecamera;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import com.unity.androidnativecamera.recoder.VideoRecoder;
import com.unity.libraryyuv.Libyuv;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Camera2 {
    private final static String LOGTAG = "Young";

    private static CameraManager mCameraManager = null;
    private static String[] mCameraIds = null;
    private CameraDevice mCameraDevice;
    private static Semaphore mCameraOpenCloseLock = new Semaphore(1);

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;

    private Rect mImageSize;
    private Rect mActiveArraySize;
    private int mFocusAreaHalfSize;
    private int mMaxAutoFocusRegions;
    private float mAutoFocusX = -1, mAutoFocusY = -1;
    private int mCropX, mCropY;
    private boolean mManualFocusEngaged = false;
    private Range<Integer> mFpsRange;
    private ImageReader mImageReader = null;
    private Image mAcquiredImage;
    private CaptureRequest.Builder mPreviewRequestBuilder;
    private CameraCaptureSession mCaptureSession = null;
    private Object mCaptureSessionLock = new Object();
    //private int mTextureId;
    //private SurfaceTexture mSurfaceTexture;
    private Surface mSurface = null;
    private int mImageFormat = ImageFormat.YUV_420_888;
    private CameraInterface mCallback;
    private static Context mContext;
    private static int mSensorOrientation;

    private static final int ANR_TIMEOUT_SECONDS = 4;

    private enum CameraCaptureState {
        STARTED,
        PAUSED,
        STOPPED
    };

/*
    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;

    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }
*/

    private CameraCaptureState mCaptureState = CameraCaptureState.STOPPED;


    protected Camera2() {
        startBackgroundThread();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static CameraManager getCameraManager(Context context) {
        if (mCameraManager == null) {
            mCameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        }
        if (mContext == null) {
            mContext = context;
        }
        return mCameraManager;
    }

    public static String[] getCameraIds(Context context) {
        if (mCameraIds == null) {
            try {
                mCameraIds = getCameraManager(context).getCameraIdList();
                Log.e(LOGTAG, "Camera2: getCameraIds() -- mCameraIds.length = " + mCameraIds.length);
            } catch (CameraAccessException ex) {
                Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
                mCameraIds = new String[0];
            }
        }
        return mCameraIds;
    }

    public static int[] getOutputFormats(Context context, int index) {

        try {
            CameraCharacteristics characteristics = getCameraManager(context).getCameraCharacteristics(getCameraIds(context)[index]);
            StreamConfigurationMap streamConfig = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            return streamConfig.getOutputFormats();
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            return null;
        }
    }

    public static int getCamera2Count(Context context) {
        return getCameraIds(context).length;
    }

    public static int getCamera2SensorOrientation(Context context, int index) {
        try {
            CameraCharacteristics characteristics = getCameraManager(context).getCameraCharacteristics(getCameraIds(context)[index]);
            mSensorOrientation = (int) characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
            return mSensorOrientation;
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            return 0;
        }
    }

    public static boolean isCamera2FrontFacing(Context context, int index) {
        try {
            CameraCharacteristics characteristics = getCameraManager(context).getCameraCharacteristics(getCameraIds(context)[index]);
            return characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            return false;
        }
    }

    public static boolean isCamera2AutoFocusPointSupported(Context context, int index) {
        try {
            CameraCharacteristics characteristics = getCameraManager(context).getCameraCharacteristics(getCameraIds(context)[index]);
            return characteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF) > 0;
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            return false;
        }
    }

    private static Size[] getCamera2ResolutionsInternal(CameraCharacteristics characteristics, int format) {
        StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (map == null) {
            Log.e(LOGTAG, "Camera2: configuration map is not available.");
            return null;
        }
        Size[] sizes = map.getOutputSizes(format);
        if (sizes == null || sizes.length == 0) {
            Log.e(LOGTAG, "Camera2: output sizes for " + format + " format are not avialable.");
            return null;
        }
        return sizes;
    }

    public static int[] getCamera2Resolutions(Context context, int index, int format) {
        CameraCharacteristics characteristics;
        try {
            characteristics = getCameraManager(context).getCameraCharacteristics(getCameraIds(context)[index]);
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            return null;
        }
        Size[] sizes = getCamera2ResolutionsInternal(characteristics, format);
        if (sizes != null) {
            int[] resolutions = new int[sizes.length * 2];
            for (int i = 0; i < sizes.length; ++i) {
                resolutions[i * 2] = sizes[i].getWidth();
                resolutions[i * 2 + 1] = sizes[i].getHeight();
            }
            return resolutions;
        }
        return null;
    }

    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundThread.quit();
        try {
            mBackgroundThread.join(ANR_TIMEOUT_SECONDS * 1000);
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException ex) {
            mBackgroundThread.interrupt();
            Log.e(LOGTAG, "Camera2: Interrupted while waiting for the background thread to finish " + ex);
            ;
        }
    }


    private String getLensFacingCameraId(Context context, int lensFacing) {
        // lensFacing --> CameraCharacteristics.LENS_FACING_BACK; or CameraCharacteristics.LENS_FACING_FRONT;

        try {
            for (String cameraId : getCameraManager(context).getCameraIdList()) {

                Log.e(LOGTAG, "Camera2: getLensFacingCameraId(): cameraId = " + cameraId);
                CameraCharacteristics characteristics = getCameraManager(context).getCameraCharacteristics(cameraId);
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == lensFacing) {
                    return cameraId;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


    public boolean init(Context context, int index, int requestedWidth, int requestedHeight, int requestedFPS, CameraInterface callback, int format) {
        Log.e(LOGTAG, "Camera2: init()");

        mCallback = callback;
        mImageFormat = format;
        CameraCharacteristics characteristics;
        try {
            //characteristics = mCameraManager.getCameraCharacteristics(getCameraIds(context)[index]);
            getCameraIds(context);
            String cameraId = mCameraIds[index];
            characteristics = mCameraManager.getCameraCharacteristics(cameraId);

            Log.e(LOGTAG, "Camera2:init() cameraID = " + cameraId + ", characteristics = " + characteristics);
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            return false;
        }


        Log.i(LOGTAG, "Camera2: Hardware level: " + characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL));
        if (characteristics.get(CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL) == CameraCharacteristics.INFO_SUPPORTED_HARDWARE_LEVEL_LEGACY) {
            Log.w(LOGTAG, "Camera2: only LEGACY hardware level is supported.");
            return false;
        }

        Size[] sizes = getCamera2ResolutionsInternal(characteristics, mImageFormat);
        if (sizes == null || sizes.length == 0) {
            return false;
        }
        mImageSize = SetMatchingFrameSize(sizes, requestedWidth, requestedHeight);

        Range<Integer>[] fpsRanges = characteristics.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES);
        if (fpsRanges == null || fpsRanges.length == 0) {
            Log.e(LOGTAG, "Camera2: target FPS ranges are not avialable.");
            return false;
        }
        int fps = SetMatchingFps(fpsRanges, requestedFPS);
        Log.i(LOGTAG, "Camera2: init() -- FPS requested: " + requestedFPS + " set: " + fps);
        mFpsRange = new Range<Integer>(fps, fps);

        try {
            if (!mCameraOpenCloseLock.tryAcquire(ANR_TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
                Log.w(LOGTAG, "Camera2: Timeout waiting to lock camera for opening.");
                return false;
            }
        } catch (InterruptedException ex) {
            Log.e(LOGTAG, "Camera2: Interrupted while trying to lock camera for opening " + ex);
            return false;
        }

        try {
            Log.e(LOGTAG, "Camera2: init() --> checkSelfPermission() == " + context.checkSelfPermission(Manifest.permission.CAMERA));
            if (context.checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.e(LOGTAG, "Camera2: init() --> openCamera()");
                mCameraManager.openCamera(getCameraIds(context)[index], mStateCallback, mBackgroundHandler);
            } else {
                Log.e(LOGTAG, "Camera2: init() --> Manifest.permission.CAMERA is not granted");
                return false;
            }
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
            mCameraOpenCloseLock.release();
            return false;
        }

        try {
            if (!mCameraOpenCloseLock.tryAcquire(ANR_TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
                Log.w(LOGTAG, "Camera2: Timeout waiting to open camera.");
                return false;
            }
            mCameraOpenCloseLock.release();
        } catch (InterruptedException ex) {
            Log.e(LOGTAG, "Camera2: Interrupted while waiting to open camera " + ex);
        }

        //mTextureId = textureId;

        initFocusPointParameters(characteristics);

        return mCameraDevice != null;
    }

    private void initFocusPointParameters(CameraCharacteristics characteristics) {
        mMaxAutoFocusRegions = characteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF);
        if (mMaxAutoFocusRegions > 0) {
            mActiveArraySize = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
            float activeArrayAspectRatio = (float) mActiveArraySize.width() / mActiveArraySize.height();
            float targetAspectRatio = (float) mImageSize.width() / mImageSize.height();
            if (targetAspectRatio > activeArrayAspectRatio) {
                mCropX = 0;
                mCropY = (int) ((mActiveArraySize.height() - mActiveArraySize.width() / targetAspectRatio) / 2);
            } else {
                mCropY = 0;
                mCropX = (int) ((mActiveArraySize.width() - mActiveArraySize.height() * targetAspectRatio) / 2);
            }
            mFocusAreaHalfSize = Math.min(mActiveArraySize.width(), mActiveArraySize.height()) / 20;
        }
    }

    public boolean setAutoFocusPoint(float x, float y) {
        if (mMaxAutoFocusRegions > 0) {
            if (!mManualFocusEngaged) {
                mAutoFocusX = x;
                mAutoFocusY = y;
                synchronized (mCaptureSessionLock) {
                    if (mCaptureSession != null && mCaptureState != CameraCaptureState.PAUSED) {
                        startCancelFocusSession();
                    }
                }
                return true;
            } else {
                Log.w(LOGTAG, "Camera2: Setting manual focus point already started.");
            }
        }
        return false;
    }

    public Rect getFrameSize() {
        Log.i(LOGTAG, "Camera2:getFrameSize() -- mImageSize = " + mImageSize);
        return mImageSize;
    }

    public void close() {
        Log.i(LOGTAG, "Camera2: Close.");
        if (mCameraDevice != null) {
            stopPreview();
            closeCameraDevice();
            mCaptureCallback = null;

            mSurface = null;
            //mSurfaceTexture = null;
            if (mAcquiredImage != null) {
                mAcquiredImage.close();
                mAcquiredImage = null;
            }
            if (mImageReader != null) {
                mImageReader.close();
                mImageReader = null;
            }
        }
        stopBackgroundThread();
    }

    private void closeCameraDevice() {
        try {
            if (!mCameraOpenCloseLock.tryAcquire(ANR_TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
                Log.w(LOGTAG, "Camera2: Timeout waiting to lock camera for closing.");
                return;
            }
        } catch (InterruptedException ex) {
            Log.e(LOGTAG, "Camera2: Interrupted while trying to lock camera for closing " + ex);
            return;
        }

        mCameraDevice.close();

        try {
            if (!mCameraOpenCloseLock.tryAcquire(ANR_TIMEOUT_SECONDS, TimeUnit.SECONDS)) {
                Log.w(LOGTAG, "Camera2: Timeout waiting to close camera.");
            }
        } catch (InterruptedException ex) {
            Log.e(LOGTAG, "Camera2: Interrupted while waiting to close camera " + ex);
        }
        mCameraDevice = null;
        mCameraOpenCloseLock.release();
    }

    private final static String FocusSession = "Focus";
    private final static String CancelFocusSession = "Cancel focus";
    private final static String RegularSession = "Regular";

    private void startNextSession(Object tag) {
        if (tag == FocusSession) {
            Log.i(LOGTAG, "Camera2: Focus completed.");
            mManualFocusEngaged = false;
            synchronized (mCaptureSessionLock) {
                if (mCaptureSession != null) {
                    try {
                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_IDLE);
                        mPreviewRequestBuilder.setTag(RegularSession);
                        mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
                    } catch (CameraAccessException ex) {
                        Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
                    }
                }
            }
        } else if (tag == CancelFocusSession) {
            Log.i(LOGTAG, "Camera2: Focus canceled.");
            synchronized (mCaptureSessionLock) {
                if (mCaptureSession != null) {
                    startCaptureSession();
                }
            }
        }
    }

    private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            startNextSession(request.getTag());
        }

        @Override
        public void onCaptureFailed(CameraCaptureSession session, CaptureRequest request, CaptureFailure failure) {
            Log.w(LOGTAG, "Camera2: Capture session failed " + request.getTag() + " reason " + failure.getReason());
            startNextSession(request.getTag());
        }

        @Override
        public void onCaptureSequenceCompleted(CameraCaptureSession session, int sequenceId, long frameNumber) {
            Log.i(LOGTAG, "Camera2: Capture sequence completed.");
        }

        @Override
        public void onCaptureSequenceAborted(CameraCaptureSession session, int sequenceId) {
            Log.i(LOGTAG, "Camera2: Capture sequence aborted.");
        }
    };

    private void startCaptureSession() {
        try {
            if (mMaxAutoFocusRegions == 0 || mAutoFocusX < 0 || mAutoFocusX > 1 || mAutoFocusY < 0 || mAutoFocusY > 1) {
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                mPreviewRequestBuilder.setTag(RegularSession);
                if (mCaptureSession != null)
                    mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
            } else {
                mManualFocusEngaged = true;
                int x = (int) ((mActiveArraySize.width() - mCropX * 2) * mAutoFocusX + mCropX);
                int y = (int) ((mActiveArraySize.height() - mCropY * 2) * (1.0 - mAutoFocusY) + mCropY);
                // Clamping coordinates so auto focus rectangle is inside focus area to avoid making it too small to focus on
                x = Math.max(1 + mFocusAreaHalfSize, Math.min(x, mActiveArraySize.width() - mFocusAreaHalfSize - 1));
                y = Math.max(1 + mFocusAreaHalfSize, Math.min(y, mActiveArraySize.height() - mFocusAreaHalfSize - 1));
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS,
                        new MeteringRectangle[]
                                {
                                        new MeteringRectangle(x - mFocusAreaHalfSize,
                                                y - mFocusAreaHalfSize,
                                                mFocusAreaHalfSize * 2,
                                                mFocusAreaHalfSize * 2,
                                                MeteringRectangle.METERING_WEIGHT_MAX - 1)
                                });
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
                mPreviewRequestBuilder.setTag(FocusSession);
                mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
        }
    }

    private void startCancelFocusSession() {
        try {
            if (mCaptureSession != null) {
                mCaptureSession.stopRepeating();
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
                mPreviewRequestBuilder.setTag(CancelFocusSession);
                mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
        }
    }


    public TextureView mTextureView;
    public void setTexture(TextureView textureView) {
        mTextureView = textureView;
    }

    public ImageReader.OnImageAvailableListener mOnImageAvailableListener;
    public IFrameReady mFrameReadyInterface;
    int lengthY = 0;
    int lengthU = 0;
    int lengthV = 0;
    byte[] dataY = null;
    byte[] dataU = null;
    byte[] dataV = null;
    byte[] dataUV = null;

    byte[] argbByte = null;
    byte[] data_Y = null;
    byte[] data_UV = null;

    public void startPreview(IFrameReady frameReadyInterface) {
        Log.e(LOGTAG, "Camera2: startPreview.");

        if (null == mCameraDevice || null == mImageSize) {
            return;
        }
        //stopPreview();
        mFrameReadyInterface = frameReadyInterface;

        if (mOnImageAvailableListener == null) {
            mOnImageAvailableListener = new ImageReader.OnImageAvailableListener() {

                @Override
                public void onImageAvailable(ImageReader reader) {
                    if (mCameraOpenCloseLock.tryAcquire()) // no need trying to process image if we already closing camera device
                    {
                        Image image = reader.acquireNextImage();
                        if (image != null) {
                            Image.Plane[] planes = image.getPlanes();

                            if (image.getFormat() == ImageFormat.YUV_420_888 && planes != null && planes.length == 3) {

                                ByteBuffer bufferY = (ByteBuffer) planes[0].getBuffer();
                                ByteBuffer bufferU = (ByteBuffer) planes[1].getBuffer();
                                ByteBuffer bufferV = (ByteBuffer) planes[2].getBuffer();

/*
                                int lengthY = bufferY.remaining();
                                int lengthU = bufferU.remaining();
                                int lengthV = bufferV.remaining();
                                byte[] dataY = new byte[lengthY];   // Y
                                bufferY.get(dataY);
                                byte[] dataU = new byte[lengthU];   // Cb
                                bufferU.get(dataU);
                                byte[] dataV = new byte[lengthV];   // Cr
                                bufferV.get(dataV);
                                Log.v(LOGTAG, "Camera2: onImageAvailable: dataY.length = " + dataY.length + ", dataU.length = " + dataU.length + ", dataV.length = " + dataV.length);

                                byte[] dataUV = new byte[lengthU + lengthV];
                                bufferU.rewind();
                                bufferV.rewind();
                                bufferU.get(dataUV, 0, lengthU);
                                bufferV.get(dataUV, lengthU, lengthV);
                                byte[] argbByte = new byte[mImageSize.width() * mImageSize.height() * 4];
                                Libyuv.NV12ToARGB(dataY, planes[0].getRowStride(), dataUV, planes[1].getRowStride()
                                        , argbByte, mImageSize.width() * 4, mImageSize.width(), mImageSize.height());
                                //Log.v(LOGTAG, "Camera2: onImageAvailable: argb = " + argbByte.length);


                                if (mSurface != null && mVideoRecoder != null && mVideoRecoder.isRecordingVideo() == true) {
                                    byte[] data_Y = new byte[lengthY];
                                    byte[] data_UV = new byte[mImageSize.width() * mImageSize.height() / 2];
                                    Libyuv.ARGBToNV21(argbByte, mImageSize.width() * 4, mImageSize.width(), mImageSize.height(), data_Y, data_UV);

                                    byte[] NV21_frameBuffer = new byte[mImageSize.width() * mImageSize.height() * 3 / 2];
                                    System.arraycopy(data_Y, 0, NV21_frameBuffer, 0, mImageSize.width() * mImageSize.height());
                                    System.arraycopy(data_UV, 0, NV21_frameBuffer, mImageSize.width() * mImageSize.height(), mImageSize.width() * mImageSize.height() / 2);
                                    Log.i(LOGTAG, "Camera2: onImageAvailable --> OnReceiveVideoData(): NV21_frameBuffer.length = " + NV21_frameBuffer.length);

                                    if (mVideoRecoder != null) {
                                        mVideoRecoder.OnReceiveVideoData(NV21_frameBuffer);
                                    }
                                }
*/

                                lengthY = bufferY.remaining();
                                lengthU = bufferU.remaining();
                                lengthV = bufferV.remaining();

                                if (dataY == null) {
                                    dataY = new byte[lengthY];
                                }
                                if (dataU == null) {
                                    dataU = new byte[lengthU];
                                }
                                if (dataV == null) {
                                    dataV = new byte[lengthV];
                                }
                                bufferY.get(dataY);
                                bufferU.get(dataU);
                                bufferV.get(dataV);
                                Log.v(LOGTAG, "Camera2: onImageAvailable: dataY.length = " + dataY.length + ", dataU.length = " + dataU.length + ", dataV.length = " + dataV.length);


                                if (dataUV == null) {
                                    dataUV = new byte[lengthU + lengthV];
                                }
                                bufferU.rewind();
                                bufferV.rewind();
                                bufferU.get(dataUV, 0, lengthU);
                                bufferV.get(dataUV, lengthU, lengthV);


                                if (argbByte == null) {
                                    argbByte = new byte[mImageSize.width() * mImageSize.height() * 4];
                                }

                                Libyuv.NV12ToARGB(dataY, planes[0].getRowStride(), dataUV, planes[1].getRowStride()
                                        , argbByte, mImageSize.width() * 4, mImageSize.width(), mImageSize.height());
                                Log.v(LOGTAG, "Camera2: onImageAvailable: argb = " + argbByte.length);


                                if (mSurface != null && mVideoRecoder != null && mVideoRecoder.isRecordingVideo() == true) {
                                    if (mVideoRecoder != null) {
                                        mVideoRecoder.OnReceiveVideoData(argbByte);
                                    }
                                }


                                if (mFrameReadyInterface != null) {
                                    mFrameReadyInterface.onFrameUpdate(new FrameData(dataY, dataU, dataV, argbByte, mImageSize.width(), mImageSize.height()),
                                            planes[0].getRowStride(), planes[1].getRowStride(), planes[1].getPixelStride());
                                }

                            } else {
                                Log.e(LOGTAG, "Camera2: Wrong image format.");
                            }
                            if (mAcquiredImage != null) {
                                mAcquiredImage.close();
                            }
                            mAcquiredImage = image;
                        }
                        mCameraOpenCloseLock.release();
                    }
                }

            };
        }


        if (mImageReader == null) {
            mImageReader = ImageReader.newInstance(mImageSize.width(), mImageSize.height(), mImageFormat /*ImageFormat.YUV_420_888*/, 2);
            mImageReader.setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);
            mAcquiredImage = null;

            if (mTextureView != null) {
                SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
                if (null == surfaceTexture) {
                    Log.e(LOGTAG, ": startPreview() -- surfaceTexture is null");
                }
                surfaceTexture.setDefaultBufferSize(mImageSize.width(), mImageSize.height());
                mSurface = new Surface(surfaceTexture);
            }
        }

        try {
            if (mCaptureSession != null) {
                // Start the repeated capture again if we're paused.
                if (mCaptureState == CameraCaptureState.PAUSED)
                    mCaptureSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
            } else {
                Log.e(LOGTAG, ": startPreview() -- mImageReader.getSurface() = " + mImageReader.getSurface());
                Log.e(LOGTAG, ": startPreview() -- mCameraDevice = " + mCameraDevice);

                mCameraDevice.createCaptureSession(
                        mSurface != null ? Arrays.asList(mSurface, mImageReader.getSurface()) : Arrays.asList(mImageReader.getSurface()),
                        new CameraCaptureSession.StateCallback() {

                            @Override
                            public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                                Log.i(LOGTAG, "Camera2: CaptureSession is configured.");

                                // The camera is already closed
                                if (mCameraDevice == null) {
                                    return;
                                }

                                synchronized (mCaptureSessionLock) {
                                    mCaptureSession = cameraCaptureSession;
                                    try {
                                        mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                                        if (mSurface != null) {
                                            mPreviewRequestBuilder.addTarget(mSurface);
                                        }
                                        mPreviewRequestBuilder.addTarget(mImageReader.getSurface());
                                        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, mFpsRange);
                                        startCaptureSession();
                                    } catch (CameraAccessException ex) {
                                        Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
                                    }
                                }
                            }

                            @Override
                            public void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
                                Log.e(LOGTAG, "Camera2: CaptureSession configuration failed.");
                            }

                        },
                        mBackgroundHandler
                );
            }
            mCaptureState = CameraCaptureState.STARTED;
        } catch (CameraAccessException ex) {
            Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
        }
    }

    public void pausePreview() {
        Log.i(LOGTAG, "Camera2: Pause preview.");
        synchronized (mCaptureSessionLock) {
            if (mCaptureSession != null) {
                try {
                    mCaptureSession.stopRepeating();
                    mCaptureState = CameraCaptureState.PAUSED;
                } catch (CameraAccessException ex) {
                    Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
                }
            }
        }
    }

    public void stopPreview() {
        Log.e(LOGTAG, "Camera2: stopPreview()");
        synchronized (mCaptureSessionLock) {
            if (mCaptureSession != null) {
                try {
                    mCaptureSession.abortCaptures();
                } catch (CameraAccessException ex) {
                    Log.e(LOGTAG, "Camera2: CameraAccessException " + ex);
                }
                Log.i(LOGTAG, "Camera2: Stop preview. -- close mCaptureSession session");
                mCaptureSession.close();
                mCaptureSession = null;
                mCaptureState = CameraCaptureState.STOPPED;
            }
        }
    }

    private void forceCloseCameraDevice(CameraDevice cameraDevice) {
        synchronized (mCaptureSessionLock) {
            mCaptureSession = null;
        }
        cameraDevice.close();
        mCameraDevice = null;
    }

    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(CameraDevice cameraDevice) {
            mCameraDevice = cameraDevice;
            Log.i(LOGTAG, "Camera2: CameraDevice opened.");
            if (mCallback != null) {
                boolean result = false;
                if (mCameraDevice != null) {
                    result = true;
                }
                mCallback.onCameraDeviceOpened(result);
            }
            mCameraOpenCloseLock.release();
        }

        @Override
        public void onClosed(CameraDevice cameraDevice) {
            Log.i(LOGTAG, "Camera2: CameraDevice closed.");
            mCameraOpenCloseLock.release();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            Log.w(LOGTAG, "Camera2: CameraDevice disconnected.");
            forceCloseCameraDevice(cameraDevice);
            mCameraOpenCloseLock.release();
        }

        @Override
        public void onError(CameraDevice cameraDevice, int error) {
            Log.e(LOGTAG, "Camera2: Error opeining CameraDevice " + error);
            forceCloseCameraDevice(cameraDevice);
            mCameraOpenCloseLock.release();
        }
    };


    private static Rect SetMatchingFrameSize(Size[] sizes, double requestedWidth, double requestedHeight) {
        Log.i(LOGTAG, "Camera2: SetMatchingFrameSize() -- requestedWidth " + requestedWidth + " x " + requestedHeight);

        int bestWidth = 0;
        int bestHeight = 0;
        double bestMatch = Double.MAX_VALUE;

        for (int i = 0; i < sizes.length; ++i) {
            int width = sizes[i].getWidth();
            int height = sizes[i].getHeight();
            double match = Math.abs(Math.log(requestedWidth / width)) + Math.abs(Math.log(requestedHeight / height));
            if (match < bestMatch) {
                bestMatch = match;
                bestWidth = width;
                bestHeight = height;
            }
            Log.i(LOGTAG, "Camera2: SetMatchingFrameSize() -- FrameSize " + width + " x " + height + " [" + match + "]");
        }
        return new Rect(0, 0, bestWidth, bestHeight);
    }

    private static int SetMatchingFps(Range<Integer>[] fpsRanges, int requestedFps) {
        int bestRange = -1;
        double minDiff = Double.MAX_VALUE;
        float epsilon = 0.1f;
        for (int i = 0; i < fpsRanges.length; ++i) {
            int min = fpsRanges[i].getLower();
            int max = fpsRanges[i].getUpper();

            if (requestedFps + epsilon > min && requestedFps - epsilon < max) {
                return requestedFps;
            } else {
                float diff = Math.min(Math.abs(requestedFps - min), Math.abs(requestedFps - max));
                if (diff < minDiff) {
                    minDiff = diff;
                    bestRange = i;
                }
            }
        }
        return requestedFps > fpsRanges[bestRange].getUpper() ? fpsRanges[bestRange].getUpper() : fpsRanges[bestRange].getLower();
    }


    private VideoRecoder mVideoRecoder = null;

    public void helpTestRecording(VideoRecoder videoRecoder) {
        Log.e(LOGTAG, "helpTestRecording()");
        mVideoRecoder = videoRecoder;
    }

    public void stopHelpTestRecording() {
        mVideoRecoder = null;
    }
}


