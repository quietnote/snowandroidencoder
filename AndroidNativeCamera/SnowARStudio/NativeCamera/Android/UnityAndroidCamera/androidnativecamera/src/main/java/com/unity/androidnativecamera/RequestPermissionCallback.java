package com.unity.androidnativecamera;

public interface RequestPermissionCallback {
    void requestPermissionResult(int result);
}
