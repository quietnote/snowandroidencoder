package com.unity.snow;

import android.Manifest;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;

import com.unity.androidnativecamera.Camera2Wrapper;
import com.unity.androidnativecamera.CameraInterface;
import com.unity.androidnativecamera.FrameData;
import com.unity.androidnativecamera.IFrameReady;
import com.unity.androidnativecamera.RequestPermissionCallback;

public class MainActivity extends Activity {
    private final static String TAG = "Young";

    public Camera2Wrapper mCameraWrapper = null;

    private int mRequestedWidth = 1280;
    private int mRequestedHeight = 720;
    private int mFrameRate = 15;



    private static final String[] CAMERA_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCameraWrapper = new Camera2Wrapper(getApplicationContext());
        mCameraWrapper.requestCameraPermissions(this, CAMERA_PERMISSIONS, new RequestPermissionCallback() {
            @Override
            public void requestPermissionResult(int result) {
                Log.e(TAG, "requestPermissionResult(): result = " + result);
            }
        });


        //test call api
        Log.e(TAG, "onCreate() : getCamera2Count() = " + getCamera2Count());
        int cameraCount = getCamera2Count();
        for (int i = 0; i < cameraCount; i++) {
            Log.e(TAG, "[" + i + "] ------ getCamera2SensorOrientation() = " + getCamera2SensorOrientation(i)
                    + ", isCamera2FrontFacing() = " + isCamera2FrontFacing(i)
                    + ", isCamera2AutoFocusPointSupported() = " + isCamera2AutoFocusPointSupported(i)
                    + ", getCameraId() = " + getCameraId(i));


            int[] outputs = mCameraWrapper.getOutputFormats(i);
            for (int c : outputs) {
                Log.e(TAG, "getOutputFormats() = " + c);
            }
        }
    }

    public boolean initializeCamera2(int index, int requestedWidth, int requestedHeight, int requestedFps, CameraInterface callback) {
        return mCameraWrapper.initializeCamera2(index, requestedWidth, requestedHeight, requestedFps, callback);
    }

    public int getCamera2Count() {
        return mCameraWrapper.getCamera2Count();
    }

    public int getCamera2SensorOrientation(int index) {
        return mCameraWrapper.getCamera2SensorOrientation(index);
    }

    public boolean isCamera2FrontFacing(int index) {
        return mCameraWrapper.isCamera2FrontFacing(index);
    }

    public String[] getCameraIds() {
        return mCameraWrapper.getCameraIds();
    }

    public String getCameraId(int index) {
        return mCameraWrapper.getCameraId(index);
    }


    int[] getCamera2Resolutions(int index, int format) {
        return mCameraWrapper.getCamera2Resolutions(index, format);
    }


    public Rect GetFrameSizeCamera2() {
        int width = mCameraWrapper.getFrameSizeCamera2Width();
        int height = mCameraWrapper.getFrameSizeCamera2Height();

        Log.e(TAG, "MainActivity : GetFrameSizeCamera2() -- width = " + width + ", height = " + height);

        return new Rect(0, 0, width, height);
    }

    public void ProcessFrame(FrameData frameData, int yStride, int uvStride, int uvStep) {
        //Log.d(TAG, "MainActivity : ProcessFrame() -- frameData.mWidth = " + frameData.mWidth + ", frameData.mHeight = " + frameData.mHeight + ", mRGB = " + frameData.mRGB);
    }

    public void stopCamera2() {
        mCameraWrapper.stopCamera2();
    }

    public void pauseCamera2() {
        mCameraWrapper.pauseCamera2();
    }

    public boolean isCamera2AutoFocusPointSupported(int index) {
        return mCameraWrapper.isCamera2AutoFocusPointSupported(index);
    }

    public boolean setAutoFocusPoint(float x, float y) {
        return mCameraWrapper.setAutoFocusPoint(x, y);
    }


    public void setTexture() {
        TextureView cameraTextureView = (TextureView) findViewById(R.id.textureView);
        mCameraWrapper.setTexture(cameraTextureView);
    }

    public void cameraOpen(View v) {
        Log.e(TAG, "cameraOpen");

        boolean result = initializeCamera2(0, mRequestedWidth, mRequestedHeight, mFrameRate, new CameraInterface() {
            public void onCameraDeviceOpened(boolean result) {
                Log.e(TAG, "onCameraDeviceOpened: result = " + result);
            }
        });
        Log.e(TAG, "cameraOpen(): result = " + result);

        setTexture();
        GetFrameSizeCamera2();
    }

    public void preview(View view) {
        Log.e(TAG, "preview");

        mCameraWrapper.startCamera2(new IFrameReady() {
            @Override
            public void onFrameUpdate(FrameData frameData, int yStride, int uvStride, int uvStep) {
                ProcessFrame(frameData, yStride, uvStride, uvStep);
            }
            /*public void frameReadyCamera2(Object yBuffer, Object uBuffer, Object vBuffer, int yStride, int uvStride, int uvStep) {
                ProcessFrame(yBuffer, uBuffer, vBuffer, yStride, uvStride, uvStep);
            }*/
        });
    }

    public void Stop(View view) {
        mCameraWrapper.closeCamera2();
    }


    public void Recording(View view) {

        if (mCameraWrapper == null) {
            return;
        }

        if (mCameraWrapper.isRecordingVideo() == false) {
            Log.e(TAG, "MainActivity -- Recording() --> startRecordingVideo");
            startRecordingVideo();
            ((Button) findViewById(R.id.recording)).setText("Stop Recording");
        } else {
            Log.e(TAG, "MainActivity -- Recording() --> stopRecordingVideo");
            if (stopRecordingVideo() == true) {
                ((Button) findViewById(R.id.recording)).setText("Start Recording");
            }

        }
    }


    public void startRecordingVideo() {
        mCameraWrapper.startRecordingVideo(mRequestedWidth, mRequestedHeight, 1, 44100, mFrameRate, this);
    }

    public boolean stopRecordingVideo() {
        return mCameraWrapper.stopRecordingVideo();
    }

}
