package com.unity.libraryyuv;

/**
 * Created by Leo Ma on 4/1/2016.
 */
public class Libyuv {
    private static final String TAG = "Libyuv";

    public native static void setEncoderResolution(int outWidth, int outHeight);
    public native static byte[] BGRAToNV21(byte[] src_frame, int src_width, int src_height, boolean flip, int rotate);
    public native static byte[] ARGBToNV21(byte[] src_frame, int src_width, int src_height, boolean flip, int rotate);
    public native static byte[] RGBAToNV21(byte[] src_frame, int src_width, int src_height, boolean flip, int rotate);

    public native static void NV21ToARGB(byte[] yBuffer,int y_stride, byte[] uvBuffer,int uv_stride, byte[] dstARGB, int dst_stride_argb, int width, int height);
    public native static void NV12ToARGB(byte[] yBuffer,int y_stride, byte[] uvBuffer,int uv_stride, byte[] dstARGB, int dst_stride_argb, int width, int height);
    public native static void I420ToARGB(byte[] yBuffer,int y_stride, byte[] uBuffer,int u_stride, byte[] vBuffer,int v_stride, byte[] dstARGB, int dst_stride_argb,int width, int height);
    public native static void I420ToRGBA(byte[] yBuffer,int y_stride, byte[] uBuffer,int u_stride, byte[] vBuffer,int v_stride, byte[] dstRGBA, int dst_stride_rgba,int width, int height);
    public native static void I420ToBGRA(byte[] yBuffer,int y_stride, byte[] uBuffer,int u_stride, byte[] vBuffer,int v_stride, byte[] dstBGRA, int dst_stride_bgra,int width, int height);

    static {
        System.loadLibrary("yuv");
        System.loadLibrary("enc");
    }
}
