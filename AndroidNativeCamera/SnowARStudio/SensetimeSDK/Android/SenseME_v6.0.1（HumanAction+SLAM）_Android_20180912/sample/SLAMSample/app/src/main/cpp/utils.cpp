#include <string.h>
#include "utils.h"
#include "st_mobile_common.h"

#define  LOG_TAG    "utils"

long getCurrentTime() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

int getImageStride(const st_pixel_format &pixel_format, const int &outputWidth) {
    int stride = 0;
    switch (pixel_format) {
        case ST_PIX_FMT_YUV420P:
        case ST_PIX_FMT_NV12:
        case ST_PIX_FMT_NV21:
            stride = outputWidth;
            break;
        case ST_PIX_FMT_BGRA8888:
        case ST_PIX_FMT_RGBA8888:
            stride = outputWidth * 4;
            break;
        case ST_PIX_FMT_BGR888:
            stride = outputWidth * 3;
            break;
        default:
            break;
    }

    return stride;
}

void convert2JavaSLAMResult(JNIEnv *env, jobject slamResultObj, const st_slam_result &result) {
    jclass slamResultClass = env->FindClass("com/sensetime/slam/SLAMData$SLAMResult");
    if (slamResultClass == NULL) {
        LOGE("SLAMData$SLAMResult class not found!");
        return;
    }

    //SLAM状态
    jfieldID stateField = env->GetFieldID(slamResultClass, "state", "I");
    env->SetIntField(slamResultObj, stateField, result.state);

    //相机横向视角
    jfieldID fovxField = env->GetFieldID(slamResultClass, "fovx", "F");
    env->SetFloatField(slamResultObj, fovxField, result.fovx);

    //相机姿态四元数
    jfieldID bodyQuaternionField = env->GetFieldID(slamResultClass, "bodyQuaternion", "[F");
    jfloatArray bodyQuaternionArray = env->NewFloatArray(4);
    env->SetFloatArrayRegion(bodyQuaternionArray, 0, 4, result.bodyQuaternion);
    env->SetObjectField(slamResultObj, bodyQuaternionField, bodyQuaternionArray);
    env->DeleteLocalRef(bodyQuaternionArray);

    //特征点数量
    //jfieldID numFeaturesField = env->GetFieldID(slamResultClass, "numFeatures", "I");
    //env->SetIntField(slamResultObj, numFeaturesField, result.num_features);

    //特征点坐标
    jfieldID cornersField = env->GetFieldID(slamResultClass, "corners", "[Lcom/sensetime/slam/SLAMData$Corner;");
    if (result.num_features > 0) {
        jclass cornerClass = env->FindClass("com/sensetime/slam/SLAMData$Corner");
        jfieldID xField = env->GetFieldID(cornerClass, "x", "F");
        jfieldID yField = env->GetFieldID(cornerClass, "y", "F");
        jobjectArray cornersArray = env->NewObjectArray(result.num_features, cornerClass, 0);
        for (int i = 0; i < result.num_features; i++) {
            jobject cornerObj = env->AllocObject(cornerClass);
            env->SetFloatField(cornerObj, xField, result.features[i].x);
            env->SetFloatField(cornerObj, yField, result.features[i].y);
            env->SetObjectArrayElement(cornersArray, i, cornerObj);
            env->DeleteLocalRef(cornerObj);
        }
        env->SetObjectField(slamResultObj, cornersField, cornersArray);
        env->DeleteLocalRef(cornerClass);
        env->DeleteLocalRef(cornersArray);
    }

    //跟踪置信度
    jfieldID trackConfidenceField = env->GetFieldID(slamResultClass, "trackConfidence", "I");
    env->SetIntField(slamResultObj, trackConfidenceField, result.track_confidence);
    //跟踪状态
    jfieldID trackStateField = env->GetFieldID(slamResultClass, "trackState", "I");
    env->SetIntField(slamResultObj, trackStateField, result.track_state);
    //相机参数
    jfieldID cameraParaField = env->GetFieldID(slamResultClass, "cameraPara", "Lcom/sensetime/slam/SLAMData$CameraPara;");
    jclass cameraParaClass = env->FindClass("com/sensetime/slam/SLAMData$CameraPara");
    jobject cameraParaObj = env->AllocObject(cameraParaClass);

    jfieldID quaternionField = env->GetFieldID(cameraParaClass, "quaternion", "[F");
    jfloatArray quaternionArray = env->NewFloatArray(4);
    env->SetFloatArrayRegion(quaternionArray, 0, 4, result.camera.quaternion);
    env->SetObjectField(cameraParaObj, quaternionField, quaternionArray);
    env->DeleteLocalRef(quaternionArray);

    jfieldID centerField = env->GetFieldID(cameraParaClass, "center", "[F");
    jfloatArray centerArray = env->NewFloatArray(3);
    env->SetFloatArrayRegion(centerArray, 0, 3, result.camera.center);
    env->SetObjectField(cameraParaObj, centerField, centerArray);
    env->DeleteLocalRef(centerArray);

    jfieldID rotationField = env->GetFieldID(cameraParaClass, "rotation", "[[F");
    jclass floatClass = env->FindClass("[F");
    jobjectArray xArray = env->NewObjectArray(3, floatClass, 0);

    jfloatArray yArray1 = env->NewFloatArray(3);
    env->SetFloatArrayRegion(yArray1, 0, 3, result.camera.rotation[0]);
    env->SetObjectArrayElement(xArray, 0, yArray1);

    jfloatArray yArray2 = env->NewFloatArray(3);
    env->SetFloatArrayRegion(yArray2, 0, 3, result.camera.rotation[1]);
    env->SetObjectArrayElement(xArray, 1, yArray2);

    jfloatArray yArray3 = env->NewFloatArray(3);
    env->SetFloatArrayRegion(yArray3, 0, 3, result.camera.rotation[2]);
    env->SetObjectArrayElement(xArray, 2, yArray3);

    env->SetObjectField(cameraParaObj, rotationField, xArray);
    env->DeleteLocalRef(floatClass);
    env->DeleteLocalRef(yArray1);
    env->DeleteLocalRef(yArray2);
    env->DeleteLocalRef(yArray3);
    env->DeleteLocalRef(xArray);


    jfieldID translationField = env->GetFieldID(cameraParaClass, "translation", "[F");
    jfloatArray translationArray = env->NewFloatArray(3);
    env->SetFloatArrayRegion(translationArray, 0, 3, result.camera.translation);
    env->SetObjectField(cameraParaObj, translationField, translationArray);

    jfieldID depthField = env->GetFieldID(cameraParaClass, "depth", "F");
    env->SetFloatField(cameraParaObj, depthField, result.camera.depth);

    jfieldID rotateTypeField = env->GetFieldID(cameraParaClass, "rotateType", "I");
    env->SetIntField(cameraParaObj, rotateTypeField, result.camera.rotate);

    env->SetObjectField(slamResultObj, cameraParaField, cameraParaObj);

    //Landmark
    jfieldID landMarkField = env->GetFieldID(slamResultClass, "landmarks", "[Lcom/sensetime/slam/SLAMData$Landmark;");
    jclass landMarkClass = env->FindClass("com/sensetime/slam/SLAMData$Landmark");
    jfieldID xField = env->GetFieldID(landMarkClass, "x", "F");
    jfieldID yField = env->GetFieldID(landMarkClass, "y", "F");
    jfieldID zField = env->GetFieldID(landMarkClass, "z", "F");
    jfieldID wField = env->GetFieldID(landMarkClass, "w", "F");
    if (result.num_landmarks > 0) {
        jobjectArray landMarkArray = env->NewObjectArray(result.num_landmarks, landMarkClass, 0);
        for (int i = 0; i < result.num_landmarks; i++) {
            jobject landMarKObj = env->AllocObject(landMarkClass);
            env->SetFloatField(landMarKObj, xField, result.landmarks[i].x);
            env->SetFloatField(landMarKObj, yField, result.landmarks[i].y);
            env->SetFloatField(landMarKObj, zField, result.landmarks[i].z);
            env->SetFloatField(landMarKObj, wField, result.landmarks[i].w);
            env->SetObjectArrayElement(landMarkArray, i, landMarKObj);
            env->DeleteLocalRef(landMarKObj);
        }
        env->SetObjectField(slamResultObj, landMarkField, landMarkArray);
        env->DeleteLocalRef(landMarkArray);
    }
    env->DeleteLocalRef(landMarkClass);

    //Plane
    jfieldID planesField = env->GetFieldID(slamResultClass, "planes", "[Lcom/sensetime/slam/SLAMData$Plane;");
    jclass planeClass = env->FindClass("com/sensetime/slam/SLAMData$Plane");
    jfieldID idField = env->GetFieldID(planeClass, "id", "I");
    jfieldID isUpdateField = env->GetFieldID(planeClass, "isUpdate", "Z");
    jfieldID denseMeshFField = env->GetFieldID(slamResultClass, "denseMeshF", "[I");
    jfieldID planeFIndexField = env->GetFieldID(planeClass, "planeFIndex", "[I");
    jfieldID planeVIndexField = env->GetFieldID(planeClass, "planeVIndex", "[I");
    jfieldID planeNormalField = env->GetFieldID(planeClass, "planeNormal", "[F");
    jfieldID planeOriginPointField = env->GetFieldID(planeClass, "planeOriginPoint", "[F");
    jfieldID planeXAxisField = env->GetFieldID(planeClass, "planeXAxis", "[F");
    //LOGE("num_planes:%d", result.num_planes);
    if (result.num_planes > 0) {
        jobjectArray planeArray = env->NewObjectArray(result.num_planes, planeClass, 0);
        for (int i = 0; i < result.num_planes; i++) {
            jobject planeObj = env->AllocObject(planeClass);
            env->SetIntField(planeObj, idField, result.planes[i].plane_id);
            env->SetBooleanField(planeObj, isUpdateField, result.planes[i].is_update);

            jintArray planeFIndexArray = env->NewIntArray(2);
            env->SetIntArrayRegion(planeFIndexArray, 0, 2, result.planes[i].plane_f_index);
            env->SetObjectField(planeObj, planeFIndexField, planeFIndexArray);
            env->DeleteLocalRef(planeFIndexArray);

            jintArray planeVIndexArray = env->NewIntArray(2);
            env->SetIntArrayRegion(planeVIndexArray, 0, 2, result.planes[i].plane_v_index);
            env->SetObjectField(planeObj, planeVIndexField, planeVIndexArray);
            env->DeleteLocalRef(planeVIndexArray);

            jfloatArray planeNormalArray = env->NewFloatArray(3);
            env->SetFloatArrayRegion(planeNormalArray, 0, 3, result.planes[i].plane_normal);
            env->SetObjectField(planeObj, planeNormalField, planeNormalArray);
            env->DeleteLocalRef(planeNormalArray);

            jfloatArray planeOriginPointArray = env->NewFloatArray(3);
            env->SetFloatArrayRegion(planeOriginPointArray, 0, 3, result.planes[i].plane_origin_point);
            env->SetObjectField(planeObj, planeOriginPointField, planeOriginPointArray);
            env->DeleteLocalRef(planeOriginPointArray);

            jfloatArray planeXAxisArray = env->NewFloatArray(3);
            env->SetFloatArrayRegion(planeXAxisArray, 0, 3, result.planes[i].plane_x_axis);
            env->SetObjectField(planeObj, planeXAxisField, planeXAxisArray);
            env->DeleteLocalRef(planeXAxisArray);
        }
        env->SetObjectField(slamResultObj, planesField, planeArray);
        env->DeleteLocalRef(planeArray);
    }
    env->DeleteLocalRef(planeClass);

    //LOGE("dense_mesh_v_size:%d", result.dense_mesh_v_size);
    //dense mesh v
    if(result.dense_mesh_v_size > 0) {
        jfieldID denseMeshVField = env->GetFieldID(slamResultClass, "denseMeshV", "[F");
        jfloatArray denseMeshVArray = env->NewFloatArray(result.dense_mesh_v_size);
        env->SetFloatArrayRegion(denseMeshVArray, 0, result.dense_mesh_v_size, result.dense_mesh_v);
        env->SetObjectField(slamResultObj, denseMeshVField, denseMeshVArray);
        env->DeleteLocalRef(denseMeshVArray);
    }

    //LOGE("dense_mesh_f_size:%d", result.dense_mesh_f_size);
    //dense mesh f
    if(result.dense_mesh_f_size > 0) {
        jintArray denseMeshFArray = env->NewIntArray(result.dense_mesh_f_size);
        env->SetIntArrayRegion(denseMeshFArray, 0, result.dense_mesh_f_size, result.dense_mesh_f);
        env->SetObjectField(slamResultObj, denseMeshFField, denseMeshFArray);
        env->DeleteLocalRef(denseMeshFArray);
    }

    //plane v index
    if(result.num_planes > 0) {
        //LOGE("planeVIndex");
        jfieldID planeVIndexField = env->GetFieldID(slamResultClass, "planeVIndex", "[I");
        jintArray planeVIndexArray = env->NewIntArray(result.num_planes + 1);
        env->SetIntArrayRegion(planeVIndexArray, 0, result.num_planes + 1, result.plane_v_index);
        env->SetObjectField(slamResultObj, planeVIndexField, planeVIndexArray);
        env->DeleteLocalRef(planeVIndexArray);
    }

    //plane f index
    if(result.num_planes > 0) {
        //LOGE("planeFIndex");
        jfieldID planeFIndexField = env->GetFieldID(slamResultClass, "planeFIndex", "[I");
        jintArray planeFIndexFieldArray = env->NewIntArray(result.num_planes + 1);
        env->SetIntArrayRegion(planeFIndexFieldArray, 0, result.num_planes + 1, result.plane_f_index);
        env->SetObjectField(slamResultObj, planeFIndexField, planeFIndexFieldArray);
        env->DeleteLocalRef(planeFIndexFieldArray);
    }

    env->DeleteLocalRef(cameraParaObj);
    env->DeleteLocalRef(cameraParaClass);
    env->DeleteLocalRef(slamResultClass);
}

bool convert2JNIImu(JNIEnv *env, jobject imuObj, st_slam_imu &imu) {
    if (imuObj == NULL) {
        return false;
    }

    jclass imuClass = env->GetObjectClass(imuObj);
    if (imuClass == NULL) {
        return false;
    }

    jfieldID accelerationXField = env->GetFieldID(imuClass, "linearAccelerationX", "D");
    imu.acceleration[0] = env->GetDoubleField(imuObj, accelerationXField);

    jfieldID accelerationYField = env->GetFieldID(imuClass, "linearAccelerationY", "D");
    imu.acceleration[1] = env->GetDoubleField(imuObj, accelerationYField);

    jfieldID accelerationZField = env->GetFieldID(imuClass, "linearAccelerationZ", "D");
    imu.acceleration[2] = env->GetDoubleField(imuObj, accelerationZField);

    jfieldID gyroscopeXField = env->GetFieldID(imuClass, "gyroscopeX", "D");
    imu.gyroscope[0] = env->GetDoubleField(imuObj, gyroscopeXField);

    jfieldID gyroscopeYField = env->GetFieldID(imuClass, "gyroscopeY", "D");
    imu.gyroscope[1] = env->GetDoubleField(imuObj, gyroscopeYField);

    jfieldID gyroscopeZField = env->GetFieldID(imuClass, "gyroscopeZ", "D");
    imu.gyroscope[2] = env->GetDoubleField(imuObj, gyroscopeZField);

    jfieldID timeStampField = env->GetFieldID(imuClass, "timestamp", "D");
    imu.timeStamp = env->GetDoubleField(imuObj, timeStampField);

    env->DeleteLocalRef(imuClass);

    return true;
}

bool convert2JNIAttitude(JNIEnv *env, jobject attitudeObj, st_slam_attitude &attitude) {
    if (attitudeObj == NULL) {
        return false;
    }

    jclass attitudeClass = env->GetObjectClass(attitudeObj);
    if (attitudeClass == NULL) {
        return false;
    }

    jfieldID rotationVectorXField = env->GetFieldID(attitudeClass, "rotationVectorX", "D");
    attitude.quaternion[0] = env->GetDoubleField(attitudeObj, rotationVectorXField);

    jfieldID rotationVectorYField = env->GetFieldID(attitudeClass, "rotationVectorY", "D");
    attitude.quaternion[1] = env->GetDoubleField(attitudeObj, rotationVectorYField);

    jfieldID rotationVectorZField = env->GetFieldID(attitudeClass, "rotationVectorZ", "D");
    attitude.quaternion[2] = env->GetDoubleField(attitudeObj, rotationVectorZField);

    jfieldID rotationVectorWField = env->GetFieldID(attitudeClass, "rotationVectorW", "D");
    attitude.quaternion[3] = env->GetDoubleField(attitudeObj, rotationVectorWField);

    jfieldID gravityXField = env->GetFieldID(attitudeClass, "gravityX", "D");
    attitude.gravity[0] = env->GetDoubleField(attitudeObj, gravityXField);

    jfieldID gravityYField = env->GetFieldID(attitudeClass, "gravityY", "D");
    attitude.gravity[1] = env->GetDoubleField(attitudeObj, gravityYField);

    jfieldID gravityZField = env->GetFieldID(attitudeClass, "gravityZ", "D");
    attitude.gravity[2] = env->GetDoubleField(attitudeObj, gravityZField);

    jfieldID timeStampField = env->GetFieldID(attitudeClass, "timestamp", "D");
    attitude.timeStamp = env->GetDoubleField(attitudeObj, timeStampField);

    return true;
}

bool convert2JNISTImage(JNIEnv *env, jobject imageObj, st_image_t &st_image) {
    if (imageObj == NULL) {
        return false;
    }

    jclass imageClass = env->GetObjectClass(imageObj);
    if (imageClass == NULL) {
        return false;
    }

    jfieldID dataField = env->GetFieldID(imageClass, "data", "[B");
    jbyteArray data = (jbyteArray) env->GetObjectField(imageObj, dataField);
    st_image.data = (unsigned char*) env->GetByteArrayElements(data, 0);

    jfieldID formatField = env->GetFieldID(imageClass, "format", "I");
    st_image.pixel_format = (st_pixel_format) env->GetIntField(imageObj, formatField);

    jfieldID widthField = env->GetFieldID(imageClass, "width", "I");
    st_image.width =  env->GetIntField(imageObj, widthField);

    jfieldID heightField = env->GetFieldID(imageClass, "height", "I");
    st_image.height =  env->GetIntField(imageObj, heightField);

    jfieldID strideField = env->GetFieldID(imageClass, "stride", "I");
    st_image.stride =  env->GetIntField(imageObj, strideField);

    jfieldID timeField = env->GetFieldID(imageClass, "timestamp", "D");
    st_image.time_stamp = env->GetDoubleField(imageObj, timeField);

    return true;
}

bool releaseSTImage(JNIEnv *env, jobject imageObj, st_image_t &st_image) {
    if (imageObj == NULL) {
        return false;
    }

    jclass imageClass = env->GetObjectClass(imageObj);
    if (imageClass == NULL) {
        return false;
    }

    if(st_image.data != NULL) {
        jfieldID dataField = env->GetFieldID(imageClass, "data", "[B");
        jbyteArray data = (jbyteArray) env->GetObjectField(imageObj, dataField);
        if(data != NULL) {
            env->ReleaseByteArrayElements(data, (jbyte *) st_image.data, JNI_FALSE);
        } else {
            return false;
        }
    }

    return true;
}
