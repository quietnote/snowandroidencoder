package com.sensetime.slam.sample;

import android.app.Activity;
import android.content.res.Resources;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.Log;

import com.sensetime.slam.SLAMAUXType;
import com.sensetime.slam.SLAMData;
import com.sensetime.slam.SLAMState;
import com.sensetime.slam.STMobileSLAMDetectJNI;
import com.sensetime.slam.STMobileSLAMRendererJNI;
import com.sensetime.slam.sample.util.CameraSource;
import com.sensetime.slam.sample.util.IMUReader;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.locks.ReentrantLock;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static com.sensetime.slam.sample.util.STCommon.ResultCode.ST_OK;
import static com.sensetime.slam.sample.util.STCommon.ST_PIX_FMT_NV21;

public class MyGLRenderer implements GLSurfaceView.Renderer {
    private static String TAG = "MySLAM";

    private static final String VERTEX_SHADER = //"uniform mat4 uMVPMatrix;" +
            "attribute vec4 position;" +
                    "attribute vec2 textCoordinate;" +
                    "varying vec2 varyTextCoord;" +
                    "void main() {" +
                    "  gl_Position = position;" +
                    "  varyTextCoord.x = textCoordinate.x;" +
                    "  varyTextCoord.y = textCoordinate.y;" +
                    "}";

    private static final String FRAGMENT_SHADER = "precision lowp float;" +
            "varying vec2 varyTextCoord;" +
            "uniform sampler2D colorMap;" +
            "void main() {" +
            "  gl_FragColor.rgb = texture2D( colorMap, varyTextCoord ).rgb;" +
            "  gl_FragColor.a = 1.0;" +
            "}";

    private static final float attrArr[] = {
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
            1.0f, 1.0f, -1.0f, 0.0f, 0.0f,
            -1.0f, -1.0f, -1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, -1.0f, 0.0f, 1.0f
    };

    private static final float attrArr270[] = {
            1.0f, -1.0f, -1.0f, 0.0f, 1.0f,
            -1.0f, 1.0f, -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, -1.0f, 0.0f, 0.0f,
            1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
            -1.0f, 1.0f, -1.0f, 1.0f, 0.0f,
            1.0f, -1.0f, -1.0f, 0.0f, 1.0f
    };

    private MyGLSurfaceView mGLSurfaceView;
    private PreviewFragment mFragment;
    private WeakReference<Activity> mActivityRef;

    private boolean mSavePicture = false;

    private IntBuffer mAttrBuffer;
    private FloatBuffer mAttrDataBuffer;
    private ByteBuffer mImageBuf = null;

    private int mPosition;
    private int mProgram;
    private int mTextureCoord;
    private boolean mTextureInit = false;
    private int[] mTargetTextureId;
    private boolean mContextSetupped = false;
    private boolean mNeedMove = false;

    // video preview texture
    private CameraSource mCameraSource;
    protected float cameraFovX = 0.0f;
    //protected float cameraFocal = 0.0f;
    protected int previewWidth = 1280;
    protected int previewHeight = 720;

    private int mSurfaceWidth = 1080;
    private int mSurfaceHeight = 1920;

    private int[] mTextureY;
    private int[] mTextureUV;

    // video preview data
    private byte[] nv21YUVData;
    private boolean nv21YUVDataDirty;
    private long nv21TimeStamp = 0;
    private long nv21BaseTime = 0;
    private int bwSize;

    private boolean frameBufferReady = false;
    private ByteBuffer frameRenderBuffer = null;
    private ReentrantLock frameLock = new ReentrantLock();

    // slam
    private boolean mSlamStarted;
    private boolean mSlamTrackSuccess;
    private SLAMData.Frame mFrame;

    // debug info
    private boolean bPerfTest = false;
    private long renderFps;
    private long videoFps;
    private long renderFrameCount;
    private long videoFrameCount;
    private long startTimeRenderFps;
    private long currentTimeRenderFps;

    private long startTimeVideoFps;
    private long currentTimeVideoFps;

    private float copyFrameBufferTime;
    private float updateYUVTextureTime;
    private float runSlamTime;
    private long mUiRefreshBaseTime = 0;

    // camera focus
    private int recentRotationMatrixNumber = 8;
    private int currentRotationMatrixIndex = 0;
    private float[][] recentRotationMatrix = new float[recentRotationMatrixNumber][16];
    private float angleSumThreshold = 0.05f;
    private float accelerateThreshold = 6.0f;
    private float staticStartTime = 0;
    private float staticLastThreshold = 500.0f;//ms
    private boolean lastFrameIsStatic = true;

    enum ChangeFocusMode {
        ACCORD_TO_MOVE_STATIC,
        ACCORD_TO_TRACK_SUCCESS_FAIL
    }

    private ChangeFocusMode changeFocusMode = ChangeFocusMode.ACCORD_TO_MOVE_STATIC;

    //sensor
    private List<IMUReader.ValueTimePair> mLinearAccelerations;
    private List<IMUReader.ValueTimePair> mGyroscopes;
    private IMUReader.ValueTimePair mRotationVectors;
    private IMUReader.ValueTimePair mGravity;

    MyGLRenderer(MyGLSurfaceView surfaceView, Resources resources, Activity pActivity) {

        mAttrDataBuffer = ByteBuffer.allocateDirect(attrArr.length * 4)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        mGLSurfaceView = surfaceView;
        mSlamStarted = false;
        mSlamTrackSuccess = false;
        mActivityRef = new WeakReference<>(pActivity);
        createNativeFrame();
    }

    private void createNativeFrame() {
        mFrame = new SLAMData.Frame();
        mFrame.image = new SLAMData.Image();
        mFrame.attitude = new SLAMData.Attitude();
    }

    public void setPreviewFragment(PreviewFragment fragment) {
        mFragment = fragment;
    }

    public void setCameraSource(CameraSource camerasource) {
        mCameraSource = camerasource;
    }

    public void setPreviewSize(int w,int h) {
        this.previewWidth = w;
        this.previewHeight = h;
    }

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        Log.d(TAG, "onSurfaceCreated");
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        Log.d(TAG, "onSurfaceChanged");
        GLES20.glViewport(0, 0, width, height);

        mSurfaceWidth = width;
        mSurfaceHeight = height;

        setupContext();

        mCameraSource.setPreviewSize(previewWidth, previewHeight);
        mCameraSource.startPreview();
    }

    @Override
    public synchronized void onDrawFrame(GL10 unused) {
        //Log.d(TAG, "onDrawFrame");

        if (!mContextSetupped)
            return;

        if (!frameBufferReady)
            return;


        long totalBegin = SystemClock.elapsedRealtime();

        SLAMData.SLAMResult slamResult = null;
        if (nv21YUVDataDirty) {
            updateFrameWhenDirty();
            updateNV21YUVTexture();

            if (mSlamStarted) {
                long slamBeginTime = SystemClock.elapsedRealtime();
                slamResult = new SLAMData.SLAMResult();
                int res = STMobileSLAMDetectJNI.run(mFrame.image, mFrame.imus.toArray(new SLAMData.IMU[0]), mFrame.attitude, slamResult);
                if (res != ST_OK.getResultCode()) {
                    Log.e(TAG, "STMobileSLAMDetectJNI.run return " + res);
                }

                //Log.i(TAG, slamResult.toString());

                if (slamResult.state == SLAMState.ST_SLAM_TRACKING_SUCCESS.ordinal()) {
                    mNeedMove = true;
                }

                long slamEndTime = SystemClock.elapsedRealtime();
                runSlamTime = slamEndTime - slamBeginTime;
            }

            ++videoFrameCount;
        }

        currentTimeVideoFps = SystemClock.elapsedRealtime();
        if (currentTimeVideoFps - startTimeVideoFps > 1000) {
            videoFps = (videoFrameCount * 1000) / (currentTimeVideoFps - startTimeVideoFps);
            startTimeVideoFps = currentTimeVideoFps;
            videoFrameCount = 0;
        }

        detectAndChangeFocusMode(slamResult);

        ++renderFrameCount;

        long drawBeginTime = SystemClock.elapsedRealtime();

        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glEnable(GLES20.GL_BLEND);

        int[] viewPortPara = new int[4];
        GLES20.glGetIntegerv(GLES20.GL_VIEWPORT, viewPortPara, 0);

        long ar3dCallBeginTime = SystemClock.elapsedRealtime();
        int ret = STMobileSLAMRendererJNI.renderSlam(mTextureY[0], mTextureUV[0], previewWidth, previewHeight, mTargetTextureId[0], null, -1);

        if (ret != ST_OK.getResultCode()) {
            Log.e(TAG, "STMobileSLAMRendererJNI.renderSlam return " + ret);
        }

        if (mSavePicture) {
            mImageBuf = textureToRGBABuffer(mTargetTextureId[0]);
        }

        long ar3dCallEndTime = SystemClock.elapsedRealtime();
        float ar3dCallTime = ar3dCallEndTime - ar3dCallBeginTime;

        if (mImageBuf != null && mSavePicture) {
            if (mActivityRef.get() != null) {
                mActivityRef.get().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        byte[] rgbaBuf = mImageBuf.array().clone();
                        Log.d(TAG, "rgbaBuf length is " + rgbaBuf.length);
                        mFragment.savePicture(rgbaBuf, previewWidth, previewHeight, mCameraSource.getCameraOrientation());
                    }
                });
            }
        }
        mSavePicture = false;

        GLES20.glDisable(GLES20.GL_DEPTH_TEST);
        GLES20.glDisable(GLES20.GL_BLEND);

        //GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        GLES20.glViewport(viewPortPara[0], viewPortPara[1], viewPortPara[2], viewPortPara[3]);
        GLES20.glUseProgram(mProgram);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mAttrBuffer.get(0));
        GLES20.glEnableVertexAttribArray(mPosition);
        GLES20.glEnableVertexAttribArray(mTextureCoord);
        GLES20.glVertexAttribPointer(mPosition, 3, GLES20.GL_FLOAT, false, 4 * 5, 0);
        GLES20.glVertexAttribPointer(mTextureCoord, 2, GLES20.GL_FLOAT, false, 4 * 5, 3 * 4);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTargetTextureId[0]);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        GLES20.glDisableVertexAttribArray(mPosition);
        GLES20.glDisableVertexAttribArray(mTextureCoord);

        GLES20.glUseProgram(0);

        long drawEndTime = SystemClock.elapsedRealtime();
        float allDrawTime = drawEndTime - drawBeginTime;

        long totalEnd = SystemClock.elapsedRealtime();
        float totalTime = totalEnd - totalBegin;

        currentTimeRenderFps = SystemClock.elapsedRealtime();
        if (currentTimeRenderFps - startTimeRenderFps > 1000) {
            renderFps = (renderFrameCount * 1000) / (currentTimeRenderFps - startTimeRenderFps);
            startTimeRenderFps = currentTimeRenderFps;
            renderFrameCount = 0;
        }

        final StringBuffer sb = new StringBuffer();

        if (mSlamStarted) {
            if (slamResult != null) {
                sb.append("frame time:").append(Float.toString(totalTime)).append("ms").append("\n")
                        .append("slam time:").append(Float.toString(runSlamTime)).append("ms").append("\n")
                        .append("track confidence:").append(slamResult.trackConfidence).append("\n")
                        .append("key points:").append(slamResult.corners != null ? slamResult.corners.length : 0)
                        .append("/").append(slamResult.landmarks != null ? slamResult.landmarks.length : 0)
                        .append("\n")
                        .append("state:").append(SLAMState.values()[slamResult.state]).append("\n");
                        //.append("render fps:").append(renderFps).append("\n")
                        //.append("video fps:").append(videoFps);
            } else {
                sb.append("frame time:").append(Float.toString(totalTime)).append("ms").append("\n")
                        .append("slam time:").append(Float.toString(runSlamTime)).append("ms").append("\n")
                        .append("track confidence:").append(0).append("\n")
                        .append("key points:0/0").append("\n")
                        .append("state:").append(SLAMState.ST_SLAM_MARKER_DETECTING).append("\n");
                        //.append("render fps:").append(renderFps).append("\n")
                        //.append("video fps:").append(videoFps);
            }
        } else {
            sb.append("frame time:").append(Float.toString(totalTime));
        }

        if(bPerfTest) {
            Log.d("PerfTest", "frame time:" + Float.toString(totalTime));
            Log.d("PerfTest", "slam time:" + Float.toString(runSlamTime));
            Log.d("PerfTest", "video fps:" + videoFps);
        }

//        if (slamResult != null && slamResult.state == SLAMState.ST_SLAM_TRACKING_FAIL.ordinal()) {
//            STMobileSLAMRendererJNI.setAuxDisplay(SLAMAUXType.ST_SLAM_ALL.ordinal(), false);
//            if (mActivityRef.get() != null) {
//                mActivityRef.get().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mFragment.updateAuxDisplay(false);
//                    }
//                });
//            }
//        }

        if (mActivityRef.get() != null) {
            long current = System.currentTimeMillis();
            //100ms刷新一次ui
            if (current - mUiRefreshBaseTime > 100) {
                mActivityRef.get().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mFragment.updateDebugText(sb.toString());
                    }
                });
                mUiRefreshBaseTime = current;
            }
        }
    }

    public void setNV21DataAndIMUData(byte[] data, int width, int height,
                                      List<IMUReader.ValueTimePair> linearAccelerations,
                                      List<IMUReader.ValueTimePair> gyroscopes,
                                      IMUReader.ValueTimePair rotationVector,
                                      IMUReader.ValueTimePair gravity,
                                      long timestamp,
                                      long baseTime) {
        frameLock.lock();
        if(data == null) {
            return;
        }


        nv21YUVData = data.clone();
        nv21TimeStamp = timestamp;
        nv21BaseTime = baseTime;
        if (!frameBufferReady) {
            initFrameRenderBuffer((width / 2) * (height / 2));
        }

        mLinearAccelerations = linearAccelerations;
        mGyroscopes = gyroscopes;
        mRotationVectors = rotationVector;
        mGravity = gravity;

        saveCurrentRotationMatrix(rotationVector.values);

        nv21YUVDataDirty = true;
        frameLock.unlock();
    }

    public int setAuxDisplay(final int type, final boolean on) {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int ret = STMobileSLAMRendererJNI.setAuxDisplay(type, on);
                return ret;
            }
        });

        mGLSurfaceView.queueEvent(task);

        int ret = -1;
        try {
            ret = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            ret = -1;
        } catch (ExecutionException e) {
            e.printStackTrace();
            ret = -1;
        }

        return ret;
    }

    public int setPlaneDisplay(final boolean on) {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int ret = STMobileSLAMRendererJNI.setAuxDisplay(SLAMAUXType.ST_SLAM_PLANE_MESH.ordinal(), on);
                return ret;
            }
        });

        mGLSurfaceView.queueEvent(task);

        int ret = -1;
        try {
            ret = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            ret = -1;
        } catch (ExecutionException e) {
            e.printStackTrace();
            ret = -1;
        }

        return ret;
    }

    public void changeDetectMode(final boolean enableMultiPlane) {
        mGLSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                //STMobileSLAMDetectJNI.reset();
                STMobileSLAMDetectJNI.enableMultiPlaneTrack(enableMultiPlane);
            }
        });
    }

    public int addObject(final String modelPath) {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int ret = STMobileSLAMRendererJNI.addObject(modelPath);
                return ret;
            }
        });

        mGLSurfaceView.queueEvent(task);

        int ret = -1;
        try {
            ret = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            ret = -1;
        } catch (ExecutionException e) {
            e.printStackTrace();
            ret = -1;
        }

        return ret;
    }

    public int deleteObject(final int id) {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                int ret = STMobileSLAMRendererJNI.deleteObject(id);
                return ret;
            }
        });

        mGLSurfaceView.queueEvent(task);

        int ret = -1;
        try {
            ret = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            ret = -1;
        } catch (ExecutionException e) {
            e.printStackTrace();
            ret = -1;
        }

        return ret;
    }

    public int deleteAllObjects() {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                STMobileSLAMRendererJNI.deleteAllObject();
                return 0;
            }
        });

        mGLSurfaceView.queueEvent(task);

        int ret = -1;
        try {
            ret = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            ret = -1;
        } catch (ExecutionException e) {
            e.printStackTrace();
            ret = -1;
        }

        return ret;
    }

    public void resetSLAM() {
        mGLSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                STMobileSLAMDetectJNI.reset();
                STMobileSLAMRendererJNI.resetObject(-1);
                STMobileSLAMRendererJNI.reset();
            }
        });
    }

    public void stopSLAM() {
        mSlamStarted = false;
        mFragment.updateTipsView(false);
    }

    public boolean isSLAMRunning() {
        return mSlamStarted;
    }

    public void sendSavePictureCommand() {
        if (!mSavePicture) {
            mSavePicture = true;
        }
    }

    public void touchDown(final float x, final float y, final float width, final float height) {
        mSlamStarted = true;
        mFragment.updateTipsView(true);
        resetSLAM();

        mGLSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                if (mSlamStarted) {
                    mNeedMove = false;
                    float xNormal = x / width;
                    float yNormal = y / height;
                    STMobileSLAMDetectJNI.setInitPos(xNormal, yNormal);

                    float[] anchorPoint = new float[3];
                    float[] anchorNormal = new float[3];

                    boolean ret = STMobileSLAMDetectJNI.calculateAnchor(x, y, width, height, anchorPoint, anchorNormal);
                    //ret为true时anchorPoint和anchorNormal有效
                    if (ret) {

                        Log.d(TAG, "anchor point:x=" + anchorPoint[0] + ";y=" + anchorPoint[1] + ";z=" + anchorPoint[2]);
                        Log.d(TAG, "anchor normal:x=" + anchorNormal[0] + ";y=" + anchorNormal[1] + ";z=" + anchorNormal[2]);
                    }

                }
            }
        });

        if (mActivityRef.get() != null) {
            mActivityRef.get().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mFragment.slamStart();
                }
            });
        }
    }

    public void touchMove(final float x, final float y, final float width, final float height) {
        mGLSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                if (mSlamStarted && mNeedMove) {
                    final float xNormal = 2.0f * x / width - 1.0f;
                    final float yNormal = 2.0f * y / height - 1.0f;
                    mGLSurfaceView.queueEvent(new Runnable() {
                        @Override
                        public void run() {
                            STMobileSLAMRendererJNI.setObjectLocation(-1, xNormal, yNormal);
//                            float[] anchorPoint = new float[3];
//                            float[] anchorNormal = new float[3];
//
//                            boolean ret = STMobileSLAMDetectJNI.calculateAnchor(x, y, width, height, anchorPoint, anchorNormal);
//                            //ret为true时anchorPoint和anchorNormal有效
//                            if(ret) {
//                                STMobileSLAMRendererJNI.setObjectLocation(-1, xNormal, yNormal);
//                                Log.d(TAG, "anchor point:x=" + anchorPoint[0] + ";y=" + anchorPoint[1] + ";z=" + anchorPoint[2]);
//                                Log.d(TAG, "anchor normal:x=" + anchorNormal[0] + ";y=" + anchorNormal[1] + ";z=" + anchorNormal[2]);
//                            }
                        }
                    });
                }
            }
        });
    }

    public void releaseHandle() {
        mGLSurfaceView.queueEvent(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG,"releaseHandle");
                GLES20.glDeleteBuffers(1, mAttrBuffer.array(), 0);
                Log.d(TAG, "glDeleteTextures mAttrBuffer");
                mAttrBuffer = null;

                GLES20.glDeleteTextures(1, mTargetTextureId, 0);
                mTargetTextureId = null;
                Log.d(TAG, "glDeleteTextures mTargetTextureId");

                GLES20.glDeleteTextures(1, mTextureY, 0);
                mTextureY =null;
                Log.d(TAG, "glDeleteTextures mTextureY");

                GLES20.glDeleteTextures(1, mTextureUV, 0);
                mTextureUV = null;

                Log.d(TAG, "glDeleteTextures mTextureUV");

                STMobileSLAMDetectJNI.destroy();
                Log.d(TAG, "STMobileSLAMDetectJNI after destroy");

                STMobileSLAMRendererJNI.destroy();
                Log.d(TAG,"STMobileSLAMRendererJNI after destroy");

                frameRenderBuffer = null;
                mContextSetupped = false;
                frameBufferReady = false;
                mTextureInit = false;

                mSlamStarted = false;
                mSlamTrackSuccess = false;
                mGLSurfaceView.setFirstLoc(true);

            }
        });
    }

    private void initFrameRenderBuffer(int size) {
        frameRenderBuffer = ByteBuffer.allocateDirect(size * 6);
        frameRenderBuffer.position(0);

        bwSize = size;
        frameBufferReady = true;
    }

    public void setupContext() {
        if (!mCameraSource.isOpen())
            return;

        if (mContextSetupped) {
            return;
        }

        renderFrameCount = 0;
        videoFrameCount = 0;
        startTimeRenderFps = SystemClock.elapsedRealtime();
        currentTimeRenderFps = SystemClock.elapsedRealtime();

        cameraFovX = mCameraSource.getFovH();
        int ret = STMobileSLAMDetectJNI.createInstance(previewWidth, previewHeight, cameraFovX, -0.05, -0.002, mCameraSource.getCameraOrientation().ordinal());
        STMobileSLAMDetectJNI.enableMultiPlaneTrack(true);
        Log.d(TAG, "width=" + previewWidth + ";height=" + previewHeight + ";fovX=" + cameraFovX + ";orientation=" + mCameraSource.getCameraOrientation().ordinal());
        Log.d(TAG, "STMobileSLAM orientation is " + mCameraSource.getCameraOrientation().toString());
        Log.d(TAG, "STMobileSLAM createInstance return " + ret);

        ret = STMobileSLAMRendererJNI.createInstance();
        Log.d(TAG, "STMobileSLAMRendererJNI createInstance return " + ret);

        STMobileSLAMRendererJNI.setAppInitialOrientation(mCameraSource.getCameraOrientation().ordinal());

        adjustDisplayZone();

//        if(mCameraSource.getCameraOrientation() == CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_270) {
//            mAttrDataBuffer.put(attrArr270);
//        } else {
//            mAttrDataBuffer.put(attrArr);
//        }
//
//        mAttrDataBuffer.position(0);

        // video texture rendering
        mProgram = GLES20.glCreateProgram();
        int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, VERTEX_SHADER);
        int fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER);
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);

        mPosition = GLES20.glGetAttribLocation(mProgram, "position");
        mTextureCoord = GLES20.glGetAttribLocation(mProgram, "textCoordinate");

        // vertex buffer
        mAttrBuffer = IntBuffer.allocate(1);
        GLES20.glGenBuffers(1, mAttrBuffer);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, mAttrBuffer.get(0));
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, attrArr.length * 4, mAttrDataBuffer, GLES20.GL_DYNAMIC_DRAW);

        // background target texture
        mTargetTextureId = new int[1];
        GLES20.glGenTextures(1, mTargetTextureId, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTargetTextureId[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, previewWidth, previewHeight, 0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, null);

        // nv21 y texture
        mTextureY = new int[1];
        GLES20.glGenTextures(1, mTextureY, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureY[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        // nv21 uv texture
        mTextureUV = new int[1];
        GLES20.glGenTextures(1, mTextureUV, 0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureUV[0]);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

        if (!mCameraSource.isPreviewing()) {
            mCameraSource.setPreviewSize(previewWidth, previewHeight);
            mCameraSource.startPreview();
        }

        mContextSetupped = true;
    }

    private void updateFrameWhenDirty() {
        long copyBeginTime = SystemClock.elapsedRealtime();

        frameLock.lock();
        frameRenderBuffer.position(0);
        if (nv21YUVData.length <= bwSize * 6) {
            frameRenderBuffer.put(nv21YUVData);
            mFrame.image.timestamp = adjustTime(nv21TimeStamp, nv21BaseTime);

            //why don't use frameRenderBuffer.array()?
            //frameRenderBuffer.array().length is larger than capacity.
            frameRenderBuffer.clear();
            byte[] bytes = new byte[frameRenderBuffer.capacity()];
            frameRenderBuffer.get(bytes, 0, bytes.length);

            mFrame.image.data = bytes;
            mFrame.image.width = previewWidth;
            mFrame.image.height = previewHeight;
            mFrame.image.format = ST_PIX_FMT_NV21;
            mFrame.image.stride = previewWidth;
            setIMUData(mLinearAccelerations, mGyroscopes, mRotationVectors, mGravity, nv21BaseTime);
        }
        frameRenderBuffer.position(0);
        nv21YUVDataDirty = false;
        frameLock.unlock();

        long copyEndTime = SystemClock.elapsedRealtime();
        copyFrameBufferTime = copyEndTime - copyBeginTime;
    }

    //根据surface的宽高和预览图像数据的宽高调整图像的显示比例
    private void adjustDisplayZone() {
        //屏幕预览宽高（竖屏方向）
        int outputHeight = mSurfaceHeight;
        int outputWidth = mSurfaceWidth;

        //图像宽高（横屏方向）
        float ratio1 = (float) outputWidth / previewHeight;
        float ratio2 = (float) outputHeight / previewWidth;
        float ratioMin = Math.min(ratio1, ratio2);

        int imageWidthNew = Math.round(previewHeight * ratioMin);
        int imageHeightNew = Math.round(previewWidth * ratioMin);

        float ratioWidth = imageWidthNew / (float) outputWidth;
        float ratioHeight = imageHeightNew / (float) outputHeight;

        attrArr[0] = -1.0f / ratioHeight;
        attrArr[5] = 1.0f / ratioHeight;
        attrArr[10] = 1.0f / ratioHeight;
        attrArr[15] = -1.0f / ratioHeight;
        attrArr[20] = 1.0f / ratioHeight;
        attrArr[25] = -1.0f / ratioHeight;

        attrArr[1] = 1.0f / ratioWidth;
        attrArr[6] = -1.0f / ratioWidth;
        attrArr[11] = 1.0f / ratioWidth;
        attrArr[16] = -1.0f / ratioWidth;
        attrArr[21] = -1.0f / ratioWidth;
        attrArr[26] = 1.0f / ratioWidth;

        attrArr270[0] = 1.0f / ratioHeight;
        attrArr270[5] = -1.0f / ratioHeight;
        attrArr270[10] = -1.0f / ratioHeight;
        attrArr270[15] = 1.0f / ratioHeight;
        attrArr270[20] = -1.0f / ratioHeight;
        attrArr270[25] = 1.0f / ratioHeight;

        attrArr270[1] = -1.0f / ratioWidth;
        attrArr270[6] = 1.0f / ratioWidth;
        attrArr270[11] = -1.0f / ratioWidth;
        attrArr270[16] = 1.0f / ratioWidth;
        attrArr270[21] = 1.0f / ratioWidth;
        attrArr270[26] = -1.0f / ratioWidth;

        if (mCameraSource.getCameraOrientation() == CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_270) {
            mAttrDataBuffer.put(attrArr270);
        } else {
            mAttrDataBuffer.put(attrArr);
        }

        mAttrDataBuffer.position(0);
    }

    private void updateNV21YUVTexture() {
        long update_begin_time = SystemClock.elapsedRealtime();

        if (!mTextureInit) {
            frameRenderBuffer.position(0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureY[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, previewWidth, previewHeight, 0,
                    GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, frameRenderBuffer);

            frameRenderBuffer.position(4 * (previewWidth / 2) * (previewHeight / 2));
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureUV[0]);
            GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE_ALPHA, previewWidth / 2, previewHeight / 2, 0,
                    GLES20.GL_LUMINANCE_ALPHA, GLES20.GL_UNSIGNED_BYTE, frameRenderBuffer);

            mTextureInit = true;
        } else {
            frameRenderBuffer.position(0);
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureY[0]);
            GLES20.glTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0, previewWidth, previewHeight, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, frameRenderBuffer);

            frameRenderBuffer.position(4 * (previewWidth / 2) * (previewHeight / 2));
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureUV[0]);
            GLES20.glTexSubImage2D(GLES20.GL_TEXTURE_2D, 0, 0, 0, previewWidth / 2, previewHeight / 2, GLES20.GL_LUMINANCE_ALPHA, GLES20.GL_UNSIGNED_BYTE, frameRenderBuffer);
        }
        long update_end_time = SystemClock.elapsedRealtime();
        updateYUVTextureTime = update_end_time - update_begin_time;
    }

    private ByteBuffer textureToRGBABuffer(int textureId) {
        ByteBuffer rgbaBuf = ByteBuffer.allocate(previewWidth * previewHeight * 4);

        int[] mFrameBuffers = new int[1];
        if (textureId != -1) {
            GLES20.glGenFramebuffers(1, mFrameBuffers, 0);
            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
            GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, mFrameBuffers[0]);
            GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, textureId, 0);
        }
        GLES20.glReadPixels(0, 0, previewWidth, previewHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, rgbaBuf);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0);
        GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
        return rgbaBuf;
    }

    private float[] getAngleChangeFromPreRotationMatrix() {
        float[] angleChanged = new float[3];
        SensorManager.getAngleChange(angleChanged, recentRotationMatrix[currentRotationMatrixIndex], getPreRotationMatrix(recentRotationMatrixNumber - 1));
        return angleChanged;
    }

    private void saveCurrentRotationMatrix(float[] rotationVector) {
        SensorManager.getRotationMatrixFromVector(recentRotationMatrix[currentRotationMatrixIndex], rotationVector);
        currentRotationMatrixIndex = (currentRotationMatrixIndex + 1) % recentRotationMatrixNumber;
    }

    private float[] getPreRotationMatrix(int framesBefore) {
        return recentRotationMatrix[(currentRotationMatrixIndex - framesBefore + recentRotationMatrixNumber) % recentRotationMatrixNumber];
    }

    private boolean angleChangedTooMuch() {
        float[] angleChanged = getAngleChangeFromPreRotationMatrix();
        float angleChangedAbsolute = Math.abs(angleChanged[0]) + Math.abs(angleChanged[1]) + Math.abs(angleChanged[2]);
        boolean result = (angleChangedAbsolute > angleSumThreshold);
        return result;
    }

    private boolean detectAcceleration() {
        float[] accelerationsSinceLastFrame = new float[]{0, 0, 0};
        for (int i = 0; i < mLinearAccelerations.size(); i++) {
            float[] values = mLinearAccelerations.get(i).values;
            for (int j = 0; j < 3; j++)
                accelerationsSinceLastFrame[j] += Math.abs(values[j]);
        }

        float acceleAbsSum = accelerationsSinceLastFrame[0]
                + accelerationsSinceLastFrame[1]
                + accelerationsSinceLastFrame[2];

        boolean result = acceleAbsSum >= accelerateThreshold;
        return result;
    }

    private void detectAndChangeFocusMode(SLAMData.SLAMResult trackingResult) {
        frameLock.lock();
        if (changeFocusMode == ChangeFocusMode.ACCORD_TO_MOVE_STATIC) {
            if (angleChangedTooMuch() || detectAcceleration()) {
                if (!mCameraSource.getFocusMode().equals(Camera.Parameters.FOCUS_MODE_FIXED)) {
                    mCameraSource.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
                }

                lastFrameIsStatic = false;
            } else {
                if (!lastFrameIsStatic) {
                    //record staticStartTime here and only the last assign will take effect.
                    staticStartTime = SystemClock.elapsedRealtime();
                    lastFrameIsStatic = true;
                }

                if (mCameraSource.getFocusMode().equals(Camera.Parameters.FOCUS_MODE_FIXED)) {
                    float accumTime = (SystemClock.elapsedRealtime() - staticStartTime);
                    if (accumTime > staticLastThreshold) {
                        Camera.AutoFocusCallback aF = new Camera.AutoFocusCallback() {
                            @Override
                            public void onAutoFocus(boolean arg0, Camera arg1) {
                                if (arg0) {
                                    Log.d(TAG, "onAutoFocus true");
                                }
                            }
                        };

                        mCameraSource.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                        mCameraSource.autoFocus(aF);
                    }
                }
            }
        } else {
            if (trackingResult != null && trackingResult.state == SLAMState.ST_SLAM_TRACKING_SUCCESS.ordinal()) {
                if (!mCameraSource.getFocusMode().equals(Camera.Parameters.FOCUS_MODE_FIXED)) {
                    mCameraSource.setFocusMode(Camera.Parameters.FOCUS_MODE_FIXED);
                }
            } else {
                if (mCameraSource.getFocusMode().equals(Camera.Parameters.FOCUS_MODE_FIXED)) {
                    mCameraSource.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
                }
            }
        }

        frameLock.unlock();
    }

    private void setIMUData(List<IMUReader.ValueTimePair> linearAccelerations,
                            List<IMUReader.ValueTimePair> gyroscopes,
                            IMUReader.ValueTimePair rotationVector,
                            IMUReader.ValueTimePair gravity,
                            long baseTime) {
        // imus
        mFrame.imus = new ArrayList<>();
        for (int i = 0; i < linearAccelerations.size(); ++i) {

            SLAMData.IMU imu = new SLAMData.IMU();
            imu.linearAccelerationX = linearAccelerations.get(i).values[0];
            imu.linearAccelerationY = linearAccelerations.get(i).values[1];
            imu.linearAccelerationZ = linearAccelerations.get(i).values[2];

            imu.gyroscopeX = gyroscopes.get(i).values[0];
            imu.gyroscopeY = gyroscopes.get(i).values[1];
            imu.gyroscopeZ = gyroscopes.get(i).values[2];

            imu.timestamp = adjustTime((linearAccelerations.get(i).timestamp + gyroscopes.get(i).timestamp) / 2, baseTime);
            mFrame.imus.add(imu);
        }

        //discard imu data
        //mFrame.imus.clear();

        // attitude
        mFrame.attitude.rotationVectorX = rotationVector.values[0];
        mFrame.attitude.rotationVectorY = rotationVector.values[1];
        mFrame.attitude.rotationVectorZ = rotationVector.values[2];
        mFrame.attitude.rotationVectorW = rotationVector.values[3];

        mFrame.attitude.gravityX = gravity.values[0];
        mFrame.attitude.gravityY = gravity.values[1];
        mFrame.attitude.gravityZ = gravity.values[2];

        // if rotationVector has problem
        if (true) {
            //long startTime = System.nanoTime();
            double[][] R = new double[3][3];
            //make R as I
            R[0][0] = 1.0;
            R[0][1] = 0.0;
            R[0][2] = 0.0;
            R[1][0] = 0.0;
            R[1][1] = 1.0;
            R[1][2] = 0.0;
            R[2][0] = 0.0;
            R[2][1] = 0.0;
            R[2][2] = 1.0;

            // Gram–Schmidt process
            // row Z
            R[2][0] = gravity.values[0];
            R[2][1] = gravity.values[1];
            R[2][2] = gravity.values[2];
            scale(R[2], 1.0f / length(R[2]));

            // row X
            double[] proj = new double[3];
            copy(proj, R[2]);
            final double s = dot(R[0], R[2]);
            scale(proj, s);
            R[0][0] -= proj[0];
            R[0][1] -= proj[1];
            R[0][2] -= proj[2];
            scale(R[0], 1.0f / length(R[0]));

            // row Y
            copy(R[1], cross(R[2], R[0]));

            // rotation matrix to quaternion
            mFrame.attitude.rotationVectorW = Math.sqrt(1.0 + R[0][0] + R[1][1] + R[2][2]) * 0.5;
            double inv_w4 = (1.0f / (4.0 * mFrame.attitude.rotationVectorW));
            mFrame.attitude.rotationVectorX = (R[2][1] - R[1][2]) * inv_w4;
            mFrame.attitude.rotationVectorY = (R[0][2] - R[2][0]) * inv_w4;
            mFrame.attitude.rotationVectorZ = (R[1][0] - R[0][1]) * inv_w4;

            //mFrame.attitude.timestamp = gravity.timestamp;
        }

        //adjust data
//        double inv_g = 1.0 / 9.81;
//        for (int i = 0; i < mFrame.imus.size(); ++i) {
//            mFrame.imus.get(i).linearAccelerationX *= inv_g;
//            mFrame.imus.get(i).linearAccelerationY *= inv_g;
//            mFrame.imus.get(i).linearAccelerationZ *= inv_g;
//        }
//
//        mFrame.attitude.gravityX *= -inv_g;
//        mFrame.attitude.gravityY *= -inv_g;
//        mFrame.attitude.gravityZ *= -inv_g;


        if (rotationVector.timestamp > 0) {
            mFrame.attitude.timestamp = adjustTime((rotationVector.timestamp + gravity.timestamp) / 2.0, baseTime);
        } else {
            mFrame.attitude.timestamp = adjustTime(gravity.timestamp, baseTime);
        }
    }

    private int loadShader(int type, String shaderCode) {
        int shader = GLES20.glCreateShader(type);
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        return shader;
    }

    private double adjustTime(double time, double baseTime) {
        //return (time - baseTime) * 1E-3;
        return (time - baseTime) * 1E-9;
    }

    private double dot(final double[] r1, final double[] r2) {
        return r1[0] * r2[0] + r1[1] * r2[1] + r1[2] * r2[2];
    }

    private double[] cross(final double[] r1, final double[] r2) {
        double[] r = new double[3];
        r[0] = r1[1] * r2[2] - r1[2] * r2[1];
        r[1] = r1[2] * r2[0] - r1[0] * r2[2];
        r[2] = r1[0] * r2[1] - r1[1] * r2[0];
        return r;
    }

    private void scale(double[] r, final double s) {
        r[0] *= s;
        r[1] *= s;
        r[2] *= s;
    }

    private void copy(double[] a, final double[] b) {
        a[0] = b[0];
        a[1] = b[1];
        a[2] = b[2];
    }

    private double length(final double[] r) {
        return Math.sqrt(dot(r, r));
    }
}
