package com.sensetime.slam;


public class STMobileSLAMDetectJNI {

    static {
        System.loadLibrary("st_mobile");
        System.loadLibrary("st_slam");
    }

    private static long nativeHandle = 0;

    private static long nativeSlamResultPtr = 0;

    /**
     * 创建SLAM检测句柄
     * @param width  被检测图片宽
     * @param height 被检测图片高
     * @param fovx 相机横向视角
     * @param px 相机和imu传感器间横向距离, 米为单位
     * @param py 相机和imu传感器间的纵向距离，米为单位
     * @param orientation 相机方向
     * @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int createInstance(int width, int height, double fovx, double px, double py, int orientation);

    /**
     * 根据屏幕触摸点设置slam世界坐标系初始位置
     * @param fx view中归一化的触摸点横向坐标
     * @param fy view中归一化的触摸点纵向坐标
     */
    public native static void setInitPos(float fx,float fy);

    /**
     * 计算锚点
     * @param fx              点击屏幕水平方向坐标
     * @param fy              点击屏幕垂直方向坐标
     * @param screenX         屏幕宽度
     * @param screenY         屏幕高度
     * @param anchorPoint     [out] 输出参数，锚点坐标
     * @param anchorNormal    [out] 输出参数，锚点法线
     * @return true表示计算锚点坐标成功， outHitPoint和outNormal值有效，否则outHitPoint和outNormal值无效
     */
    public native static boolean calculateAnchor(float fx, float fy, float screenX, float screenY, float[] anchorPoint, float[] anchorNormal);

    /**
     *  重置SLAM
     */
    public native static void reset();

    /**
     *  开启多平面检测
     */
    public native static void enableMultiPlaneTrack(boolean enabled);

    /**
     * 释放SLAM句柄
     */
    public native static void destroy();

    /**
     * 运行SLAM检测
     * @param image 被检测图片的数据，包括原始数据、宽、高、stride、图像格式
     * @param imus 传感器数据结果数组
     * @param attitude attitude
     * @param slam_output 检测得到的slam数据
     * @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int run(SLAMData.Image image,
                                 SLAMData.IMU[] imus,
                                 SLAMData.Attitude attitude,
                                 SLAMData.SLAMResult slam_output);

    /**
     * 运行SLAM检测（双摄）
     * @param masterImage 被检测图片的数据(主摄像头)，包括原始数据、宽、高、stride、图像格式
     * @param slaveImage 被检测图片的数据(从摄像头)，包括原始数据、宽、高、stride、图像格式
     * @param imus 传感器数据结果数组
     * @param attitude attitude
     * @param slam_output 检测得到的slam数据
     * @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int runStereo(SLAMData.Image masterImage,
                                 SLAMData.Image slaveImage,
                                 SLAMData.IMU[] imus,
                                 SLAMData.Attitude attitude,
                                 SLAMData.SLAMResult slam_output);
}
