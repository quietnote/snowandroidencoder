#ifndef ST_ANCHOR_H
#define ST_ANCHOR_H

#include <st_mobile_slam.h>

#include "vmath.h"

#include <stdio.h>
#include <math.h>
#include <string>

typedef stmath::vec3f Vector3f;
typedef stmath::vec4f Vector4f;
typedef stmath::mat4f Matrix4x4f;

class Anchor {

private:
    float m_Focal;
    float m_Aspect;
    int m_VideoHeight;
    int m_VideoWidth;

public:
    Anchor(int viewWidth, int viewHeight, float focal) {
        m_VideoWidth = viewWidth;
        m_VideoHeight = viewHeight;
        m_Aspect = (float) (viewWidth) / (float) (viewHeight);
        m_Focal = viewWidth * 0.5f / (float) tan((double) focal * 0.5 * 3.1415926 / 180.0);
    }

    ~Anchor() {

    }

    bool insect(float x,
                float y,
                float screenwidth,
                float screenheight,
                const st_slam_result &result,
                float *hitpoint,
                float *normal);

private:

    Vector3f calTriangleNormal(Vector3f hitpoint,
                               Vector3f vert1,
                               Vector3f vert2);

    Vector3f calTriangleIntersectPoint(Vector3f vert0,
                                       Vector3f vert1,
                                       Vector3f vert2,
                                       Vector3f lineVector,
                                       Vector3f linePoint,
                                       bool &infinit);

    Vector3f calPlaneLineIntersectPoint(Vector3f lineVector,
                                        Vector3f linePoint,
                                        bool& infinit);

    Matrix4x4f makeInverseViewMatrix(const st_slam_result &result);
};

#endif
