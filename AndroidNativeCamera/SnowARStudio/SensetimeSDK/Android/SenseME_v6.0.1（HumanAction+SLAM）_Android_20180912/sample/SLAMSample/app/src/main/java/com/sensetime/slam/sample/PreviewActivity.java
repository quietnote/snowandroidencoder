package com.sensetime.slam.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.sensetime.slam.sample.util.STLicenseUtils;

public class PreviewActivity extends Activity {
	private PreviewFragment placeFragment = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_preview);

		//进程后台时被系统强制kill，需重新checkLicense
		if(savedInstanceState!=null && savedInstanceState.getBoolean("process_killed")) {
			if (!STLicenseUtils.checkLicense(this)) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						Toast.makeText(getApplicationContext(), "请检查License授权！", Toast.LENGTH_SHORT).show();
					}
				});
			}
		}

		placeFragment = PreviewFragment.newInstance();
		if (null == savedInstanceState) {
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.container, placeFragment)
					.commit();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putBoolean("process_killed",true);
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	public void onBackPressed() {
        if(placeFragment != null)
		    placeFragment.onBackPressed();
	}
}
