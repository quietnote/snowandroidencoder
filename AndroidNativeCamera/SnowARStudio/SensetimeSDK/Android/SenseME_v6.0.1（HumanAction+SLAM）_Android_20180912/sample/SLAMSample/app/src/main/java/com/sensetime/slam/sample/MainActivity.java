package com.sensetime.slam.sample;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sensetime.slam.sample.util.STLicenseUtils;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {

    private boolean bCameraGranted = false;
    private boolean bReadGranted = false;
    private boolean bWriteGranted = false;

    private static final int REQUEST_RECORD_PERMISSIONS = 1;
    private static final String[] RECORD_PERMISSIONS = {
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        ImageView enter = (ImageView) findViewById(R.id.enter_preview);
        enter.setOnClickListener(this);
        TextView textView = (TextView) findViewById(R.id.version);
        textView.setText("v " + BuildConfig.VERSION_NAME);

        if (!STLicenseUtils.checkLicense(MainActivity.this)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "You should be authorized first!", Toast.LENGTH_SHORT).show();
                }
            });
            enter.setClickable(false);
        }

        ArrayList<String> needRequest = getPermissionsStat(RECORD_PERMISSIONS);
        if(needRequest.size() > 0) {
            requestRecordPermissions(needRequest.toArray(new String[needRequest.size()]), REQUEST_RECORD_PERMISSIONS);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (bCameraGranted && bReadGranted && bWriteGranted) {
            Intent i = new Intent(this, PreviewActivity.class);
            startActivity(i);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "相机权限或读写SD卡权限被禁止", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean shouldShowRequestPermissionRationale(String[] permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return true;
            }
        }
        return false;
    }

    private void requestRecordPermissions(String[] needRequest , int code) {
        if (shouldShowRequestPermissionRationale(needRequest)) {

        } else {
            ActivityCompat.requestPermissions(this, needRequest, code);
        }
    }

    private ArrayList<String> getPermissionsStat(String[] permissions) {
        ArrayList<String> needRequestList = new ArrayList<>();
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                needRequestList.add(permission);
            } else {
                if (permission.equals(Manifest.permission.CAMERA)) {
                    bCameraGranted = true;
                } else if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    bReadGranted = true;
                } else if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    bWriteGranted = true;
                }
            }
        }
        return needRequestList;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_RECORD_PERMISSIONS) {
            if (grantResults.length == permissions.length) {
                for (int i = 0; i < grantResults.length; i++) {
                    int result = grantResults[i];
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        if (permissions[i].equals(Manifest.permission.CAMERA)) {
                            bCameraGranted = false;
                        } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            bReadGranted = false;
                        } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            bWriteGranted = false;
                        }
                    } else if(result == PackageManager.PERMISSION_GRANTED){
                        if (permissions[i].equals(Manifest.permission.CAMERA)) {
                            bCameraGranted = true;
                        } else if (permissions[i].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            bReadGranted = true;
                        } else if (permissions[i].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            bWriteGranted = true;
                        }
                    }
                }
            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
