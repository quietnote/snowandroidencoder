#include <stdio.h>
#include <stdlib.h>
#include "st_mobile_slam.h"
#include <string.h>
#include "utils.h"
#include "anchor.h"


#define LOG_TAG "STMobileSLAM"

extern "C" {
JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_createInstance(JNIEnv *env, jobject obj,
                                                                                    jint width,
                                                                                    jint height,
                                                                                    jdouble fovx,
                                                                                    jdouble px,
                                                                                    jdouble py,
                                                                                    jint orientation);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_run(JNIEnv *env, jobject obj,
                                                                         jobject imgObj,
                                                                         jobjectArray imuObjs,
                                                                         jobject attitudeObj,
                                                                         jobject slam_output);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_runStereo(JNIEnv *env, jobject obj,
                                                                               jobject masterImgObj,
                                                                               jobject slaveImgObj,
                                                                               jobjectArray imuObjs,
                                                                               jobject attitudeObj,
                                                                               jobject slam_output);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_setInitPos(JNIEnv *env, jobject obj,
                                                                                jfloat fx,
                                                                                jfloat fy);

JNIEXPORT bool JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_calculateAnchor(JNIEnv *env, jobject obj,
                                                                                     jfloat fx,
                                                                                     jfloat fy,
                                                                                     jfloat screenx,
                                                                                     jfloat screeny,
                                                                                     jfloatArray anchorPoint,
                                                                                     jfloatArray anchorNormal);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_reset(JNIEnv *env, jobject obj);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_enableMultiPlaneTrack(JNIEnv *env, jobject obj,
                                                                                           jboolean multiTrack);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_destroy(JNIEnv *env, jobject obj);
};

Anchor *g_Anchor = NULL;

//impl
JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_createInstance(JNIEnv *env, jobject obj,
                                                                                    jint width,
                                                                                    jint height,
                                                                                    jdouble fovx,
                                                                                    jdouble px,
                                                                                    jdouble py,
                                                                                    jint orientation) {
    st_handle_t handle;
    int result = (int) st_mobile_slam_create(&handle,
                                             width,
                                             height,
                                             fovx,
                                             (STSLAMOrientation) orientation,
                                             false);

    if (result != 0) {
        LOGE("st_mobile_slam_create failed");
        return result;
    }

    g_Anchor = new Anchor(width, height, fovx);

    setPointerToJava(env, obj, handle, "com/sensetime/slam/STMobileSLAMDetectJNI");
    return result;
}

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_run(JNIEnv *env, jobject obj, jobject imgObj,
                                                                         jobjectArray imuObjs,
                                                                         jobject attitudeObj,
                                                                         jobject slam_output) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        int imu_count = env->GetArrayLength(imuObjs);

        st_slam_imu *slam_imu = NULL;
        if (imu_count > 0) {
            slam_imu = new st_slam_imu[imu_count];
        }

        for (int i = 0; i < imu_count; i++) {
            jobject imuObj = env->GetObjectArrayElement(imuObjs, i);
            convert2JNIImu(env, imuObj, slam_imu[i]);
//            LOGI("slam_imu %d: %f %f %f; %f %f %f; %f", i, slam_imu[i].acceleration[0], slam_imu[i].acceleration[1], slam_imu[i].acceleration[2],
//                 slam_imu[i].gyroscope[0], slam_imu[i].gyroscope[1], slam_imu[i].gyroscope[2], slam_imu[i].timeStamp);
            env->DeleteLocalRef(imuObj);
        }


        st_slam_attitude slam_attitude;
        convert2JNIAttitude(env, attitudeObj, slam_attitude);

        st_slam_result *slam_output_res = getPointerFromJava<st_slam_result>(env, obj, NULL, "nativeSlamResultPtr");
        if (slam_output_res == NULL) {
            LOGI("New slam_output_res");
            slam_output_res = new st_slam_result();
        } else {
            memset(slam_output_res, '\0', sizeof(st_slam_result));
        }

        st_image_t image;
        memset(&image, '\0', sizeof(image));

        if (!convert2JNISTImage(env, imgObj, image)) {
            LOGE("Convert image data to jni st_image_t failed");
            return ST_E_INVALIDARG;
        }

        result = st_mobile_slam_run(handle,
                                    image,
                                    slam_imu,
                                    imu_count,
                                    slam_attitude,
                                    slam_output_res);

//        LOGI("features number %d", slam_output_res->num_features);
//
//        for (int i = 0; i < slam_output_res->num_features; i++) {
//            LOGI("feature point:%f, %f", slam_output_res->features[i].x, slam_output_res->features[i].y);
//        }
//
//
//        LOGI("viewmatrix:%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f ", slam_output_res->camera.rotation[0][0],slam_output_res->camera.rotation[1][0],slam_output_res->camera.rotation[2][0], 0.0,
//             slam_output_res->camera.rotation[0][1],slam_output_res->camera.rotation[1][1],slam_output_res->camera.rotation[2][1], 0.0,
//             slam_output_res->camera.rotation[0][2],slam_output_res->camera.rotation[1][2],slam_output_res->camera.rotation[2][2], 0.0,
//             slam_output_res->camera.translation[0],slam_output_res->camera.translation[1],slam_output_res->camera.translation[2], 1.0);

        if (slam_imu != NULL) {
            delete[] slam_imu;
            slam_imu = NULL;
        }

        if (!releaseSTImage(env, imgObj, image)) {
            LOGE("Release st_image_t failed, please check to avoid memory leak");
        }

        if (result == ST_OK) {
            setPointerToJava(env, obj, slam_output_res, NULL, "nativeSlamResultPtr");
            convert2JavaSLAMResult(env, slam_output, *slam_output_res);
        }

    } else {
        LOGE("SLAMDetect handle is null %d", __LINE__);
    }

    return result;
}

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_runStereo(JNIEnv *env, jobject obj,
                                                                               jobject masterImgObj,
                                                                               jobject slaveImgObj,
                                                                               jobjectArray imuObjs,
                                                                               jobject attitudeObj,
                                                                               jobject slam_output) {
//    int result = ST_E_INVALIDARG;
//    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
//    if (handle != NULL) {
//        int imu_count = env->GetArrayLength(imuObjs);
//
//        st_slam_imu* slam_imu = NULL;
//        if(imu_count > 0) {
//            slam_imu = new st_slam_imu[imu_count];
//        }
//
//        for (int i = 0; i < imu_count; i++) {
//            jobject imuObj = env->GetObjectArrayElement(imuObjs, i);
//            convert2JNIImu(env, imuObj, slam_imu[i]);
//            LOGI("slam_imu %d: %f %f %f; %f %f %f; %f", i, slam_imu[i].acceleration[0], slam_imu[i].acceleration[1], slam_imu[i].acceleration[2],
//                 slam_imu[i].gyroscope[0], slam_imu[i].gyroscope[1], slam_imu[i].gyroscope[2], slam_imu[i].timeStamp);
//            env->DeleteLocalRef(imuObj);
//        }
//
//
//        st_slam_attitude slam_attitude;
//        convert2JNIAttitude(env, attitudeObj, slam_attitude);
//
//        st_slam_result *slam_output_res = getPointerFromJava<st_slam_result>(env, obj, NULL, "nativeSlamResultPtr");
//        if (slam_output_res == NULL) {
//            LOGE("New slam_output_res");
//            slam_output_res = new st_slam_result();
//        } else  {
//            memset(slam_output_res, '\0',  sizeof(st_slam_result));
//        }
//
//        st_image_t masterImage;
//        memset(&masterImage,'\0', sizeof(masterImage));
//
//        if(!convert2JNISTImage(env, masterImgObj, masterImage)) {
//            LOGE("Convert master image data to jni st_image_t failed");
//            return ST_E_INVALIDARG;
//        }
//
//        st_image_t slaveImage;
//        memset(&slaveImage,'\0', sizeof(slaveImage));
//
//        if(!convert2JNISTImage(env, slaveImgObj, slaveImage)) {
//            LOGE("Convert slave image data to jni st_image_t failed");
//            return ST_E_INVALIDARG;
//        }
//
//        result = st_mobile_slam_run_stereo(handle,
//                                           masterImage,
//                                           slaveImage,
//                                           slam_imu,
//                                           imu_count,
//                                           slam_attitude,
//                                           0,
//                                           slam_output_res);
//        if(slam_imu != NULL) {
//            delete [] slam_imu;
//            slam_imu = NULL;
//        }
//
//        if(!releaseSTImage(env, masterImgObj, masterImage)) {
//            LOGE("Release master st_image_t failed, please check to avoid memory leak");
//        }
//
//        if(!releaseSTImage(env, slaveImgObj, slaveImage)) {
//            LOGE("Release slave st_image_t failed, please check to avoid memory leak");
//        }
//
//        if (result == ST_OK) {
//            setPointerToJava(env, obj, slam_output_res, NULL, "nativeSlamResultPtr");
//            convert2JavaSLAMResult(env, slam_output, *slam_output_res);
//        }
//
//    } else {
//        LOGE("SLAMDetect handle is null %d", __LINE__);
//    }

    return ST_OK;
}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_setInitPos(JNIEnv *env, jobject obj, jfloat fx, jfloat fy) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        st_mobile_slam_set_init_pos(handle, fx, fy);
    } else {
        LOGE("SLAMDetect handle is null %d", __LINE__);
    }
}

JNIEXPORT bool JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_calculateAnchor(JNIEnv *env, jobject obj,
                                                                                     jfloat fx,
                                                                                     jfloat fy,
                                                                                     jfloat screenx,
                                                                                     jfloat screeny,
                                                                                     jfloatArray anchorPoint,
                                                                                     jfloatArray anchorNormal) {
    bool insected = false;
    if (g_Anchor != NULL) {
        st_slam_result *result = getPointerFromJava<st_slam_result>(env, obj, NULL, "nativeSlamResultPtr");
        if (result) {
            float hitpoint[3];
            float normal[3];
            insected = g_Anchor->insect(fx, fy, screenx, screeny, *result, hitpoint, normal);
            if (insected) {
                if (anchorPoint == NULL
                    || anchorNormal == NULL
                    || env->GetArrayLength(anchorPoint) < 3
                    || env->GetArrayLength(anchorNormal) < 3) {
                    LOGE("Insect is true but output array parameters invalid");
                } else {
                    env->SetFloatArrayRegion(anchorPoint, 0, 3, hitpoint);
                    env->SetFloatArrayRegion(anchorNormal, 0, 3, normal);
                }
                LOGI("Insect: %d, anchor point:%f,%f,%f, anchor normal:%f,%f,%f", insected, hitpoint[0], hitpoint[1], hitpoint[2], normal[0], normal[1], normal[2]);
            }
        }
    } else {
        LOGE("g_Anchor is NULL");
    }

    return insected;
}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_reset(JNIEnv *env, jobject obj) {
    st_slam_result *result = getPointerFromJava<st_slam_result>(env, obj, "com/sensetime/slam/STMobileSLAMDetectJNI", "nativeSlamResultPtr");
    if (result) {
        delete result;
        result = NULL;
        setPointerToJava<st_slam_result>(env, obj, NULL, "com/sensetime/slam/STMobileSLAMDetectJNI", "nativeSlamResultPtr");
    }
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        st_mobile_slam_reset(handle);
    } else {
        LOGE("SLAMDetect handle is null %d", __LINE__);
    }
}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_enableMultiPlaneTrack(JNIEnv *env, jobject obj,
                                                                                           jboolean multiTrack) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        st_mobile_slam_open_multi_planar_track(handle, multiTrack);
    } else {
        LOGE("SLAMDetect handle is null %d", __LINE__);
    }
}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMDetectJNI_destroy(JNIEnv *env, jobject obj) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    st_slam_result *result = getPointerFromJava<st_slam_result>(env, obj, NULL, "nativeSlamResultPtr");
    if (result) {
        delete result;
        result = NULL;
        setPointerToJava<st_slam_result>(env, obj, NULL, NULL, "nativeSlamResultPtr");
    }

    if (handle != NULL) {
        setPointerToJava<st_handle_t>(env, obj, NULL);
        st_mobile_slam_destroy(handle);
    } else {
        LOGE("SLAMDetect handle is null %d", __LINE__);
    }

    if (g_Anchor != NULL) {
        delete g_Anchor;
        g_Anchor = NULL;
    }
}
