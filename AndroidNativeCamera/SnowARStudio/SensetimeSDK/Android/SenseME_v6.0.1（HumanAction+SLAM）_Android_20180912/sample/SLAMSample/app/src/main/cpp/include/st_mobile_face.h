﻿#ifndef INCLUDE_STMOBILE_ST_MOBILE_FACE_H_
#define INCLUDE_STMOBILE_ST_MOBILE_FACE_H_

#include "st_mobile_common.h"

/// 该文件中的API不保证线程安全.多线程调用时,需要确保安全调用.例如在 create handle 没有执行完就执行 process 可能造成crash;在 process 执行过程中调用 destroy 函数可能会造成crash.

/// @defgroup st_mobile_tracker_106
/// @brief face 106 points tracking interfaces
/// This set of interfaces processing face 106 points tracking routines
///
/// @{

/// @brief tracking配置选项,对应st_mobile_tracker_106_create中的config参数,具体配置如下：
/// 默认tracking配置,使用多线程,默认打开人脸三维旋转角度去抖动
#define ST_MOBILE_TRACKING_DEFAULT_CONFIG   ST_MOBILE_TRACKING_MULTI_THREAD | ST_MOBILE_TRACKING_ENABLE_DEBOUNCE

/// @brief 创建实时人脸106关键点跟踪句柄
/// @param[in] model_path 模型文件的绝对路径或相对路径,例如models/track.model
/// @param[in] config 配置选项 例如ST_MOBILE_TRACKING_DEFAULT_CONFIG,默认使用双线程跟踪,实时视频预览建议使用该配置.
///     使用单线程算法建议选择（ST_MOBILE_TRACKING_SINGLE_THREAD)
/// @parma[out] handle 人脸跟踪句柄,失败返回NULL
/// @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
ST_SDK_API st_result_t
st_mobile_tracker_106_create(
    const char *model_path,
    unsigned int config,
    st_handle_t *handle
);

/// @brief 设置检测到的最大人脸数目N,持续track已检测到的N个人脸直到人脸数小于N再继续做detect.
/// @param[in] handle 已初始化的关键点跟踪句柄
/// @param[in] max_facecount 设置为1即是单脸跟踪,有效范围为[1, 32]
/// @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
ST_SDK_API
st_result_t st_mobile_tracker_106_set_facelimit(
    st_handle_t handle,
    int max_facecount
);

/// @brief 设置tracker每多少帧进行一次detect.
/// @param[in] handle 已初始化的关键点跟踪句柄
/// @param[in] val 有效范围[1, -)
/// @return 成功返回ST_OK,失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
ST_SDK_API
st_result_t st_mobile_tracker_106_set_detect_interval(
    st_handle_t handle,
    int val
);

/// @brief 重置实时人脸106关键点跟踪,清空track造成的缓存,重新检测下一帧图像中的人脸并跟踪,建议在两帧图片相差较大时使用
/// @param [in] handle 已初始化的实时目标人脸106关键点跟踪句柄
/// @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
ST_SDK_API st_result_t
st_mobile_tracker_106_reset(
    st_handle_t handle
);

/// @brief 对连续视频帧进行实时快速人脸106关键点跟踪
/// @param[in] handle 已初始化的实时人脸跟踪句柄
/// @param[in] image 用于检测的图像数据
/// @param[in] pixel_format 用于检测的图像数据的像素格式. 推荐使用GRAY8、NV12、NV21、YUV420P
/// @param[in] image_width 用于检测的图像的宽度(以像素为单位)
/// @param[in] image_height 用于检测的图像的高度(以像素为单位)
/// @param[in] image_stride 用于检测的图像的跨度(以像素为单位),即每行的字节数；目前仅支持字节对齐的padding,不支持roi
/// @param[in] orientation 视频中人脸的方向
/// @param[out] p_faces_array 检测到的人脸信息数组,api负责管理内存,会覆盖上一次调用获取到的数据
/// @param[out] p_faces_count 检测到的人脸数量
/// @return 成功返回ST_OK,失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
ST_SDK_API st_result_t
st_mobile_tracker_106_track(
    st_handle_t handle,
    const unsigned char *image,
    st_pixel_format pixel_format,
    int image_width,
    int image_height,
    int image_stride,
    st_rotate_type orientation,
    st_mobile_106_t **p_faces_array,
    int *p_faces_count
);

/// @brief 对连续视频帧进行实时快速人脸106关键点跟踪,并检测脸部动作
/// @param[in] handle 已初始化的实时人脸跟踪句柄
/// @param[in] image 用于检测的图像数据
/// @param[in] pixel_format 用于检测的图像数据的像素格式. 推荐使用GRAY8、NV12、NV21、YUV420P
/// @param[in] image_width 用于检测的图像的宽度(以像素为单位)
/// @param[in] image_height 用于检测的图像的高度(以像素为单位)
/// @param[in] image_stride 用于检测的图像的跨度(以像素为单位),即每行的字节数；目前仅支持字节对齐的padding,不支持roi
/// @param[in] orientation 视频中人脸的方向
/// @param[out] p_faces_array 检测到的人脸106点信息和脸部动作的数组,api负责管理内存,会覆盖上一次调用获取到的数据
/// @param[out] p_faces_count 检测到的人脸数量
/// @return 成功返回ST_OK,失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
ST_SDK_API
st_result_t
st_mobile_tracker_106_track_face_action(
    st_handle_t handle,
    const unsigned char *image,
    st_pixel_format pixel_format,
    int image_width,
    int image_height,
    int image_stride,
    st_rotate_type orientation,
    st_mobile_face_t **p_faces_array,
    int *p_faces_count
);

/// @brief 设置需要检测的脸部动作.若不设置,默认检测所有脸部动作
/// @param[in] handle 已初始化的实时人脸跟踪句柄
/// @param[in] actions 需要检测的脸部动作.在st_mobile_common.h中定义：ST_MOBILE_EYE_BLINK | ST_MOBILE_MOUTH_AH | ST_MOBILE_HEAD_YAW | ST_MOBILE_HEAD_PITCH | ST_MOBILE_BROW_JUMP
/// @return 成功返回ST_OK,失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_INVALIDARG等
ST_SDK_API
st_result_t
st_mobile_tracker_106_set_detect_actions(
    st_handle_t handle,
    unsigned long long actions
);

/// @brief 销毁已初始化的track106句柄
/// @param[in] handle 已初始化的人脸跟踪句柄
ST_SDK_API void
st_mobile_tracker_106_destroy(
    st_handle_t handle
);
/// @}

#endif // INCLUDE_STMOBILE_ST_MOBILE_FACE_H_
