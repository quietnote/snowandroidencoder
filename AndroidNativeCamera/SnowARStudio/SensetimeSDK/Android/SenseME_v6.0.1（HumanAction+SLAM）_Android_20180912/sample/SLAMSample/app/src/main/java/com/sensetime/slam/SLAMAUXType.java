package com.sensetime.slam;

public enum SLAMAUXType {
    ST_SLAM_AXIS,
    ST_SLAM_GRIDPLANE,
    ST_SLAM_HORIXZONPLANE,
    ST_SLAM_UNITCUBE,
    ST_SLAM_FEATURE,
    ST_SLAM_PLANE_MESH,
    ST_SLAM_ALL
}
