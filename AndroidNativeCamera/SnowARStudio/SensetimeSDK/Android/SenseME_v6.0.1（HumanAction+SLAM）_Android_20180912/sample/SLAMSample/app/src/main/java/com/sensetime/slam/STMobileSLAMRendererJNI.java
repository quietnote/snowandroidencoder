package com.sensetime.slam;


public class STMobileSLAMRendererJNI {
    static {
        System.loadLibrary("st_mobile");
        System.loadLibrary("st_slam");
    }

    private static long nativeHandle;

    /**
     * 创建renderer句柄
     * @return  成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int createInstance();

    /**
     * 释放renderer句柄
     */
    public native static void destroy();

    /**
     * 用于设置初始imu方向
     * @param orientation 相机方向
     */
    public native static void setAppInitialOrientation(int orientation);

    /**
     * 用于设置需渲染的模型
     * @param path 模型zip的路径
     * @return 成功返回模型ID,失败返回-1
     */
    public native static int addObject(String path);

    /**
     * 删除指定模型
     * @param obj_id 模型的ID
     * @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int deleteObject(int obj_id);

    /**
     * 删除全部模型
     */
    public native static void deleteAllObject();

    /**
     * 重置指定模型位置, 在调用STMobileSLAMDetectJNI.setInitPos后调用
     * @param object_id
     */
    public native static void resetObject(int object_id);

    /**
     * 根据触碰点改变模型在slam平面上的位置
     * @param object_id  模型的ID
     * @param x 2.0f*x/viewWidth - 1.0f来计算
     * @param y 2.0f*y/viewHeight - 1.0f来计算
     * @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int setObjectLocation(int object_id, float x, float y);

    /**
     * 设置需显示的slam信息
     * @param type 需改变的信息种类，参考SLAMAUXType
     * @param visible 信息是否可见
     * @return 成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int setAuxDisplay(int type, boolean visible);

    /**
     * 渲染slam
     * @param texture_y  输入的原始纹理y通道
     * @param texture_uv  输入的原始纹理uv通道
     * @param width  输入纹理宽
     * @param height  输入纹理高
     * @param texture_output 输出纹理
     * @param img_out  输出图片数据, 可为空
     * @param pixel_format 输出图片数据格式，支持ST_PIX_FMT_RGBA8888, ST_PIX_FMT_RGBA, ST_PIX_FMT_NV12, ST_PIX_FMT_NV21
     * @return  成功返回ST_OK, 失败返回其他错误码,错误码定义在st_mobile_common.h中,如ST_E_FAIL等
     */
    public native static int renderSlam(int texture_y,
                                        int texture_uv,
                                        int width,
                                        int height,
                                        int texture_output,
                                        byte[] img_out,
                                        int pixel_format);

    public native static void reset();
}
