package com.sensetime.slam;

import java.util.List;

public class SLAMData {

    public static class Image {
        /**
         * the image data, row major order, one byte per pixel
         */
        public byte[] data;

        /**
         * image format
         */
        public int format;

        /**
         * image width
         */
        public int width;

        /**
         * image height
         */
        public int height;

        /**
         * image stride
         */
        public int stride;

        /**
         * timestamp, in seconds
         */
        public double timestamp;
    }

    public static class Frame {
        public Image image;

        /**
         * All IMU measures between last frame and current frame
         */
        public List<IMU> imus;

        /**
         * The attitude <strong>just</strong> before current frame
         */
        public Attitude attitude;

    }

    /**
     * IMU data, @see
     * https://developer.android.com/reference/android/hardware/SensorEvent.html
     */
    public static class IMU {
        /**
         * TYPE_LINEAR_ACCELERATION, double[3] type
         */
        public double linearAccelerationX;
        public double linearAccelerationY;
        public double linearAccelerationZ;

        /**
         * TYPE_GYROSCOPE, double[3] type
         */
        public double gyroscopeX;
        public double gyroscopeY;
        public double gyroscopeZ;
        /**
         * timestamp, in seconds
         */
        public double timestamp;
    }

    public static class Attitude {
        /**
         * TYPE_ROTATION_VECTOR, double[4] type
         */
        public double rotationVectorX;
        public double rotationVectorY;
        public double rotationVectorZ;
        public double rotationVectorW;
        /**
         * TYPE_GRAVITY, double[3] type
         */
        public double gravityX;
        public double gravityY;
        public double gravityZ;
        /**
         * timestamp, in seconds
         */
        public double timestamp;
    }

    public static class CameraPara {
        public float [] quaternion;
        public float [] center;
        public float [][] rotation;
        public float [] translation;
        public float depth;
        public int rotateType;
        public String toString() {
            StringBuffer sb = new StringBuffer();
            if(quaternion != null) {
                sb.append("quaternion:");
                for (int i = 0; i < quaternion.length; i++) {
                    sb.append(quaternion[i]).append(" ");
                }
            }
            if(center != null) {
                sb.append("center:");
                for (int i = 0; i < center.length; i++) {
                    sb.append(center[i]).append(" ");
                }
            }

            if(translation != null) {
                sb.append("translation:");
                for (int i = 0; i < translation.length; i++) {
                    sb.append(translation[i]).append(" ");
                }
            }

            if (rotation != null) {
                sb.append("rotation:");
                for (int i = 0; i < rotation.length; i++) {
                    for (int j = 0; j < rotation[i].length; j++) {
                        sb.append(rotation[i][j]).append(" ");
                    }
                }
            }

            sb.append("depth:").append(depth);
            return sb.toString();
        }
    }

    public static class Corner {
        public float x;
        public float y;
    }


    public static class Plane {
        public int id;
        public boolean isUpdate;
        public int planeFIndex[];
        public int planeVIndex[];
        public float planeNormal[];
        public float planeOriginPoint[];
        public float planeXAxis[];
    }

    public static class Landmark {
        public float x;
        public float y;
        public float z;
        public float w;
    }

    public static class SLAMResult {
        /**
         * 状态
         */
        public int state;

        /**
         * 相机信息
         */
        public CameraPara cameraPara;

        /**
         * 相机横向视角
         */
        public float fovx;

        /**
         * 机身姿态四元数
         */
        public float bodyQuaternion[];

        /**
         * 特征点坐标
         */
        public Corner[] corners;

        /**
         * 跟踪置信度，阈值0~100
         */
        public int trackConfidence;

        public int trackState;

        /////////////////////////////////////////////////////////
        // Plane Example
        // 0 ----- 1       4 ----- 5
        //  |   / |         |   /
        //  |  /  |         |  /
        //  | /   |         | /
        // 3 ----- 2       6
        //
        // plane_count: 2
        // dense_mesh_v: [x0,y0,z0,nx0,ny0,nz0,r0,g0,b0,
        //                x1,y1,z1,nx1,ny1,nz1,r1,g1,b1,
        //                x2,y2,z2,...,
        //                x3,y3,z3,...,
        //                x4,y4,z4,...,
        //                x5,y5,z5,...,
        //                x6,y6,z6,...]
        // dense_mesh_f: [0,3,1,1,3,2,4,6,5]
        // dense_mesh_v_size: 63
        // dense_mesh_f_size: 9

        // plane_v_index: [0, 36, 63], size is plane_count + 1
        //                - [0, 36): the [start, end) verts data of the 1st plane in dense_mesh_v array
        //                - [36,63): the [start, end) verts data of the 2nd plane in dense_mesh_v array
        // plane_f_index: [0, 6, 9], size is plane_count + 1
        //                - [0,6): (0,3,1,1,3,2), the [start, end) triangles data of the 1st plane in dense_mesh_f array
        //                - [6,9): (4,6,5), the [start, end) triangle data of the 2nd plane in dense_mesh_f array
        ////////////////////////////////////////////////////////

        public Landmark[] landmarks;

        public Plane[] planes;
        public float[] denseMeshV;
        public int[] denseMeshF;

        public int[] planeVIndex;
        public int[] planeFIndex;

        //public byte[] info;


        public String toString() {
            StringBuffer sb = new StringBuffer();
            //sb.append("numFeatures:" + numFeatures + "\n");
            sb.append("trackConfidence:" + trackConfidence + "\n");
            sb.append("state:" + state + "\n");
            if(cameraPara != null) {
                sb.append("CameraPara:").append(cameraPara.toString()).append("\n");
            }
            sb.append("Corners:");
            if(corners != null) {

                for (int i = 0; i < corners.length; i++) {
                    if(corners[i] != null) {
                        sb.append(i).append(":").append(corners[i].x).append(" ").append(corners[i].y).append(" | ");
                    }
                }
            }

            sb.append("\n=========================================================\n");
            return sb.toString();
        }
    }
}
