package com.sensetime.slam;

public enum SLAMState {
    ST_SLAM_MARKER_DETECTING,
    ST_SLAM_TRACKING_SUCCESS,
    ST_SLAM_TRACKING_FAIL
}
