#include <stdio.h>
#include "st_mobile_renderer.h"
#include "utils.h"

#define LOG_TAG "STMobileSLAM"

extern "C" {
JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_createInstance(JNIEnv *env, jobject obj);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_setAppInitialOrientation(JNIEnv *env, jobject obj, jint orientation);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_addObject(JNIEnv *env, jobject obj, jstring path);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_renderSlam(JNIEnv *env, jobject obj,
                                                                                  jint texture_y, jint texture_uv,
                                                                                  jint width, jint height,
                                                                                  jint texture_output, jbyteArray img_out, jint pixel_format);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_setAuxDisplay(JNIEnv *env, jobject obj, jint type, jboolean visible);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_deleteObject(JNIEnv *env, jobject obj, jint obj_id);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_deleteAllObject(JNIEnv *env, jobject obj);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_resetObject(JNIEnv *env, jobject obj, jint object_id);

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_setObjectLocation(JNIEnv *env, jobject obj, jint object_id, jfloat x, jfloat y);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_destroy(JNIEnv *env, jobject obj);

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_reset(JNIEnv *env, jobject obj);

//JNIEXPORT int JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_imageRotate(JNIEnv *env, jobject thiz,
//                                                                                  jbyteArray imagesrc,
//                                                                                  jbyteArray imagedst,
//                                                                                  jint imageWidth,
//                                                                                  jint imageHeight,
//                                                                                  jint format,
//                                                                                  jint rotation);
};

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_createInstance(JNIEnv *env, jobject obj) {
    st_handle_t handle;
    LOGE("show:st_mobile_renderer_create");
    int result = (int) st_mobile_renderer_create(&handle);
    if (result != 0) {
        LOGE("st_mobile_renderer_create failed");
        return result;
    }
    setPointerToJava(env, obj, handle);
    return result;
}

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_addObject(JNIEnv *env, jobject obj, jstring path) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        const char *dirPath = env->GetStringUTFChars(path, 0);
        LOGE("show:st_mobile_renderer_add_object");
        result = st_mobile_renderer_add_object(handle, dirPath);
        env->ReleaseStringUTFChars(path, dirPath);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }

    return result;
}

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_renderSlam(JNIEnv *env, jobject obj,
                                                                                  jint texture_y, jint texture_uv,
                                                                                  jint width, jint height,
                                                                                  jint texture_output, jbyteArray img_out, jint pixel_format) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    st_slam_result *info = getPointerFromJava<st_slam_result>(env, obj, "com/sensetime/slam/STMobileSLAMDetectJNI", "nativeSlamResultPtr");
    jbyte *imageOutputBuf = NULL;
    int len = 0;
    if (img_out != NULL) {
        imageOutputBuf = env->GetByteArrayElements(img_out, 0);
        int len = env->GetArrayLength(img_out);
    }

    if (handle != NULL) {
        st_slam_result dumb;
        dumb.state = ST_SLAM_TRACKING_FAIL;
        //LOGE("show:st_mobile_renderer_render_slam_yuv");
        result = st_mobile_renderer_render_slam_yuv(handle,
                                                    texture_y,
                                                    texture_uv,
                                                    info == NULL ? dumb : *info,
                                                    width, height,
                                                    texture_output, (unsigned char *) imageOutputBuf, (st_pixel_format) pixel_format);
        if (result == ST_OK) {
            if (imageOutputBuf) {
                env->SetByteArrayRegion(img_out, 0, len, imageOutputBuf);
            }
        }
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }

    if (imageOutputBuf) {
        env->ReleaseByteArrayElements(img_out, imageOutputBuf, JNI_FALSE);
    }

    return result;
}


JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_setAuxDisplay(JNIEnv *env, jobject obj, jint type, jboolean visible) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        LOGE("show:st_mobile_renderer_set_aux_display %d %d", type, visible);
        result = st_mobile_renderer_set_aux_display(handle, (st_slam_aux_type) type, visible);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }

    return result;
}

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_deleteObject(JNIEnv *env, jobject obj, jint obj_id) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        LOGE("show:st_mobile_renderer_delete_object");
        result = st_mobile_renderer_delete_object(handle, obj_id);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }
    return result;
}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_deleteAllObject(JNIEnv *env, jobject obj) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        LOGE("show:st_mobile_renderer_delete_all_objects");
        st_mobile_renderer_delete_all_objects(handle);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }
}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_resetObject(JNIEnv *env, jobject obj, jint object_id) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        st_mobile_renderer_reset_object(handle, object_id);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }
}

JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_setObjectLocation(JNIEnv *env, jobject obj, jint object_id, jfloat x, jfloat y) {
    int result = ST_E_INVALIDARG;
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        result = st_mobile_renderer_set_object_location(handle, object_id, x, y);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }

    return result;
}

//JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_translateObject(JNIEnv *env, jobject obj, jint object_id, jfloat xt, jfloat yt, jfloat zt) {
//    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
//    int ret;
//    if (handle != NULL) {
//        ret = st_mobile_renderer_translate_object(handle, object_id, xt, yt, zt);
//    } else {
//        LOGE("Renderer handle is null %d", __LINE__);
//    }
//    return ret;
//}

//JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_scaleObject(JNIEnv *env, jobject obj, jint object_id, jfloat xs, jfloat ys, jfloat zs) {
//    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
//    int ret;
//    if (handle != NULL) {
//        ret = st_mobile_renderer_scale_object(handle, object_id, xs, ys, zs);
//    } else {
//        LOGE("Renderer handle is null %d", __LINE__);
//    }
//    return ret;
//}

//JNIEXPORT jint JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_rotateObject(JNIEnv *env, jobject obj, jint object_id, jfloat xr, jfloat yr, jfloat zr) {
//    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
//    int ret;
//    if (handle != NULL) {
//        ret = st_mobile_renderer_rotate_object(handle, object_id, xr, yr, zr);
//    } else {
//        LOGE("Renderer handle is null %d", __LINE__);
//    }
//    return ret;
//}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_destroy(JNIEnv *env, jobject obj) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        setPointerToJava<st_handle_t>(env, obj, NULL);
        st_mobile_renderer_destroy(handle);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }
}

JNIEXPORT void JNICALL
Java_com_sensetime_slam_STMobileSLAMRendererJNI_setAppInitialOrientation(JNIEnv *env, jobject obj,
                                                                         jint orientation) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        st_mobile_renderer_set_app_initial_orientation(handle, (st_rotate_type) orientation);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }

}

JNIEXPORT void JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_reset(JNIEnv *env, jobject obj) {
    st_handle_t handle = getPointerFromJava<st_handle_t>(env, obj);
    if (handle != NULL) {
        st_mobile_renderer_reset(handle);
    } else {
        LOGE("Renderer handle is null %d", __LINE__);
    }
}


//JNIEXPORT int
//JNICALL Java_com_sensetime_slam_STMobileSLAMRendererJNI_imageRotate(JNIEnv *env, jobject thiz, jbyteArray imagesrc, jbyteArray imagedst, jint imageWidth, jint imageHeight, jint format, jint rotation) {
//    jbyte *srcdata = (jbyte *) (env->GetPrimitiveArrayCritical(imagesrc, 0));
//    jbyte *dstdata = (jbyte *) env->GetPrimitiveArrayCritical(imagedst, 0);
//
//    st_pixel_format pixel_format = (st_pixel_format) format;
//    int stride = getImageStride(pixel_format, imageWidth);
//
//    int result = st_mobile_image_rotate((unsigned char *) srcdata, (unsigned char *) dstdata, imageWidth, imageHeight, stride, pixel_format, (st_rotate_type) rotation);
//
//    env->ReleasePrimitiveArrayCritical(imagesrc, srcdata, 0);
//    env->ReleasePrimitiveArrayCritical(imagedst, dstdata, 0);
//    return result;
//}