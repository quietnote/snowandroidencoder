#include <string.h>
#include "anchor.h"
#include "utils.h"

#define LOG_TAG "Anchor"

bool Anchor::insect(float x, float y, float screenwidth, float screenheight, const st_slam_result &result, float *hitpoint, float *normal) {
    float tmpy = (screenheight - y) / screenheight;
    float tmpx = x / screenwidth;

    x = tmpy;
    y = tmpx;

    float zNear = 0.1f;
    float zFar = 50.0f;
    float half_tan_fov = 0.5f * m_VideoHeight / m_Focal;
    float halffrustumHeight = zNear * half_tan_fov;
    float halffrustumWidth = halffrustumHeight * m_Aspect;

    float ratiox = 2.0f * x - 1.0f;
    float ratioy = 2.0f * y - 1.0f;

    Vector4f tmppt;
    tmppt.x = -ratiox * halffrustumWidth;
    tmppt.y = ratioy * halffrustumHeight;
    tmppt.z = -zNear;
    tmppt.w = 1.0f;

    Matrix4x4f invViewMat = makeInverseViewMatrix(result);
    Vector4f tmppt2 = invViewMat * tmppt;

    Vector3f viewpt;
    viewpt.x = invViewMat.data[12];
    viewpt.y = invViewMat.data[13];
    viewpt.z = invViewMat.data[14];

    Vector3f dirvec;
    dirvec.x = tmppt2.x - viewpt.x;
    dirvec.y = tmppt2.y - viewpt.y;
    dirvec.z = tmppt2.z - viewpt.z;
    dirvec.normalize();

    float minDistance = 0x7fffffff;
    Vector3f minInsectpt;
    minInsectpt.x = 100000.0f;
    minInsectpt.y = 100000.0f;
    minInsectpt.z = 100000.0f;

    int triangle_count = result.dense_mesh_f_size / 3;
    float *vertexs = result.dense_mesh_v;
    int *triangles = result.dense_mesh_f;

    Vector3f insectPoint;
    Vector3f minNormal;
    bool sectWithRealPlane = false;
    for (int i = 0; i < triangle_count; ++i) {
        int index0, index1, index2;
        index0 = triangles[i * 3];
        index1 = triangles[i * 3 + 1];
        index2 = triangles[i * 3 + 2];

        Vector3f vert0, vert1, vert2;
        vert0.x = vertexs[index0 * 9];
        vert0.y = vertexs[index0 * 9 + 1];
        vert0.z = vertexs[index0 * 9 + 2];

        vert1.x = vertexs[index1 * 9];
        vert1.y = vertexs[index1 * 9 + 1];
        vert1.z = vertexs[index1 * 9 + 2];

        vert2.x = vertexs[index2 * 9];
        vert2.y = vertexs[index2 * 9 + 1];
        vert2.z = vertexs[index2 * 9 + 2];

        bool insected = false;
        insectPoint = calTriangleIntersectPoint(vert0, vert1, vert2, dirvec, viewpt, insected);
        if (insected) {
            float dis = pow((viewpt.x - insectPoint.x), 2) + pow((viewpt.y - insectPoint.y), 2) + pow((viewpt.z - insectPoint.z), 2);
            if (dis < minDistance) {
                minInsectpt = insectPoint;
                minNormal = calTriangleNormal(insectPoint, vert1, vert2);
                minDistance = dis;
                sectWithRealPlane = true;
            }
        }
    }

    if(!sectWithRealPlane) {
        //和零平面相交
        bool insected = false;
        insectPoint = calPlaneLineIntersectPoint(dirvec, viewpt, insected);
        float dis = pow((viewpt.x - insectPoint.x), 2) + pow((viewpt.y - insectPoint.y), 2) + pow((viewpt.z - insectPoint.z), 2);
        if (dis < minDistance) {
            minInsectpt = insectPoint;
            minNormal.x = 0.0;
            minNormal.y = 0.0;
            minNormal.z = 1.0;
        }
    }

    memcpy(hitpoint, minInsectpt, 3 * sizeof(float));
    memcpy(normal, minNormal, 3 * sizeof(float));

    if (hitpoint[0] == 100000.0f && hitpoint[1] == 100000.0f && hitpoint[2] == 100000.0f)
        return false;

    return true;
}

//求穿过交点的法线
Vector3f Anchor::calTriangleNormal(Vector3f hitpoint, Vector3f vert1, Vector3f vert2) {
    Vector3f edge1, edge2, Normal;
    edge1.x = hitpoint.x - vert1.x;
    edge1.y = hitpoint.y - vert1.y;
    edge1.z = hitpoint.z - vert1.z;
    edge2.x = hitpoint.x - vert2.x;
    edge2.y = hitpoint.y - vert2.y;
    edge2.z = hitpoint.z - vert2.z;

    Normal.x = edge1.y * edge2.z - edge1.z * edge2.y;
    Normal.y = edge1.z * edge2.x - edge1.x * edge2.z;
    Normal.z = edge1.x * edge2.y - edge1.y * edge2.x;

    return Normal;
}

//射线与三角形求交
//lineVector 直线的方向向量，长度为3
//linePoint 直线经过的一点坐标，长度为3
Vector3f Anchor::calTriangleIntersectPoint(Vector3f vert0, Vector3f vert1, Vector3f vert2, Vector3f lineVector, Vector3f linePoint, bool &infinit) {
    float det, inv_det, u, v, t;
    Vector3f edge1, edge2, pvec, tvec, qvec, returnResult;
    edge1.x = vert1.x - vert0.x;
    edge1.y = vert1.y - vert0.y;
    edge1.z = vert1.z - vert0.z;
    edge2.x = vert2.x - vert0.x;
    edge2.y = vert2.y - vert0.y;
    edge2.z = vert2.z - vert0.z;


    pvec.x = lineVector.y * edge2.z - lineVector.z * edge2.y;
    pvec.y = lineVector.z * edge2.x - lineVector.x * edge2.z;
    pvec.z = lineVector.x * edge2.y - lineVector.y * edge2.x;

    det = pvec.x * edge1.x + pvec.y * edge1.y + pvec.z * edge1.z;

    if (det < 0.000001 && det > -0.000001) {
        infinit = false;
        returnResult.x = 100000.0f;
        returnResult.y = 100000.0f;
        returnResult.z = 100000.0f;

        return returnResult;
    }

    inv_det = 1.0 / det;

    tvec.x = linePoint.x - vert0.x;
    tvec.y = linePoint.y - vert0.y;
    tvec.z = linePoint.z - vert0.z;

    u = tvec.x * pvec.x + tvec.y * pvec.y + tvec.z * pvec.z;
    u *= inv_det;
    if (u < 0.0 || u > 1.0) {
        infinit = false;
        returnResult.x = 100000.0f;
        returnResult.y = 100000.0f;
        returnResult.z = 100000.0f;

        return returnResult;
    }

    qvec.x = tvec.y * edge1.z - tvec.z * edge1.y;
    qvec.y = tvec.z * edge1.x - tvec.x * edge1.z;
    qvec.z = tvec.x * edge1.y - tvec.y * edge1.x;

    v = lineVector.x * qvec.x + lineVector.y * qvec.y + lineVector.z * qvec.z;
    v *= inv_det;
    if (v < 0.0 || u + v > 1.0) {
        infinit = false;
        returnResult.x = 100000.0f;
        returnResult.y = 100000.0f;
        returnResult.z = 100000.0f;

        return returnResult;
    }

    t = edge2.x * qvec.x + edge2.y * qvec.y + edge2.z * qvec.z;
    t *= inv_det;

    returnResult.x = linePoint.x + lineVector.x * t;
    returnResult.y = linePoint.y + lineVector.y * t;
    returnResult.z = linePoint.z + lineVector.z * t;

    infinit = true;
    return returnResult;
}

Vector3f Anchor::calPlaneLineIntersectPoint(Vector3f lineVector, Vector3f linePoint, bool &infinit) {
    Vector3f returnResult;
    float vp1, vp2, vp3, n1, n2, n3, v1, v2, v3, m1, m2, m3, t, vpt;

    //水平面法线
    Vector3f planeNormal;
    planeNormal.x = 0.0f;
    planeNormal.y = 0.0f;
    planeNormal.z = 1.0f;

    //水平面点
    Vector3f planePoint;
    planePoint.x = 0.0f;
    planePoint.y = 0.0f;
    planePoint.z = 0.0f;

    vp1 = planeNormal.x;
    vp2 = planeNormal.y;
    vp3 = planeNormal.z;

    n1 = planePoint.x;
    n2 = planePoint.y;
    n3 = planePoint.z;

    v1 = lineVector.x;
    v2 = lineVector.y;
    v3 = lineVector.z;

    m1 = linePoint.x;
    m2 = linePoint.y;
    m3 = linePoint.z;

    vpt = v1 * vp1 + v2 * vp2 + v3 * vp3;

    infinit = true;
    //首先判断直线是否与平面平行
    if (vpt == 0) {
        infinit = false;
        returnResult.x = 100000.0f;
        returnResult.y = 100000.0f;
        returnResult.z = 100000.0f;
    } else {
        t = ((n1 - m1) * vp1 + (n2 - m2) * vp2 + (n3 - m3) * vp3) / vpt;
        returnResult.x = m1 + v1 * t;
        returnResult.y = m2 + v2 * t;
        returnResult.z = m3 + v3 * t;
    }

    return returnResult;
}

Matrix4x4f Anchor::makeInverseViewMatrix(const st_slam_result &result) {
    float matrix[16];
    matrix[0] = result.camera.rotation[0][0];
    matrix[1] = -result.camera.rotation[1][0];
    matrix[2] = -result.camera.rotation[2][0];
    matrix[3] = 0.0f;

    matrix[4] = result.camera.rotation[0][1];
    matrix[5] = -result.camera.rotation[1][1];
    matrix[6] = -result.camera.rotation[2][1];
    matrix[7] = 0.0f;

    matrix[8] = result.camera.rotation[0][2];
    matrix[9] = -result.camera.rotation[1][2];
    matrix[10] = -result.camera.rotation[2][2];
    matrix[11] = 0.0f;

    matrix[12] = result.camera.translation[0];
    matrix[13] = -result.camera.translation[1];
    matrix[14] = -result.camera.translation[2];
    matrix[15] = 1.0f;

    Matrix4x4f viewMatrix;
    memcpy(viewMatrix.data, matrix, 16 * sizeof(float));
    return viewMatrix.inverse();
}

