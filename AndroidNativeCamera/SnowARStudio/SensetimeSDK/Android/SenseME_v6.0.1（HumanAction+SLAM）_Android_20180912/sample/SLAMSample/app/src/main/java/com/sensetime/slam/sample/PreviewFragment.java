package com.sensetime.slam.sample;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sensetime.slam.SLAMAUXType;
import com.sensetime.slam.sample.util.Accelerometer;
import com.sensetime.slam.sample.util.CameraSource;
import com.sensetime.slam.sample.util.FileUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PreviewFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "PreviewFragment";

    static final int MSG_CHANGE_MODEL = 1;
    static final int MSG_SAVE_PICTURE = 2;

    private LinearLayout mTipsView;
    private ImageView mBackView;
    private ImageView mResetView;
    private ImageView mSavePictureView;
    private ImageView mAddModelView;
    private TextView mDebugTextView;
    private MyGLSurfaceView mGLSurfaceView;
    private RecyclerView mRecyclerView;
    private ImageView mSettingView;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private List<ModelInfo> mData = new ArrayList<>();
    private int mCurrentSelectPos = 0;
    private Accelerometer mAccelerometer;
    private boolean mFirstSlamStart = true;

    private boolean mHighResolution = true;
    private boolean mHighResolutionLast = true;
    private boolean mMultiPlane = true;
    private boolean mMultiPlaneLast = true;
    private boolean mShowPlane = false;
    private boolean mShowPlaneLast = false;
    private boolean mShowWidget = false;
    private boolean mShowWidgetLast = false;

    public static class ModelInfo {
        public int id;
        public String modelPath;
        public Drawable drawable;
        public String desText;

        public ModelInfo(int id, String modelPath, Drawable drawable, String desText) {
            this.id = id;
            this.modelPath = modelPath;
            this.drawable = drawable;
            this.desText = desText;
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_CHANGE_MODEL:
                    int pos = msg.arg1;
                    changeModel(pos);
                    break;
                case MSG_SAVE_PICTURE:
                    if (msg.obj != null) {
                        String info = (String) msg.obj;
                        Toast.makeText(getActivity().getApplicationContext(), info, Toast.LENGTH_SHORT).show();
                    }
                    break;
            }

        }
    };

    private void changeModel(int pos) {
        if (pos > mData.size() - 1 || pos < 0) {
            Log.e(TAG, "Position overflow");
            return;
        }

        ModelInfo item = mData.get(pos);
        if (mCurrentSelectPos != 0 && mCurrentSelectPos != pos && mData.get(mCurrentSelectPos).id != -1) {
            //模型发生变化
            mData.get(mCurrentSelectPos).id = -1;
        }

        mGLSurfaceView.deleteAllObjects();

        mCurrentSelectPos = pos;
        if (mCurrentSelectPos != 0 && mData.get(mCurrentSelectPos).id == -1 && mGLSurfaceView.isSLAMStarted()) {
            mData.get(mCurrentSelectPos).id = mGLSurfaceView.addObject(mData.get(mCurrentSelectPos).modelPath);
            if (mData.get(mCurrentSelectPos).id == -1) {
                Log.e(TAG, "Add model" + mData.get(mCurrentSelectPos).modelPath + " failed");
            }
        }

        mAddModelView.setImageDrawable(mCurrentSelectPos == 0 ? getResources().getDrawable(R.drawable.add_model) : mData.get(mCurrentSelectPos).drawable);
    }

    public PreviewFragment() {
        mGLSurfaceView = null;
    }

    public static PreviewFragment newInstance() {
        return new PreviewFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initModelData();
        initView(view);
        initAccelerometer(getActivity().getApplicationContext());
    }

    @Override
    public void onResume() {
        super.onResume();

        startAccelerometer();
        bindModelData();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopAccelerometer();
        //mGLSurfaceView.deleteAllObjects();
    }

    private void reset()
    {
        mGLSurfaceView.resetSLAM();
        mGLSurfaceView.stopSLAM();

        if(mCurrentSelectPos >=0 && mCurrentSelectPos < mData.size()) {
            mData.get(mCurrentSelectPos).id = -1;
            mCurrentSelectPos = 0;
        }

        if (mRecyclerView.getVisibility() == View.VISIBLE) {
            mRecyclerView.setVisibility(View.INVISIBLE);
        }

        //默认选择第一个模型
        Message msg = Message.obtain();
        msg.what = MSG_CHANGE_MODEL;
        msg.arg1 = 1;
        mHandler.sendMessage(msg);

        //reset时不显示平面和widget信息
        mShowPlane = false;
        mShowPlaneLast = false;
        mShowWidget = false;
        mShowWidgetLast = false;
        mGLSurfaceView.setAuxDisplay(SLAMAUXType.ST_SLAM_ALL.ordinal(), false);
        mGLSurfaceView.setPlaneDisplay(false);
        mGLSurfaceView.setFirstLoc(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                getActivity().finish();
                mGLSurfaceView.releaseHandle();
                break;
            case R.id.reset:
               reset();
                break;
            case R.id.add_model:
                bindModelData();
                if (mRecyclerView.getVisibility() == View.VISIBLE) {
                    mRecyclerView.setVisibility(View.INVISIBLE);
                } else {
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mRecyclerView.setAdapter(mRecyclerViewAdapter);
                }
                break;
            case R.id.save_picture:
                mGLSurfaceView.sendSavePictureCommand();
                break;
            case R.id.iv_settings:
                showSettingDialog();
                break;
            default:
                break;
        }
    }

    private void switchPreviewSize() {
        mGLSurfaceView.changeToBigPreviewSize(mHighResolution);
        mData.get(mCurrentSelectPos).id = -1;
    }

    private void changeDetectMode() {
       mGLSurfaceView.changeDetectMode(mMultiPlane);
    }

    public void onBackPressed() {
        getActivity().finish();
        mGLSurfaceView.releaseHandle();
    }

    public void savePicture(byte[] rgbaBuf, int imageWidth, int imageHeight, CameraSource.CameraOrientation orientation) {
        Message msg = Message.obtain();
        msg.what = MSG_SAVE_PICTURE;

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            msg.obj = "No WRITE_EXTERNAL_STORAGE permission, save picture failed";
            mHandler.sendMessage(msg);
            return;
        }

        if (rgbaBuf.length != imageWidth * imageHeight * 4) {
            msg.obj = "rgba buffer with wrong size";
            mHandler.sendMessage(msg);
            return;
        }

        int[] argb = new int[imageWidth * imageWidth];
        for (int i = 0; i < imageHeight; i++) {
            for (int j = 0; j < imageWidth; j++) {
                argb[i * imageWidth + j] = 255;
                int r = rgbaBuf[(i * imageWidth + j) * 4 + 0];
                r = r & 0xff;
                argb[i * imageWidth + j] = (argb[i * imageWidth + j] << 8) + r; //+r
                int g = rgbaBuf[(i * imageWidth + j) * 4 + 1];
                g = g & 0xff;
                argb[i * imageWidth + j] = (argb[i * imageWidth + j] << 8) + g; //+g
                int b = rgbaBuf[(i * imageWidth + j) * 4 + 2];
                b = b & 0xff;
                argb[i * imageWidth + j] = (argb[i * imageWidth + j] << 8) + b; //+b
            }
        }

        File file = FileUtils.getOutputMediaFile();

        Bitmap srcBitmap = Bitmap.createBitmap(argb, imageWidth, imageHeight, Bitmap.Config.ARGB_8888);
        if (srcBitmap == null) {
            msg.obj = "Bitmap create failed";
            mHandler.sendMessage(msg);
            return;
        }

        Matrix matrix = new Matrix();
        if(orientation == CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_90) {
            matrix.postRotate(90);
        } else if (orientation == CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_270) {
            matrix.postRotate(270);
        }
        Bitmap rotate = Bitmap.createBitmap(srcBitmap, 0, 0, imageWidth, imageHeight, matrix, false);

        if (!saveToSDCard(file, rotate)) {
            msg.obj = "Compress failed";
            mHandler.sendMessage(msg);
            return;
        }
        srcBitmap.recycle();
        msg.obj = "Save at:" + file.getAbsolutePath();
        mHandler.sendMessage(msg);
    }

    private boolean saveToSDCard(File file, Bitmap bmp) {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(file));
            bmp.compress(Bitmap.CompressFormat.PNG, 100, bos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (bos != null)
                try {
                    bos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        String path = file.getAbsolutePath();
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        getActivity().getApplicationContext().sendBroadcast(mediaScanIntent);

        if (Build.VERSION.SDK_INT >= 19) {
            MediaScannerConnection.scanFile(getActivity().getApplicationContext(), new String[]{path}, null, null);
        }

        return true;
    }

    public void updateDebugText(String debugText) {
        mDebugTextView.setText(debugText);
    }

//    public void updateAuxDisplay(boolean show) {
//        mIsSetAux = show;
//        mSettomgView.setImageDrawable(show ? getResources().getDrawable(R.drawable.debug_on) : getResources().getDrawable(R.drawable.debug_off));
//    }

    public void slamStart() {
        if(mData == null || mCurrentSelectPos >= mData.size()) {
            Log.e(TAG, "mCurrentSelectPos overflow");
            return;
        }
        if (mRecyclerView.getVisibility() == View.VISIBLE) {
            mRecyclerView.setVisibility(View.INVISIBLE);
        }

        if (mCurrentSelectPos != 0 && mData.get(mCurrentSelectPos).id == -1) {
            mData.get(mCurrentSelectPos).id = mGLSurfaceView.addObject(mData.get(mCurrentSelectPos).modelPath);
            if (mData.get(mCurrentSelectPos).id == -1) {
                Log.e(TAG, "Add model" + mData.get(mCurrentSelectPos).modelPath + " failed");
            }
        }
    }

    public void updateTipsView(boolean started) {
        if (started) {
            mTipsView.setVisibility(View.INVISIBLE);
        } else {
            mTipsView.setVisibility(View.VISIBLE);
        }
    }

    private void initView(View view) {
        mGLSurfaceView = (MyGLSurfaceView) view.findViewById(R.id.image_view);
        mGLSurfaceView.setPreviewFragment(this);

        mTipsView = (LinearLayout) view.findViewById(R.id.tips);
        mTipsView.bringToFront();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mRecyclerView.bringToFront();

        mBackView = (ImageView) view.findViewById(R.id.back);
        mBackView.bringToFront();
        mBackView.setOnClickListener(this);

        mResetView = (ImageView) view.findViewById(R.id.reset);
        mResetView.bringToFront();
        mResetView.setOnClickListener(this);

        mAddModelView = (ImageView) view.findViewById(R.id.add_model);
        mAddModelView.bringToFront();
        mAddModelView.setOnClickListener(this);

        mSavePictureView = (ImageView) view.findViewById(R.id.save_picture);
        mSavePictureView.bringToFront();
        mSavePictureView.setOnClickListener(this);

        mDebugTextView = (TextView) view.findViewById(R.id.debug_text);
        mDebugTextView.bringToFront();
        mDebugTextView.setTextColor(0xFFFFFFF0);

        mSettingView = (ImageView) view.findViewById(R.id.iv_settings);
        mSettingView.bringToFront();
        mSettingView.setOnClickListener(this);
    }

    private void initModelData() {
        if (mData != null && mData.size() > 0)
            return;
        mData = FileUtils.getModelFiles(this.getActivity().getApplicationContext());
    }

    private void bindModelData() {
        if (mData.size() == 0)
            initModelData();

        if(mFirstSlamStart) {
            if(mCurrentSelectPos == 0) {
                mCurrentSelectPos = 1;
            }

            mAddModelView.setImageDrawable(mData.get(mCurrentSelectPos).drawable);
            mFirstSlamStart = false;
        }

        mRecyclerViewAdapter = new RecyclerViewAdapter(getActivity(), mData, mHandler, mCurrentSelectPos);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    private void showSettingDialog() {
        final Dialog mCameraDialog = new Dialog(getActivity(), R.style.dialog_style);

        mHighResolutionLast = mHighResolution;
        mMultiPlaneLast = mMultiPlane;
        mShowPlaneLast = mShowPlane;
        mShowWidgetLast = mShowWidget;

        final LinearLayout root = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.layout_control, null);

        updateResolutionUI(root);
        root.findViewById(R.id.tv_high).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHighResolution = true;
                //切换分辨率默认引擎默认为多平面检测
                mMultiPlaneLast = mMultiPlane = true;
                updateResolutionUI(root);
                updateDetectModeUI(root);
            }
        });

        root.findViewById(R.id.tv_low).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHighResolution = false;
                //切换分辨率默认引擎默认为多平面检测
                mMultiPlaneLast = mMultiPlane = true;
                updateResolutionUI(root);
                updateDetectModeUI(root);
            }
        });

        updateDetectModeUI(root);
        root.findViewById(R.id.tv_multi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMultiPlane = true;
                updateDetectModeUI(root);
            }
        });

        root.findViewById(R.id.tv_single).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMultiPlane = false;
                updateDetectModeUI(root);
            }
        });

        final ImageView ivShowPlane = (ImageView) root.findViewById(R.id.switch_plane);
        ivShowPlane.setImageResource(mShowPlane ? R.drawable.b_on : R.drawable.b_off);
        ivShowPlane.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShowPlane = !mShowPlane;
                ivShowPlane.setImageResource(mShowPlane ? R.drawable.b_on : R.drawable.b_off);
            }
        });

        final ImageView ivShowWidget = (ImageView) root.findViewById(R.id.switch_widget);
        ivShowWidget.setImageResource(mShowWidget ? R.drawable.b_on : R.drawable.b_off);
        ivShowWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mShowWidget = !mShowWidget;
                ivShowWidget.setImageResource(mShowWidget ? R.drawable.b_on : R.drawable.b_off);
            }
        });

        root.findViewById(R.id.bn_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHighResolutionLast != mHighResolution) {
                    switchPreviewSize();
                    updateTipsView(false);
                }

                if(mMultiPlaneLast != mMultiPlane) {
                    reset();
                    changeDetectMode();
                }

                mGLSurfaceView.setAuxDisplay(SLAMAUXType.ST_SLAM_ALL.ordinal(), mShowWidget);
                mGLSurfaceView.setPlaneDisplay(mShowPlane);

                mHighResolutionLast = mHighResolution;
                mMultiPlaneLast = mMultiPlane;
                mShowPlaneLast = mShowPlane;
                mShowWidgetLast = mShowWidget;

                if (mCameraDialog != null) {
                    mCameraDialog.dismiss();
                }
            }
        });

        mCameraDialog.setContentView(root);

        mCameraDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                    mHighResolution = mHighResolutionLast;
                    mMultiPlane = mMultiPlaneLast;
                    mShowPlane = mShowPlaneLast;
                    mShowWidget = mShowWidgetLast;
                    mCameraDialog.dismiss();
                }
                return false;
            }
        });


        Window dialogWindow = mCameraDialog.getWindow();
        dialogWindow.setGravity(Gravity.BOTTOM);
        dialogWindow.setWindowAnimations(R.style.dialog_anim_style); // 添加动画

        WindowManager.LayoutParams lp = dialogWindow.getAttributes(); //获取对话框当前的参数值
        lp.x = 0; // 新位置X坐标
        lp.y = 0; // 新位置Y坐标
        lp.width = (int) getResources().getDisplayMetrics().widthPixels; // 宽度
        //root.measure(0, 0);

        //lp.height = root.getMeasuredHeight();
        lp.alpha = 9f; // 透明度
        dialogWindow.setLayout( (int) (lp.width * 0.95) , WindowManager.LayoutParams.WRAP_CONTENT);
        dialogWindow.setAttributes(lp);

        mCameraDialog.show();
    }

    private void updateResolutionUI(View root) {
        if(mHighResolution) {
            root.findViewById(R.id.tv_high).setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
            root.findViewById(R.id.tv_low).setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        } else {
            root.findViewById(R.id.tv_high).setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            root.findViewById(R.id.tv_low).setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
        }
    }

    private void updateDetectModeUI(View root) {
        if(mMultiPlane) {
            root.findViewById(R.id.tv_multi).setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
            root.findViewById(R.id.tv_single).setBackgroundColor(getActivity().getResources().getColor(R.color.white));
        } else {
            root.findViewById(R.id.tv_multi).setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            root.findViewById(R.id.tv_single).setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimary));
        }
    }

    /**
     * 初始化重力传感器对象
     */
    private void initAccelerometer(Context appContext) {
        mAccelerometer = new Accelerometer(appContext);
    }

    /**
     * 传感器开始工作
     */
    private void startAccelerometer() {
        mAccelerometer.start();
    }

    /**
     * 传感器停止工作
     */
    private void stopAccelerometer() {
        mAccelerometer.stop();
    }
}
