#ifndef _ST_UTILS_H__
#define _ST_UTILS_H__

#include <sys/time.h>
#include <time.h>
#include <jni.h>

#include <jni.h>
#include <android/log.h>
#include <st_mobile_slam.h>

#define LOGGABLE 0

#define LOGI(...) if(LOGGABLE) __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define LOGD(...) if(LOGGABLE) __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#ifndef MIN
#define MIN(x, y) ((x)<(y)?(x):(y))
#endif

static inline jfieldID getField(JNIEnv *env, jclass cls, const char *name) {
    return env->GetStaticFieldID(cls, name, "J");
}

template<typename T>
T *getPointerFromJava(JNIEnv *env, jobject obj, const char *clsName = NULL, const char *fieldName = "nativeHandle") {
    bool staticCls = false;
    jclass cls = NULL;
    if (clsName != NULL) {
        cls = env->FindClass(clsName);
    } else {
        cls = (jclass) env->NewGlobalRef(obj);
        staticCls = true;
    }

    if (cls == NULL)
        return NULL;

    jlong handle = env->GetStaticLongField(cls, getField(env, cls, fieldName));
    if (staticCls) {
        env->DeleteGlobalRef(cls);
    } else {
        env->DeleteLocalRef(cls);
    }

    return reinterpret_cast<T *>(handle);
}

template<typename T>
void setPointerToJava(JNIEnv *env, jobject obj, T *t, const char *clsName = NULL, const char *fieldName = "nativeHandle") {
    bool staticCls = false;
    jclass cls = NULL;
    if (clsName != NULL) {
        cls = env->FindClass(clsName);
    } else {
        cls = (jclass) env->NewGlobalRef(obj);
        staticCls = true;
    }

    if (cls == NULL)
        return;

    jlong handle = reinterpret_cast<jlong>(t);
    env->SetStaticLongField(cls, getField(env, cls, fieldName), handle);
    if (staticCls) {
        env->DeleteGlobalRef(cls);
    } else {
        env->DeleteLocalRef(cls);
    }
}

long getCurrentTime();

int getImageStride(const st_pixel_format &pixel_format, const int &outputWidth);

// convert c object to java object
void convert2JavaSLAMResult(JNIEnv *env, jobject slamResult, const st_slam_result &result);

// convert java object to c object
bool convert2JNIImu(JNIEnv *env, jobject imuObj, st_slam_imu &imu);

bool convert2JNIAttitude(JNIEnv *env, jobject attitudeObj, st_slam_attitude &attitude);

bool convert2JNISTImage(JNIEnv *env, jobject imageObj, st_image_t &st_image);
bool releaseSTImage(JNIEnv *env, jobject imageObj, st_image_t &st_image);


#endif // _ST_UTILS_H__