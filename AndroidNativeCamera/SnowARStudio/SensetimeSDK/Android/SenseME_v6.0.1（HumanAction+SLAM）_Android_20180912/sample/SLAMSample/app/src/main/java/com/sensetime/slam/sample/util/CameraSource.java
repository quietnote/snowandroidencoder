package com.sensetime.slam.sample.util;

import android.app.Activity;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.os.SystemClock;

import com.sensetime.slam.sample.MyGLRenderer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.sensetime.slam.sample.util.CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_0;
import static com.sensetime.slam.sample.util.CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_180;
import static com.sensetime.slam.sample.util.CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_270;
import static com.sensetime.slam.sample.util.CameraSource.CameraOrientation.ST_CLOCKWISE_ROTATE_90;

public class CameraSource implements Camera.PreviewCallback {

    public enum CameraOrientation {
        ST_CLOCKWISE_ROTATE_0,
        ST_CLOCKWISE_ROTATE_90,
        ST_CLOCKWISE_ROTATE_180,
        ST_CLOCKWISE_ROTATE_270
    }

    private Camera camera;
    private int preWidth = -1;
    private int preHeight = -1;
    private int mCurrentBufSize = 0;
    private boolean isPreviewing;
    private String mFocusMode;
    private SurfaceTexture surfaceTexture;

    private long mBaseTime;
    private IMUReader mIMUReader;
    private MyGLRenderer mGLRender;

    private int mCameraOrientation;
    private boolean didFirstAutoFocus;

    private boolean mNeedFlipH = false;
    private boolean mNeedFlipV = false;

    public CameraSource(Activity activity, MyGLRenderer glRender) {
        preWidth = 1280;
        preHeight = 720;
        isPreviewing = false;
        mFocusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;
        mCameraOrientation = 0;
        mGLRender = glRender;
        didFirstAutoFocus = false;
        mIMUReader = new IMUReader(activity);
    }

    private int getFrontCameraIndex() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras(); // get cameras number
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                mCameraOrientation = cameraInfo.orientation;
                mNeedFlipH = true;
                mNeedFlipV = true;
                return camIdx;
            }
        }
        return 0;
    }

    private int getBackCameraIndex() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        int cameraCount = Camera.getNumberOfCameras(); // get cameras number
        for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                mCameraOrientation = cameraInfo.orientation;
                mNeedFlipH = false;
                mNeedFlipV = true;
                return camIdx;
            }
        }
        return 0;
    }

    public boolean isOpen() {
        return camera != null;
    }

    public final static int CAMERA_DIRECTION_DEFAULT = 0;
    public final static int CAMERA_DIRECTION_BACK = 1;
    public final static int CAMERA_DIRECTION_FRONT = 2;

    public int openCamera(int direction) {
        if (camera != null) {
            camera.release();
            camera = null;
        }
        try {
            switch (direction) {
                case CAMERA_DIRECTION_DEFAULT:
                    camera = Camera.open();
                    break;
                case CAMERA_DIRECTION_BACK:
                    camera = Camera.open(getBackCameraIndex());
                    break;
                case CAMERA_DIRECTION_FRONT:
                    camera = Camera.open(getFrontCameraIndex());
                    break;
                default:
                    camera = Camera.open();
                    break;
            }


        } catch (Exception e) {
            return -1;
        }
        return 0;
    }


    public CameraOrientation getCameraOrientation() {
        CameraOrientation ret = ST_CLOCKWISE_ROTATE_90;
        if (!isOpen()) {
            return ret;
        }

        switch (mCameraOrientation) {
            case 0:
                ret = ST_CLOCKWISE_ROTATE_0;
                break;
            case 90:
                ret = ST_CLOCKWISE_ROTATE_90;
                break;
            case 180:
                ret = ST_CLOCKWISE_ROTATE_180;
                break;
            case 270:
                ret = ST_CLOCKWISE_ROTATE_270;
                break;
        }
        return ret;
    }

    public boolean isNeedFlipH() {
        return mNeedFlipH;
    }

    public boolean isNeedFlipV() {
        return mNeedFlipV;
    }

    public List<Size> getSupportsSizes() {
        if (camera == null) return null;
        return camera.getParameters().getSupportedPreviewSizes();
    }

    public int getPreviewWidth() {
        return preWidth;
    }

    public int getPreviewHeight() {
        return preHeight;
    }

    public int setPreviewSize(int width, int height) {
        preWidth = width;
        preHeight = height;
        return 0;
    }

    public void autoFocus(Camera.AutoFocusCallback cb) {
        if (camera != null)
            camera.autoFocus(cb);
    }

    float getFocalLength() {
        float output = -1;
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters != null) {
                output = parameters.getFocalLength();
            }
        }
        return output;
    }

    public boolean isPreviewing() {
        return isPreviewing;
    }

    public int startPreview() {
        if (camera == null)
            return -1;

        if (isPreviewing)
            return 0;

        try {

            mBaseTime = SystemClock.elapsedRealtime();
            mIMUReader.start();

            Camera.Parameters parameters = camera.getParameters();
            List<Size> list = parameters.getSupportedPreviewSizes();
            parameters.setPreviewSize(preWidth, preHeight);
            parameters.setPreviewFormat(ImageFormat.NV21);

            List<String> focusModes = parameters.getSupportedFocusModes();
            if (focusModes.contains(mFocusMode)) {
                parameters.setFocusMode(mFocusMode);
            }

            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

            camera.setParameters(parameters);
            if(surfaceTexture == null)
            {
                surfaceTexture = new SurfaceTexture(-1);
                camera.setPreviewTexture(surfaceTexture);
            }

            Camera.Parameters para = camera.getParameters();
            int pixelformat = para.getPreviewFormat();
            PixelFormat pixelinfo = new PixelFormat();
            PixelFormat.getPixelFormatInfo(pixelformat, pixelinfo);
            int bufSize = preWidth * preHeight * pixelinfo.bitsPerPixel / 8;
            mCurrentBufSize = bufSize;
            for (int i = 0; i < 5; i++) {
                camera.addCallbackBuffer(new byte[bufSize]);
            }
            camera.setPreviewCallbackWithBuffer(this);
            System.gc();
            camera.startPreview();
            isPreviewing = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setFocusMode(String value) {
        if (camera != null) {
            Camera.Parameters parameters = camera.getParameters();
            if (parameters != null) {
                List<String> focusModes = parameters.getSupportedFocusModes();
                if (focusModes.contains(value)) {
                    parameters.setFocusMode(value);
                    camera.setParameters(parameters);
                    mFocusMode = value;
                } else {
                }
            }
        }
    }

    public String getFocusMode() {
        return mFocusMode;
    }

    public int stopPreview() {
        if (camera != null) {
            camera.setPreviewCallbackWithBuffer(null);
            camera.stopPreview();
            isPreviewing = false;

            mIMUReader.stop();
        }
        try {
            camera.setPreviewTexture(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (surfaceTexture != null) {
            surfaceTexture.release();
            surfaceTexture = null;
        }
        return 0;
    }

    public int closeCamera() {
        if (isPreviewing) {
            stopPreview();
        }

        try {
            mIMUReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (camera != null) {
            camera.setPreviewCallbackWithBuffer(null);
            camera.release();
            camera = null;
        }
        if (surfaceTexture != null) {
            surfaceTexture.release();
            surfaceTexture = null;
        }
        return 0;
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if(data ==null || mCurrentBufSize != data.length)
        {
            return;
        }
        //long timestamp = SystemClock.elapsedRealtime();

        long timestamp = SystemClock.elapsedRealtime() * 1000000;
        List<IMUReader.ValueTimePair> linearAccelerations = new ArrayList<>();
        List<IMUReader.ValueTimePair> gyroscopes = new ArrayList<>();
        IMUReader.ValueTimePair rotationVector = new IMUReader.ValueTimePair();
        IMUReader.ValueTimePair gravity = new IMUReader.ValueTimePair();

        mIMUReader.get(linearAccelerations, gyroscopes, rotationVector, gravity, timestamp);
        mIMUReader.mergeIMUs(linearAccelerations, gyroscopes);

        mGLRender.setNV21DataAndIMUData(data, preWidth, preHeight,
                linearAccelerations, gyroscopes, rotationVector, gravity,
                timestamp, mBaseTime);

        camera.addCallbackBuffer(data);

        if (!didFirstAutoFocus) {
            Camera.AutoFocusCallback aF = new Camera.AutoFocusCallback(){
                @Override
                public void onAutoFocus(boolean arg0, Camera arg1) {
                    if(arg0){

                    }

                }};
            autoFocus(aF);

            didFirstAutoFocus = true;
        }
    }

    public int setFlashTorchMode(boolean on) {
        if (camera == null) return -1;
        Camera.Parameters parameters = camera.getParameters();
        if (on) {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);//
        } else {
            parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);//
        }
        camera.setParameters(parameters);
        return 0;
    }
//
//    public static final int FOCUS_MODE_NORMAL = 0;           ///< Default focus mode
//    public static final int FOCUS_MODE_TRIGGERAUTO = 1;      ///< Triggers a single autofocus operation
//    public static final int FOCUS_MODE_CONTINUOUSAUTO = 2;   ///< Continuous autofocus mode
//    public static final int FOCUS_MODE_INFINITY = 3;         ///< Focus set to infinity
//    public static final int FOCUS_MODE_MACRO = 4;             ///< Macro mode for close-up focus
//
//    public int setFocusMode(int mode) {
//        if (camera == null) return -1;
//        Camera.Parameters parameters = camera.getParameters();
//        List<String> focusModes = parameters.getSupportedFocusModes();
//        switch (mode) {
//            case 0:
//                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
//                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//                break;
//            case 1:
//                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO))
//                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//                break;
//            case 2:
//                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
//                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//                break;
//            case 3:
//                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_INFINITY))
//                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
//                break;
//            case 4:
//                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_MACRO))
//                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
//                break;
//            default:
//                break;
//        }
//        camera.setParameters(parameters);
//        return 0;
//    }

    public void setFlash() {
        if (camera == null) return;
        Camera.Parameters params = camera.getParameters();
        String flashMode = params.getFlashMode();
        if (flashMode.equals(Camera.Parameters.FLASH_MODE_OFF)) {
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        } else {
            params.setFlashMode(Camera.Parameters.ANTIBANDING_OFF);
        }
        camera.setParameters(params);
    }

    public float getFovH() {
        Camera.Parameters parameters = camera.getParameters();
        return parameters.getHorizontalViewAngle();
    }

    public float getFov() {
        Camera.Parameters parameters = camera.getParameters();
        return parameters.getVerticalViewAngle();
    }
}
