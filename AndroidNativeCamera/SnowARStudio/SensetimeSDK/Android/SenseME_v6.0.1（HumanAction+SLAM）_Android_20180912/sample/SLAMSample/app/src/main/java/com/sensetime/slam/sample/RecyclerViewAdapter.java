package com.sensetime.slam.sample;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapter extends Adapter<ViewHolder> {
    private static final int TYPE_ITEM_DETAIL = 1;
    private Context mContext;
    private List<PreviewFragment.ModelInfo> mData;
    private Handler mHandler;
    private CheckBox lastCheckBox;
    private int mCurrentPos;

    public RecyclerViewAdapter(Context context, List<PreviewFragment.ModelInfo> data, Handler handler, int currentPos) {
        this.mContext = context;
        this.mData = data;
        this.mHandler = handler;
        this.mCurrentPos = currentPos;
    }

    @Override
    public int getItemCount() {
        return mData.size() == 0 ? 0 : mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return TYPE_ITEM_DETAIL;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM_DETAIL) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.detail_item, parent, false);
            return new DetailItemViewHolder(view, mHandler);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PreviewFragment.ModelInfo item = mData.get(position);
        final DetailItemViewHolder detailItemVH = (DetailItemViewHolder) holder;
        detailItemVH.icon.setImageDrawable(item.drawable);
        detailItemVH.des.setText(item.desText);
        if(position == mCurrentPos) {
            detailItemVH.checkBox.setChecked(true);
            lastCheckBox = detailItemVH.checkBox;
        }
    }

    private class DetailItemViewHolder extends ViewHolder implements View.OnClickListener {
        ImageView icon;
        TextView des;
        CheckBox checkBox;
        public DetailItemViewHolder(View view, Handler handler) {
            super(view);
            mHandler = handler;
            icon = (ImageView) view.findViewById(R.id.item_icon);
            des = (TextView) view.findViewById(R.id.item_des);
            checkBox = (CheckBox) view.findViewById(R.id.item_check);
            checkBox.setClickable(false);
            //checkBox.setOnClickListener(this);
            view.setTag(checkBox);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int pos = getPosition();
            CheckBox checkBox = (CheckBox) v.getTag();
            checkBox.setChecked(!checkBox.isChecked());
            Message msg = Message.obtain();
            msg.what = PreviewFragment.MSG_CHANGE_MODEL;
            if(checkBox.isChecked()) {
                if(lastCheckBox != null && lastCheckBox != checkBox && lastCheckBox.isChecked()) {
                    lastCheckBox.setChecked(false);
                }
                msg.arg1 = pos;
                mCurrentPos = pos;
                lastCheckBox = checkBox;
            } else {
                msg.arg1 = 0;
            }
            mHandler.sendMessage(msg);
        }
    }

}