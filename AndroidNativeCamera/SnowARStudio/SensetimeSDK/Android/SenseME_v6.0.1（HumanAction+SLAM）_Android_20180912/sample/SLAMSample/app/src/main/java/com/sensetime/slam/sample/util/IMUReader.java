package com.sensetime.slam.sample.util;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * A class designed for reading IMU data, the class is thread safe
 */
public class IMUReader implements SensorEventListener, AutoCloseable {

    private static final String LOG_TAG = "IMUReader";

    /**
     * A simple class contains value and timestamp pair
     */
    public static class ValueTimePair {
        public ValueTimePair() {
        }

        public ValueTimePair(float[] val, long time) {
            values = val.clone();
            timestamp = time;
        }

        public float[] values;
        /**
         * timestamp in nanoseconds
         */
        public long timestamp;

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append("timestamp:").append(timestamp).append("\n");
            if (values != null) {
                for (int i = 0; i < values.length; i++) {
                    sb.append(values[i]).append(";");
                }
            } else {
                sb.append("null");
            }
            return sb.toString();
        }
    }

    private WeakReference<Activity> mActivityRef;
    private Semaphore mLock = new Semaphore(1);
    private ArrayList<ValueTimePair> mLinearAccelerations = new ArrayList<>();
    private ArrayList<ValueTimePair> mGyroscopes = new ArrayList<>();
    private ArrayList<ValueTimePair> mRotationVectors = new ArrayList<>();
    private ArrayList<ValueTimePair> mGravitys = new ArrayList<>();

    private Sensor mLinearAccelerationSensor;
    private Sensor mGyroscopeSensor;
    private Sensor mRotationVectorSensor;
    private Sensor mGravitySensor;

    private boolean hasGyroscopeSensor = false;

    private void logSystemTime() {
        Log.d(LOG_TAG, "System.currentTimeMillis " + System.currentTimeMillis());
        Log.d(LOG_TAG, "SystemClock.uptimeMills " + SystemClock.uptimeMillis());
        Log.d(LOG_TAG, "SystemClock.elapsedRealtime " + SystemClock.elapsedRealtime());
    }

    public IMUReader(Activity activity) {
        mActivityRef = new WeakReference<Activity>(activity);
    }

    private Activity getActivity() {
        return mActivityRef.get();
    }

    /*
     * Start recording IMU data for further retrieve
     */
    public void start() {
        mLinearAccelerations.clear();
        mGyroscopes.clear();
        mRotationVectors.clear();
        mGravitys.clear();

        // make sure at least one value exists, and we'll not get a invalid one
        ValueTimePair rot = new ValueTimePair();
        rot.values = new float[]{0, 0, 0, 1};
        rot.timestamp = 0;
        mRotationVectors.add(rot);

        ValueTimePair g = new ValueTimePair();
        g.values = new float[]{0, 0, 9.8f};
        g.timestamp = 0;
        mGravitys.add(g);

        RegisterListener();
    }

    /**
     * Stop recording
     */
    public void stop() {
        UnregisterListener();
    }

    @Override
    public void close() throws Exception {
        stop();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        float[] values = event.values;
        //long timestamp = event.timestamp;
        //long timestamp = SystemClock.elapsedRealtime();
        long timestamp = SystemClock.elapsedRealtime() * 1000000;
        Sensor sensor = event.sensor;

        ValueTimePair valueTimePair = new ValueTimePair(values, timestamp);
        try {
            mLock.acquire();
            if (sensor == mLinearAccelerationSensor) {
                //Log.d(LOG_TAG, "linear acceleration");
                //Log.d(LOG_TAG, valueTimePair.toString());
                mLinearAccelerations.add(valueTimePair);
            } else if (sensor == mGyroscopeSensor) {
                //Log.d(LOG_TAG, "gyroscope");
                //Log.d(LOG_TAG, valueTimePair.toString());
                mGyroscopes.add(valueTimePair);
            } else if (sensor == mRotationVectorSensor) {
                mRotationVectors.add(valueTimePair);
                //Log.d(LOG_TAG, "rotation vector");
                //Log.d(LOG_TAG, valueTimePair.toString());
            } else if (sensor == mGravitySensor) {
                mGravitys.add(valueTimePair);
                //Log.d(LOG_TAG, "gravity");
                //Log.d(LOG_TAG, valueTimePair.toString());
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Can not acquire IMU lock");
        } finally {
            mLock.release();
        }

        // Log.d(LOG_TAG, "onSensorChanges timestamp " + timestamp);
        // logSystemTime();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }


    private void RegisterListener() {
        SensorManager manager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);

        mLinearAccelerationSensor = manager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        if (null == mLinearAccelerationSensor) {
            Log.e(LOG_TAG, "Do not support linear acceleration sensor");
        }

        mGyroscopeSensor = manager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        if (null == mGyroscopeSensor) {
            Log.e(LOG_TAG, "Do not support gyroscope sensor");
        } else {
            hasGyroscopeSensor = true;
        }

        mRotationVectorSensor = manager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        if (null == mRotationVectorSensor) {
            Log.e(LOG_TAG, "Do not support rotation vector sensor");
        }

        mGravitySensor = manager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        if (null == mGravitySensor) {
            Log.e(LOG_TAG, "Do not support gravity sensor");
        }

        if (mLinearAccelerationSensor != null) {
            manager.registerListener(this, mLinearAccelerationSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }

        if (mGyroscopeSensor != null) {
            manager.registerListener(this, mGyroscopeSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }

        if (mRotationVectorSensor != null) {
            manager.registerListener(this, mRotationVectorSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }

        if (mGravitySensor != null) {
            manager.registerListener(this, mGravitySensor, SensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    private void UnregisterListener() {
        SensorManager manager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        manager.unregisterListener(this);
    }

    /**
     * Get all linear accelerations before @param time, the retrieved data will be
     * removed internally
     *
     * @param time time in nanoseconds
     * @return All linear accelerations before
     */
    public List<ValueTimePair> getAllLinearAccelerations(long time) {
        try {
            mLock.acquire();
            return getAndRemoveAllBefore(mLinearAccelerations, time);
        } catch (InterruptedException e) {
            throw new RuntimeException("Can not acquire IMU lock");
        } finally {
            mLock.release();
        }
    }

    /**
     * Get all gyroscopes before @param time, the retrieved data will be  removed
     * internally
     *
     * @param time time in nanoseconds
     * @return All gyroscopes before
     */
    public List<ValueTimePair> getAllGyroscopes(long time) {
        try {
            mLock.acquire();
            return getAndRemoveAllBefore(mGyroscopes, time);
        } catch (InterruptedException e) {
            throw new RuntimeException("Can not acquire IMU lock");
        } finally {
            mLock.release();
        }
    }

    /**
     * Get last recorded rotation vector before @param time, the data will not be
     * removed
     *
     * @param time time in nanoseconds
     * @return last recorded rotation vector, if has not recorded any data,
     * returns null
     */
    public ValueTimePair getLastRotationVector(long time) {
        try {
            mLock.acquire();
            List<ValueTimePair> allBefore = getAllBefore(mRotationVectors, time);
            // and remove several
            for (int i = 0; i + 1 < allBefore.size(); ++i)
                mRotationVectors.remove(0);
            if (!allBefore.isEmpty())
                return allBefore.get(allBefore.size() - 1);
            else
                return null;
        } catch (InterruptedException e) {
            throw new RuntimeException("Can not acquire IMU lock");
        } finally {
            mLock.release();
        }
    }

    /**
     * Get last gravity before @param time, the data will not be removed
     *
     * @param time time in nanoseconds
     * @return last recorded gravity, if has not recorded any data, return null
     */
    public ValueTimePair getLastGravity(long time) {
        try {
            mLock.acquire();
            List<ValueTimePair> allBefore = getAllBefore(mGravitys, time);
            for (int i = 0; i + 1 < allBefore.size(); ++i)
                mGravitys.remove(0);
            if (!allBefore.isEmpty())
                return allBefore.get(allBefore.size() - 1);
            else
                return null;
        } catch (InterruptedException e) {
            throw new RuntimeException("Can not acquire IMU lock");
        } finally {
            mLock.release();
        }
    }

    /**
     * Get all linear accleration, gyroscopes, last rotation vector and gravity
     * before @param time
     *
     * @param time time in nanoseconds
     */
    public void get(List<ValueTimePair> linearAccelerations, List<ValueTimePair> gyroscopes,
                    ValueTimePair rotationVector, ValueTimePair gravity, long time) {

//        linearAccelerations.clear();
//        gyroscopes.clear();
//        rotationVector.values = null;
//        rotationVector.timestamp = -1;
//        gravity.values = null;
//        gravity.timestamp = -1;
//        try {
//            mLock.acquire();
//
//            linearAccelerations.addAll(getAndRemoveAllBefore(mLinearAccelerations, time));
//            gyroscopes.addAll(getAndRemoveAllBefore(mGyroscopes, time));
//
//            List<ValueTimePair> allBeforeRotationVector = getAllBefore(mRotationVectors, time);
//            for (int i = 0; i + 1 < allBeforeRotationVector.size(); ++i)
//                mRotationVectors.remove(0);
//
//            ValueTimePair last = allBeforeRotationVector.get(allBeforeRotationVector.size() - 1);
//            rotationVector.values = last.values;
//            rotationVector.timestamp = last.timestamp;
//
//            List<ValueTimePair> allBeforeGravity = getAllBefore(mGravitys, time);
//            for (int i = 0; i + 1 < allBeforeGravity.size(); ++i)
//                mGravitys.remove(0);
//
//            last = allBeforeGravity.get(allBeforeGravity.size() - 1);
//            gravity.values = last.values;
//            gravity.timestamp = last.timestamp;
//        } catch (InterruptedException e) {
//            throw new RuntimeException("Can not acquire IMU lock");
//        } finally {
//            mLock.release();
//        }

        linearAccelerations.clear();
        gyroscopes.clear();
        rotationVector.values = null;
        rotationVector.timestamp = -1;
        gravity.values = null;
        gravity.timestamp = -1;
        try {
            mLock.acquire();

            List<ValueTimePair> allBeforeRotationVector = getAllBefore(mRotationVectors, time);
            for (int i = 0; i + 1 < allBeforeRotationVector.size(); ++i)
                mRotationVectors.remove(0);

            if (allBeforeRotationVector.size() == 0) {
                mLock.release();
                return;
            }

            ValueTimePair last = allBeforeRotationVector.get(allBeforeRotationVector.size() - 1);
            rotationVector.values = last.values;
            rotationVector.timestamp = last.timestamp;

            List<ValueTimePair> allBeforeGravity = getAllBefore(mGravitys, time);
            for (int i = 0; i + 1 < allBeforeGravity.size(); ++i)
                mGravitys.remove(0);

            if (allBeforeGravity.size() == 0) {
                mLock.release();
                return;
            }

            last = allBeforeGravity.get(allBeforeGravity.size() - 1);
            gravity.values = last.values;
            gravity.timestamp = last.timestamp;

            linearAccelerations.addAll(getAndRemoveAllBeforeButLast(mLinearAccelerations, time));
            gyroscopes.addAll(getAndRemoveAllBeforeButLast(mGyroscopes, time));

        } catch (InterruptedException e) {
            throw new RuntimeException("Can not acquire IMU lock");
        } finally {
            mLock.release();
        }
        return;
    }

    /**
     * Align linear accelerations and gyroscopes such that the the two list have
     * same length, and the timestamp at same index will be identical
     */
    public void mergeIMUs(List<ValueTimePair> linearAccelerations, List<ValueTimePair> gyroscopes) {
        if (!hasGyroscopeSensor) {
            gyroscopes.clear();
            linearAccelerations.clear();
            return;
        }

        int diff = linearAccelerations.size() - gyroscopes.size();
        // Log.d(LOG_TAG, "linearAccelerations.size() - gyroscopes.size(): " + diff);
        if (linearAccelerations.isEmpty()) {
            gyroscopes.clear();
            return;
        }

        if (gyroscopes.isEmpty()) {
            linearAccelerations.clear();
            return;
        }
        boolean merge_type = true;
        // reset IMU data, acc use origin timestamp, gyr use interpolation timestamp
        if (merge_type) {
            List<ValueTimePair> new_gyrs = new ArrayList();
            List<ValueTimePair> new_accs = new ArrayList();
            int id_gyr = 0;
            for (int id_acc = 0; id_acc < linearAccelerations.size(); ) {
                ValueTimePair acc = linearAccelerations.get(id_acc);
                ValueTimePair pre_gyr = null, cur_gyr = null;
                if (id_gyr + 1 < gyroscopes.size()) {
                    pre_gyr = gyroscopes.get(id_gyr);
                    cur_gyr = gyroscopes.get(id_gyr + 1);
                }
                if (pre_gyr != null && cur_gyr != null && acc.timestamp >= pre_gyr.timestamp && acc.timestamp < cur_gyr.timestamp) {
                    float weight = (float) (acc.timestamp - pre_gyr.timestamp) / (float) (cur_gyr.timestamp - pre_gyr.timestamp);
                    ValueTimePair new_gyr = new ValueTimePair(cur_gyr.values, cur_gyr.timestamp);
                    for (int i = 0; i < 3; ++i) {
                        new_gyr.values[i] = pre_gyr.values[i] + weight * (cur_gyr.values[i] - pre_gyr.values[i]);
                    }
                    new_gyr.timestamp = acc.timestamp;
                    new_gyrs.add(new_gyr);
                    new_accs.add(acc);
                    ++id_acc;
                    ++id_gyr;
                } else if (pre_gyr != null && cur_gyr != null && acc.timestamp >= cur_gyr.timestamp) {
                    gyroscopes.remove(id_gyr);
                } else {
                    linearAccelerations.remove(id_acc);
                }
            }
            linearAccelerations.clear();
            linearAccelerations.addAll(new_accs);
            gyroscopes.clear();
            gyroscopes.addAll(new_gyrs);
            for (int i = 0; i < linearAccelerations.size(); ++i) {
                ValueTimePair vp = linearAccelerations.get(i);
                //Log.d("IMUReader", "acc timestamp " + vp.timestamp);
            }
            for (int i = 0; i < gyroscopes.size(); ++i) {
                ValueTimePair vp = gyroscopes.get(i);
                //Log.d("IMUReader", "gyr timestamp " + vp.timestamp);
            }

        } else {

            if (diff > 0) // this should be rare
            {
                // drop head or tail ?
                long headDiff = linearAccelerations.get(diff - 1).timestamp - gyroscopes.get(0).timestamp;
                long tailDiff = linearAccelerations.get(linearAccelerations.size() - 1 - diff).timestamp - gyroscopes.get(gyroscopes.size() - 1).timestamp;

                if (Math.abs(headDiff) > Math.abs(tailDiff))
                    for (int i = 0; i < diff; ++i)
                        linearAccelerations.remove(0);
                else
                    for (int i = 0; i < diff; ++i)
                        linearAccelerations.remove(linearAccelerations.size() - 1);
            } else if (diff < 0) {
                diff = -diff;
                // drop head or tail ?
                long headDiff = gyroscopes.get(diff - 1).timestamp - linearAccelerations.get(0).timestamp;
                long tailDiff = gyroscopes.get(gyroscopes.size() - 1 - diff).timestamp - linearAccelerations.get(linearAccelerations.size() - 1).timestamp;

                if (Math.abs(headDiff) > Math.abs(tailDiff))
                    for (int i = 0; i < diff; ++i)
                        gyroscopes.remove(0);
                else
                    for (int i = 0; i < diff; ++i)
                        gyroscopes.remove(linearAccelerations.size() - 1);
            }

            for (int idx = 0; idx < linearAccelerations.size(); ++idx)
                linearAccelerations.get(idx).timestamp = gyroscopes.get(idx).timestamp =
                        (linearAccelerations.get(idx).timestamp + gyroscopes.get(idx).timestamp) / 2;
        }

//        int diff = linearAccelerations.size() - gyroscopes.size();
//        // Log.d(LOG_TAG, "linearAccelerations.size() - gyroscopes.size(): " + diff);
//        if (linearAccelerations.isEmpty()) {
//            gyroscopes.clear();
//            return;
//        }
//
//        if (gyroscopes.isEmpty()) {
//            linearAccelerations.clear();
//            return;
//        }
//
//        // this should be rare
//        if (diff > 0) {
//            // drop head or tail ?
//            long headDiff = linearAccelerations.get(diff - 1).timestamp - gyroscopes.get(0).timestamp;
//            long tailDiff = linearAccelerations.get(linearAccelerations.size() - 1 - diff).timestamp - gyroscopes.get(gyroscopes.size() - 1).timestamp;
//
//            if (Math.abs(headDiff) > Math.abs(tailDiff))
//                for (int i = 0; i < diff; ++i)
//                    linearAccelerations.remove(0);
//            else
//                for (int i = 0; i < diff; ++i)
//                    linearAccelerations.remove(linearAccelerations.size() - 1);
//        } else if (diff < 0) {
//            diff = -diff;
//            // drop head or tail ?
//            long headDiff = gyroscopes.get(diff - 1).timestamp - linearAccelerations.get(0).timestamp;
//            long tailDiff = gyroscopes.get(gyroscopes.size() - 1 - diff).timestamp - linearAccelerations.get(linearAccelerations.size() - 1).timestamp;
//
//            if (Math.abs(headDiff) > Math.abs(tailDiff))
//                for (int i = 0; i < diff; ++i)
//                    gyroscopes.remove(0);
//            else
//                for (int i = 0; i < diff; ++i)
//                    gyroscopes.remove(linearAccelerations.size() - 1);
//        }
//
//        for (int idx = 0; idx < linearAccelerations.size(); ++idx)
//            linearAccelerations.get(idx).timestamp = gyroscopes.get(idx).timestamp =
//                    (linearAccelerations.get(idx).timestamp + gyroscopes.get(idx).timestamp) / 2;
    }

    private List<ValueTimePair> getAndRemoveAllBefore(List<ValueTimePair> lst, long time) {
        List<ValueTimePair> ret = new ArrayList<>();
        while (!lst.isEmpty() && lst.get(0).timestamp <= time) {
            ret.add(lst.get(0));
            lst.remove(0);
        }

        return ret;
    }

    private static List<ValueTimePair> getAndRemoveAllBeforeButLast(List<ValueTimePair> lst, long time) {
        List<ValueTimePair> ret = new ArrayList<>();
        while (!lst.isEmpty() && lst.get(0).timestamp <= time) {
            ret.add(lst.get(0));
            if (lst.size() > 1 && lst.get(1).timestamp <= time)
                lst.remove(0);
            else break;
        }
        return ret;
    }

    private List<ValueTimePair> getAllBefore(List<ValueTimePair> lst, long time) {
        List<ValueTimePair> ret = new ArrayList<>();
        for (int i = 0; i < lst.size() && lst.get(i).timestamp <= time; ++i)
            ret.add(lst.get(i));

        return ret;
    }
}
