package com.sensetime.slam.sample;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

import com.sensetime.slam.sample.util.CameraSource;

import static java.lang.Math.abs;

public class MyGLSurfaceView extends GLSurfaceView {


    private MyGLRenderer mRenderer;
    private CameraSource mCameraSource;

    private long mStartEventTime = 0;
    private long mEndEventTime = 0;
    private long mThreshold = 500; //500ms
    private boolean mFirstLoc = true;

    float lastX = 0.0f;
    float lastY = 0.0f;

    public MyGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        try {
            // Create GLES context. Even though we are specifying OpenGL ES 2, it will try to
            // create the highest possible context on DirectDrawer phone
            setEGLContextClientVersion(2);
            setPreserveEGLContextOnPause(true);

            // set our custom Renderer for drawing on the created SurfaceView
            mRenderer = new MyGLRenderer(this, getResources(), (Activity) context);
            setRenderer(mRenderer);

            // calls onDrawFrame(...) continuously
            setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

            mCameraSource = new CameraSource((Activity) context, mRenderer);
            mRenderer.setCameraSource(mCameraSource);

        } catch (Exception e) {
            // Trouble, something's wrong!
            Log.e("MyGLSurfaceView", "Unable to create GLES context!", e);

        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        super.surfaceCreated(holder);
        mCameraSource.openCamera(CameraSource.CAMERA_DIRECTION_BACK);
    }

    public void setPreviewFragment(PreviewFragment fragment) {
        mRenderer.setPreviewFragment(fragment);
    }

    public void openCamera() {
        if (!mCameraSource.isOpen())
            mCameraSource.openCamera(CameraSource.CAMERA_DIRECTION_BACK);
    }



    public void releaseHandle() {
        mRenderer.releaseHandle();
    }

    public int addObject(String modelPath) {
        return mRenderer.addObject(modelPath);
    }

    public void deleteObject(int id) {
        mRenderer.deleteObject(id);
    }

    public void deleteAllObjects() {
        mRenderer.deleteAllObjects();
    }

    public void stopSLAM() {
        mRenderer.stopSLAM();
    }

    public boolean isSLAMStarted() {
        return mRenderer.isSLAMRunning();
    }

    public void resetSLAM() {
        mRenderer.resetSLAM();
    }

    public void sendSavePictureCommand() {
        mRenderer.sendSavePictureCommand();
    }

    /**
     * 处理屏幕触摸事件
     * @param e
     * @return
     */
    public boolean onTouchEvent(MotionEvent e) {
        float width = getWidth();
        float height = getHeight();
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStartEventTime = System.currentTimeMillis();
                lastX = e.getX();
                lastY = e.getY();
                Log.d("MotionEvent", "ACTION_DOWN X:" + e.getX() + ";Y:"+e.getY());
                if(mFirstLoc){
                    mRenderer.touchDown(e.getX(), e.getY(), width, height);
                    mFirstLoc = false;
                } else {
                    mRenderer.touchMove(e.getX(), e.getY(), width, height);
                }

                break;
            case MotionEvent.ACTION_MOVE:
                mEndEventTime = System.currentTimeMillis();
                if(mEndEventTime - mStartEventTime >= mThreshold) {
                    Log.d("MotionEvent", "ACTION_MOVE X:" + e.getX() + ";Y:"+e.getY());
                    mRenderer.touchMove(e.getX(), e.getY(), width, height);
                    lastX = e.getX();
                    lastY = e.getY();
                }
                break;
            default:
                break;
        }

        return true;
    }

    /**
     * 设置是否显示坐标、corner等辅助信息
     * @param type 辅助信息类型
     * @param on 是否显示
     */
    public void setAuxDisplay(int type, boolean on) {
        mRenderer.setAuxDisplay(type, on);
    }

    /**
     * 设置是否显示检测到的平面
     * @param on 是否显示
     */
    public void setPlaneDisplay(boolean on) {
        mRenderer.setPlaneDisplay(on);
    }

    /**
     * 切换分辨率
     * @param bigSize 是否为高分辨率
     */
    public void changeToBigPreviewSize(final boolean bigSize) {
        mCameraSource.stopPreview();
        mRenderer.releaseHandle();

        this.queueEvent(new Runnable() {
            @Override
            public void run() {
                int w, h;
                if (!bigSize) {
                    w = 640;
                    h = 480;
                } else {
                    w = 1280;
                    h = 720;
                }
                mRenderer.setPreviewSize(w, h);
                mCameraSource.setPreviewSize(w, h);
                mRenderer.setupContext();
            }
        });
    }

    /**
     * 切换检测模式
     * @param enableMultiPlane 是否启动多平面检测，true为启动多平面检测，false为启动单平面检测
     */
    public void changeDetectMode(final boolean enableMultiPlane) {
        mRenderer.changeDetectMode(enableMultiPlane);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        super.surfaceDestroyed(holder);
        mCameraSource.stopPreview();
        mCameraSource.closeCamera();
    }

    public void setFirstLoc(boolean mFirstLoc) {
        this.mFirstLoc = mFirstLoc;
    }

}
