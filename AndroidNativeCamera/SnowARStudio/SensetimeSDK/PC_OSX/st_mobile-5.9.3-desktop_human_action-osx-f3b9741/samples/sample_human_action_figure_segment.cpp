#include <vector>
#include <stdio.h>
#include <st_mobile_human_action.h>
#include "helper_human_action.h"
#include <opencv2/opencv.hpp>
#include "helper.h"
using namespace std;
using namespace cv;

st_handle_t g_handle_action = NULL;
int g_blur_strength = 35;
int g_edge_width = 3;
void on_track_segblur(int, void*) {
	st_mobile_human_action_setparam(g_handle_action, ST_HUMAN_ACTION_PARAM_BACKGROUND_BLUR_STRENGTH, g_blur_strength / 100.0f);
}

int main(int argc, char *argv[]) {
	if (check_license() < 0)
		return -1;
	print_help(argv);
	if (!ParseInput(argc, argv))
		return -1;

	Mat read_frame;
	VideoCapture capture;
	VideoWriter writer;
	unsigned int create_config = ST_MOBILE_ENABLE_SEGMENT_DETECT;
	if (!OpenInputFile(capture, writer, read_frame, create_config))
		return -1;
	std::string ModelPath = g_ModelDir + segment_model;

	Mat temp_frame;
	std::string wnd_name = "human-action-segment";
	namedWindow(wnd_name, CV_WINDOW_AUTOSIZE);
	WIN("blur_strength", g_blur_strength, on_track_segblur);
	createTrackbar("edge_width", wnd_name, &g_edge_width, 5, NULL);
	// init handle
	st_result_t ret = ST_OK;

	ret = st_mobile_human_action_create(ModelPath.c_str(), create_config, &g_handle_action);
	if (ret != ST_OK) {
		fprintf(stderr, "fail to init handle %d\n", ret);
		return -1;
	}
	st_mobile_human_action_setparam(g_handle_action, ST_HUMAN_ACTION_PARAM_BACKGROUND_RESULT_ROTATE, 1);
	st_mobile_human_action_setparam(g_handle_action, ST_HUMAN_ACTION_PARAM_BACKGROUND_MAX_SIZE,
		std::max(read_frame.cols, read_frame.rows));
	while (true) {
		if (!g_PictureMode && !capture.read(read_frame))
			break;

		if (g_bMirror)
			flip(read_frame, temp_frame, 1);
		else
			read_frame.copyTo(temp_frame);

		st_mobile_human_action_t result = { 0 };
		__TIC__();
		ret = st_mobile_human_action_detect(g_handle_action, read_frame.data,
			ST_PIX_FMT_BGR888, read_frame.cols, read_frame.rows,
			read_frame.cols * 3, ST_CLOCKWISE_ROTATE_0,
			ST_MOBILE_SEG_BACKGROUND, &result);
		__TOC__();
		if (g_bMirror)
			st_mobile_human_action_mirror(read_frame.cols, &result);

	//	show_segment_result(result.p_background, temp_frame);
		draw_human_edge(temp_frame, result.p_background, g_edge_width);
		cv::imshow(wnd_name, temp_frame);
		if (g_PictureMode) {
			if (!g_OutputFile.empty())
				cv::imwrite(g_OutputFile, temp_frame);
			waitKey(0);
			break;
		}
		else {
			if (!g_OutputFile.empty())
				writer << temp_frame;
			char c = (char)waitKey(10);
			if (c == 27 || c == 'q')
				break;
		}
	}
	// destroy handle
	st_mobile_human_action_destroy(g_handle_action);
}
