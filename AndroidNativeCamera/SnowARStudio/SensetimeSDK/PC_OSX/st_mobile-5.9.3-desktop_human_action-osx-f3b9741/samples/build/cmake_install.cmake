# Install script for directory: /Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_face_attribute")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_face_attribute")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_face_attribute" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_face_attribute")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_face_attribute")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_face_attribute")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_face_attribute")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_body_keypoints")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_body_keypoints")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_body_keypoints" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_body_keypoints")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_body_keypoints")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_body_keypoints")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_body_keypoints")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_center")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_eyeball_center")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_center" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_center")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_center")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_center")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_center")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_contour")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_eyeball_contour")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_contour" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_contour")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_contour")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_contour")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_eyeball_contour")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_106")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_face_track_106")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_106" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_106")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_106")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_106")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_106")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_240")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_face_track_240")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_240" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_240")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_240")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_240")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_face_track_240")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_figure_segment")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_figure_segment")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_figure_segment" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_figure_segment")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_figure_segment")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_figure_segment")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_figure_segment")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_hair_segment")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_hair_segment")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_hair_segment" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_hair_segment")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_hair_segment")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_hair_segment")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_hair_segment")
    endif()
  endif()
endif()

if("${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified" OR NOT CMAKE_INSTALL_COMPONENT)
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_track_hand")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin" TYPE EXECUTABLE FILES "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/test_sample_human_action_track_hand")
  if(EXISTS "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_track_hand" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_track_hand")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../libs/osx-x86_64"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_track_hand")
    execute_process(COMMAND /usr/bin/install_name_tool
      -delete_rpath "/usr/local/lib"
      "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_track_hand")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/strip" "$ENV{DESTDIR}/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/../bin/test_sample_human_action_track_hand")
    endif()
  endif()
endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/Users/cloud/0001_업체 자료/센스타임/20180820_Desktop/SenseME_v5.9.3(HumanAction)_Desktop_Beta_ snow_ snow_20180917/st_mobile-5.9.3-desktop_human_action-osx-f3b9741/samples/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
