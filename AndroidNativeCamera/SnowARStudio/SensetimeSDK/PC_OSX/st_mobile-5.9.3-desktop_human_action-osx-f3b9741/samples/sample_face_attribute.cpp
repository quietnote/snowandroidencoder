#include <vector>
#include <stdio.h>
#include <st_mobile_human_action.h>
#include <st_mobile_face_attribute.h>
#include <opencv2/opencv.hpp>
#include "helper.h"
using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {
	if (check_license() < 0)
		return -1;
	//fprintf(stderr, "USage: %s model_detection model_attribute\n", argv[0]);
	const char *model_name_detect = argv[1];
	const char *model_name_attr = argv[2];
	if (argc > 2) {
		model_name_detect = argv[1];
		model_name_attr = argv[2];
	} else {
		model_name_detect = "../models/M_SenseME_Face_Video_5.3.3.model";
		model_name_attr = "../models/face_attribute_2.2.0.model";
	}
	VideoCapture capture;
	capture.open(0);         // open the camera
	if (!capture.isOpened()) {
		fprintf(stderr, "can not open camera!\n");
		return -1;
	}
	capture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);

	st_handle_t handle_track = NULL;
	st_handle_t handle_attr = NULL;
	st_mobile_human_action_t result = { 0 };
	//st_mobile_attributes_t* p_attr = NULL;
	float* p_attr = NULL;
	Mat read_frame;
	const char* attr_name[] = { "age", "male", "attractive", "eyeglass", "sunglass", "smile",
		"mask", "eye_open", "mouth_open", "beard", "race_yellow", "race_black", "race_white",
		"angry", "calm", "confused", "disgust", "happy", "sad", "scared", "surprised",
		"squint", "scream" };
	// init handle
	st_result_t ret = ST_OK;
	ret = st_mobile_human_action_create(model_name_detect,
		ST_MOBILE_DETECT_MODE_VIDEO| ST_MOBILE_FACE_DETECT, &handle_track);
	if (ret != ST_OK) {
		fprintf(stderr, "fail to init detect handle: %d\n", ret);
		goto RETURN;
	}
	ret = st_mobile_face_attribute_create(model_name_attr, &handle_attr);
	if (ret != ST_OK) {
		fprintf(stderr, "fail to init attribute handle %d \n", ret);
		goto RETURN;
	}
	while (capture.read(read_frame)) {
		// realtime detect
		ret = st_mobile_human_action_detect(handle_track,
				read_frame.data, ST_PIX_FMT_BGR888, read_frame.cols, read_frame.rows,
				read_frame.step, ST_CLOCKWISE_ROTATE_0,
				ST_MOBILE_FACE_DETECT, &result);
		if (ret != ST_OK) {
			fprintf(stderr, "fail to track\n");
			goto RETURN;
		}

		__TIC__();
//		ret = st_mobile_face_attribute_detect(handle_attr, read_frame.data,
//						      ST_PIX_FMT_BGR888, read_frame.cols, read_frame.rows, read_frame.step,
//						      p_face, face_count, &p_attr);
		ret = st_mobile_face_attribute_detect_ext2(handle_attr, read_frame.data,
						      ST_PIX_FMT_BGR888, read_frame.cols, read_frame.rows, read_frame.step,
						      result.p_faces, result.face_count, &p_attr);
		__TOC__();
		if (ret != ST_OK) {
			fprintf(stderr, "fail to attribute detect\n");
			goto RETURN;
		}

		for (int i = 0; i < result.face_count; i++) {
			st_mobile_face_t * p_face = result.p_faces;
			st_mobile_106_t & face = p_face[i].face106;
			rectangle(read_frame,
				Point2f(static_cast<float>(face.rect.left),
						static_cast<float>(face.rect.top)),
				Point2f(static_cast<float>(face.rect.right),
						static_cast<float>(face.rect.bottom)),
				CV_RGB(255, 0, 0), 2);
			fprintf(stderr, "\nface: %d---score:%.4f---[%d, %d, %d, %d]",
				face.ID,face.score,
				face.rect.left,face.rect.top,face.rect.right, face.rect.bottom);
			/*for (int j = 0; j < p_attr->attribute_count; j++) {
				char s[64] = "\0";
				sprintf(s, "%s: %s, %.4f", p_attr[i].attributes[j].category,
					p_attr[i].attributes[j].label,
					p_attr[i].attributes[j].score);
				putText(read_frame, s, Point(i * 210 + 10, j * 20 + 20),
					FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(0, 255, 0));
				fprintf(stderr, ", %s", s);
			}*/
			fprintf(stderr, "attr: ");
			for (int j = 0; j <= ST_ATTR_EMOTION_SCREAM; j++) {
				fprintf(stderr, "%s: %.2f ", attr_name[j], p_attr[i* ST_ATTR_LENGTH + j]);
			}
			fprintf(stderr, "\n");
		}

		cv::imshow("FaceAttribute", read_frame);
		if (waitKey(1) != -1)
			break;
	}

RETURN:
	// destroy detect handle
	st_mobile_human_action_destroy(handle_track);
	st_mobile_face_attribute_destroy(handle_attr);
}
