#include <vector>
#include <stdio.h>
#include <st_mobile_human_action.h>
#include <opencv2/opencv.hpp>
#include "helper.h"
#include "helper_human_action.h"

using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {
	st_handle_t handle_action;
	if (check_license() < 0)
		return -1;
	print_help(argv);
	if (!ParseInput(argc, argv))
		return -1;

	std::string ModelPath = g_ModelDir + face_model;
	std::string ModelEyeBallPath = g_ModelDir + eyeball_model;
	Mat read_frame;
	VideoCapture capture;
	VideoWriter writer;
	unsigned int create_config = ST_MOBILE_ENABLE_FACE_DETECT;
	if (!OpenInputFile(capture, writer, read_frame, create_config))
		return -1;

	st_mobile_human_action_t result = { 0 };
	Mat temp_frame;
	std::string wnd_name = "human-action-eyeball";
	namedWindow(wnd_name, CV_WINDOW_AUTOSIZE);

	// init handle
	st_result_t ret = ST_OK;
	ret = st_mobile_human_action_create(ModelPath.c_str(), create_config, &handle_action);
	if (ret != ST_OK) {
		fprintf(stderr, "fail to init action handle %d, %s\n", ret, ModelPath.c_str());
		return -1;
	}
	ret = st_mobile_human_action_add_sub_model(handle_action, ModelEyeBallPath.c_str());
	if (ret != ST_OK) {
		fprintf(stderr, "fail to add eyeball model %d, %s\n", ret, ModelEyeBallPath.c_str());
		return -1;
	}

	while (true) {
		if (!g_PictureMode && !capture.read(read_frame))
			break;
		if (g_bMirror)
			flip(read_frame, temp_frame, 1);
		else
			read_frame.copyTo(temp_frame);

		__TIC__();
		ret = st_mobile_human_action_detect(handle_action, read_frame.data,
			ST_PIX_FMT_BGR888, read_frame.cols, read_frame.rows,
			read_frame.cols * 3, ST_CLOCKWISE_ROTATE_0,
			ST_MOBILE_FACE_DETECT_FULL | ST_MOBILE_DETECT_EYEBALL_CENTER,
			&result);
		__TOC__();
		if (g_bMirror)
			st_mobile_human_action_mirror(read_frame.cols, &result);

		show_face_result(result.p_faces, result.face_count, temp_frame);
		cv::imshow(wnd_name, temp_frame);
		if (g_PictureMode) {
			if (!g_OutputFile.empty())
				cv::imwrite(g_OutputFile, temp_frame);
			waitKey(0);
			break;
		}
		else {
			if (!g_OutputFile.empty())
				writer << temp_frame;
			char c = (char)waitKey(10);
			if (c == 27 || c == 'q')
				break;
		}
	}
	// destroy handle
	st_mobile_human_action_destroy(handle_action);
}
