#include <vector>
#include <stdio.h>
#include <st_mobile_human_action.h>
#include <opencv2/opencv.hpp>
#include "helper.h"
#include "helper_human_action.h"

using namespace std;
using namespace cv;

int main(int argc, char *argv[]) {
	st_handle_t g_handle_action;
	if (check_license() < 0)
		return -1;
	print_help(argv);
	if (!ParseInput(argc, argv))
		return -1;

	std::string ModelPath = g_ModelDir + hand_model;
	Mat read_frame;
	VideoCapture capture;
	VideoWriter writer;
	unsigned int create_config = ST_MOBILE_ENABLE_HAND_DETECT;
	if (!OpenInputFile(capture, writer, read_frame, create_config))
		return -1;

	st_mobile_human_action_t result = { 0 };
	Mat temp_frame;
	std::string wnd_name = "human-action-hand";
	namedWindow(wnd_name, CV_WINDOW_AUTOSIZE);

	// init handle
	st_result_t ret = ST_OK;
	ret = st_mobile_human_action_create(ModelPath.c_str(), create_config, &g_handle_action);
	if (ret != ST_OK) {
		fprintf(stderr, "fail to init action handle %d, %s\n", ret, ModelPath.c_str());
		return -1;
	}

	while (true) {
		if (!g_PictureMode && !capture.read(read_frame))
			break;
		if (g_bMirror)
			flip(read_frame, temp_frame, 1);
		else
			read_frame.copyTo(temp_frame);

		st_rotate_type orientation = ST_CLOCKWISE_ROTATE_0;
		__TIC__();
		ret = st_mobile_human_action_detect(g_handle_action, read_frame.data,
			ST_PIX_FMT_BGR888, read_frame.cols, read_frame.rows,
			read_frame.cols * 3, orientation, ST_MOBILE_HAND_DETECT_FULL, &result);
		__TOC__();
		if (g_bMirror)
			st_mobile_human_action_mirror(read_frame.cols, &result);

		//	get_expression(result, orientation, temp_frame);

		show_hand_result(result.p_hands, result.hand_count, temp_frame);
		cv::imshow(wnd_name, temp_frame);
		if (g_PictureMode) {
			if(!g_OutputFile.empty())
				cv::imwrite(g_OutputFile, temp_frame);
			waitKey(0);
			break;
		}
		else {
			if (!g_OutputFile.empty())
				writer << temp_frame;
			char c = (char)waitKey(10);
			if (c == 27 || c == 'q')
				break;
		}
	}
	// destroy handle
	st_mobile_human_action_destroy(g_handle_action);
}
