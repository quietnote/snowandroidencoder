#ifndef INCLUDE_HELPER_HUMAN_ACTION_H_
#define INCLUDE_HELPER_HUMAN_ACTION_H_
#include "helper.h"
#include <st_mobile_human_action.h>
#include <opencv2/opencv.hpp>
#include <map>
#include <string>
#define DUBUG_TIME 1
#define DEBUG_LOG 1

static void print_help(char** argv) {
	fprintf(stdout, "Usage: %s [options]\n", argv[0]);
	fprintf(stdout, "Specific options :\n");
	fprintf(stdout, "    -p                   set Picture mode on, Video Mode is defaultly\n");
	fprintf(stdout, "    -b                   set show mirror show on, nomirror is defaultly\n");
	fprintf(stdout, "    -md ModelDirectory   set model directory, \"../models/\" is defaultly\n");
	fprintf(stdout, "    -i InputFileName     set input file name, null is defaultly(process camera)\n");
	fprintf(stdout, "    -o OutputFileName    set out file name, null is defaultly(no save)\n");
	fprintf(stdout, "    -h,--help            show this message\n");
	fprintf(stdout, "samples videomode: %s -i 1.mp4 -o out.mp4\n", argv[0]);
	fprintf(stdout, "samples picturemode: %s -p -i 1.jpg -o out.jpg\n", argv[0]);
}
static std::string g_ModelDir = "../models/";
static bool g_bMirror = false;
static bool g_PictureMode = false;
static std::string g_InputFile;
static std::string g_OutputFile;

static bool ParseInput(int argc, char** argv) {
	if (find_argument(argc, argv, "-p") > 0)
		g_PictureMode = true;
	if (find_argument(argc, argv, "-b") > 0)
		g_bMirror = true;

	int fileIndex = find_argument(argc, argv, "-md");
	if (fileIndex > 0) {
		g_ModelDir = argv[fileIndex + 1] + std::string("/");
	}
	else {
		g_ModelDir = "../models/";
	}

	fileIndex = find_argument(argc, argv, "-i");
	if (fileIndex > 0)
		g_InputFile = argv[fileIndex + 1];

	if (g_PictureMode && g_InputFile.empty()) {
		fprintf(stderr, "input image is needed on PictureMode\n");
		return false;
	}

	fileIndex = find_argument(argc, argv, "-o");
	if (fileIndex > 0)
		g_OutputFile = argv[fileIndex + 1];
	return true;
}

static bool OpenInputFile(cv::VideoCapture &capture, cv::VideoWriter& writer, cv::Mat& read_frame, unsigned int& create_config) {
	if (!g_PictureMode) {
		create_config |= ST_MOBILE_HUMAN_ACTION_DEFAULT_CONFIG_VIDEO;
		if (!g_InputFile.empty()) {
			capture.open(g_InputFile);
		}
		else {
			capture.open(0);         // open the camera
			capture.set(CV_CAP_PROP_FRAME_WIDTH, 640);
			capture.set(CV_CAP_PROP_FRAME_HEIGHT, 480);
		}
		if (!capture.isOpened()) {
			fprintf(stderr, "can not open %s!\n", g_InputFile.empty() ? "camera" : g_InputFile.c_str());
			return -1;
		}
		if (!capture.read(read_frame)) {
			fprintf(stderr, "invalid input video %s\n", g_InputFile.empty() ? "camera" : g_InputFile.c_str());
			return -1;
		}
		if (!g_OutputFile.empty()) {
			int w = capture.get(CV_CAP_PROP_FRAME_WIDTH);
			int h = capture.get(CV_CAP_PROP_FRAME_HEIGHT);
			int fps = capture.get(CV_CAP_PROP_FPS);
			if (fps == 0)
				fps = 25;
			writer = cv::VideoWriter(g_OutputFile, CV_FOURCC('M', 'J', 'P', 'G'), fps, cv::Size(w, h));
			if (!writer.isOpened())
				fprintf(stderr, "invalid output video name %s\n", g_OutputFile.c_str());
		}
	}
	else {
		create_config |= ST_MOBILE_HUMAN_ACTION_DEFAULT_CONFIG_IMAGE;
		read_frame = cv::imread(g_InputFile);
		if (read_frame.cols == 0) {
			fprintf(stderr, "invalid input image %s\n", g_InputFile.c_str());
			return false;
		}
	}
	return true;
}

static void show_face_result(st_mobile_face_t *p_faces, int face_count, cv::Mat &img) {
	// draw face
	for (int i = 0; i < face_count; i++) {
		st_mobile_106_t face = p_faces[i].face106;

		rectangle(img, cv::Point2f(static_cast<float>(face.rect.left),
			static_cast<float>(face.rect.top)),
			cv::Point2f(static_cast<float>(face.rect.right),
			static_cast<float>(face.rect.bottom)), cv::Scalar(255, 0, 0), 2);
		for (int j = 0; j < 106; j++) {
			cv::circle(img, cv::Point2f(face.points_array[j].x,
				face.points_array[j].y), 1, cv::Scalar(0, 255, 0));
		}
		if (p_faces[i].p_extra_face_points) {
			for (int j = 0; j < p_faces[i].extra_face_points_count; j++) {
				cv::circle(img, cv::Point2f(p_faces[i].p_extra_face_points[j].x, p_faces[i].p_extra_face_points[j].y), 1, cv::Scalar(255, 0, 0));
			}
		}
		if (p_faces[i].p_eyeball_center) {
			for (int j = 0; j < p_faces[i].eyeball_center_points_count; j++) {
				cv::circle(img, cv::Point2f(p_faces[i].p_eyeball_center[j].x, p_faces[i].p_eyeball_center[j].y), 3, cv::Scalar(0, 255, 0));
			}
		}
		if (p_faces[i].p_eyeball_contour) {
			for (int j = 0; j < p_faces[i].eyeball_contour_points_count; j++) {
				cv::circle(img, cv::Point2f(p_faces[i].p_eyeball_contour[j].x, p_faces[i].p_eyeball_contour[j].y), 1, cv::Scalar(0, 255, 0));
			}
		}
	}
}

	static std::map<unsigned long long, std::string> g_HandStringMap;
static void show_hand_result(st_mobile_hand_t *p_hands, int hand_count, cv::Mat &img) {
    if(g_HandStringMap.empty()) {
		g_HandStringMap[ST_MOBILE_HAND_OK] = "ok";
		g_HandStringMap[ST_MOBILE_HAND_SCISSOR] = "scissor";
		g_HandStringMap[ST_MOBILE_HAND_GOOD] =  "good";
		g_HandStringMap[ST_MOBILE_HAND_PALM] = "palm";
		g_HandStringMap[ST_MOBILE_HAND_PISTOL] = "pistol";
		g_HandStringMap[ST_MOBILE_HAND_LOVE] = "love";
		g_HandStringMap[ST_MOBILE_HAND_HOLDUP] = "holdup";
		g_HandStringMap[ST_MOBILE_HAND_CONGRATULATE] = "congratulate";
		g_HandStringMap[ST_MOBILE_HAND_FINGER_HEART] = "figure heart";
		g_HandStringMap[ST_MOBILE_HAND_FINGER_INDEX] = "figure index";
		g_HandStringMap[ST_MOBILE_HAND_FIST] = "fist";
	}
	// draw hand
	for (int i = 0; i < hand_count; i++) {
		st_rect_t hand_rect = p_hands[i].rect;
		std::string handstr = g_HandStringMap[p_hands[i].hand_action];
		fprintf(stderr, "%d hand_rect: [%d, %d, %d, %d], hand score: %f hand_type:%s\n", i,
			hand_rect.left, hand_rect.top, hand_rect.right, hand_rect.bottom,
			p_hands[i].score, handstr.c_str());

		// draw the video
		cv::rectangle(img, cv::Point2f(static_cast<float>(hand_rect.left),
			static_cast<float>(hand_rect.top)),
			cv::Point2f(static_cast<float>(hand_rect.right),
			static_cast<float>(hand_rect.bottom)), CV_RGB(255, 0, 0), 2);

		for (int j = 0; j < p_hands[i].key_points_count; j++) {
			cv::circle(img, cv::Point2f(p_hands[i].p_key_points[j].x, p_hands[i].p_key_points[j].y),
				2, cv::Scalar(255, 0, 0), -1);
		}
		cv::putText(img, handstr, cv::Point(hand_rect.left, hand_rect.bottom),
			cv::FONT_HERSHEY_SIMPLEX, 2, CV_RGB(0, 255,0), 3, 8);
	}
}
static void show_segment_result(st_image_t *p_background, cv::Mat &img, bool bHair = false) {
	// draw segment
	if (p_background) {
		st_image_t* seg_img = p_background;
		cv::Mat mask_img = cv::Mat(seg_img->height, seg_img->width, CV_8UC1, seg_img->data);
		if (mask_img.size() != img.size()) {
			cv::resize(mask_img, mask_img, cv::Size(img.cols, img.rows));
		}
	
		unsigned char* pdata = mask_img.data;
		unsigned char* pimg = img.data;
		if (bHair) {
			imshow("mask-hair", mask_img);
			for (int i = 0; i < img.cols*img.rows; i++){
				float alpha = 1 - float(pdata[i]) / 255.0; 
				pimg[3 * i + 2] = pimg[3 * i + 2] * (1+alpha);
			}
		}
		else {
			imshow("mask-background", mask_img);
			for (int i = 0; i < img.cols*img.rows; i++){
				float alpha = 1 - float(pdata[i]) / 255.0;
				pimg[3 * i] = pimg[3 * i] * alpha;
				pimg[3 * i + 1] = pimg[3 * i + 1] * alpha;
				pimg[3 * i + 2] = pimg[3 * i + 2] * alpha;
			}
		}
	}
}

static void show_body_result(st_mobile_body_t *p_bodys, int body_count, cv::Mat &img) {
	// draw body
	if (p_bodys && body_count > 0) {
		std::vector<std::pair<int, int> > limbs;
		std::vector<std::pair<int, int> > limbs_contour;
		if (p_bodys->key_points_count > 0) {
			limbs.push_back(std::make_pair(0, 1));
			if (p_bodys->key_points_count == 14) {
				limbs.push_back(std::make_pair(2, 3));
				limbs.push_back(std::make_pair(2, 4));
				limbs.push_back(std::make_pair(4, 6));
				limbs.push_back(std::make_pair(3, 5));
				limbs.push_back(std::make_pair(5, 7));
				limbs.push_back(std::make_pair(2, 8));
				limbs.push_back(std::make_pair(3, 9));
				limbs.push_back(std::make_pair(8, 9));
				limbs.push_back(std::make_pair(8, 10));
				limbs.push_back(std::make_pair(10, 12));
				limbs.push_back(std::make_pair(9, 11));
				limbs.push_back(std::make_pair(11, 13));
			}
			else if (p_bodys->key_points_count == 4) {
				limbs.push_back(std::make_pair(1, 2));
				limbs.push_back(std::make_pair(1, 3));
			}
		}
		if (p_bodys->contour_points_count == 59) {
			for (int i = 0; i < (p_bodys->contour_points_count-1); i++) {
				limbs_contour.push_back(std::make_pair(i, i + 1));
			}
		}

		for (int i = 0; i < body_count; i++) {
			for (int j = 0; j < p_bodys[i].key_points_count; j++) {
				if (p_bodys[i].p_key_points_score[j]>0) {
					cv::circle(img, cv::Point2f(p_bodys[i].p_key_points[j].x, p_bodys[i].p_key_points[j].y),
						2, cv::Scalar(255, 0, 0), -1);
					char str[10];
					sprintf(str, "%d", j);
					cv::putText(img, str, cv::Point2f(p_bodys[i].p_key_points[j].x, p_bodys[i].p_key_points[j].y),
						0, 0.3, cv::Scalar(0, 0, 0));
				}
			}

			for (int j = 0; j < p_bodys[i].contour_points_count; j++) {
				if (p_bodys[i].p_contour_points_score[j]>0) {
					cv::circle(img, cv::Point2f(p_bodys[i].p_contour_points[j].x, p_bodys[i].p_contour_points[j].y),
						2, cv::Scalar(255, 0, 0), -1);
					char str[10];
					sprintf(str, "%d", j);
					cv::putText(img, str, cv::Point2f(p_bodys[i].p_contour_points[j].x, p_bodys[i].p_contour_points[j].y),
						0, 0.3, cv::Scalar(255, 0, 0));
				}
			}

			for (int j = 0; j < limbs.size(); j++) {
				int ia = limbs[j].first;
				int ib = limbs[j].second;
				if (p_bodys[i].p_key_points_score[ia] > 0.15 && p_bodys[i].p_key_points_score[ib] > 0.15) {
					cv::line(img, cv::Point2f(p_bodys[i].p_key_points[ia].x, p_bodys[i].p_key_points[ia].y),
						cv::Point2f(p_bodys[i].p_key_points[ib].x, p_bodys[i].p_key_points[ib].y),
						cv::Scalar(0, 0, 255), 1);
				}
			}
			for (int j = 0; j < limbs_contour.size(); j++) {
				int ia = limbs_contour[j].first;
				int ib = limbs_contour[j].second;
				if (p_bodys[i].p_contour_points_score[ia] > 0.15 && p_bodys[i].p_contour_points_score[ib] > 0.15) {
					cv::line(img, cv::Point2f(p_bodys[i].p_contour_points[ia].x, p_bodys[i].p_contour_points[ia].y),
						cv::Point2f(p_bodys[i].p_contour_points[ib].x, p_bodys[i].p_contour_points[ib].y),
						cv::Scalar(0, 255, 0), 1);
				}
			}
			int body_action = 0;
			for (int j = 1; j < 5; j++) {
				unsigned long long flag = (unsigned long long)1 << (31 + j);
				if (CHECK_FLAG(p_bodys[i].body_action, flag)) {
					body_action = j;
					break;
				}
			}
			if (body_action > 0) {
				char s[64];
				sprintf(s, "%d", body_action);
				cv::Point center(p_bodys[i].p_key_points[0].x, p_bodys[i].p_key_points[0].y);
				cv::putText(img, s, center, cv::FONT_HERSHEY_SCRIPT_SIMPLEX,
					2, CV_RGB(0, 255,0), 3, 8);
			}
		}
	}
}

static void get_expression(st_mobile_human_action_t& result, st_rotate_type orientation, cv::Mat &img) {
	std::string eye_state = "";
	std::string mouth_state = "mouth:";
	std::string head_state = "head:";
	std::string hand_state = "hand:";
	bool expression_result[ST_MOBILE_EXPRESSION_COUNT];
	st_mobile_get_expression(&result, orientation, false, expression_result);
	if (expression_result[ST_MOBILE_EXPRESSION_TWO_EYE_CLOSE])
		eye_state += "two_eye_close ";
	if (expression_result[ST_MOBILE_EXPRESSION_TWO_EYE_OPEN])
		eye_state += "two_eye_open ";
	if (expression_result[ST_MOBILE_EXPRESSION_LEFTEYE_OPEN_RIGHTEYE_CLOSE])
		eye_state += "left_eye_open,right_eye_close ";
	if (expression_result[ST_MOBILE_EXPRESSION_LEFTEYE_CLOSE_RIGHTEYE_OPEN])
		eye_state += "left_eye_close,right_eye_open ";


	if (expression_result[ST_MOBILE_EXPRESSION_MOUTH_OPEN])
		mouth_state += "open ";
	if (expression_result[ST_MOBILE_EXPRESSION_MOUTH_CLOSE])
		mouth_state += "close ";
	if (expression_result[ST_MOBILE_EXPRESSION_FACE_LIPS_UPWARD])
		mouth_state += "upward ";
	if (expression_result[ST_MOBILE_EXPRESSION_FACE_LIPS_POUTED])
		mouth_state += "pout ";
	if (expression_result[ST_MOBILE_EXPRESSION_FACE_LIPS_CURL_LEFT])
		mouth_state += "curl_left ";
	if (expression_result[ST_MOBILE_EXPRESSION_FACE_LIPS_CURL_RIGHT])
		mouth_state += "curl_right ";

	if (expression_result[ST_MOBILE_EXPRESSION_HEAD_NORMAL])
		head_state += "normal ";
	if (expression_result[ST_MOBILE_EXPRESSION_SIDE_FACE_LEFT])
		head_state += "side_left ";
	if (expression_result[ST_MOBILE_EXPRESSION_SIDE_FACE_RIGHT])
		head_state += "side_right ";
	if (expression_result[ST_MOBILE_EXPRESSION_TILTED_FACE_LEFT])
		head_state += "tiled_left ";
	if (expression_result[ST_MOBILE_EXPRESSION_TILTED_FACE_RIGHT])
		head_state += "tiled_right ";
	if (expression_result[ST_MOBILE_EXPRESSION_HEAD_RISE])
		head_state += "rise ";
	if (expression_result[ST_MOBILE_EXPRESSION_HEAD_LOWER])
		head_state += "lower ";

	if (expression_result[ST_MOBILE_EXPRESSION_HAND_OK])
		hand_state += "ok ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_SCISSOR])
		hand_state += "scissor ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_GOOD])
		hand_state += "good ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_PALM])
		hand_state += "palm ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_PISTOL])
		hand_state += "pistol ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_LOVE])
		hand_state += "love ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_HOLDUP])
		hand_state += "holdup ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_CONGRATULATE])
		hand_state += "congratulate ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_FINGER_HEART])
		hand_state += "figure_heart ";
	if (expression_result[ST_MOBILE_EXPRESSION_HAND_FINGER_INDEX])
		hand_state += "figure_index ";


	cv::putText(img, eye_state.c_str(), cv::Point2f(0, 300), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 0, 255), 2);
	cv::putText(img, mouth_state.c_str(), cv::Point2f(0, 350), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 0, 255), 2);
	cv::putText(img, head_state.c_str(), cv::Point2f(0, 400), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 0, 255), 2);
	cv::putText(img, hand_state.c_str(), cv::Point2f(0, 450), cv::FONT_HERSHEY_PLAIN, 2, cv::Scalar(0, 0, 255), 2);
}

static st_result_t draw_human_edge(cv::Mat& img, st_image_t* seg_img, int edge_width) {
	st_image_t edge_img = { 0 };
	edge_img.data = new uchar[seg_img->width * seg_img->height];
	edge_img.width = seg_img->width;
	edge_img.height = seg_img->height;
	edge_img.stride = seg_img->stride;
	st_result_t ret = st_mobile_retrieve_human_edge(seg_img, &edge_img, edge_width, true);
	if (ret != ST_OK) {
		fprintf(stderr, "st_mobile_retrieve_human_edge error : %d\n", ret);
		delete[]edge_img.data; edge_img.data = NULL;
		return ret;
	}
	cv::Mat mask_img, mask_img_edge;

	/*cv::Mat mask_img_small = cv::Mat(seg_img->height, seg_img->width, CV_8UC1, seg_img->data);
	cv::Mat mask_img_small_edge = cv::Mat(seg_img->height, seg_img->width, CV_8UC1, edge_img.data);
	cv::resize(mask_img_small, mask_img, cv::Size(img.cols, img.rows));
	cv::resize(mask_img_small_edge, mask_img_edge, cv::Size(img.cols, img.rows));*/
	mask_img = cv::Mat(seg_img->height, seg_img->width, CV_8UC1, seg_img->data);
	mask_img_edge = cv::Mat(edge_img.height, edge_img.width, CV_8UC1, edge_img.data);

	unsigned char* pbackground = mask_img.data;
	unsigned char* pedge = mask_img_edge.data;
	unsigned char* pimg = img.data;

	for (int i = 0; i < img.cols*img.rows; i++){
		if (pedge[i] >= 50) { // pink
			pimg[3 * i] = 255;
			pimg[3 * i + 1] = 0;
			pimg[3 * i + 2] = 255;
		}
		else{
			float alpha = 1 - float(pbackground[i]) / 255.0;
			pimg[3 * i] = pimg[3 * i] * alpha;
			pimg[3 * i + 1] = pimg[3 * i + 1] * alpha;
			pimg[3 * i + 2] = pimg[3 * i + 2] * alpha;
		}
	}
	if (edge_img.data && edge_img.width * edge_img.height != seg_img->width * seg_img->height) {
		delete[] edge_img.data;
		edge_img.data = NULL;
	}
	return ST_OK;
}

#endif // INCLUDE_HELPER_HUMAN_ACTION_H_
