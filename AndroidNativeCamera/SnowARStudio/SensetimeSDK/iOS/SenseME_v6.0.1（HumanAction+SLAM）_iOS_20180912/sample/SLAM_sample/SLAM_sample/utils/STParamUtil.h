//
//  STParamUtil.h
//
//  Created by HaifengMay on 16/11/5.
//  Copyright © 2016年 SenseTime. All rights reserved.
//

/*
 * function: 主要用来获取一些系统的参数，如 CPU占用率，帧率等
 */
#import <Foundation/Foundation.h>

#define SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface STParamUtil : NSObject

/*
 * 返回CPU占用率的分子（分母为100）
 */
+ (float) getCpuUsage;



/**
 获取所有滤镜模型的路径

 @return 路径数组
 */
+ (NSArray *)getFilterModelPaths;



/**
 获取所有贴纸素材包路径

 @return 路径数组
 */
+ (NSArray *)getStickerZipPaths;



/**
 获取通用物体素材路径

 @return 路径数组
 */
+ (NSArray *)getTrackerPaths;

@end
