//
//  STTableViewCell.m
//  SLAM_sample
//
//  Created by sluin on 2017/7/13.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "STTableViewCell.h"

@implementation STTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
