//
//  STTableViewCell.h
//  SLAM_sample
//
//  Created by sluin on 2017/7/13.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;


@end
