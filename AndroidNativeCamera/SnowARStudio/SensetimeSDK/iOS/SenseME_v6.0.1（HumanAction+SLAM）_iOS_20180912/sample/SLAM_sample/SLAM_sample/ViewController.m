//
//  ViewController.m
//  SLAM_sample
//
//  Created by sluin on 2017/7/10.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "ViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import "SLAMViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    ALAssetsLibrary *photoLibrary = [[ALAssetsLibrary alloc] init];
    [photoLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:nil failureBlock:nil];
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
    }];
    
    [self setupUI];
}



- (void)setupUI
{
    self.btnStart.layer.cornerRadius = 15.0f;
    self.btnStart.layer.borderWidth = 1.0f;
    self.btnStart.layer.borderColor = [[UIColor blackColor] CGColor];
    
    NSDictionary *dicBundleInfo = [[NSBundle mainBundle] infoDictionary];
    
    NSString *strAppName = [dicBundleInfo objectForKey:@"CFBundleName"];
    NSString *strVersion = [dicBundleInfo objectForKey:@"CFBundleShortVersionString"];
    
    self.lblName.text = strAppName;
    self.lblInfo.text = [NSString stringWithFormat:@"v%@\nPowered by SenseTime" , strVersion];
}


- (IBAction)onBtnStart:(id)sender {
    
    SLAMViewController *slamVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SLAMViewController"];
    
    [self.navigationController pushViewController:slamVC animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
