//
//  STPanel.h
//  SLAM_sample
//
//  Created by sluin on 2018/1/25.
//  Copyright © 2018年 SenseTime. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    
    PREVIEW_RESOLUTION_SEG = 100,
    
    DETECTION_MODEL_SEG,
    
    PLANE_SWITCH,
    
    WIDGET_SWITCH,

} STPanelValueChangedType;

@interface STPanel : UIView

@property (weak, nonatomic) IBOutlet UILabel *resolutionLabel;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *planeSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resolutionSegmentCtrlConstraint;
@property (weak, nonatomic) IBOutlet UISegmentedControl *planeSegmentCtl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *planeSegmentCtrlConstraint;

@property (nonatomic , copy) void (^valueDidChange)(UIView *sender , STPanelValueChangedType iChangedType);

@end
