//
//  STPanel.m
//  SLAM_sample
//
//  Created by sluin on 2018/1/25.
//  Copyright © 2018年 SenseTime. All rights reserved.
//

#import "STPanel.h"

@implementation STPanel

- (IBAction)onValueChanged:(id)sender {
    
    if (self.valueDidChange) {
        
        self.valueDidChange(sender , ((UIView *)sender).tag);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
