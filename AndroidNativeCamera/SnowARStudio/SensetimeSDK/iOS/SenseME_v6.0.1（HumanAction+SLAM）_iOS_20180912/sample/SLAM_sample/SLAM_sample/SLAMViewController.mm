//
//  SLAMViewController.m
//  SLAM_sample
//
//  Created by sluin on 2017/7/10.
//  Copyright © 2017年 SenseTime. All rights reserved.
//

#import "SLAMViewController.h"
#import <OpenGLES/ES2/glext.h>
#import <CoreMotion/CoreMotion.h>
#import <CommonCrypto/CommonDigest.h>

#import <vector>
#import <queue>

#import "STParamUtil.h"
#import "STMobileLog.h"

#import "STGLPreview.h"
#import "STCamera.h"

#import "STTableViewCell.h"
#import "STPanel.h"

#import "st_mobile_renderer.h"
#import "st_mobile_license.h"

#define CELL_IDENTIFIER @"identifier"

#define SENSEAR_WEAKSELF_DEFINITION __weak typeof(self) weakSelf = self;

#define STSemaphoreLock() dispatch_semaphore_wait(self->_lock, DISPATCH_TIME_FOREVER)
#define STSemaphoreUnlock() dispatch_semaphore_signal(self->_lock)

// 两种 check license 的方式 , 一种是根据 license 文件的路径 , 另一种是 license 文件的缓存选择应用场景合适的即可
#define CHECK_LICENSE_WITH_PATH 1

@interface SLAMViewController () <STCameraDelegate , UITableViewDelegate , UITableViewDataSource>
{
    dispatch_semaphore_t _lock;
    
    CVOpenGLESTextureCacheRef _cvTextureCache;
    
    CVOpenGLESTextureRef _cvTextureOrigin;
    CVOpenGLESTextureRef _cvTextureResult;
    
    GLuint _textureOrigin;
    GLuint _textureResult;
    
    CVPixelBufferRef _cvResultBuffer;
    
    std::queue<st_slam_imu> _allIMUs;
}


@property (nonatomic , strong) STCamera *stCamera;
@property (nonatomic , strong) STGLPreview *stPreview;
@property (nonatomic , strong) EAGLContext *glContext;
@property (nonatomic , assign) int iWidth;
@property (nonatomic , assign) int iHeight;

@property (nonatomic , assign) double dPreviousFrameTimestamp;

@property (nonatomic) st_handle_t hRender;
@property (nonatomic) st_handle_t hSlam;

@property (nonatomic , strong) CMMotionManager *motionManager;
@property (nonatomic , strong) NSOperationQueue *imuQueue;

@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnSnap;
@property (weak, nonatomic) IBOutlet UILabel *lblPrompt;
@property (weak, nonatomic) IBOutlet UIImageView *imageLighting;


@property (strong, nonatomic) UIButton *btnReset;
@property (strong, nonatomic) UIButton *btnList;
@property (strong, nonatomic) UIButton *btnSetting;

@property (nonatomic , assign) int iCurrentObjID;
@property (nonatomic , copy) NSString *strCurrentObjPath;
@property (nonatomic , copy) NSString *strPrepareObjPath;
@property (nonatomic , assign) BOOL needRemoveObj;

@property (nonatomic , assign) BOOL hasInitPosition;

@property (nonatomic , assign) BOOL needSnap;

@property (nonatomic , assign) STSLAMState iSlamState;

@property (nonatomic , strong) UITableView *objectList;
@property (nonatomic , strong) NSArray *arrObjPaths;

@property (nonatomic , assign) BOOL isAppActive;

@property (nonatomic , strong) STPanel *settingPanel;
@property (nonatomic , strong) UIButton *btnConfirmSetting;



@end

@implementation SLAMViewController

#pragma - mark -
#pragma - mark Life cycle

- (void)appWillResignActive
{
    self.isAppActive = NO;
}

- (void)appWillEnterForeground
{
    self.isAppActive = YES;
}

- (void)appDidBecomeActive
{
    self.isAppActive = YES;
}


- (void)dealloc
{
    NSLog(@"xxx dealloc");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil
     ];
    
    _lock = dispatch_semaphore_create(1);
    
    self.needSnap = NO;
    self.hasInitPosition = NO;
    
    self.needRemoveObj = NO;
    
    self.isAppActive = YES;
    
    self.iCurrentObjID = -1;
    self.iSlamState = ST_SLAM_TRACKING_FAIL;
    self.dPreviousFrameTimestamp = -1.0;
    self.iWidth = 720;
    self.iHeight = 1280;
    
    [self setupUI];
    [self setupCameraAndPreview];
    [self setupGestureRecognizer];
    [self setupHandle];
    [self setupSensors];
    [self startSensors];
    [self selectTheFirstObj];
    
    [self.stCamera startRunning];
}


#pragma - mark -
#pragma - mark IMU

- (void)setupSensors
{
    const double imuFPS = 100.0;
    
    //motion manager
    self.motionManager = [[CMMotionManager alloc] init];
    
    assert(   self.motionManager.isAccelerometerAvailable
           && self.motionManager.isDeviceMotionAvailable
           && self.motionManager.isGyroAvailable
           && self.motionManager.isMagnetometerAvailable
           );
    
    self.motionManager.deviceMotionUpdateInterval = 1.0 / imuFPS;
    self.motionManager.accelerometerUpdateInterval= 1.0 / imuFPS;
    self.motionManager.gyroUpdateInterval = 1.0 / imuFPS;
    
    self.imuQueue = [[NSOperationQueue alloc] init];
}

- (void)startSensors
{
    // start a new thread to read IMU data with a high fps
    [self.motionManager startDeviceMotionUpdatesToQueue:self.imuQueue withHandler:^(CMDeviceMotion *motion, NSError *error) {
        
        if (error) {
            
            NSLog(@"motionManager update failed : %@" , error.description);
            
            return;
        }
        
        st_slam_imu imu = {
            .acceleration = {motion.userAcceleration.x + motion.gravity.x , motion.userAcceleration.y + motion.gravity.y, motion.userAcceleration.z + motion.gravity.z},
            .gyroscope = {motion.rotationRate.x, motion.rotationRate.y, motion.rotationRate.z},
            .timeStamp = motion.timestamp
        };
        
        STSemaphoreLock();
        
        _allIMUs.push(imu);
        
        STSemaphoreUnlock();
    }];
}

- (void)readSensors:(std::vector<st_slam_imu>*)imus withPreviousFrameTimeStamp:(double)previousFrameTimestamp
{
    imus->clear();
    static double last_header = -1;
    
    if (last_header < 0 || _allIMUs.empty()) {
        
        last_header = previousFrameTimestamp;
        
        return;
    }
    
    STSemaphoreLock();
    
    while (!_allIMUs.empty() && _allIMUs.front().timeStamp <= last_header) {
        
        _allIMUs.pop();
    }
    
    while (!_allIMUs.empty() && _allIMUs.front().timeStamp <= previousFrameTimestamp) {
        
        const st_slam_imu& imu = _allIMUs.front();
        imus->emplace_back(imu);
        _allIMUs.pop();
    }
    
    last_header = previousFrameTimestamp;
    
    STSemaphoreUnlock();
}

#pragma - mark -
#pragma - mark Camera & Preview


- (void)setupUI
{
    CGFloat fBtnWidth = 35.0;
    
    self.btnReset = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnReset.frame = CGRectMake(0, SCREEN_HEIGHT - fBtnWidth - 15.0, fBtnWidth, fBtnWidth);
    [self.btnReset setImage:[UIImage imageNamed:@"btn_reset"] forState:UIControlStateNormal];
    [self.btnReset addTarget:self action:@selector(onBtnReset) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnReset];
    
    self.btnList = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnList.frame = CGRectMake((SCREEN_WIDTH - fBtnWidth) / 2.0, SCREEN_HEIGHT - 15.0 - fBtnWidth, fBtnWidth, fBtnWidth);
    [self.btnList setImage:[UIImage imageNamed:@"btn_list"] forState:UIControlStateNormal];
    [self.btnList addTarget:self action:@selector(onBtnList) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnList];
    
    self.btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnSetting.frame = CGRectMake(SCREEN_WIDTH - fBtnWidth - 10.0, SCREEN_HEIGHT - fBtnWidth - 15.0, fBtnWidth, fBtnWidth);
    [self.btnSetting setImage:[UIImage imageNamed:@"setting"] forState:UIControlStateNormal];
    [self.btnSetting addTarget:self action:@selector(onBtnSetting) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btnSetting];
    
    
    self.btnConfirmSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnConfirmSetting.frame = CGRectMake(20, SCREEN_HEIGHT - 50.0 - 15.0, SCREEN_WIDTH - 40.0, 50.0);
    self.btnConfirmSetting.backgroundColor = [UIColor whiteColor];
    [self.btnConfirmSetting setTitle:@"OK" forState:UIControlStateNormal];
    [self.btnConfirmSetting setTitleColor:[UIColor colorWithRed:0.0 green:122.0 / 255.0 blue:1.0 alpha:1.0f] forState:UIControlStateNormal];
    self.btnConfirmSetting.layer.cornerRadius = 5.0f;
    [self.btnConfirmSetting addTarget:self action:@selector(onBtnConfirmSetting) forControlEvents:UIControlEventTouchUpInside];
    self.btnConfirmSetting.hidden = YES;
    
    [self.view addSubview:self.btnConfirmSetting];
    
    NSArray* arrNibViews = [[NSBundle mainBundle] loadNibNamed:@"STPanel" owner:nil options:nil];
    self.settingPanel = arrNibViews.firstObject;
    self.settingPanel.frame = CGRectMake(CGRectGetMinX(self.btnConfirmSetting.frame), CGRectGetMinY(self.btnConfirmSetting.frame) - 275.0 - 15.0, CGRectGetWidth(self.btnConfirmSetting.frame), 275.0);
    
    if ([UIScreen mainScreen].bounds.size.width <= 320.0) {
        self.settingPanel.modeLabel.font = [UIFont systemFontOfSize:11.0];
        self.settingPanel.resolutionLabel.font = [UIFont systemFontOfSize:11.0];
        
        self.settingPanel.resolutionSegmentCtrlConstraint.constant = 140;
        self.settingPanel.planeSegmentCtrlConstraint.constant = 165;
    }
    
    self.settingPanel.hidden = YES;
    
    SENSEAR_WEAKSELF_DEFINITION
    
    self.settingPanel.valueDidChange = ^(UIView *sender,
                                         STPanelValueChangedType iChangedType)
    {
        switch (iChangedType) {
                
            case PREVIEW_RESOLUTION_SEG:
            {
                UISegmentedControl *seg = (UISegmentedControl *)sender;
                
                if (0 == seg.selectedSegmentIndex) {
                    
                    weakSelf.stCamera.sessionPreset = AVCaptureSessionPreset1280x720;
                }else{
                    
                    weakSelf.stCamera.sessionPreset = AVCaptureSessionPreset640x480;
                }
                
                [weakSelf resetUI];
                
                CGRect previewRect = [weakSelf.stCamera getZoomedRectWithRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) scaleToFit:NO];
                weakSelf.stPreview.frame = previewRect;
            }
                break;
                
            case DETECTION_MODEL_SEG:
            {
                UISegmentedControl *seg = (UISegmentedControl *)sender;
                int iSelecedIndex = (int)seg.selectedSegmentIndex;
                bool needOpenMultiPlannar = 1 - iSelecedIndex;
                
                st_mobile_slam_open_multi_planar_track(weakSelf.hSlam, needOpenMultiPlannar);
            }
                break;
                
            case PLANE_SWITCH:
            {
                UISwitch *planeSwitch = (UISwitch *)sender;
                bool needDisplay = planeSwitch.isOn;
                
                st_result_t iRet = st_mobile_renderer_set_aux_display(weakSelf.hRender, ST_SLAM_PLANE_MESH, needDisplay);
                
                if (ST_OK != iRet) {
                    
                    NSLog(@"st_mobile_renderer_set_aux_display failed %d" , iRet);
                }
            }
                break;
                
            case WIDGET_SWITCH:
            {
                UISwitch *planeWidget = (UISwitch *)sender;
                bool needDisplay = planeWidget.isOn;
                
                st_result_t iRet = st_mobile_renderer_set_aux_display(weakSelf.hRender, ST_SLAM_ALL, needDisplay);
                
                needDisplay = weakSelf.settingPanel.planeSwitch.isOn;
                
                iRet = st_mobile_renderer_set_aux_display(weakSelf.hRender, ST_SLAM_PLANE_MESH, needDisplay);
                
                if (ST_OK != iRet) {
                    
                    NSLog(@"st_mobile_renderer_set_aux_display failed %d" , iRet);
                }
            }
                break;
                
            default:
                break;
        }
    };
    
    [self.view addSubview:self.settingPanel];
    
    
    self.objectList = [[UITableView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH - 240.0) / 2.0, CGRectGetMinY(self.btnList.frame) - 250.0, 240.0, 240.0) style:UITableViewStylePlain];
    self.objectList.delegate = self;
    self.objectList.dataSource = self;
    self.objectList.hidden = YES;
    
    [self.view addSubview:self.objectList];
    
    [self.objectList registerNib:[UINib nibWithNibName:@"STTableViewCell" bundle:nil] forCellReuseIdentifier:CELL_IDENTIFIER];
    
    [self loadObjectsAndReloadObjList];
}

- (void)loadObjectsAndReloadObjList
{
    NSFileManager * fileManager = [[NSFileManager alloc] init];
    NSString *strBundlePath = [[NSBundle mainBundle] resourcePath];
    NSArray *arrFileNames = [fileManager contentsOfDirectoryAtPath:strBundlePath error:nil];
    
    NSMutableArray *arrObjectPaths = [NSMutableArray array];
    
    for (NSString *strFileName in arrFileNames) {
        
        if ([strFileName.lastPathComponent.pathExtension isEqualToString:@"zip"]) {
            
            [arrObjectPaths addObject:[NSString pathWithComponents:@[strBundlePath , strFileName]]];
        }
    }
    
    NSString *strDocumentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    
    arrFileNames = [fileManager contentsOfDirectoryAtPath:strDocumentsPath error:nil];
    
    for (NSString *strFileName in arrFileNames) {
        
        if ([strFileName.lastPathComponent.pathExtension isEqualToString:@"zip"]) {
            
            [arrObjectPaths addObject:[NSString pathWithComponents:@[strDocumentsPath , strFileName]]];
        }
    }
    
    self.arrObjPaths = [arrObjectPaths sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        NSString *strFileName1 = obj1;
        NSString *strFileName2 = obj2;
        
        return [strFileName1 compare:strFileName2];
    }];
    
    [self.objectList reloadData];
}

- (void)setupCameraAndPreview
{
    self.stCamera = [[STCamera alloc] init];
    self.stCamera.bOutputYUV = NO;
    self.stCamera.sessionPreset = AVCaptureSessionPreset1280x720;
    self.stCamera.devicePosition = AVCaptureDevicePositionBack;
    // 是否限制帧率? self.stCamera.iFPS = 25;
    self.stCamera.delegate = self;
    
    CGRect previewRect = [self.stCamera getZoomedRectWithRect:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT) scaleToFit:NO];
    
    self.glContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    EAGLContext *previewContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2 sharegroup:self.glContext.sharegroup];
    
    self.stPreview = [[STGLPreview alloc] initWithFrame:previewRect context:previewContext];
    
    [self.view insertSubview:self.stPreview atIndex:0];
}

- (void)setupGestureRecognizer
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPreviewTap:)];
    
    [self.stPreview addGestureRecognizer:tap];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPreviewPan:)];
    
    [self.stPreview addGestureRecognizer:pan];
}

- (void)initResultTexture
{
    // 创建结果纹理
    [self setupTextureWithPixelBuffer:&_cvResultBuffer
                                width:self.iWidth
                               height:self.iHeight
                            glTexture:&_textureResult
                            cvTexture:&_cvTextureResult];
}

- (void)releaseResultTexture
{
    _textureResult = 0;
    
    if (_cvTextureOrigin) {
        
        CFRelease(_cvTextureOrigin);
        _cvTextureOrigin = NULL;
    }
    
    CVPixelBufferRelease(_cvTextureResult);
    CVPixelBufferRelease(_cvResultBuffer);
}

- (void)releaseResources
{
    if ([EAGLContext currentContext] != self.glContext) {
        
        [EAGLContext setCurrentContext:self.glContext];
    }
    
    if (self.hRender) {
        
        st_mobile_renderer_destroy(self.hRender);
        self.hRender = NULL;
    }
    
    if (self.hSlam) {
        
        st_mobile_slam_destroy(self.hSlam);
        self.hSlam = NULL;
    }
    
    [self releaseResultTexture];
    
    if (_cvTextureCache) {
        
        CFRelease(_cvTextureCache);
        _cvTextureCache = NULL;
    }
    
    clear(_allIMUs);
    
    [EAGLContext setCurrentContext:nil];
}

void clear(std::queue<st_slam_imu>& q) {
    std::queue<st_slam_imu> empty;
    swap(empty, q);
}

#pragma - mark -
#pragma - mark Check License

//验证license
- (BOOL)checkActiveCode
{
    NSString *strLicensePath = [[NSBundle mainBundle] pathForResource:@"SENSEME" ofType:@"lic"];
    NSData *dataLicense = [NSData dataWithContentsOfFile:strLicensePath];
    
    NSString *strKeySHA1 = @"SENSEME";
    NSString *strKeyActiveCode = @"ACTIVE_CODE";
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strStoredSHA1 = [userDefaults objectForKey:strKeySHA1];
    NSString *strLicenseSHA1 = [self getSHA1StringWithData:dataLicense];
    
    st_result_t iRet = ST_OK;
    
    
    if (strStoredSHA1.length > 0 && [strLicenseSHA1 isEqualToString:strStoredSHA1]) {
        
        // Get current active code
        // In this app active code was stored in NSUserDefaults
        // It also can be stored in other places
        NSData *activeCodeData = [userDefaults objectForKey:strKeyActiveCode];
        
        // Check if current active code is available
#if CHECK_LICENSE_WITH_PATH
        
        // use file
        iRet = st_mobile_check_activecode(
                                          strLicensePath.UTF8String,
                                          (const char *)[activeCodeData bytes],
                                          (int)[activeCodeData length]
                                          );
        
#else
        
        // use buffer
        NSData *licenseData = [NSData dataWithContentsOfFile:strLicensePath];
        
        iRet = st_mobile_check_activecode_from_buffer(
                                                      [licenseData bytes],
                                                      (int)[licenseData length],
                                                      [activeCodeData bytes],
                                                      (int)[activeCodeData length]
                                                      );
#endif
        
        
        if (ST_OK == iRet) {
            
            // check success
            return YES;
        }
    }
    
    /*
     1. check fail
     2. new one
     3. update
     */
    
    char active_code[1024];
    int active_code_len = 1024;
    
    // generate one
#if CHECK_LICENSE_WITH_PATH
    
    // use file
    iRet = st_mobile_generate_activecode(
                                         strLicensePath.UTF8String,
                                         active_code,
                                         &active_code_len
                                         );
    
#else
    
    // use buffer
    NSData *licenseData = [NSData dataWithContentsOfFile:strLicensePath];
    
    iRet = st_mobile_generate_activecode_from_buffer(
                                                     [licenseData bytes],
                                                     (int)[licenseData length],
                                                     active_code,
                                                     &active_code_len
                                                     );
#endif
    
    if (ST_OK != iRet) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"错误提示" message:@"使用 license 文件生成激活码时失败，可能是授权文件过期。" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        
        [alert show];
        
        return NO;
        
    } else {
        
        // Store active code
        NSData *activeCodeData = [NSData dataWithBytes:active_code length:active_code_len];
        
        [userDefaults setObject:activeCodeData forKey:strKeyActiveCode];
        [userDefaults setObject:strLicenseSHA1 forKey:strKeySHA1];
        
        [userDefaults synchronize];
    }
    
    return YES;
}


- (NSString *)getSHA1StringWithData:(NSData *)data
{
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, (unsigned int)data.length, digest);
    
    NSMutableString *strSHA1 = [NSMutableString string];
    
    for (int i = 0 ; i < CC_SHA1_DIGEST_LENGTH ; i ++) {
        
        [strSHA1 appendFormat:@"%02x" , digest[i]];
    }
    
    return strSHA1;
}


#pragma - mark -
#pragma - mark Render & SLAM


- (void)setupHandle
{
    [EAGLContext setCurrentContext:self.glContext];
    
    CVReturn err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, self.glContext, NULL, &_cvTextureCache);
    
    if (err) {
        
        NSLog(@"CVOpenGLESTextureCacheCreate failed %d" , err);
    }
    
    if (![self checkActiveCode]) {
        
        return;
    }
    
    [self initResultTexture];
    
    st_handle_t hRender = NULL;
    st_handle_t hSlam = NULL;
    st_result_t iRet = ST_E_FAIL;
    
    iRet = st_mobile_renderer_create(&hRender);
    
    if (ST_OK != iRet || !hRender) {
        
        NSLog(@"st_mobile_renderer_create failed . %d" , iRet);
    }else{
        
        self.hRender = hRender;
    }
    
    st_mobile_renderer_set_app_initial_orientation(self.hRender, ST_CLOCKWISE_ROTATE_0);
    
    float fovX = self.stCamera.device.activeFormat.videoFieldOfView;
    
    iRet = st_mobile_slam_create(&hSlam, 720, 1280, fovX, ST_SLAM_Portrait , false);
    
    if (ST_OK != iRet || !hSlam) {
        
        NSLog(@"st_mobile_slam_create failed . %d" , iRet);
    }else{
        
        self.hSlam = hSlam;
    }
}

- (void)addAnObject:(NSString *)strObjPath
{
    if (strObjPath && ![strObjPath isEqualToString:self.strCurrentObjPath]) {
        
        BOOL isObjExist = [[NSFileManager defaultManager] fileExistsAtPath:strObjPath];
        
        if (!isObjExist) {
            
            NSLog(@"set object failed , can not find the object .");
            
            return;
        }
        
        self.strPrepareObjPath = strObjPath;
    }
}

- (void)moveObject:(int)iObjID toPosition:(CGPoint)position inView:(UIView *)view
{
    if (iObjID >= 0 && ST_SLAM_TRACKING_SUCCESS == self.iSlamState) {
 
        
        
        float fRatioX = 2.0 * position.x / CGRectGetWidth(view.frame) - 1.0;
        float fRatioY = 2.0 * position.y / CGRectGetHeight(view.frame) - 1.0;
        
        st_result_t iRet = st_mobile_renderer_set_object_location(self.hRender,
                                                                  iObjID,
                                                                  fRatioX,
                                                                  fRatioY);
        
        if (ST_OK != iRet) {
            
            NSLog(@"st_mobile_renderer_set_object_location failed %d" , iRet);
        }
    }
}

- (void)removeAllObj
{
    self.strCurrentObjPath = nil;
    self.iCurrentObjID = -1;
    
    self.needRemoveObj = YES;
}

- (void)initWithPoint:(CGPoint)point inView:(UIView *)view
{
    CGPoint initPosition = CGPointMake(point.x / CGRectGetWidth(view.frame), point.y / CGRectGetHeight(view.frame));
    
    SENSEAR_WEAKSELF_DEFINITION
    
    [self safeCall:self.hSlam!=nil needGLContext:NO block:^{
        
        st_mobile_slam_set_init_pos(weakSelf.hSlam, initPosition.x, initPosition.y);
        
        weakSelf.hasInitPosition = YES;
    }];
}

- (void)resetUI
{
    self.hasInitPosition = NO;
    self.lblPrompt.hidden = NO;
    self.imageLighting.hidden = NO;
    [self.settingPanel.planeSegmentCtl setSelectedSegmentIndex:0];
}

- (void)resetEngine
{
    st_mobile_slam_reset(self.hSlam);
    st_mobile_renderer_reset_object(self.hRender, self.iCurrentObjID);
    st_mobile_renderer_reset(self.hRender);
}

- (void)selectTheFirstObj
{
    if (self.arrObjPaths.count > 0) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        
        [self tableView:self.objectList didSelectRowAtIndexPath:indexPath];
        [self.objectList selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    }
}

#pragma - mark -


- (BOOL)setupOriginTextureWithPixelBuffer:(CVPixelBufferRef)pixelBuffer
                                    width:(GLsizei)iWidth
                                   height:(GLsizei)iHeight
{
    CVReturn cvRet = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                                  _cvTextureCache,
                                                                  pixelBuffer,
                                                                  NULL,
                                                                  GL_TEXTURE_2D,
                                                                  GL_RGBA,
                                                                  iWidth,
                                                                  iHeight,
                                                                  GL_BGRA,
                                                                  GL_UNSIGNED_BYTE,
                                                                  0,
                                                                  &_cvTextureOrigin);
    
    if (!_cvTextureOrigin || kCVReturnSuccess != cvRet) {
        
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage failed %d" , cvRet);
        
        return NO;
    }
    
    _textureOrigin = CVOpenGLESTextureGetName(_cvTextureOrigin);
    glBindTexture(GL_TEXTURE_2D , _textureOrigin);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return YES;
}


- (BOOL)setupTextureWithPixelBuffer:(CVPixelBufferRef *)pixelBufferOut
                              width:(int)iWidth
                             height:(int)iHeight
                          glTexture:(GLuint *)glTexture
                          cvTexture:(CVOpenGLESTextureRef *)cvTexture
{
    CFDictionaryRef empty = CFDictionaryCreate(kCFAllocatorDefault,
                                               NULL,
                                               NULL,
                                               0,
                                               &kCFTypeDictionaryKeyCallBacks,
                                               &kCFTypeDictionaryValueCallBacks);
    
    CFMutableDictionaryRef attrs = CFDictionaryCreateMutable(kCFAllocatorDefault,
                                                             1,
                                                             &kCFTypeDictionaryKeyCallBacks,
                                                             &kCFTypeDictionaryValueCallBacks);
    
    CFDictionarySetValue(attrs, kCVPixelBufferIOSurfacePropertiesKey, empty);
    
    CVReturn cvRet = CVPixelBufferCreate(kCFAllocatorDefault,
                                         iWidth,
                                         iHeight,
                                         kCVPixelFormatType_32BGRA,
                                         attrs,
                                         pixelBufferOut);
    
    if (kCVReturnSuccess != cvRet) {
        
        NSLog(@"CVPixelBufferCreate failed %d" , cvRet);
    }
    
    cvRet = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                         _cvTextureCache,
                                                         *pixelBufferOut,
                                                         NULL,
                                                         GL_TEXTURE_2D,
                                                         GL_RGBA,
                                                         iWidth,
                                                         iHeight,
                                                         GL_RGBA,
                                                         GL_UNSIGNED_BYTE,
                                                         0,
                                                         cvTexture);
    
    CFRelease(attrs);
    CFRelease(empty);
    
    if (kCVReturnSuccess != cvRet) {
        
        NSLog(@"CVOpenGLESTextureCacheCreateTextureFromImage failed %d" , cvRet);
        
        return NO;
    }
    
    *glTexture = CVOpenGLESTextureGetName(*cvTexture);
    glBindTexture(CVOpenGLESTextureGetTarget(*cvTexture), *glTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glBindTexture(GL_TEXTURE_2D, 0);
    
    return YES;
}


- (void)safeCall:(BOOL)bCondition
   needGLContext:(BOOL)needGLContext
           block:(void (^)(void))block
{
    if (bCondition && block && self.stCamera.bufferQueue) {
        
        SENSEAR_WEAKSELF_DEFINITION
        
        //        dispatch_async(self.stCamera.bufferQueue, ^{
        
        if (needGLContext) {
            
            if ([EAGLContext setCurrentContext:weakSelf.glContext]) {
                
                block();
            }else{
                
                NSLog(@"setCurrentContext failed .");
            }
        }else{
            
            block();
        }
        //        });
    }
}

- (UIImage *)getUIImageFromBytes:(unsigned char *)pImage
                   withImageSize:(CGSize)size
                        isOrigin:(BOOL)isOrigin
{
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    
    int iBitsPerComponent = 8;
    int iBytesPerPixel = 4;
    int iBytesPerRow = iBytesPerPixel * size.width;
    
    uint32_t bitmapInfo = isOrigin ? kCGBitmapByteOrder32Host | kCGImageAlphaPremultipliedFirst : kCGImageAlphaNoneSkipLast;
    
    CGContextRef context = CGBitmapContextCreate(pImage,
                                                 size.width,
                                                 size.height,
                                                 iBitsPerComponent,
                                                 iBytesPerRow,
                                                 colorspace,
                                                 bitmapInfo);
    
    CGImageRef iOffscreen = CGBitmapContextCreateImage(context);
    UIImage* image = [UIImage imageWithCGImage: iOffscreen];
    
    CGColorSpaceRelease(colorspace);
    CGContextRelease(context);
    CGImageRelease(iOffscreen);
    
    return image;
}

- (void)closeSettingPanel
{
    self.settingPanel.hidden = YES;
    self.btnConfirmSetting.hidden = YES;
    
    self.btnReset.hidden = NO;
    self.btnList.hidden = NO;
    self.btnSetting.hidden = NO;
}

- (void)openSettingPanel
{
    self.btnConfirmSetting.hidden = NO;
    self.settingPanel.hidden = NO;
    
    self.btnReset.hidden = YES;
    self.btnList.hidden = YES;
    self.btnSetting.hidden = YES;
}

#pragma - mark -
#pragma - mark TableView delegate & dataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrObjPaths.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STTableViewCell *cell = (STTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row) {
        
        NSString *strObjPath = [self.arrObjPaths objectAtIndex:indexPath.row - 1];
        NSString *strObjName = [strObjPath.lastPathComponent stringByDeletingPathExtension];
        NSString *strThumbPath = [[strObjPath stringByDeletingLastPathComponent] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png" , strObjName]];
        
        UIImage *imageThumb = [UIImage imageWithContentsOfFile:strThumbPath];
        
        cell.thumbView.image = imageThumb;
        cell.lblName.text = strObjName;
    }else{
        
        cell.thumbView.image = nil;
        cell.lblName.text = @"无素材";
    }
    
    cell.accessoryType = cell.isSelected ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self removeAllObj];
    [self addAnObject:indexPath.row ? [self.arrObjPaths objectAtIndex:indexPath.row - 1] : nil];
    
    STTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    UIImage *image = indexPath.row ? cell.thumbView.image : [UIImage imageNamed:@"btn_list"];
    
    [self.btnList setImage:image forState:UIControlStateNormal];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}

#pragma - mark -
#pragma - mark STCameraDelegate


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    if (!self.isAppActive) {
        
        return;
    }
    
    double dFrameStart = CFAbsoluteTimeGetCurrent();
    
    //获取每一帧图像信息
    CVPixelBufferRef cvOriginBuffer = (CVPixelBufferRef)CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(cvOriginBuffer, 0);
    
    double timestamp = CMTimeGetSeconds(CMSampleBufferGetPresentationTimeStamp(sampleBuffer));
    
    unsigned char *pImageBGRA = (unsigned char *)CVPixelBufferGetBaseAddress(cvOriginBuffer);
    
    int iBytesPerRow = (int)CVPixelBufferGetBytesPerRow(cvOriginBuffer);
    int iWidth = (int)CVPixelBufferGetWidth(cvOriginBuffer);
    int iHeight = (int)CVPixelBufferGetHeight(cvOriginBuffer);
    
    size_t iTop , iBottom , iLeft , iRight;
    CVPixelBufferGetExtendedPixels(cvOriginBuffer, &iLeft, &iRight, &iTop, &iBottom);
    
    iWidth = iWidth + (int)iLeft + (int)iRight;
    iHeight = iHeight + (int)iTop + (int)iBottom;
    iBytesPerRow = iBytesPerRow + (int)iLeft + (int)iRight;
    
    // 设置 OpenGL 环境 , 需要与初始化 SDK 时一致
    if ([EAGLContext currentContext] != self.glContext) {
        [EAGLContext setCurrentContext:self.glContext];
    }
    
    if (iWidth != self.iWidth || iHeight != self.iHeight) {
        
        [self releaseResultTexture];
        
        self.iWidth = iWidth;
        self.iHeight = iHeight;
        
        [self initResultTexture];
        
        [self resetEngine];
    }
    
    BOOL needSnap = self.needSnap;
    BOOL hasInitPosition = self.hasInitPosition;
    BOOL needRemoveObj = self.needRemoveObj;
    
    NSString *strObjPath = self.strCurrentObjPath;
    NSString *strPrepareObjPath = self.strPrepareObjPath;
    
    int iTrackConfidence = 0;
    int iFeaturePointsNum = 0;
    int iLandmarkPointsNum = 0;
    
    double dSlamCost = 0.0;
    
    // 获取原图纹理
    BOOL isTextureOriginReady = [self setupOriginTextureWithPixelBuffer:cvOriginBuffer
                                                                  width:iWidth
                                                                 height:iHeight];
    
    GLuint textureResult = _textureOrigin;
    CVPixelBufferRef resultPixelBuffer = cvOriginBuffer;
    
    std::vector<st_slam_imu> imus;
    
    CMDeviceMotion *motion = self.motionManager.deviceMotion;
    
    // 获取设备姿势
    st_slam_attitude attitude;
    memset(&attitude, 0, sizeof(st_slam_attitude));
    
    attitude = {
        .quaternion = {motion.attitude.quaternion.x, motion.attitude.quaternion.y, motion.attitude.quaternion.z, motion.attitude.quaternion.w},
        .gravity = {motion.gravity.x, motion.gravity.y, motion.gravity.z},
        .timeStamp = motion.timestamp
    };
    
    
    // 读取帧间运动信息
    [self readSensors:&imus withPreviousFrameTimeStamp:self.dPreviousFrameTimestamp];
    
    // need remove obj
    if (needRemoveObj) {
        
        st_mobile_renderer_delete_all_objects(self.hRender);
        
        self.needRemoveObj = NO;
    }
    
    // need add obj
    if (strPrepareObjPath) {
        
        int iCurrentObjID = st_mobile_renderer_add_object(self.hRender,
                                                          strPrepareObjPath.UTF8String);
        if (iCurrentObjID >= 0) {
            
            self.strCurrentObjPath = strObjPath;
            self.iCurrentObjID = iCurrentObjID;
        }else{
            
            self.strCurrentObjPath = nil;
            
            NSLog(@"st_mobile_renderer_add_object faild %d" , self.iCurrentObjID);
        }
        
        self.strPrepareObjPath = nil;
    }
    
    st_slam_result slamResult;
    memset(&slamResult, 0, sizeof(st_slam_result));
    st_result_t iRet = ST_E_FAIL;
    
    for (int i = 0; i < 1; i ++) {
        
        if (!isTextureOriginReady) {
            
            NSLog(@"setupOriginTextureWithPixelBuffer failed .");
            
            break;
        }
        
        // SLAM
        
        if (hasInitPosition) {
            
            double dSlamStart = CFAbsoluteTimeGetCurrent();
            
            // SLAM 检测
            
            st_image_t stImage = {0};
            stImage.data = pImageBGRA;
            stImage.width = iWidth;
            stImage.height = iHeight;
            stImage.stride = iBytesPerRow;
            stImage.time_stamp = timestamp;
            stImage.pixel_format = ST_PIX_FMT_BGRA8888;
            
            iRet = st_mobile_slam_run(self.hSlam, stImage, &imus[0], (int)imus.size(), attitude, &slamResult);
            
            dSlamCost = CFAbsoluteTimeGetCurrent() - dSlamStart;
            
            self.iSlamState = slamResult.state;
            iTrackConfidence = slamResult.track_confidence;
            iFeaturePointsNum = slamResult.num_features;
            iLandmarkPointsNum = slamResult.num_landmarks;
            
            if (ST_OK != iRet) {
                
                NSLog(@"st_mobile_slam_run failed %d" , iRet);
                
                break;
            }
            
            NSLog(@"Camera = (%.5f, %.5f, %.5f)", slamResult.camera.center[0], slamResult.camera.center[1], slamResult.camera.center[2]);
        }
        
        
        // RENDER
        
        double dRenderCost = CFAbsoluteTimeGetCurrent();
        
        // 渲染结果
        iRet = st_mobile_renderer_render_slam(self.hRender,
                                              textureResult,
                                              slamResult,
                                              iWidth,
                                              iHeight,
                                              _textureResult,
                                              NULL,
                                              ST_PIX_FMT_RGBA8888);
        
        printf("render cost\t%.0f\n" , (CFAbsoluteTimeGetCurrent() - dRenderCost) * 1000.0);
        
        if (ST_OK != iRet) {
            
            NSLog(@"st_mobile_renderer_render_slam failed %d" , iRet);
            
            break;
        }else{
            
            textureResult = _textureResult;
            resultPixelBuffer = _cvResultBuffer;
        }
    }
    
    // 渲染到预览
    [self.stPreview renderTexture:textureResult];
    
    if (needSnap) {
        
        self.needSnap = NO;
        
        if (resultPixelBuffer == _cvResultBuffer) {
            
            CVPixelBufferLockBaseAddress(resultPixelBuffer, kCVPixelBufferLock_ReadOnly);
        }
        unsigned char* pImageResult = (unsigned char*)CVPixelBufferGetBaseAddress(resultPixelBuffer);
        
        UIImage *image = [self getUIImageFromBytes:pImageResult
                                     withImageSize:CGSizeMake(iWidth, iHeight)
                                          isOrigin:resultPixelBuffer != _cvResultBuffer];
        
        if (resultPixelBuffer == _cvResultBuffer) {
            
            CVPixelBufferUnlockBaseAddress(resultPixelBuffer, kCVPixelBufferLock_ReadOnly);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        });
    }
    
    CVPixelBufferUnlockBaseAddress(cvOriginBuffer, 0);
    CVOpenGLESTextureCacheFlush(_cvTextureCache, 0);
    
    if (_cvTextureOrigin) {
        
        CFRelease(_cvTextureOrigin);
        _cvTextureOrigin = NULL;
    }
    
    NSString *strSlamState = @"";
    
    if (hasInitPosition) {
        
        switch (self.iSlamState) {
                
            case ST_SLAM_INITIALIZING:
                break;
                
            case ST_SLAM_TRACKING_SUCCESS:
            {
                strSlamState = @"  Tracking Normal";
            }
                break;
                
            case ST_SLAM_TRACKING_FAIL:
            {
                strSlamState = @"  Tracking Limited , Not enough surface detail";
            }
                break;
                
            default:
                break;
        }
    }else{
        
        strSlamState = @"  click to initialize slam and place object";
    }
    
    self.dPreviousFrameTimestamp = timestamp;
    
    double dFrameCost = (CFAbsoluteTimeGetCurrent() - dFrameStart) * 1000.0;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.lblStatus.text = [NSString stringWithFormat:@"  单帧时间:%.0f ms\n  SLAM处理时间:%.0f ms\n  key points: %d/%d\n  track confidence : %d\n%@" , dFrameCost , dSlamCost * 1000.0 ,  iFeaturePointsNum, iLandmarkPointsNum, iTrackConfidence, strSlamState];
    });
    
    
    printf("frame costt\t%.0f\n" , dFrameCost);
    printf("slam cost\t%.0f\n" , dSlamCost * 1000.0);
}

#pragma - mark -
#pragma - mark Touch Event

- (IBAction)onBtnBack:(id)sender {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    
    [self.motionManager stopDeviceMotionUpdates];
    [self.imuQueue cancelAllOperations];
    
    [self.stCamera setDelegate:nil];
    [self.stCamera stopRunning];
    
    // 需要保证 SDK 的线程安全 , 顺序调用.
    dispatch_sync(self.stCamera.bufferQueue, ^{
        
        [self releaseResources];
    });
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBtnSnap:(id)sender {
    
    self.needSnap = YES;
}

- (void)onBtnReset
{
    [self resetUI];
    
    dispatch_async(self.stCamera.bufferQueue, ^{
        
        [self resetEngine];
    });
}


- (void)onBtnList
{
    self.objectList.hidden = !self.objectList.hidden;
    
    [self closeSettingPanel];
}

- (void)onBtnSetting
{
    [self openSettingPanel];
    
    self.objectList.hidden = YES;
}

- (void)onPreviewTap:(UITapGestureRecognizer *)tap
{
    if (!self.objectList.hidden) {
        
        self.objectList.hidden = YES;
    }else if (!self.settingPanel.hidden){
        
        [self closeSettingPanel];
    }else{
        
        CGPoint touchPoint = [tap locationInView:self.stPreview];
        
        if (!self.hasInitPosition) {
            
            self.lblPrompt.hidden = YES;
            self.imageLighting.hidden = YES;
            
            [self initWithPoint:touchPoint inView:self.stPreview];
        }else{
            
            [self moveObject:self.iCurrentObjID toPosition:touchPoint inView:self.stPreview];
        }
    }
}

- (void)onPreviewPan:(UIPanGestureRecognizer *)pan
{
    CGPoint touchPoint = [pan locationInView:self.stPreview];
    
    [self moveObject:self.iCurrentObjID toPosition:touchPoint inView:self.stPreview];
}

- (void)onBtnConfirmSetting
{
    [self closeSettingPanel];
}




#pragma - mark -

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
    
    [alert setMessage:error ? @"保存失败" : @"保存成功"];
    
    [alert show];
}



#pragma - mark -


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
