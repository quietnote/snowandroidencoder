#include <vector>
#include <stdio.h>
#include "st_mobile_human_action.h"
#include "st_mobile_slam.h"
#include "SnowSTTypeExtensions.h"
#include "SnowSTCommon.h"
#include "SnowSTHumanEnum.h"


st_handle_t g_HumanActionHandle = NULL;

unsigned long long g_DetectConfig = 0;
bool g_NeedToDetectExpression = false;

st_mobile_human_action_t g_HumanActionResult;
bool g_Expressions_result[ST_MOBILE_EXPRESSION_COUNT];


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_InitHumanAction()
{
	// We only support one human action instance
	if (g_HumanActionHandle != NULL)
	{
		return ST_E_FAIL;
	}

	const unsigned int config = ST_MOBILE_DETECT_MODE_VIDEO
								| ST_MOBILE_ENABLE_FACE_DETECT | ST_MOBILE_ENABLE_FACE_EXTRA_DETECT | ST_MOBILE_ENABLE_EYEBALL_CENTER_DETECT | ST_MOBILE_ENABLE_EYEBALL_CONTOUR_DETECT | ST_MOBILE_ENABLE_HAIR_SEGMENT
								| ST_MOBILE_ENABLE_HAND_DETECT | ST_MOBILE_ENABLE_BODY_CONTOUR | ST_MOBILE_ENABLE_BODY_KEYPOINTS;

	int ret = st_mobile_human_action_create(NULL, config, &g_HumanActionHandle);
	if (ret != ST_OK)
		return ret;

	// Succeed to construct new human action detector
	{
		// Initialize cached result of detection
		g_HumanActionResult.p_faces = NULL;
		g_HumanActionResult.face_count = 0;
		g_HumanActionResult.p_hands = NULL;
		g_HumanActionResult.hand_count = 0;
		g_HumanActionResult.p_background = NULL;
		g_HumanActionResult.background_score = 0;
		g_HumanActionResult.p_bodys = NULL;
		g_HumanActionResult.body_count = 0;
		g_HumanActionResult.p_hair = NULL;
		g_HumanActionResult.hair_score = 0;

		memset(g_Expressions_result, 0, sizeof(bool) * ST_MOBILE_EXPRESSION_COUNT);
	}
	return ret;
}

extern "C" void U_IEXPORT U_IAPI SnowSenseTime_DestroyHumanAction()
{
	if (g_HumanActionHandle != NULL)
	{
		st_mobile_human_action_destroy(g_HumanActionHandle);
		g_HumanActionHandle = NULL;
	}
}


// A model may have multiple DetectType (for example one model does both kEyeballCenter and kEyeballContour)
extern "C" int U_IEXPORT U_IAPI SnowSenseTime_LoadHumanActionModel(const unsigned char* buffer, int bufferSize)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	return st_mobile_human_action_add_sub_model_from_buffer(g_HumanActionHandle, buffer, bufferSize);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_UnloadHumanActionModel(DetectType type)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	switch (type)
	{
		case DetectType::kFace:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_FACE_DETECT);
		case DetectType::kFaceExtra:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_FACE_EXTRA_DETECT);
		case DetectType::kEyeballCenter:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_EYEBALL_CENTER_DETECT);
		case DetectType::kEyeballContour:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_EYEBALL_CONTOUR_DETECT);
		case DetectType::kHairSegment:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_HAIR_SEGMENT);
		case DetectType::kHand:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_HAND_DETECT);
		case DetectType::kBodyKeyPoints:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_BODY_KEYPOINTS);
		case DetectType::kBodyContour:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_BODY_CONTOUR);
		case DetectType::kBackgroundSegment:
			return st_mobile_human_action_remove_model_by_config(g_HumanActionHandle, ST_MOBILE_ENABLE_SEGMENT_DETECT);
		default:
			return ST_E_FAIL;
	}
}


// bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible.
extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetDetectionMask(DetectType type, unsigned char setToOn)
{
	if (setToOn != 0)
	{
		switch (type)
		{
			case DetectType::kFace:
				g_DetectConfig |= ST_MOBILE_FACE_DETECT;
				return ST_OK;
			case DetectType::kFaceExtra:
				g_DetectConfig |= ST_MOBILE_DETECT_EXTRA_FACE_POINTS;
				return ST_OK;
			case DetectType::kEyeballCenter:
				g_DetectConfig |= ST_MOBILE_DETECT_EYEBALL_CENTER;
				return ST_OK;
			case DetectType::kEyeballContour:
				g_DetectConfig |= ST_MOBILE_DETECT_EYEBALL_CONTOUR;
				return ST_OK;
			case DetectType::kHairSegment:
				g_DetectConfig |= ST_MOBILE_SEG_HAIR;
				return ST_OK;
			case DetectType::kHand:
				g_DetectConfig |= ST_MOBILE_HAND_DETECT;
				return ST_OK;
			case DetectType::kBodyKeyPoints:
				g_DetectConfig |= ST_MOBILE_BODY_KEYPOINTS;
				return ST_OK;
			case DetectType::kBodyContour:
				g_DetectConfig |= ST_MOBILE_BODY_CONTOUR;
				return ST_OK;
			case DetectType::kBackgroundSegment:
				g_DetectConfig |= ST_MOBILE_SEG_BACKGROUND;
				return ST_OK;
			default:
				return ST_E_INVALIDARG;
		}
	}
	else
	{
		switch (type)
		{
			case DetectType::kFace:
				g_DetectConfig &= ~ST_MOBILE_FACE_DETECT;
				return ST_OK;
			case DetectType::kFaceExtra:
				g_DetectConfig &= ~ST_MOBILE_DETECT_EXTRA_FACE_POINTS;
				return ST_OK;
			case DetectType::kEyeballCenter:
				g_DetectConfig &= ~ST_MOBILE_DETECT_EYEBALL_CENTER;
				return ST_OK;
			case DetectType::kEyeballContour:
				g_DetectConfig &= ~ST_MOBILE_DETECT_EYEBALL_CONTOUR;
				return ST_OK;
			case DetectType::kHairSegment:
				g_DetectConfig &= ~ST_MOBILE_SEG_HAIR;
				return ST_OK;
			case DetectType::kHand:
				g_DetectConfig &= ~ST_MOBILE_HAND_DETECT;
				return ST_OK;
			case DetectType::kBodyKeyPoints:
				g_DetectConfig &= ~ST_MOBILE_BODY_KEYPOINTS;
				return ST_OK;
			case DetectType::kBodyContour:
				g_DetectConfig &= ~ST_MOBILE_BODY_CONTOUR;
				return ST_OK;
			case DetectType::kBackgroundSegment:
				g_DetectConfig &= ~ST_MOBILE_SEG_BACKGROUND;
				return ST_OK;
			default:
				return ST_E_INVALIDARG;
		}
	}
}


// bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible.
extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetFaceActionMask(FaceAction faceAction, unsigned char setToOn)
{
	if (setToOn != 0)
	{
		switch (faceAction)
		{
			case FaceAction::kEyeBlink:
				g_DetectConfig |= ST_MOBILE_EYE_BLINK;
				return ST_OK;
			case FaceAction::kMouthAh:
				g_DetectConfig |= ST_MOBILE_MOUTH_AH;
				return ST_OK;
			case FaceAction::kHeadYaw:
				g_DetectConfig |= ST_MOBILE_HEAD_YAW;
				return ST_OK;
			case FaceAction::kHeadPitch:
				g_DetectConfig |= ST_MOBILE_HEAD_PITCH;
				return ST_OK;
			case FaceAction::kBrowJump:
				g_DetectConfig |= ST_MOBILE_BROW_JUMP;
				return ST_OK;
			default:
				return ST_E_INVALIDARG;
		}
	}
	else
	{
		switch (faceAction)
		{
			case FaceAction::kEyeBlink:
				g_DetectConfig &= ~ST_MOBILE_EYE_BLINK;
				return ST_OK;
			case FaceAction::kMouthAh:
				g_DetectConfig &= ~ST_MOBILE_MOUTH_AH;
				return ST_OK;
			case FaceAction::kHeadYaw:
				g_DetectConfig &= ~ST_MOBILE_HEAD_YAW;
				return ST_OK;
			case FaceAction::kHeadPitch:
				g_DetectConfig &= ~ST_MOBILE_HEAD_PITCH;
				return ST_OK;
			case FaceAction::kBrowJump:
				g_DetectConfig &= ~ST_MOBILE_BROW_JUMP;
				return ST_OK;
			default:
				return ST_E_INVALIDARG;
		}
	}
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetFaceActionThreshold(FaceAction faceAction, float threshold)
{
	switch (faceAction)
	{
		case FaceAction::kEyeBlink:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_EYE_BLINK, threshold);
		case FaceAction::kMouthAh:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_MOUTH_AH, threshold);
		case FaceAction::kHeadYaw:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HEAD_YAW, threshold);
		case FaceAction::kHeadPitch:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HEAD_PITCH, threshold);
		case FaceAction::kBrowJump:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_BROW_JUMP, threshold);
		default:
			return ST_E_INVALIDARG;
	}
}


// bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible.
extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHandActionMask(HandAction handAction, unsigned char setToOn)
{
	if (setToOn != 0)
	{
		switch (handAction)
		{
			case HandAction::kOK:
				g_DetectConfig |= ST_MOBILE_HAND_OK;
				return ST_OK;
			case HandAction::kScissor:
				g_DetectConfig |= ST_MOBILE_HAND_SCISSOR;
				return ST_OK;
			case HandAction::kGood:
				g_DetectConfig |= ST_MOBILE_HAND_GOOD;
				return ST_OK;
			case HandAction::kPalm:
				g_DetectConfig |= ST_MOBILE_HAND_PALM;
				return ST_OK;
			case HandAction::kPistol:
				g_DetectConfig |= ST_MOBILE_HAND_PISTOL;
				return ST_OK;
			case HandAction::kLove:
				g_DetectConfig |= ST_MOBILE_HAND_LOVE;
				return ST_OK;
			case HandAction::kHoldup:
				g_DetectConfig |= ST_MOBILE_HAND_HOLDUP;
				return ST_OK;
			case HandAction::kCongratulate:
				g_DetectConfig |= ST_MOBILE_HAND_CONGRATULATE;
				return ST_OK;
			case HandAction::kFingerHeart:
				g_DetectConfig |= ST_MOBILE_HAND_FINGER_HEART;
				return ST_OK;
			case HandAction::kFingerIndex:
				g_DetectConfig |= ST_MOBILE_HAND_FINGER_INDEX;
				return ST_OK;
			case HandAction::kFist:
				g_DetectConfig |= ST_MOBILE_HAND_FIST;
				return ST_OK;
			default:
				return ST_E_INVALIDARG;
		}
	}
	else
	{
		switch (handAction)
		{
			case HandAction::kOK:
				g_DetectConfig &= ~ST_MOBILE_HAND_OK;
				return ST_OK;
			case HandAction::kScissor:
				g_DetectConfig &= ~ST_MOBILE_HAND_SCISSOR;
				return ST_OK;
			case HandAction::kGood:
				g_DetectConfig &= ~ST_MOBILE_HAND_GOOD;
				return ST_OK;
			case HandAction::kPalm:
				g_DetectConfig &= ~ST_MOBILE_HAND_PALM;
				return ST_OK;
			case HandAction::kPistol:
				g_DetectConfig &= ~ST_MOBILE_HAND_PISTOL;
				return ST_OK;
			case HandAction::kLove:
				g_DetectConfig &= ~ST_MOBILE_HAND_LOVE;
				return ST_OK;
			case HandAction::kHoldup:
				g_DetectConfig &= ~ST_MOBILE_HAND_HOLDUP;
				return ST_OK;
			case HandAction::kCongratulate:
				g_DetectConfig &= ~ST_MOBILE_HAND_CONGRATULATE;
				return ST_OK;
			case HandAction::kFingerHeart:
				g_DetectConfig &= ~ST_MOBILE_HAND_FINGER_HEART;
				return ST_OK;
			case HandAction::kFingerIndex:
				g_DetectConfig &= ~ST_MOBILE_HAND_FINGER_INDEX;
				return ST_OK;
			case HandAction::kFist:
				g_DetectConfig &= ~ST_MOBILE_HAND_FIST;
				return ST_OK;
			default:
				return ST_E_INVALIDARG;
		}
	}
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHandActionThreshold(HandAction handAction, float threshold)
{
	switch (handAction)
	{
		case HandAction::kOK:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_OK, threshold);
		case HandAction::kScissor:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_SCISSOR, threshold);
		case HandAction::kGood:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_GOOD, threshold);
		case HandAction::kPalm:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_PALM, threshold);
		case HandAction::kPistol:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_PISTOL, threshold);
		case HandAction::kLove:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_LOVE, threshold);
		case HandAction::kHoldup:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_HOLDUP, threshold);
		case HandAction::kCongratulate:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_CONGRATULATE, threshold);
		case HandAction::kFingerHeart:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_FINGER_HEART, threshold);
		case HandAction::kFingerIndex:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HAND_FINGER_INDEX, threshold);
		case HandAction::kFist:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_EYE_BLINK, threshold);
		default:
			return ST_E_INVALIDARG;
	}
}


// bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible.
extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetExpressionDetect(unsigned char turnOn)
{
	g_NeedToDetectExpression = (turnOn != 0);
	return ST_OK;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHeadExpressionThreshold(HeadExpression headExpression, float threshold)
{
	switch (headExpression)
	{
		case HeadExpression::kHeadNormal:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HEAD_NORMAL, threshold);
		case HeadExpression::kSideFaceLeft:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_SIDE_FACE_LEFT, threshold);
		case HeadExpression::kSideFaceRight:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_SIDE_FACE_RIGHT, threshold);
		case HeadExpression::kTiltedFaceLeft:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_TILTED_FACE_LEFT, threshold);
		case HeadExpression::kTiltedFaceRight:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_TILTED_FACE_RIGHT, threshold);
		case HeadExpression::kHeadRise:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HEAD_RISE, threshold);
		case HeadExpression::kHeadLower:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_HEAD_LOWER, threshold);
		default:
			return ST_E_INVALIDARG;
	}
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetEyeExpressionThreshold(EyeExpression eyeExpression, float threshold)
{
	switch (eyeExpression)
	{
		case EyeExpression::kTwoEyeClose:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_TWO_EYE_CLOSE, threshold);
		case EyeExpression::kTwoEyeOpen:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_TWO_EYE_OPEN, threshold);
		case EyeExpression::kLeftEyeOpenRightEyeClose:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_LEFTEYE_OPEN_RIGHTEYE_CLOSE, threshold);
		case EyeExpression::kLeftEyeCloseRightEyeOpen:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_LEFTEYE_CLOSE_RIGHTEYE_OPEN, threshold);
		default:
			return ST_E_INVALIDARG;
	}
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetMouthExpressionThreshold(MouthExpression mouthExpression, float threshold)
{
	switch (mouthExpression)
	{
		case MouthExpression::kMouthOpen:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_MOUTH_OPEN, threshold);
		case MouthExpression::kMouthClose:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_MOUTH_CLOSE, threshold);
		case MouthExpression::kLipsUpward:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_FACE_LIPS_UPWARD, threshold);
		case MouthExpression::kLipsPouted:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_FACE_LIPS_POUTED, threshold);
		case MouthExpression::kLipsCurlLeft:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_FACE_LIPS_CURL_LEFT, threshold);
		case MouthExpression::kLipsCurlRight:
			return st_mobile_set_expression_threshold(ST_MOBILE_EXPRESSION_FACE_LIPS_CURL_RIGHT, threshold);
		default:
			return ST_E_INVALIDARG;
	}
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetSmoothThreshold(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_SMOOTH_THRESHOLD, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHeadposeThreshold(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HEADPOSE_THRESHOLD, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetFaceLimit(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_FACELIMIT, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetFaceDetectInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_FACE_DETECT_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetFaceProcessInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_FACE_PROCESS_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHandLimit(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HAND_LIMIT, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHandDetectInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HAND_DETECT_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHandProcessInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HAND_PROCESS_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetBodyLimit(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_BODY_LIMIT, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetBodyDetectInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_BODY_DETECT_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetBodyProcessInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_BODY_PROCESS_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetBackgroundMaxSize(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_BACKGROUND_MAX_SIZE, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetBackgroundBlurStrength(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_BACKGROUND_BLUR_STRENGTH, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetBackgroundProcessInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_BACKGROUND_PROCESS_INTERVAL, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHairMaxSize(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HAIR_MAX_SIZE, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHairBlurStrength(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HAIR_BLUR_STRENGTH, value);
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_SetHairProcessInterval(float value)
{
	return st_mobile_human_action_setparam(g_HumanActionHandle, ST_HUMAN_ACTION_PARAM_HAIR_PROCESS_INTERVAL, value);
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_UpdateHumanAction(
		const unsigned char* image, st_pixel_format pixelFormat, int width, int height, int stride, st_rotate_type orientation)
{
	int ret = st_mobile_human_action_detect(g_HumanActionHandle, image, pixelFormat, width, height, stride, orientation, g_DetectConfig, &g_HumanActionResult);
	if (ret != ST_OK)
		return ret;

	if (g_NeedToDetectExpression)
	{
		ret = st_mobile_get_expression(&g_HumanActionResult, orientation, false, g_Expressions_result);
	}
	return ret;
}

// Important to call this between change of sticker or front/back camera
extern "C" int U_IEXPORT U_IAPI SnowSenseTime_ResetHumanAction()
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	return st_mobile_human_action_reset(g_HumanActionHandle);
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetFaceCount()
{
	if (g_HumanActionHandle == NULL)
	{
		return 0;
	}

	return g_HumanActionResult.face_count;
}

static int GetSTFaceAtIndex(int index, st_mobile_face_t** ppFaceToGet)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	if (index + 1 > g_HumanActionResult.face_count)
	{
		return ST_E_INVALIDARG;
	}

	if (g_HumanActionResult.p_faces == NULL)
	{
		return ST_E_FAIL;
	}

	*ppFaceToGet = &(g_HumanActionResult.p_faces[index]);

	return ST_OK;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetFaceData(int index,
														  int* ID, float* score, st_rect_t* rect,
														  st_pointf_t* keyPointsArray, float* keyPointsVisibilityArray,
														  float* yaw, float* pitch, float* roll,
														  int* validExtraPointsCount, st_pointf_t* extraPointsArray,
														  int* validIrisCenterPointsCount, st_pointf_t* irisCenterPointsArray,
														  int* validIrisContourPointsCount, st_pointf_t* irisContourPointsArray,
														  FaceActionData* faceActionData)
{
	st_mobile_face_t* pFaceToReadFrom;
	int ret = GetSTFaceAtIndex(index, &pFaceToReadFrom);
	if (ret != ST_OK)
		return ret;

	*ID = pFaceToReadFrom->face106.ID;
	*score = pFaceToReadFrom->face106.score;

	*rect = pFaceToReadFrom->face106.rect;

	memcpy(keyPointsArray, pFaceToReadFrom->face106.points_array, sizeof(st_pointf_t) * FACE_FEATURE_POINTS_ARRAY_SIZE);
	memcpy(keyPointsVisibilityArray, pFaceToReadFrom->face106.visibility_array, sizeof(float) * FACE_FEATURE_POINTS_ARRAY_SIZE);

	*yaw = pFaceToReadFrom->face106.yaw;
	*pitch = pFaceToReadFrom->face106.pitch;
	*roll = pFaceToReadFrom->face106.roll;

	*validExtraPointsCount = pFaceToReadFrom->extra_face_points_count;
	if (pFaceToReadFrom->extra_face_points_count > 0 && pFaceToReadFrom->p_extra_face_points != NULL)
	{
		memcpy(extraPointsArray, pFaceToReadFrom->p_extra_face_points, sizeof(st_pointf_t) * pFaceToReadFrom->extra_face_points_count);
	}

	*validIrisCenterPointsCount = pFaceToReadFrom->eyeball_center_points_count;
	if (pFaceToReadFrom->eyeball_center_points_count > 0 && pFaceToReadFrom->p_eyeball_center != NULL)
	{
		memcpy(irisCenterPointsArray, pFaceToReadFrom->p_eyeball_center, sizeof(st_pointf_t) * pFaceToReadFrom->eyeball_center_points_count);
	}

	*validIrisContourPointsCount = pFaceToReadFrom->eyeball_contour_points_count;
	if (pFaceToReadFrom->eyeball_contour_points_count > 0 && pFaceToReadFrom->p_eyeball_contour != NULL)
	{
		memcpy(irisContourPointsArray, pFaceToReadFrom->p_eyeball_contour, sizeof(st_pointf_t) * pFaceToReadFrom->eyeball_contour_points_count);
	}

	faceActionData->eyeBlink = (pFaceToReadFrom->face_action & ST_MOBILE_EYE_BLINK) ? 1 : 0;
	faceActionData->mouthAh = (pFaceToReadFrom->face_action & ST_MOBILE_MOUTH_AH) ? 1 : 0;
	faceActionData->headYaw = (pFaceToReadFrom->face_action & ST_MOBILE_HEAD_YAW) ? 1 : 0;
	faceActionData->headPitch = (pFaceToReadFrom->face_action & ST_MOBILE_HEAD_PITCH) ? 1 : 0;
	faceActionData->browJump = (pFaceToReadFrom->face_action & ST_MOBILE_BROW_JUMP) ? 1 : 0;

	return ST_OK;
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetHandCount()
{
	if (g_HumanActionHandle == NULL)
	{
		return 0;
	}

	return g_HumanActionResult.hand_count;
}

static int GetSTHandAtIndex(int index, st_mobile_hand_t** ppHandToGet)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	if (index + 1 > g_HumanActionResult.hand_count)
	{
		return ST_E_INVALIDARG;
	}

	if (g_HumanActionResult.p_hands == NULL)
	{
		return ST_E_FAIL;
	}

	*ppHandToGet = &(g_HumanActionResult.p_hands[index]);

	return ST_OK;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetHandData(int index,
														  int* ID, float* score, st_rect_t* rect,
														  int* validKeyPointsCount, st_pointf_t* keyPointsArray,
														  HandActionData* handActionData)
{
	st_mobile_hand_t* pHandToReadFrom;
	int ret = GetSTHandAtIndex(index, &pHandToReadFrom);
	if (ret != ST_OK)
		return ret;

	*ID = pHandToReadFrom->id;
	*score = pHandToReadFrom->score;

	*rect = pHandToReadFrom->rect;

	*validKeyPointsCount = pHandToReadFrom->key_points_count;
	if (pHandToReadFrom->key_points_count > 0 && pHandToReadFrom->p_key_points != NULL)
	{
		memcpy(keyPointsArray, pHandToReadFrom->p_key_points, sizeof(st_pointf_t) * *validKeyPointsCount);
	}

	handActionData->ok = pHandToReadFrom->hand_action & ST_MOBILE_HAND_OK ? 1 : 0;
	handActionData->scissor = pHandToReadFrom->hand_action & ST_MOBILE_HAND_SCISSOR ? 1 : 0;
	handActionData->good = pHandToReadFrom->hand_action & ST_MOBILE_HAND_GOOD ? 1 : 0;
	handActionData->palm = pHandToReadFrom->hand_action & ST_MOBILE_HAND_PALM ? 1 : 0;
	handActionData->pistol = pHandToReadFrom->hand_action & ST_MOBILE_HAND_PISTOL ? 1 : 0;
	handActionData->love = pHandToReadFrom->hand_action & ST_MOBILE_HAND_LOVE ? 1 : 0;
	handActionData->holdUp = pHandToReadFrom->hand_action & ST_MOBILE_HAND_HOLDUP ? 1 : 0;
	handActionData->congratulate = pHandToReadFrom->hand_action & ST_MOBILE_HAND_CONGRATULATE ? 1 : 0;
	handActionData->fingerHeart = pHandToReadFrom->hand_action & ST_MOBILE_HAND_FINGER_HEART ? 1 : 0;
	handActionData->fingerIndex = pHandToReadFrom->hand_action & ST_MOBILE_HAND_FINGER_INDEX ? 1 : 0;

	return ST_OK;
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetBodyCount()
{
	if (g_HumanActionHandle == NULL)
	{
		return 0;
	}

	return g_HumanActionResult.body_count;
}

static int GetSTBodyAtIndex(int index, st_mobile_body_t** ppBodyToGet)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	if (index + 1 > g_HumanActionResult.body_count)
	{
		return ST_E_INVALIDARG;
	}

	if (g_HumanActionResult.p_bodys == NULL)
	{
		return ST_E_FAIL;
	}

	*ppBodyToGet = &(g_HumanActionResult.p_bodys[index]);

	return ST_OK;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetBodyData(int index,
														  int* ID,
														  int* validKeyPointsCount, st_pointf_t* keyPointsArray, float* keyPointsScoreArray,
														  int* validContourPointsCount, st_pointf_t* contourPointsArray, float* contourPointsScoreArray)
{
	st_mobile_body_t* pBodyToReadFrom;
	int ret = GetSTBodyAtIndex(index, &pBodyToReadFrom);
	if (ret != ST_OK)
		return ret;

	*ID = pBodyToReadFrom->id;

	*validKeyPointsCount = pBodyToReadFrom->key_points_count;
	if (pBodyToReadFrom->key_points_count > 0 && pBodyToReadFrom->p_key_points != NULL && pBodyToReadFrom->p_key_points_score != NULL)
	{
		memcpy(keyPointsArray, pBodyToReadFrom->p_key_points, sizeof(st_pointf_t) * pBodyToReadFrom->key_points_count);
		memcpy(keyPointsScoreArray, pBodyToReadFrom->p_key_points_score, sizeof(float) * pBodyToReadFrom->key_points_count);
	}

	*validContourPointsCount = pBodyToReadFrom->contour_points_count;
	if (pBodyToReadFrom->contour_points_count > 0 && pBodyToReadFrom->p_contour_points != NULL && pBodyToReadFrom->p_contour_points_score != NULL)
	{
		memcpy(contourPointsArray, pBodyToReadFrom->p_contour_points, sizeof(st_pointf_t) * pBodyToReadFrom->contour_points_count);
		memcpy(contourPointsScoreArray, pBodyToReadFrom->p_contour_points_score, sizeof(float) * pBodyToReadFrom->contour_points_count);
	}

	return ST_OK;
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetExpressionData(ExpressionData* expressionToFill)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	expressionToFill->head.headNormal = g_Expressions_result[ST_MOBILE_EXPRESSION_HEAD_NORMAL];
	expressionToFill->head.sideFaceLeft = g_Expressions_result[ST_MOBILE_EXPRESSION_SIDE_FACE_LEFT];
	expressionToFill->head.sideFaceRight = g_Expressions_result[ST_MOBILE_EXPRESSION_SIDE_FACE_RIGHT];
	expressionToFill->head.tiltedFaceLeft = g_Expressions_result[ST_MOBILE_EXPRESSION_TILTED_FACE_LEFT];
	expressionToFill->head.tiltedFaceRight = g_Expressions_result[ST_MOBILE_EXPRESSION_TILTED_FACE_RIGHT];
	expressionToFill->head.headRise = g_Expressions_result[ST_MOBILE_EXPRESSION_HEAD_RISE];
	expressionToFill->head.headLower = g_Expressions_result[ST_MOBILE_EXPRESSION_HEAD_LOWER];

	expressionToFill->eye.twoEyeClose = g_Expressions_result[ST_MOBILE_EXPRESSION_TWO_EYE_CLOSE];
	expressionToFill->eye.twoEyeOpen = g_Expressions_result[ST_MOBILE_EXPRESSION_TWO_EYE_OPEN];
	expressionToFill->eye.leftEyeOpenRightEyeClose = g_Expressions_result[ST_MOBILE_EXPRESSION_LEFTEYE_OPEN_RIGHTEYE_CLOSE];
	expressionToFill->eye.leftEyeCloseRightEyeOpen = g_Expressions_result[ST_MOBILE_EXPRESSION_LEFTEYE_CLOSE_RIGHTEYE_OPEN];

	expressionToFill->mouth.mouthOpen = g_Expressions_result[ST_MOBILE_EXPRESSION_MOUTH_OPEN];
	expressionToFill->mouth.mouthClose = g_Expressions_result[ST_MOBILE_EXPRESSION_MOUTH_CLOSE];
	expressionToFill->mouth.lipsUpward = g_Expressions_result[ST_MOBILE_EXPRESSION_FACE_LIPS_UPWARD];
	expressionToFill->mouth.lipsPouted = g_Expressions_result[ST_MOBILE_EXPRESSION_FACE_LIPS_POUTED];
	expressionToFill->mouth.lipsCurlLeft = g_Expressions_result[ST_MOBILE_EXPRESSION_FACE_LIPS_CURL_LEFT];
	expressionToFill->mouth.lipsCurlRight = g_Expressions_result[ST_MOBILE_EXPRESSION_FACE_LIPS_CURL_RIGHT];

	return ST_OK;
};


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetBackgroundSegmentationBufferSize()
{
	if (g_HumanActionHandle == NULL)
	{
		return 0;
	}

	if (g_HumanActionResult.p_background == NULL)
	{
		return 0;
	}

	if (g_HumanActionResult.p_background->pixel_format != ST_PIX_FMT_GRAY8)
	{
		return 0;
	}

	return g_HumanActionResult.p_background->height * g_HumanActionResult.p_background->stride;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetBackgroundSegmentationBuffer(unsigned char* buffer, int bufferSize, int* width, int* height, int* stride)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	if (g_HumanActionResult.p_background == NULL)
	{
		return ST_E_FAIL;
	}

	if (g_HumanActionResult.p_background->pixel_format != ST_PIX_FMT_GRAY8)
	{
		return ST_E_FAIL;
	}

	int actualBufferSize = SnowSenseTime_GetBackgroundSegmentationBufferSize();
	if (bufferSize < actualBufferSize)
	{
		return ST_E_INVALIDARG;
	}

	st_image_t* background = g_HumanActionResult.p_background;

	memcpy(buffer, background->data, actualBufferSize);

	*width = background->width;
	*height = background->height;
	*stride = background->stride;

	return ST_OK;
}


extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetHairSegmentationBufferSize()
{
	if (g_HumanActionHandle == NULL)
	{
		return 0;
	}

	if (g_HumanActionResult.p_hair == NULL)
	{
		return 0;
	}

	if (g_HumanActionResult.p_hair->pixel_format != ST_PIX_FMT_GRAY8)
	{
		return 0;
	}

	return g_HumanActionResult.p_hair->height * g_HumanActionResult.p_hair->stride;
}

extern "C" int U_IEXPORT U_IAPI SnowSenseTime_GetHairSegmentationBuffer(unsigned char* buffer, int bufferSize, int* width, int* height, int* stride)
{
	if (g_HumanActionHandle == NULL)
	{
		return ST_E_FAIL;
	}

	if (g_HumanActionResult.p_hair == NULL)
	{
		return ST_E_FAIL;
	}

	if (g_HumanActionResult.p_hair->pixel_format != ST_PIX_FMT_GRAY8)
	{
		return ST_E_FAIL;
	}

	int actualBufferSize = SnowSenseTime_GetHairSegmentationBufferSize();
	if (bufferSize < actualBufferSize)
	{
		return ST_E_INVALIDARG;
	}

	st_image_t* hairSegBuffer = g_HumanActionResult.p_hair;

	memcpy(buffer, hairSegBuffer->data, actualBufferSize);

	*width = hairSegBuffer->width;
	*height = hairSegBuffer->height;
	*stride = hairSegBuffer->stride;

	return ST_OK;
}


st_handle_t g_SLAMHandle;

st_slam_result g_SLAMResult;
extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowSenseTime_CreateSLAMSession(int width, int height, float fovx, st_rotate_type toRotateBy)
{
    // We only support one SLAM instance
    if (g_SLAMHandle != NULL)
    {
        return ST_E_FAIL;
    }

    int ret = st_mobile_slam_create(&g_SLAMHandle, width, height, fovx, (STSLAMOrientation)toRotateBy, true);
    if (ret != ST_OK)
        return ret;

    // Succeed to construct new SLAM detector
    {
        memset(&g_SLAMResult, 0, sizeof(st_slam_result));
    }
    return ret;
}
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowSenseTime_DestroySLAMSession()
{
    if (g_SLAMHandle != NULL)
    {
        st_mobile_slam_destroy(g_SLAMHandle);
        g_SLAMHandle = NULL;
    }
}
extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowSenseTime_UpdateSLAMSession(
        st_image_t image,
        st_slam_imu* IMUArray, int IMUArraySize,
        st_slam_attitude attitude)
{
    return st_mobile_slam_run(g_SLAMHandle, image, IMUArray, IMUArraySize, attitude, &g_SLAMResult);
}

// Important to call this between change of sticker or front/back camera
extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowSenseTime_ResetSLAM()
{
    if (g_SLAMHandle == NULL)
    {
        return ST_E_FAIL;
    }

    st_mobile_slam_reset(g_SLAMHandle);
    return ST_OK;
}

extern "C" int UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowSenseTime_GetCameraPose(st_point4f_t* quaternion, st_point3f_t* center)
{
    if (g_SLAMHandle == NULL)
    {
        return ST_E_FAIL;
    }
    memcpy(quaternion, g_SLAMResult.camera.quaternion, sizeof(float) * 4);
	memcpy(center, g_SLAMResult.camera.center, sizeof(float) * 3);

    return ST_OK;
}
