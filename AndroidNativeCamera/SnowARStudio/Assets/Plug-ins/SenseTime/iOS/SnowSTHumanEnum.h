//
//  SensetimeCommon.cpp
//  SnowSenseTime
//
//  Created by heejun on 2018. 12. 26..
//  Copyright © 2018년 SnowUnity. All rights reserved.
//

// common snow enum values, this enums should be sync with SenseTimeLib.cs
#include <vector>
#include <stdio.h>
#include "st_mobile_human_action.h"

static const int FACE_FEATURE_POINTS_ARRAY_SIZE = 106;
static const int FACE_EXTRA_POINTS_ARRAY_MAX_SIZE = 134;
static const int IRIS_CENTER_POINTS_ARRAY_MAX_SIZE = 2;
static const int IRIS_CONTOUR_POINTS_ARRAY_MAX_SIZE = 38;
static const int HAND_KEY_POINTS_ARRAY_MAX_SIZE = 20;
static const int BODY_KEYPOINTS_ARRAY_MAX_SIZE = 14;
static const int BODY_CONTOUR_ARRAY_MAX_SIZE = 59;

enum class DetectType
{
    kFace,
    kFaceExtra,
    kEyeballCenter,
    kEyeballContour,
    kHairSegment,
    kHand,
    kBodyKeyPoints,
    kBodyContour,
    kBackgroundSegment,
};

enum class FaceAction
{
    kEyeBlink,
    kMouthAh,
    kHeadYaw,
    kHeadPitch,
    kBrowJump,
};

enum class HandAction
{
    kOK,
    kScissor,
    kGood,
    kPalm,
    kPistol,
    kLove,
    kHoldup,
    kCongratulate,
    kFingerHeart,
    kFingerIndex,
    kFist,
};

enum class HeadExpression
{
    kHeadNormal,
    kSideFaceLeft,
    kSideFaceRight,
    kTiltedFaceLeft,
    kTiltedFaceRight,
    kHeadRise,
    kHeadLower,
};

enum class EyeExpression
{
    kTwoEyeClose,
    kTwoEyeOpen,
    kLeftEyeOpenRightEyeClose,
    kLeftEyeCloseRightEyeOpen,
};

enum class MouthExpression
{
    kMouthOpen,
    kMouthClose,
    kLipsUpward,
    kLipsPouted,
    kLipsCurlLeft,
    kLipsCurlRight,
};

// bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible.
struct FaceActionData
{
    unsigned char eyeBlink;
    unsigned char mouthAh;
    unsigned char headYaw;
    unsigned char headPitch;
    unsigned char browJump;
};

// bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible.
struct HandActionData
{
    unsigned char ok;
    unsigned char scissor;
    unsigned char good;
    unsigned char palm;
    unsigned char pistol;
    unsigned char love;
    unsigned char holdUp;
    unsigned char congratulate;
    unsigned char fingerHeart;
    unsigned char fingerIndex;
    unsigned char fist;
};

struct HeadExpressionData
{
    unsigned char headNormal;
    unsigned char sideFaceLeft;
    unsigned char sideFaceRight;
    unsigned char tiltedFaceLeft;
    unsigned char tiltedFaceRight;
    unsigned char headRise;
    unsigned char headLower;
};

struct EyeExpressionData
{
    unsigned char twoEyeClose;
    unsigned char twoEyeOpen;
    unsigned char leftEyeOpenRightEyeClose;
    unsigned char leftEyeCloseRightEyeOpen;
};

struct MouthExpressionData
{
    unsigned char mouthOpen;
    unsigned char mouthClose;
    unsigned char lipsUpward;
    unsigned char lipsPouted;
    unsigned char lipsCurlLeft;
    unsigned char lipsCurlRight;
};

struct ExpressionData
{
    HeadExpressionData head;
    EyeExpressionData eye;
    MouthExpressionData mouth;
};
