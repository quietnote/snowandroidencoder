#include "IUnityGraphics.h"

extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CreateSnowNativeVideoRecorder(int width, int height, int startX, int startY, int audioChannels, int audioFrequency);

static void UNITY_INTERFACE_API RenderEvent_SnowNativeVideoRecorder_PullSnapshot(int eventID, void* data);

extern "C" UnityRenderingEventAndData UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetSnowNativeVideoRecorderRenderEventFunc();

extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CreateSnowNativeCapture(int width, int height, int startX, int startY);

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API DestroySnowNativeVideoCapture(long token);

static void UNITY_INTERFACE_API RenderEvent_SnowNativeVideoCapture_PullSnapshot(int eventID, void* data);

extern "C" UnityRenderingEventAndData UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetSnowNativeVideoCaptureRenderEventFunc();

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowNativeVideoRecorder_FillSoundData(long token, float* array, int length);

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API DestroySnowNativeVideoRecorder(long token);
//By Joseph begin
extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API NativeCapture(uint8_t* pixels, int width, int height);

extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API NativeStartVideoRecord(int width, int height, int audioChannels, int audioFrequency);

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API NativeFillVideoData(long token, uint8_t* pixels, int width, int height);
//By Joseph end
