#import "SnowVideoRecorder.h"

#include <OpenGLES/ES2/glext.h>
#include <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@implementation SaveToAlbumHandler

+ (void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    NSString* path = (__bridge_transfer NSString *)(contextInfo);
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

+ (void)video:(NSString *)videoPath finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    NSString* path = (__bridge_transfer NSString *)(contextInfo);
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

@end

enum BitRateMode
{
    kLow,
    kMedium,
    kHigh,
};

static int GetVideoBitrate(const uint width, const uint height, const BitRateMode bitRateMode)
{
    // Values are empirically defined using a 1080 stream as the basis for scaling
    int base1080pBitRate;
    switch(bitRateMode)
    {
        case kLow:
            base1080pBitRate = 2300000;
            
        case kMedium:
            base1080pBitRate = 5400166;
            
        case kHigh:
            base1080pBitRate = 8000000;
    }
    return base1080pBitRate;
    //return base1080pBitRate * (float)(width * height) / (float)(1920 * 1080);
}

static int GetAudioBitrate(const BitRateMode bitRateMode)
{
    int bitRate;
    switch(bitRateMode)
    {
        case kLow:
            bitRate = 20000;
            break;
        case kHigh:
            bitRate = 80000;
            break;
        case kMedium:
        default:
            bitRate = 40000;
            break;
    }
    return bitRate;
}

static CVPixelBufferRef CVPixelBufferFromImage(VideoFrameData data)
{
    CVPixelBufferRef pixelBuffer = NULL;
    CVPixelBufferCreate(kCFAllocatorDefault, data.width, data.height, kCVPixelFormatType_32BGRA, NULL, &pixelBuffer);
    CVPixelBufferLockBaseAddress(pixelBuffer, 0);
    
    uint8_t* pxdata = (uint8_t*)CVPixelBufferGetBaseAddress(pixelBuffer);
    const size_t pixelBytesPerRow = (size_t)CVPixelBufferGetBytesPerRow(pixelBuffer);
    
    for (int row = 0; row < data.height; ++row)
    {
        uint8_t* pixel = pxdata + (row * pixelBytesPerRow);
        const uint8_t* buffer = data.data + ((data.height - 1 - row) * data.bytesPerRow);
        
        for (int column = 0; column < data.width; ++column)
        {
            // BGRA <- RGBA
            // Alpha not supported
            pixel[0] = buffer[2];
            pixel[1] = buffer[1];
            pixel[2] = buffer[0];
            pixel[3] = (uint8_t)255;
            
            pixel += 4;
            buffer += 4;
        }
    }
    
    CVPixelBufferUnlockBaseAddress(pixelBuffer, 0);
    return pixelBuffer;
}

bool SnowVideoRecorder::StartRecording(uint width, uint height, uint startX, uint startY, uint audioChannels, uint audioFrequency)
{
    m_startX = startX;
    m_startY = startY;
    if (width < 34 || height < 26)
    {
        // 34x26 is empirically measured as being the smallest clip size that will work with
        // our libraries (AVFoundation and MediaFoundation) and codecs (H264)
        return false;
    }
    
    NSArray* searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentPath = [searchPaths objectAtIndex:0];
    NSString* fileName = [NSString stringWithFormat:@"%@_%@", [[NSProcessInfo processInfo] globallyUniqueString], @"file.MOV"];
    NSURL* tempFileURL = [[NSURL alloc] initFileURLWithPath:[NSString pathWithComponents:@[documentPath, fileName]]];
    
    // Remove the temporary file if it already exists
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:tempFileURL error:nil];
    
    
    
    // Write to the temporary file
    NSError* localError = nil;
    m_AssetWriter = [[AVAssetWriter alloc] initWithURL:tempFileURL fileType:AVFileTypeMPEG4 error:&localError];
    if (m_AssetWriter == nil)
    {
        // Failed to create asset writer
        return false;
    }
    
    // Set up video input
    {
        const int bitRate = GetVideoBitrate(width, height, kHigh);//Medium);  // for high bitrate mode
        NSDictionary* codecSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithInt:bitRate], AVVideoAverageBitRateKey,
                                       nil];
        
        // Make 720P video
        if(width > 1080 || height > 1080)
        {
            if(height > width*1.7)      // 16:9 or higher
            {
                m_VideoRecordingType = videoRecordingTypeFull;
                height = (float)height / ((float)width/720);
                width = 720;
            }
            else if(width == height)    // 1:1
            {
                m_VideoRecordingType = videoRecordingType1to1;
                width = 720;
                height = 720;
            }
            else                        // 4:3
            {
                m_VideoRecordingType = videoRecordingType4to3;
                width = 720;
                height = 960;
            }
        }
        
        NSMutableDictionary* videoSettings = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                              AVVideoCodecH264, AVVideoCodecKey,
                                              codecSettings, AVVideoCompressionPropertiesKey,
                                              [NSNumber numberWithDouble: (double)width], AVVideoWidthKey,
                                              [NSNumber numberWithDouble: (double)height], AVVideoHeightKey,
                                              nil];
        
        m_VideoOutput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
        m_VideoOutput.expectsMediaDataInRealTime = YES;
        
        m_VideoWriterAdaptor = [
                                [AVAssetWriterInputPixelBufferAdaptor alloc]
                                initWithAssetWriterInput:m_VideoOutput
                                sourcePixelBufferAttributes:
                                [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithInt: kCVPixelFormatType_32BGRA], kCVPixelBufferPixelFormatTypeKey,
                                 nil]];
        
        [m_AssetWriter addInput:m_VideoOutput];
    }
    
    // Set up audio input
    {
        m_AudioFormat.mSampleRate = audioFrequency;
        m_AudioFormat.mFormatID = kAudioFormatLinearPCM;
        m_AudioFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat | kAudioFormatFlagIsAlignedHigh;
        m_AudioFormat.mFramesPerPacket = 1;
        m_AudioFormat.mChannelsPerFrame = audioChannels;
        m_AudioFormat.mBitsPerChannel = 8 * sizeof(float);
        m_AudioFormat.mBytesPerPacket = sizeof(float) * m_AudioFormat.mChannelsPerFrame;
        m_AudioFormat.mBytesPerFrame = m_AudioFormat.mBytesPerPacket;
        
        CMAudioFormatDescriptionCreate(kCFAllocatorDefault, &m_AudioFormat, 0, NULL, 0, NULL, NULL, &m_AudioFormatDesc);
        
        NSDictionary* const audioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [NSNumber numberWithUnsignedInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                             [NSNumber numberWithUnsignedInt: GetAudioBitrate(kHigh)], AVEncoderBitRatePerChannelKey,
                                             nil];
        
        m_AudioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioSettings sourceFormatHint:m_AudioFormatDesc];
        m_AudioInput.expectsMediaDataInRealTime = YES;
        
        [m_AssetWriter addInput:m_AudioInput];
    }
    
    [m_AssetWriter startWriting];
    
    m_AudioFrameIndex = 0;
    
    m_RecordingStartTime = CACurrentMediaTime();
    CMTime zeroTime = CMTimeMakeWithSeconds(0, NSEC_PER_SEC);
    [m_AssetWriter startSessionAtSourceTime: zeroTime];
    
    m_SaveToAlbumHandler = [[SaveToAlbumHandler alloc] init];
    
    return true;
}

// Called from render thread
void SnowVideoRecorder::PullSnapshot()
{
    // Called from the Unity Rendering Thread through IssuePluginEvent
    // Assumes to be called regularly after startRecording is called
    
    // Collect snapshot data on rendering thread
    VideoFrameData newData;
    {
        int originalHeight = 0;
        GLint viewportParams[4];
        glGetIntegerv(GL_VIEWPORT, viewportParams);
        newData.width = viewportParams[2];
        newData.height = viewportParams[3];
        newData.bytesPerRow = newData.width * 4; // RGBA with no padding
        switch(m_VideoRecordingType)
        {
            case videoRecordingTypeFull:
            default:
                newData.recordingType = videoRecordingTypeFull;
                newData.data = (uint8_t*)malloc(newData.height * newData.bytesPerRow);
                break;
            case videoRecordingType4to3:
                originalHeight = newData.height;
                newData.recordingType = videoRecordingType4to3;
                newData.height = (uint)((float)newData.width*1.333f);     // change height to width size
                newData.data = (uint8_t*)malloc(newData.height * newData.bytesPerRow);
                break;
            case videoRecordingType1to1:
                originalHeight = newData.height;
                newData.recordingType = videoRecordingType1to1;
                newData.height = newData.width;     // change height to width size
                newData.data = (uint8_t*)malloc(newData.height * newData.bytesPerRow);
                break;
        }
        
        newData.time = CACurrentMediaTime();
        
        // Get pixel data from framebuffer
        {
            GLint drawFboID(0);
            GLint readFboID(0);
            glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING_APPLE, &drawFboID);
            glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING_APPLE, &readFboID);
            glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, drawFboID);
            switch(m_VideoRecordingType)
            {
                case videoRecordingTypeFull:
                default:
                    glReadPixels(0, 0, newData.width, newData.height, GL_RGBA, GL_UNSIGNED_BYTE, newData.data);
                    break;
                case videoRecordingType4to3:
                    glReadPixels(m_startX, originalHeight-(m_startY+newData.height), newData.width, newData.height , GL_RGBA, GL_UNSIGNED_BYTE, newData.data);
                    break;
                case videoRecordingType1to1:
                    glReadPixels(m_startX, originalHeight-(m_startY+newData.height), newData.width, newData.height , GL_RGBA, GL_UNSIGNED_BYTE, newData.data);
                    break;
            }
            glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, readFboID);
        }
    }
    
    // Deliver the data to the main thread
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            this->m_VideoFrameQueue.push(newData);
            this->ProcessVideoFrames();
        });
    }
}

void SnowVideoRecorder::ProcessVideoFrames()
{
    while (m_VideoWriterAdaptor.assetWriterInput.readyForMoreMediaData && !m_VideoFrameQueue.empty())
    {
        VideoFrameData data = m_VideoFrameQueue.front();
        
        CMTime timeFromStart = CMTimeMakeWithSeconds(data.time - m_RecordingStartTime, NSEC_PER_SEC);
        CVPixelBufferRef pixelBuffer = CVPixelBufferFromImage(data);
        [m_VideoWriterAdaptor appendPixelBuffer: pixelBuffer withPresentationTime: timeFromStart];

        CVBufferRelease(pixelBuffer);
        delete[] data.data;
        m_VideoFrameQueue.pop();
    }
}

bool SnowVideoRecorder::CaptureImage(uint width, uint height, uint startX, uint startY)
{
    m_startX = startX;
    m_startY = startY;
    
    // Make 720P video
    if(width > 1080 || height > 1080)
    {
        if(height > width*1.7)      // 16:9 or higher
        {
            m_VideoRecordingType = videoRecordingTypeFull;
            height = (float)height / ((float)width/720);
            width = 720;
        }
        else if(width == height)    // 1:1
        {
            m_VideoRecordingType = videoRecordingType1to1;
            width = 720;
            height = 720;
        }
        else                        // 4:3
        {
            m_VideoRecordingType = videoRecordingType4to3;
            width = 720;
            height = 960;
        }
    }
    return true;
}


void SnowVideoRecorder::PullSnapshotForImage()
{
    // Called from the Unity Rendering Thread through IssuePluginEvent
    // Assumes to be called regularly after startRecording is called
    
    // Collect snapshot data on rendering thread
    VideoFrameData newData;
    {
        int originalHeight = 0;
        GLint viewportParams[4];
        glGetIntegerv(GL_VIEWPORT, viewportParams);
        newData.width = viewportParams[2];
        newData.height = viewportParams[3];
        newData.bytesPerRow = newData.width * 4; // RGBA with no padding
        switch(m_VideoRecordingType)
        {
            case videoRecordingTypeFull:
            default:
                newData.recordingType = videoRecordingTypeFull;
                newData.data = (uint8_t*)malloc(newData.height * newData.bytesPerRow);
                break;
            case videoRecordingType4to3:
                originalHeight = newData.height;
                newData.recordingType = videoRecordingType4to3;
                newData.height = (uint)((float)newData.width*1.333f);     // change height to width size
                newData.data = (uint8_t*)malloc(newData.height * newData.bytesPerRow);
                break;
            case videoRecordingType1to1:
                originalHeight = newData.height;
                newData.recordingType = videoRecordingType1to1;
                newData.height = newData.width;     // change height to width size
                newData.data = (uint8_t*)malloc(newData.height * newData.bytesPerRow);
                break;
        }
        
        newData.time = CACurrentMediaTime();
        
        // Get pixel data from framebuffer
        {
            GLint drawFboID(0);
            GLint readFboID(0);
            glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING_APPLE, &drawFboID);
            glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING_APPLE, &readFboID);
            glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, drawFboID);
            switch(m_VideoRecordingType)
            {
                case videoRecordingTypeFull:
                default:
                    glReadPixels(0, 0, newData.width, newData.height, GL_RGBA, GL_UNSIGNED_BYTE, newData.data);
                    break;
                case videoRecordingType4to3:
                    glReadPixels(m_startX, originalHeight-(m_startY+newData.height), newData.width, newData.height , GL_RGBA, GL_UNSIGNED_BYTE, newData.data);
                    break;
                case videoRecordingType1to1:
                    glReadPixels(m_startX, originalHeight-(m_startY+newData.height), newData.width, newData.height , GL_RGBA, GL_UNSIGNED_BYTE, newData.data);
                    break;
            }
            glBindFramebuffer(GL_READ_FRAMEBUFFER_APPLE, readFboID);
        }
    }
    
    // Deliver the data to the main thread
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            this->m_VideoFrameQueue.push(newData);
            this->ProcessVideoFramesForImage();
        });
    }
}


void SnowVideoRecorder::ProcessVideoFramesForImage()
{
    while (!m_VideoFrameQueue.empty())
    {
        VideoFrameData data = m_VideoFrameQueue.front();
        CVPixelBufferRef pixelBuffer = CVPixelBufferFromImage(data);
        
        CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
        CIContext *temporaryContext = [CIContext contextWithOptions:nil];
        CGImageRef videoImage = [temporaryContext
                                 createCGImage:ciImage
                                 fromRect:CGRectMake(0, 0,
                                                     CVPixelBufferGetWidth(pixelBuffer),
                                                     CVPixelBufferGetHeight(pixelBuffer))];
        
        UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
        UIImageWriteToSavedPhotosAlbum(uiImage, nil, nil, nil);
        CGImageRelease(videoImage);
        CVBufferRelease(pixelBuffer);
        delete[] data.data;
        m_VideoFrameQueue.pop();
    }
}

//void SnowVideoRecorder::SaveImage()
//{
//    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
//    CIContext *temporaryContext = [CIContext contextWithOptions:nil];
//    CGImageRef videoImage = [temporaryContext createCGImage:ciImage fromRect:CGRectMake(0, 0, CVPixelBufferGetWidth(pixelBuffer), CVPixelBufferGetHeight(pixelBuffer))];
//
//    UIImage *uiImage = [UIImage imageWithCGImage:videoImage];
//    CGImageRelease(videoImage);
//}

// Called from audio thread
void SnowVideoRecorder::FillSoundData(float* array, int length)
{
    if (length <= 0 || length % m_AudioFormat.mChannelsPerFrame != 0)
        return;
    
    m_AudioSampleQueue.insert(m_AudioSampleQueue.end(), array, array + length);
    
    ProcessSoundFrames();
}

void SnowVideoRecorder::ProcessSoundFrames()
{
    if (m_AudioInput.readyForMoreMediaData && !m_AudioSampleQueue.empty())
    {
        CMBlockBufferRef tempCmBlockBufferRef = NULL;
        const size_t dataSize = m_AudioSampleQueue.size() * sizeof(float);
        OSStatus status = CMBlockBufferCreateWithMemoryBlock(
                                                             kCFAllocatorDefault,
                                                             m_AudioSampleQueue.data(),
                                                             dataSize,
                                                             kCFAllocatorNull,
                                                             NULL,
                                                             0,
                                                             dataSize,
                                                             0,
                                                             &tempCmBlockBufferRef);
        
        CMBlockBufferRef cmBlockBufferRef = NULL;
        CMBlockBufferCreateContiguous(
                                      kCFAllocatorDefault,
                                      tempCmBlockBufferRef,
                                      kCFAllocatorDefault,
                                      NULL,
                                      0,
                                      dataSize,
                                      kCMBlockBufferAlwaysCopyDataFlag,
                                      &cmBlockBufferRef);
        
        CMSampleBufferRef cmSampleBufferRef = NULL;
        const uint64_t numFrames = m_AudioSampleQueue.size() / m_AudioFormat.mChannelsPerFrame;
        const CMTime audioTimeStamp = CMTimeMake(m_AudioFrameIndex, m_AudioFormat.mSampleRate);
        status = CMAudioSampleBufferCreateWithPacketDescriptions(
                                                                 kCFAllocatorDefault,
                                                                 cmBlockBufferRef,
                                                                 TRUE,
                                                                 0,
                                                                 NULL,
                                                                 m_AudioFormatDesc,
                                                                 numFrames,
                                                                 audioTimeStamp,
                                                                 NULL,
                                                                 &cmSampleBufferRef);
        
        const bool appendSuccess = [m_AudioInput appendSampleBuffer: cmSampleBufferRef];
        if (appendSuccess)
        {
            m_AudioFrameIndex += numFrames;
            m_AudioSampleQueue.resize(0);
        }
        
        if (cmSampleBufferRef)
            CFRelease(cmSampleBufferRef);
        
        if (cmBlockBufferRef)
            CFRelease(cmBlockBufferRef);
        
        if (tempCmBlockBufferRef)
            CFRelease(tempCmBlockBufferRef);
    }
}

void SnowVideoRecorder::FinishCapture()
{
    FlushData();
}

void SnowVideoRecorder::FinishRecording()
{
    FlushData();
    
    CMTime timeFromStart = CMTimeMakeWithSeconds(CACurrentMediaTime() - m_RecordingStartTime, NSEC_PER_SEC);
    [m_AssetWriter endSessionAtSourceTime:timeFromStart];
    [m_AssetWriter finishWriting];
    
    NSURL* tempURL = m_AssetWriter.outputURL;
    
    m_SaveToAlbumHandler = NULL;
    
    m_VideoWriterAdaptor = NULL;
    m_VideoOutput = NULL;
    
    if (m_AudioFormatDesc)
        CFRelease(m_AudioFormatDesc);
    m_AudioInput = NULL;
    
    m_AssetWriter = NULL;
    
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:tempURL completionBlock:^(NSURL *assetURL, NSError *error) {
        [[NSFileManager defaultManager] removeItemAtURL:tempURL error:NULL];
    }];
}

void SnowVideoRecorder::FlushData()
{
    int numConsecutiveUnsuccessfulAttempts = 0;
    int numQueuesStillActive;
    
    do
    {
        numQueuesStillActive = 0;
        
        if (m_VideoFrameQueue.empty())
        {
            [m_VideoOutput markAsFinished];
        }
        else
        {
            ++numQueuesStillActive;
            size_t sizeBefore = m_VideoFrameQueue.size();
            ProcessVideoFrames();
            size_t sizeAfter = m_VideoFrameQueue.size();
            
            if (sizeAfter == sizeBefore)
                ++numConsecutiveUnsuccessfulAttempts;
            else
                numConsecutiveUnsuccessfulAttempts = 0;
        }
        
        if (m_AudioSampleQueue.empty())
        {
            [m_AudioInput markAsFinished];
        }
        else
        {
            ++numQueuesStillActive;
            size_t sizeBefore = m_AudioSampleQueue.size();
            ProcessSoundFrames();
            size_t sizeAfter = m_AudioSampleQueue.size();
            
            if (sizeAfter == sizeBefore)
                ++numConsecutiveUnsuccessfulAttempts;
            else
                numConsecutiveUnsuccessfulAttempts = 0;
        }
        
        // Give up after 1000 consecutive unsuccessful attempts
        if (numConsecutiveUnsuccessfulAttempts > 1000)
        {
            break;
        }
        
        // Every 20 consecutive unsuccessful attempts, sleep a while
        // Leaves time for the encoder thread to do its work
        if (0 == (numConsecutiveUnsuccessfulAttempts % 20))
        {
            [NSThread sleepForTimeInterval: 0.05];
        }
    }
    while (numQueuesStillActive > 0);
    
    // Clear the buffers in any case
    {
        if (!m_VideoFrameQueue.empty())
        {
            while (!m_VideoFrameQueue.empty())
            {
                VideoFrameData data = m_VideoFrameQueue.front();
                delete[] data.data;
                m_VideoFrameQueue.pop();
            }
        }
        
        m_AudioSampleQueue.resize(0);
    }
}

bool SnowVideoRecorder::CaptureImage(uint8_t* pixels, uint width, uint height)
{
    VideoFrameData data;
    SetVideoFrames(&data, pixels, width, height);
    
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            this->m_VideoFrameQueue.push(data);
            this->ProcessVideoFramesForImage();
        });
    }
}

bool SnowVideoRecorder::StartRecording(uint width, uint height, uint audioChannels, uint audioFrequency)
{
    if (width < 34 || height < 26)
    {
        // 34x26 is empirically measured as being the smallest clip size that will work with
        // our libraries (AVFoundation and MediaFoundation) and codecs (H264)
        return false;
    }
    
    NSArray* searchPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* documentPath = [searchPaths objectAtIndex:0];
    NSString* fileName = [NSString stringWithFormat:@"%@_%@", [[NSProcessInfo processInfo] globallyUniqueString], @"file.MOV"];
    NSURL* tempFileURL = [[NSURL alloc] initFileURLWithPath:[NSString pathWithComponents:@[documentPath, fileName]]];
    
    // Remove the temporary file if it already exists
    NSFileManager* fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtURL:tempFileURL error:nil];
 
    // Write to the temporary file
    NSError* localError = nil;
    m_AssetWriter = [[AVAssetWriter alloc] initWithURL:tempFileURL fileType:AVFileTypeMPEG4 error:&localError];
    if (m_AssetWriter == nil)
    {
        // Failed to create asset writer
        return false;
    }

    // Set up video input
    {
        const int bitRate = GetVideoBitrate(width, height, kHigh);//Medium);  // for high bitrate mode
        NSDictionary* codecSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithInt:bitRate], AVVideoAverageBitRateKey,
                                       nil];

        NSMutableDictionary* videoSettings = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                              AVVideoCodecH264, AVVideoCodecKey,
                                              codecSettings, AVVideoCompressionPropertiesKey,
                                              [NSNumber numberWithDouble: (double)width], AVVideoWidthKey,
                                              [NSNumber numberWithDouble: (double)height], AVVideoHeightKey,
                                              nil];

        m_VideoOutput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
        m_VideoOutput.expectsMediaDataInRealTime = YES;
        
        m_VideoWriterAdaptor = [
                                [AVAssetWriterInputPixelBufferAdaptor alloc]
                                initWithAssetWriterInput:m_VideoOutput
                                sourcePixelBufferAttributes:
                                [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithInt: kCVPixelFormatType_32BGRA], kCVPixelBufferPixelFormatTypeKey,
                                 nil]];

        [m_AssetWriter addInput:m_VideoOutput];
    }
    
    // Set up audio input
    {
        m_AudioFormat.mSampleRate = audioFrequency;
        m_AudioFormat.mFormatID = kAudioFormatLinearPCM;
        m_AudioFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat | kAudioFormatFlagIsAlignedHigh;
        m_AudioFormat.mFramesPerPacket = 1;
        m_AudioFormat.mChannelsPerFrame = audioChannels;
        m_AudioFormat.mBitsPerChannel = 8 * sizeof(float);
        m_AudioFormat.mBytesPerPacket = sizeof(float) * m_AudioFormat.mChannelsPerFrame;
        m_AudioFormat.mBytesPerFrame = m_AudioFormat.mBytesPerPacket;
        
        CMAudioFormatDescriptionCreate(kCFAllocatorDefault, &m_AudioFormat, 0, NULL, 0, NULL, NULL, &m_AudioFormatDesc);
        
        NSDictionary* const audioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                             [NSNumber numberWithUnsignedInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
                                             [NSNumber numberWithUnsignedInt: GetAudioBitrate(kHigh)], AVEncoderBitRatePerChannelKey,
                                             nil];
        
        m_AudioInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeAudio outputSettings:audioSettings sourceFormatHint:m_AudioFormatDesc];
        m_AudioInput.expectsMediaDataInRealTime = YES;
        
        [m_AssetWriter addInput:m_AudioInput];
    }
    
    [m_AssetWriter startWriting];
    
    m_AudioFrameIndex = 0;
    
    m_RecordingStartTime = CACurrentMediaTime();
    CMTime zeroTime = CMTimeMakeWithSeconds(0, NSEC_PER_SEC);
    [m_AssetWriter startSessionAtSourceTime: zeroTime];
    
    m_SaveToAlbumHandler = [[SaveToAlbumHandler alloc] init];
    
    return true;
}

void SnowVideoRecorder::FillVideoData(uint8_t* pixels, uint width, uint height)
{
    VideoFrameData data;
    SetVideoFrames(&data, pixels, width, height);

    {
        dispatch_async(dispatch_get_main_queue(), ^{
            this->m_VideoFrameQueue.push(data);
            this->ProcessVideoFrames();
        });
    }
}

void SnowVideoRecorder::SetVideoFrames(VideoFrameData* data, uint8_t* pixels, uint width, uint height)
{
    data->time = CACurrentMediaTime();
    data->width = width;
    data->height = height;
    data->bytesPerRow = data->width * 4; // RGBA with no padding
    data->data = new uint8_t[data->height * data->bytesPerRow];
    memcpy(data->data, pixels, sizeof(uint8_t) * data->height * data->bytesPerRow);
    data->recordingType = videoRecordingTypeFull;
}
