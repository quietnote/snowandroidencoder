#include "PluginLab.h"
#include "SnowVideoRecorder.h"

#pragma iOSNativeCamera
extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CreateSnowNativeVideoRecorder(int width, int height, int startX, int startY, int audioChannels, int audioFrequency)
{
    SnowVideoRecorder* recorder = new SnowVideoRecorder();
    if (recorder->StartRecording(width, height, startX, startY, audioChannels, audioFrequency))
    {
        return (long)recorder;
    }
    
    return 0;
}


static void UNITY_INTERFACE_API RenderEvent_SnowNativeVideoRecorder_PullSnapshot(int eventID, void* data)
{
    SnowVideoRecorder* recorder = (SnowVideoRecorder*)data;
    recorder->PullSnapshot();
}

extern "C" UnityRenderingEventAndData UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetSnowNativeVideoRecorderRenderEventFunc()
{
    return RenderEvent_SnowNativeVideoRecorder_PullSnapshot;
}


extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API CreateSnowNativeCapture(int width, int height, int startX, int startY)
{
    SnowVideoRecorder* recorder = new SnowVideoRecorder();
    if (recorder->CaptureImage(width, height, startX, startY))
    {
        return (long)recorder;
    }
    
    return 0;
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API DestroySnowNativeVideoCapture(long token)
{
    SnowVideoRecorder* recorder = (SnowVideoRecorder*)token;
    recorder->FinishCapture();
    delete recorder;
}

extern "C" UnityRenderingEventAndData UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetSnowNativeVideoCaptureRenderEventFunc()
{
    return RenderEvent_SnowNativeVideoCapture_PullSnapshot;
}

static void UNITY_INTERFACE_API RenderEvent_SnowNativeVideoCapture_PullSnapshot(int eventID, void* data)
{
    SnowVideoRecorder* recorder = (SnowVideoRecorder*)data;
    recorder->PullSnapshotForImage();
}


extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SnowNativeVideoRecorder_FillSoundData(long token, float* array, int length)
{
    SnowVideoRecorder* recorder = (SnowVideoRecorder*)token;
    recorder->FillSoundData(array, length);
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API DestroySnowNativeVideoRecorder(long token)
{
    SnowVideoRecorder* recorder = (SnowVideoRecorder*)token;
    recorder->FinishRecording();
    delete recorder;
}

//By Joseph begin
extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API NativeCapture(uint8_t* pixels, int width, int height)
{
    SnowVideoRecorder* recorder = new SnowVideoRecorder();
    if (recorder->CaptureImage(pixels, width, height))
    {
        return (long)recorder;
    }
    
    return 0;
}

extern "C" long UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API NativeStartVideoRecord(int width, int height, int audioChannels, int audioFrequency)
{
    SnowVideoRecorder* recorder = new SnowVideoRecorder();
    if (recorder->StartRecording(width, height, audioChannels, audioFrequency))
    {
        return (long)recorder;
    }
    
    return 0;
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API NativeFillVideoData(long token, uint8_t* pixels, int width, int height)
{
    SnowVideoRecorder* recorder = (SnowVideoRecorder*)token;
    recorder->FillVideoData(pixels, width, height);
}
//By Joseph end
