#pragma once

#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAudioSession.h>
#include <queue>
#include <string>

typedef enum VideoRecordingType
{
    videoRecordingType1to1,
    videoRecordingType4to3,
    videoRecordingTypeFull
}VideoRecordingType;

struct VideoFrameData
{
    double      time;
    uint        width;
    uint        height;
    uint        bytesPerRow;
    uint8_t*    data;
    VideoRecordingType recordingType;
};

@interface SaveToAlbumHandler: NSObject

+ (void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;
+ (void)video:(NSString *)videoPath finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;

@end

class SnowVideoRecorder
{
public:
    bool StartRecording(uint width, uint height, uint startX, uint startY, uint audioChannels, uint audioFrequency);
    bool CaptureImage(uint width, uint height, uint startX, uint startY);
    //void SaveImage();
    void PullSnapshot();
    void PullSnapshotForImage();
    void FillSoundData(float* array, int length);
    void FinishRecording();
    void FinishCapture();

    //By Joseph begin
    bool CaptureImage(uint8_t* pixels, uint width, uint height);
    bool StartRecording(uint width, uint height, uint audioChannels, uint audioFrequency);
    void FillVideoData(uint8_t* pixels, uint width, uint height);
    //By Joseph end

private:
    void SetVideoFrames(VideoFrameData* data, uint8_t* pixels, uint width, uint height);
    void ProcessVideoFrames();
    void ProcessVideoFramesForImage();
    void ProcessSoundFrames();
    void FlushData();
    
private:
    AVAssetWriter* m_AssetWriter;
    double m_RecordingStartTime;
    
    AVAssetWriterInput* m_VideoOutput;
    AVAssetWriterInputPixelBufferAdaptor* m_VideoWriterAdaptor;
    std::queue<VideoFrameData> m_VideoFrameQueue;

    AVAssetWriterInput* m_AudioInput;
    AudioStreamBasicDescription m_AudioFormat;
    CMAudioFormatDescriptionRef m_AudioFormatDesc;
    uint64_t m_AudioFrameIndex;
    std::vector<float> m_AudioSampleQueue;
    
    SaveToAlbumHandler* m_SaveToAlbumHandler;
    VideoRecordingType m_VideoRecordingType;
    //RecordRatio m_RecordRatio;
    uint m_startX;
    uint m_startY;
};


