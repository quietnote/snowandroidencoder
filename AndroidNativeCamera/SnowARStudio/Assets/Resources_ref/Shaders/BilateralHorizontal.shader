// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ARStudio/SkinSmoothing/BilateralHorizontal"
{
	Properties
	{
		_MainTex("_MainTex", 2D) = "white" {}
		_filterStep("filterStep", Range( 0 , 3)) = 1
		_distanceNormalizationFactor("distanceNormalizationFactor", Range( 0 , 5)) = 2.746
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _distanceNormalizationFactor;
		uniform float _filterStep;
		uniform sampler2D _Sampler0132;
		uniform float4 _MainTex_TexelSize;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float gaussianWeight0202 = 0.18;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode94 = tex2D( _MainTex, uv_MainTex );
			float4 tex2DNode128 = tex2D( _MainTex, uv_MainTex );
			float distanceNormalizationFactor173 = _distanceNormalizationFactor;
			float temp_output_104_0 = ( gaussianWeight0202 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode128 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float gaussianWeight4201 = 0.05;
			float filterStep164 = _filterStep;
			float texelSize155 = min( _MainTex_TexelSize.x , _MainTex_TexelSize.y );
			float2 appendResult18 = (float2(( filterStep164 * -4.0 * texelSize155 ) , 0.0));
			float4 tex2DNode41 = tex2D( _MainTex, ( uv_MainTex + appendResult18 ) );
			float temp_output_11_0 = ( gaussianWeight4201 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode41 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float gaussianWeight3200 = 0.09;
			float2 appendResult17 = (float2(( filterStep164 * -3.0 * texelSize155 ) , 0.0));
			float4 tex2DNode123 = tex2D( _MainTex, ( uv_MainTex + appendResult17 ) );
			float temp_output_111_0 = ( gaussianWeight3200 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode123 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float gaussianWeight2199 = 0.12;
			float2 appendResult63 = (float2(( filterStep164 * -2.0 * texelSize155 ) , 0.0));
			float4 tex2DNode124 = tex2D( _MainTex, ( uv_MainTex + appendResult63 ) );
			float temp_output_107_0 = ( gaussianWeight2199 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode124 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float gaussianWeight1198 = 0.15;
			float2 appendResult14 = (float2(( filterStep164 * -1.0 * texelSize155 ) , 0.0));
			float4 tex2DNode84 = tex2D( _MainTex, ( uv_MainTex + appendResult14 ) );
			float temp_output_122_0 = ( gaussianWeight1198 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode84 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float2 appendResult54 = (float2(( filterStep164 * 1.0 * texelSize155 ) , 0.0));
			float4 tex2DNode95 = tex2D( _MainTex, ( uv_MainTex + appendResult54 ) );
			float temp_output_106_0 = ( gaussianWeight1198 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode95 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float2 appendResult53 = (float2(( filterStep164 * 2.0 * texelSize155 ) , 0.0));
			float4 tex2DNode93 = tex2D( _MainTex, ( uv_MainTex + appendResult53 ) );
			float temp_output_108_0 = ( gaussianWeight2199 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode93 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float2 appendResult42 = (float2(( filterStep164 * 3.0 * texelSize155 ) , 0.0));
			float4 tex2DNode70 = tex2D( _MainTex, ( uv_MainTex + appendResult42 ) );
			float temp_output_89_0 = ( gaussianWeight3200 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode70 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float2 appendResult112 = (float2(( filterStep164 * 4.0 * texelSize155 ) , 0.0));
			float4 tex2DNode67 = tex2D( _MainTex, ( uv_MainTex + appendResult112 ) );
			float temp_output_105_0 = ( gaussianWeight4201 * ( 1.0 - min( ( distance( tex2DNode94 , tex2DNode67 ) * distanceNormalizationFactor173 ) , 1.0 ) ) );
			float gaussianWeightTotal192 = ( temp_output_104_0 + temp_output_11_0 + temp_output_111_0 + temp_output_107_0 + temp_output_122_0 + temp_output_106_0 + temp_output_108_0 + temp_output_89_0 + temp_output_105_0 );
			float4 temp_output_151_0 = ( ( ( temp_output_104_0 * tex2DNode128 ) + ( temp_output_11_0 * tex2DNode41 ) + ( temp_output_111_0 * tex2DNode123 ) + ( temp_output_107_0 * tex2DNode124 ) + ( temp_output_122_0 * tex2DNode84 ) + ( temp_output_106_0 * tex2DNode95 ) + ( temp_output_108_0 * tex2DNode93 ) + ( temp_output_89_0 * tex2DNode70 ) + ( temp_output_105_0 * tex2DNode67 ) ) / gaussianWeightTotal192 );
			float temp_output_1_0_g1 = ( ( gaussianWeightTotal192 - 0.4 ) / 0.1 );
			float4 temp_output_141_0 = ( ( temp_output_151_0 * ( 1.0 - temp_output_1_0_g1 ) ) + ( tex2DNode94 * temp_output_1_0_g1 ) );
			float4 ifLocalVar140 = 0;
			if( gaussianWeightTotal192 >= 0.4 )
				ifLocalVar140 = temp_output_141_0;
			else
				ifLocalVar140 = tex2DNode94;
			float4 ifLocalVar138 = 0;
			if( gaussianWeightTotal192 >= 0.5 )
				ifLocalVar138 = temp_output_151_0;
			else
				ifLocalVar138 = ifLocalVar140;
			o.Emission = ifLocalVar138.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15900
640;186;1820;1105;2545.354;2203.942;1;True;True
Node;AmplifyShaderEditor.CommentaryNode;213;-2518.74,-1896.582;Float;False;783.2252;254.5;Comment;3;132;133;155;TexelSize;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;252;-2832.94,-1106.207;Float;False;587.6216;307.5;Comment;2;76;164;FilterStep;1,1,1,1;0;0
Node;AmplifyShaderEditor.TexelSizeNode;132;-2468.74,-1846.582;Float;False;94;1;0;SAMPLER2D;_Sampler0132;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMinOpNode;133;-2165.721,-1818.889;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;76;-2782.94,-1056.207;Float;True;Property;_filterStep;filterStep;1;0;Create;True;0;0;False;0;1;1.35;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;251;-2012.352,1352.32;Float;False;2428.565;408.2671;Comment;17;170;35;161;29;53;46;93;180;40;87;33;75;226;189;108;242;125;CenterPixel+2;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;164;-2478.818,-1055.975;Float;False;filterStep;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;234;-2018.448,-232.0263;Float;False;2420.747;398.0119;Comment;17;167;28;158;36;63;65;124;19;177;21;61;116;186;223;107;233;117;CenterPixel-2;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;230;-2005.667,-1285.289;Float;False;2412.461;405.215;Comment;17;165;5;10;18;13;41;25;175;66;44;71;184;216;11;220;103;156;CenterPixel-4;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;241;-2021.27,795.4209;Float;False;2435.779;416.2022;Comment;17;49;54;47;95;225;31;160;169;60;55;113;188;106;179;58;6;240;CenterPixel+1;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;248;-2019.954,1885.915;Float;False;2441.214;406.5054;Comment;17;162;34;171;82;42;50;70;181;48;115;56;74;190;227;89;247;77;CenterPixel+3;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;155;-1969.015,-1823.796;Float;False;texelSize;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;232;-2016.518,-771.2347;Float;False;2423.901;422.7506;Comment;17;16;166;157;64;17;59;123;30;176;7;126;98;185;222;111;221;118;CenterPixel-3;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;250;-2010.794,2415.579;Float;False;2453.156;412.658;Comment;17;172;20;163;52;112;51;67;8;182;43;57;114;191;105;228;22;249;CenterPixel+4;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;236;-2018.766,278.4408;Float;False;2426.773;390.5891;Comment;17;27;168;159;23;14;45;84;178;39;24;110;97;187;224;122;127;239;CenterPixel-1;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;165;-1955.667,-1138.632;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;34;-1932.69,2089.958;Float;False;Constant;_Float27;Float 27;15;0;Create;True;0;0;False;0;3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;161;-1959.272,1646.087;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;162;-1969.954,2177.92;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-1930.613,1553.862;Float;False;Constant;_Float29;Float 29;15;0;Create;True;0;0;False;0;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;170;-1962.352,1468.678;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-1937.397,-36.70358;Float;False;Constant;_Float16;Float 16;15;0;Create;True;0;0;False;0;-2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;168;-1966.863,382.1498;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;156;-1954.63,-957.9392;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;160;-1971.27,1097.123;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;171;-1963.37,2003.109;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1941.387,1016.884;Float;False;Constant;_Float24;Float 24;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;159;-1968.766,554.5299;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;167;-1961.035,-128.7707;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;163;-1960.794,2713.737;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-1926.071,-1049.661;Float;False;Constant;_Float13;Float 13;15;0;Create;True;0;0;False;0;-4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-1923.767,2630.767;Float;False;Constant;_Float34;Float 34;15;0;Create;True;0;0;False;0;4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;172;-1958.406,2546.943;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;166;-1962.708,-654.0645;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;169;-1967.318,932.8738;Float;False;164;filterStep;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-1938.652,-558.2018;Float;False;Constant;_Float14;Float 14;15;0;Create;True;0;0;False;0;-3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;157;-1966.518,-462.9843;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;27;-1953.59,469.6401;Float;False;Constant;_Float17;Float 17;15;0;Create;True;0;0;False;0;-1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;158;-1968.448,51.48565;Float;False;155;texelSize;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-1752.323,1537.694;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1736.022,2613.303;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;-1764.617,1001.69;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1757.552,453.8199;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-1751.854,-51.93092;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-1747.159,2074.126;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;-1744.436,-572.482;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-1733.459,-1064.843;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;17;-1587.878,-574.9681;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;18;-1581.514,-1062.514;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;14;-1585.811,453.2486;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;63;-1585.301,-52.02989;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;53;-1581.347,1538.613;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;131;-1632.276,-1701.864;Float;False;0;94;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;54;-1597.039,1001.249;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;42;-1572.594,2075.045;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;112;-1550.49,2610.269;Float;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;13;-1353.91,-1083.073;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;47;-1363.398,977.7773;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;45;-1367.056,431.2382;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;65;-1367.356,-74.08698;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;32;-1891.743,-2102.588;Float;False;Property;_distanceNormalizationFactor;distanceNormalizationFactor;2;0;Create;True;0;0;False;0;2.746;0.522;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;229;-1231.125,-1726.276;Float;False;1636.448;386.9337;Comment;11;128;96;174;62;91;100;183;207;104;215;119;CenterPixel;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;59;-1368.02,-591.7189;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;50;-1326.92,2058.608;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-1318.846,2588.33;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;46;-1342.309,1518.372;Float;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;93;-1173.329,1489.863;Float;True;Property;_TextureSample14;Texture Sample 14;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;84;-1185.761,402.8675;Float;True;Property;_TextureSample13;Texture Sample 13;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;95;-1188.213,949.3434;Float;True;Property;_TextureSample16;Texture Sample 16;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;124;-1181.096,-100.966;Float;True;Property;_TextureSample12;Texture Sample 12;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;123;-1188.786,-615.1927;Float;True;Property;_TextureSample11;Texture Sample 11;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;70;-1145.55,2033.142;Float;True;Property;_TextureSample15;Texture Sample 15;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;67;-1141.901,2561.478;Float;True;Property;_TextureSample17;Texture Sample 17;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;94;-1262.55,-2133.467;Float;True;Property;_MainTex;_MainTex;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;41;-1178.597,-1110.074;Float;True;Property;_TextureSample8;Texture Sample 8;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;173;-1533.438,-2101.41;Float;False;distanceNormalizationFactor;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;128;-1181.125,-1569.343;Float;True;Property;_TextureSample9;Texture Sample 9;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Instance;94;Auto;Texture2D;6;0;SAMPLER2D;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;174;-756.0644,-1665.016;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;40;-664.628,1490.088;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;178;-798.0687,337.294;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;96;-700.3787,-1584.961;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;39;-686.0559,420.0877;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;180;-785.956,1402.32;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;182;-748.9615,2470.751;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;175;-754.3158,-1209.564;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;25;-696.1561,-1123.207;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;48;-755.5138,2019.235;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;177;-778.6099,-182.0263;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;212;-2830.463,-654.8491;Float;False;451.8503;475.5206;Comment;10;203;202;205;206;198;201;204;197;200;199;GaussianWeight;1,1,1,1;0;0
Node;AmplifyShaderEditor.DistanceOpNode;19;-685.9403,-105.5408;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;179;-800.4495,845.4209;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;30;-701.4429,-620.0261;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;8;-742.2252,2554.181;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;176;-769.4061,-721.2347;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;181;-765.7134,1938.296;Float;False;173;distanceNormalizationFactor;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;60;-683.4282,938.5976;Float;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;-467.1223,1935.915;Float;False;Constant;_Float26;Float 26;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;87;-478.5083,1404.837;Float;False;Constant;_Float30;Float 30;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;126;-487.1629,-619.7683;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;58;-491.9341,853.5336;Float;False;Constant;_Float28;Float 28;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-492.0094,-76.22221;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;91;-452.1854,-1661.423;Float;False;Constant;_Float10;Float 10;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-493.7543,1488.713;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;203;-2780.463,-604.8491;Float;False;Constant;_Float4;Float 4;3;0;Create;True;0;0;False;0;0.18;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;205;-2779.449,-372.9061;Float;False;Constant;_Float6;Float 6;3;0;Create;True;0;0;False;0;0.09;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-453.8755,-1201.18;Float;False;Constant;_Float11;Float 11;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;57;-448.7565,2469.929;Float;False;Constant;_Float35;Float 35;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-507.258,2021.121;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;55;-500.4091,942.0624;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;62;-483.1979,-1584.206;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;-495.9458,2554.871;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;197;-2779.612,-528.7103;Float;False;Constant;_Float3;Float 3;3;0;Create;True;0;0;False;0;0.15;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-493.7728,420.2502;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;110;-496.3378,334.1135;Float;False;Constant;_Float18;Float 18;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;21;-484.2138,-174.5582;Float;False;Constant;_Float15;Float 15;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;204;-2780.462,-450.8956;Float;False;Constant;_Float5;Float 5;3;0;Create;True;0;0;False;0;0.12;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;206;-2779.449,-293.9037;Float;False;Constant;_Float7;Float 7;3;0;Create;True;0;0;False;0;0.05;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;66;-481.2415,-1122.024;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-464.1867,-707.8987;Float;False;Constant;_Float12;Float 12;13;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;75;-318.9153,1488.434;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;198;-2613.234,-528.3815;Float;False;gaussianWeight1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;113;-331.0609,941.7834;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;199;-2616.272,-450.3867;Float;False;gaussianWeight2;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;200;-2614.247,-373.1807;Float;False;gaussianWeight3;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;98;-300.2793,-620.0472;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;116;-322.8499,-82.27659;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;202;-2616.381,-603.8361;Float;False;gaussianWeight0;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;71;-299.8327,-1125.123;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;201;-2612.613,-293.8284;Float;False;gaussianWeight4;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;100;-302.2971,-1584.485;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;97;-325.6442,413.9566;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;74;-332.8505,2020.841;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;114;-302.5293,2553.396;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;228;-146.4453,2465.579;Float;False;201;gaussianWeight4;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;184;-165.5746,-1125.89;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;227;-172.2931,1946.5;Float;False;200;gaussianWeight3;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;190;-138.7161,2020.558;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;188;-163.1131,942.4547;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;226;-178.4026,1405.971;Float;False;199;gaussianWeight2;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;186;-166.9447,-81.93948;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;187;-167.1269,413.7817;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;216;-154.6138,-1235.289;Float;False;201;gaussianWeight4;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;223;-183.9343,-162.9875;Float;False;199;gaussianWeight2;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;225;-181.6412,860.3293;Float;False;198;gaussianWeight1;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;185;-159.7691,-617.1123;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;183;-166.4693,-1584.124;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;207;-166.8099,-1676.276;Float;False;202;gaussianWeight0;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;222;-179.4722,-711.3936;Float;False;200;gaussianWeight3;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;191;-123.9791,2554.646;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;189;-146.4268,1490.893;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;224;-156.1669,328.4408;Float;False;198;gaussianWeight1;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;122;32.84563,390.2066;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;28.35795,921.3947;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;104;43.94516,-1607.375;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;108;44.39586,1469.042;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;111;40.42173,-636.4749;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;105;70.01702,2534.302;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;49.82761,-1147.486;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;89;52.92524,1998.265;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;107;28.67093,-104.4131;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;120;855.3273,193.873;Float;False;9;9;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;242;-341.7291,1621.322;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;249;-314.0011,2667.478;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;220;-352.5435,-1017.397;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;215;-344.9707,-1462.262;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;192;1080.532,187.7244;Float;False;gaussianWeightTotal;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;247;-332.0546,2133.085;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;239;-378.7185,532.4235;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;233;-376.1449,31.36187;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;221;-360.828,-467.2925;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;240;-384.6631,1056.837;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;125;250.2127,1467.445;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;208;959.7151,-532.2427;Float;False;Constant;_Float8;Float 8;3;0;Create;True;0;0;False;0;0.4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;194;883.5231,-618.223;Float;False;192;gaussianWeightTotal;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;248.5086,914.4499;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;77;255.2604,1996.857;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;236.2993,-102.2483;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;22;276.3623,2535.198;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;118;241.3838,-638.0123;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;127;242.0078,390.6123;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;103;240.7947,-1147.282;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;119;239.3232,-1607.651;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;147;1181.764,-611.9816;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;146;1213.525,-719.517;Float;False;Constant;_Float2;Float 2;3;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;196;997.1776,518.1709;Float;False;192;gaussianWeightTotal;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;121;857.5292,640.6888;Float;False;9;9;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;143;1261.733,-1721.344;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;151;1248.196,640.6807;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;145;1394.228,-612.5706;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;149;983.3664,-1594.562;Float;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;193;1848.429,-926.939;Float;False;192;gaussianWeightTotal;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;139;1910.352,-834.5178;Float;False;Constant;_Float1;Float 1;3;0;Create;True;0;0;False;0;0.4;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;141;1680.804,-659.6667;Float;False;mix;-1;;1;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ConditionalIfNode;140;2116.345,-723.0369;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;135;2365.398,-790.6982;Float;False;Constant;_Float0;Float 0;3;0;Create;True;0;0;False;0;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;195;2280.9,-879.7927;Float;False;192;gaussianWeightTotal;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;138;2574.077,-809.5345;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2860.34,-852.1637;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ARStudio/SkinSmoothing/BilateralHorizontal;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;133;0;132;1
WireConnection;133;1;132;2
WireConnection;164;0;76;0
WireConnection;155;0;133;0
WireConnection;29;0;170;0
WireConnection;29;1;35;0
WireConnection;29;2;161;0
WireConnection;52;0;172;0
WireConnection;52;1;20;0
WireConnection;52;2;163;0
WireConnection;49;0;169;0
WireConnection;49;1;31;0
WireConnection;49;2;160;0
WireConnection;23;0;168;0
WireConnection;23;1;27;0
WireConnection;23;2;159;0
WireConnection;36;0;167;0
WireConnection;36;1;28;0
WireConnection;36;2;158;0
WireConnection;82;0;171;0
WireConnection;82;1;34;0
WireConnection;82;2;162;0
WireConnection;64;0;166;0
WireConnection;64;1;16;0
WireConnection;64;2;157;0
WireConnection;10;0;165;0
WireConnection;10;1;5;0
WireConnection;10;2;156;0
WireConnection;17;0;64;0
WireConnection;18;0;10;0
WireConnection;14;0;23;0
WireConnection;63;0;36;0
WireConnection;53;0;29;0
WireConnection;54;0;49;0
WireConnection;42;0;82;0
WireConnection;112;0;52;0
WireConnection;13;0;131;0
WireConnection;13;1;18;0
WireConnection;47;0;131;0
WireConnection;47;1;54;0
WireConnection;45;0;131;0
WireConnection;45;1;14;0
WireConnection;65;0;131;0
WireConnection;65;1;63;0
WireConnection;59;0;131;0
WireConnection;59;1;17;0
WireConnection;50;0;131;0
WireConnection;50;1;42;0
WireConnection;51;0;131;0
WireConnection;51;1;112;0
WireConnection;46;0;131;0
WireConnection;46;1;53;0
WireConnection;93;1;46;0
WireConnection;84;1;45;0
WireConnection;95;1;47;0
WireConnection;124;1;65;0
WireConnection;123;1;59;0
WireConnection;70;1;50;0
WireConnection;67;1;51;0
WireConnection;41;1;13;0
WireConnection;173;0;32;0
WireConnection;128;1;131;0
WireConnection;40;0;94;0
WireConnection;40;1;93;0
WireConnection;96;0;94;0
WireConnection;96;1;128;0
WireConnection;39;0;94;0
WireConnection;39;1;84;0
WireConnection;25;0;94;0
WireConnection;25;1;41;0
WireConnection;48;0;94;0
WireConnection;48;1;70;0
WireConnection;19;0;94;0
WireConnection;19;1;124;0
WireConnection;30;0;94;0
WireConnection;30;1;123;0
WireConnection;8;0;94;0
WireConnection;8;1;67;0
WireConnection;60;0;94;0
WireConnection;60;1;95;0
WireConnection;126;0;30;0
WireConnection;126;1;176;0
WireConnection;61;0;19;0
WireConnection;61;1;177;0
WireConnection;33;0;40;0
WireConnection;33;1;180;0
WireConnection;115;0;48;0
WireConnection;115;1;181;0
WireConnection;55;0;60;0
WireConnection;55;1;179;0
WireConnection;62;0;96;0
WireConnection;62;1;174;0
WireConnection;43;0;8;0
WireConnection;43;1;182;0
WireConnection;24;0;39;0
WireConnection;24;1;178;0
WireConnection;66;0;25;0
WireConnection;66;1;175;0
WireConnection;75;0;33;0
WireConnection;75;1;87;0
WireConnection;198;0;197;0
WireConnection;113;0;55;0
WireConnection;113;1;58;0
WireConnection;199;0;204;0
WireConnection;200;0;205;0
WireConnection;98;0;126;0
WireConnection;98;1;7;0
WireConnection;116;0;61;0
WireConnection;116;1;21;0
WireConnection;202;0;203;0
WireConnection;71;0;66;0
WireConnection;71;1;44;0
WireConnection;201;0;206;0
WireConnection;100;0;62;0
WireConnection;100;1;91;0
WireConnection;97;0;24;0
WireConnection;97;1;110;0
WireConnection;74;0;115;0
WireConnection;74;1;56;0
WireConnection;114;0;43;0
WireConnection;114;1;57;0
WireConnection;184;0;71;0
WireConnection;190;0;74;0
WireConnection;188;0;113;0
WireConnection;186;0;116;0
WireConnection;187;0;97;0
WireConnection;185;0;98;0
WireConnection;183;0;100;0
WireConnection;191;0;114;0
WireConnection;189;0;75;0
WireConnection;122;0;224;0
WireConnection;122;1;187;0
WireConnection;106;0;225;0
WireConnection;106;1;188;0
WireConnection;104;0;207;0
WireConnection;104;1;183;0
WireConnection;108;0;226;0
WireConnection;108;1;189;0
WireConnection;111;0;222;0
WireConnection;111;1;185;0
WireConnection;105;0;228;0
WireConnection;105;1;191;0
WireConnection;11;0;216;0
WireConnection;11;1;184;0
WireConnection;89;0;227;0
WireConnection;89;1;190;0
WireConnection;107;0;223;0
WireConnection;107;1;186;0
WireConnection;120;0;104;0
WireConnection;120;1;11;0
WireConnection;120;2;111;0
WireConnection;120;3;107;0
WireConnection;120;4;122;0
WireConnection;120;5;106;0
WireConnection;120;6;108;0
WireConnection;120;7;89;0
WireConnection;120;8;105;0
WireConnection;242;0;93;0
WireConnection;249;0;67;0
WireConnection;220;0;41;0
WireConnection;215;0;128;0
WireConnection;192;0;120;0
WireConnection;247;0;70;0
WireConnection;239;0;84;0
WireConnection;233;0;124;0
WireConnection;221;0;123;0
WireConnection;240;0;95;0
WireConnection;125;0;108;0
WireConnection;125;1;242;0
WireConnection;6;0;106;0
WireConnection;6;1;240;0
WireConnection;77;0;89;0
WireConnection;77;1;247;0
WireConnection;117;0;107;0
WireConnection;117;1;233;0
WireConnection;22;0;105;0
WireConnection;22;1;249;0
WireConnection;118;0;111;0
WireConnection;118;1;221;0
WireConnection;127;0;122;0
WireConnection;127;1;239;0
WireConnection;103;0;11;0
WireConnection;103;1;220;0
WireConnection;119;0;104;0
WireConnection;119;1;215;0
WireConnection;147;0;194;0
WireConnection;147;1;208;0
WireConnection;121;0;119;0
WireConnection;121;1;103;0
WireConnection;121;2;118;0
WireConnection;121;3;117;0
WireConnection;121;4;127;0
WireConnection;121;5;6;0
WireConnection;121;6;125;0
WireConnection;121;7;77;0
WireConnection;121;8;22;0
WireConnection;143;0;94;0
WireConnection;151;0;121;0
WireConnection;151;1;196;0
WireConnection;145;0;147;0
WireConnection;145;1;146;0
WireConnection;149;0;94;0
WireConnection;141;3;151;0
WireConnection;141;2;143;0
WireConnection;141;1;145;0
WireConnection;140;0;193;0
WireConnection;140;1;139;0
WireConnection;140;2;141;0
WireConnection;140;3;141;0
WireConnection;140;4;149;0
WireConnection;138;0;195;0
WireConnection;138;1;135;0
WireConnection;138;2;151;0
WireConnection;138;3;151;0
WireConnection;138;4;140;0
WireConnection;0;2;138;0
ASEEND*/
//CHKSM=B80B52B1912727169DDF2A981D7284302AB9EEF4