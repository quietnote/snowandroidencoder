// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ARStudio/MultiLayerLUT"
{
	Properties
	{
		_MainTex("_MainTex", 2D) = "white" {}
		_MaskTex("_MaskTex", 2D) = "white" {}
		_weightLayer0("weightLayer0", Range( 0 , 1)) = 1
		_weightLayer1("weightLayer1", Range( 0 , 1)) = 1
		_weightLayer2("weightLayer2", Range( 0 , 1)) = 1
		_lut0Tex("_lut0Tex", 3D) = "white" {}
		_lut1Tex("_lut1Tex", 3D) = "white" {}
		_lut2Tex("_lut2Tex", 3D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _MaskTex;
		uniform float4 _MaskTex_ST;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _weightLayer0;
		uniform sampler3D _lut0Tex;
		uniform float _weightLayer1;
		uniform sampler3D _lut1Tex;
		uniform float _weightLayer2;
		uniform sampler3D _lut2Tex;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MaskTex = i.uv_texcoord * _MaskTex_ST.xy + _MaskTex_ST.zw;
			float4 tex2DNode82 = tex2D( _MaskTex, uv_MaskTex );
			float temp_output_2_0_g17 = tex2DNode82.r;
			float ifLocalVar17_g17 = 0;
			if( temp_output_2_0_g17 <= 0.0 )
				ifLocalVar17_g17 = 0.0;
			else
				ifLocalVar17_g17 = 1.0;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode1 = tex2D( _MainTex, uv_MainTex );
			float4 temp_output_12_0_g17 = tex2DNode1;
			float temp_output_1_0_g18 = _weightLayer0;
			float3 appendResult114 = (float3(tex2DNode1.r , tex2DNode1.g , tex2DNode1.b));
			float4 ifLocalVar7_g17 = 0;
			if( temp_output_2_0_g17 <= 0.0 )
				ifLocalVar7_g17 = temp_output_12_0_g17;
			else
				ifLocalVar7_g17 = ( ( temp_output_12_0_g17 * ( 1.0 - temp_output_1_0_g18 ) ) + ( tex3D( _lut0Tex, appendResult114 ) * temp_output_1_0_g18 ) );
			float4 temp_output_1_0_g23 = ifLocalVar7_g17;
			float temp_output_2_0_g21 = tex2DNode82.g;
			float ifLocalVar17_g21 = 0;
			if( temp_output_2_0_g21 <= 0.0 )
				ifLocalVar17_g21 = 0.0;
			else
				ifLocalVar17_g21 = 1.0;
			float4 temp_output_12_0_g21 = tex2DNode1;
			float temp_output_1_0_g22 = _weightLayer1;
			float4 ifLocalVar7_g21 = 0;
			if( temp_output_2_0_g21 <= 0.0 )
				ifLocalVar7_g21 = temp_output_12_0_g21;
			else
				ifLocalVar7_g21 = ( ( temp_output_12_0_g21 * ( 1.0 - temp_output_1_0_g22 ) ) + ( tex3D( _lut1Tex, appendResult114 ) * temp_output_1_0_g22 ) );
			float4 temp_output_3_0_g23 = ifLocalVar7_g21;
			float temp_output_2_0_g19 = tex2DNode82.b;
			float4 temp_output_12_0_g19 = tex2DNode1;
			float temp_output_1_0_g20 = _weightLayer2;
			float4 ifLocalVar7_g19 = 0;
			if( temp_output_2_0_g19 <= 0.0 )
				ifLocalVar7_g19 = temp_output_12_0_g19;
			else
				ifLocalVar7_g19 = ( ( temp_output_12_0_g19 * ( 1.0 - temp_output_1_0_g20 ) ) + ( tex3D( _lut2Tex, appendResult114 ) * temp_output_1_0_g20 ) );
			float4 ifLocalVar12_g23 = 0;
			if( ifLocalVar17_g21 >= 0.5 )
				ifLocalVar12_g23 = temp_output_3_0_g23;
			else
				ifLocalVar12_g23 = ifLocalVar7_g19;
			float4 ifLocalVar9_g23 = 0;
			if( ifLocalVar17_g17 >= 0.5 )
				ifLocalVar9_g23 = temp_output_1_0_g23;
			else
				ifLocalVar9_g23 = ifLocalVar12_g23;
			o.Emission = ifLocalVar9_g23.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15900
236;65;1877;1086;78.99973;843.6407;1.408571;True;False
Node;AmplifyShaderEditor.SamplerNode;1;257.065,-158.3927;Float;True;Property;_MainTex;_MainTex;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;114;842.7577,-287.7173;Float;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;103;1460.844,346.7663;Float;False;Property;_weightLayer2;weightLayer2;4;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;102;1458.531,256.5962;Float;False;Property;_weightLayer1;weightLayer1;3;0;Create;True;0;0;False;0;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;115;1139.69,-167.7542;Float;True;Property;_lut1Tex;_lut1Tex;6;0;Create;True;0;0;False;0;None;None;True;0;False;white;LockedToTexture3D;False;Object;-1;Auto;Texture3D;6;0;SAMPLER2D;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;112;1145.099,44.78673;Float;True;Property;_lut2Tex;_lut2Tex;7;0;Create;True;0;0;False;0;None;None;True;0;False;white;LockedToTexture3D;False;Object;-1;Auto;Texture3D;6;0;SAMPLER2D;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;82;1436.062,-536.8041;Float;True;Property;_MaskTex;_MaskTex;1;0;Create;True;0;0;False;0;None;bf64aa7a733ed4814a78d8e1c79830e5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;87;1456.526,164.1419;Float;False;Property;_weightLayer0;weightLayer0;2;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;110;1145.003,-388.0937;Float;True;Property;_lut0Tex;_lut0Tex;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;LockedToTexture3D;False;Object;-1;Auto;Texture3D;6;0;SAMPLER2D;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;100;1854.889,-335.039;Float;False;MaskProcess;-1;;17;8b78d02be8ebc46fd9d7446fceb1982b;0;4;2;FLOAT;0;False;9;COLOR;0,0,0,0;False;12;COLOR;0,0,0,0;False;14;FLOAT;0;False;2;FLOAT;16;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;99;1851.066,-24.54774;Float;False;MaskProcess;-1;;19;8b78d02be8ebc46fd9d7446fceb1982b;0;4;2;FLOAT;0;False;9;COLOR;0,0,0,0;False;12;COLOR;0,0,0,0;False;14;FLOAT;0;False;2;FLOAT;16;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;98;1851.577,-176.2224;Float;False;MaskProcess;-1;;21;8b78d02be8ebc46fd9d7446fceb1982b;0;4;2;FLOAT;0;False;9;COLOR;0,0,0,0;False;12;COLOR;0,0,0,0;False;14;FLOAT;0;False;2;FLOAT;16;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;101;2231.75,-172.8313;Float;False;SumColor;-1;;23;92a3c01adfa2349bcbb78d35b7820fbf;0;6;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;FLOAT;0;False;5;COLOR;0,0,0,0;False;20;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2562.126,-217.8074;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ARStudio/MultiLayerLUT;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;114;0;1;1
WireConnection;114;1;1;2
WireConnection;114;2;1;3
WireConnection;115;1;114;0
WireConnection;112;1;114;0
WireConnection;110;1;114;0
WireConnection;100;2;82;1
WireConnection;100;9;110;0
WireConnection;100;12;1;0
WireConnection;100;14;87;0
WireConnection;99;2;82;3
WireConnection;99;9;112;0
WireConnection;99;12;1;0
WireConnection;99;14;103;0
WireConnection;98;2;82;2
WireConnection;98;9;115;0
WireConnection;98;12;1;0
WireConnection;98;14;102;0
WireConnection;101;1;100;0
WireConnection;101;2;100;16
WireConnection;101;3;98;0
WireConnection;101;4;98;16
WireConnection;101;5;99;0
WireConnection;0;2;101;0
ASEEND*/
//CHKSM=FD830DC78D7A06F187B854D654C97C1AC100A039