﻿Shader "Snow/3x3AverageBlur"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Texel_Width;
            float _Texel_Height;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col[9];

                float2 widthStep = float2(_Texel_Width, 0.0);
                float2 heightStep = float2(0.0, _Texel_Height);

                col[0] = tex2D(_MainTex, i.uv + (heightStep - widthStep));
                col[1] = tex2D(_MainTex, i.uv + heightStep);
                col[2] = tex2D(_MainTex, i.uv + (heightStep + widthStep));
                col[3] = tex2D(_MainTex, i.uv  - widthStep);
                col[4] = tex2D(_MainTex, i.uv);
                col[5] = tex2D(_MainTex, i.uv + widthStep);
                col[6] = tex2D(_MainTex, i.uv + (heightStep - widthStep));
                col[7] = tex2D(_MainTex, i.uv + heightStep);
                col[8] = tex2D(_MainTex, i.uv + (heightStep + widthStep));

                float sum = 0.0;
                for (int i = 0; i < 9; ++i) {
                    sum = sum + col[i].r;
                }
                sum = sum / 9.0;

                return fixed4(sum, 0, 0, 1);
            }
            ENDCG
        }
    }
}
