﻿Shader "Snow/DistortGL"
{
    Properties
    {
        u_texture ("Texture", 2D) = "white" {}
        count("Distort_Count", int) = 0
        faceFactors("FaceFactor (Y,aspect,eyeDist,R)", Vector) = (0,0,0,0)
        validCR("center,radius", Vector) = (0,0,0,0)
        valid_threshold("threshold", float) = 0.0
        u_renderOnlyDistortion("OnlyDistortion", int) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            Name "Distort"
            Tags {
            }

            GLSLPROGRAM
            
            //#include "UnityCG.glslinc"

            #define MAXPOINTS 30


            // circles = (vec2 center, vec2 radius)
            uniform vec4 circles[MAXPOINTS];
            // groups = (scale, angle, u_min, u_max)
            uniform vec4 groups[MAXPOINTS];
            uniform float types[MAXPOINTS];
            uniform float progress[MAXPOINTS];

            uniform int count;
            // faceFactors = (faceYFactor, aspectRatio, eyeDist, faceRoll)
            uniform vec4 faceFactors;

            uniform vec4 validCR;
            //uniform vec2 valid_center;
            //uniform vec2 valid_radius;
            uniform float valid_threshold;
            
            uniform sampler2D u_texture;
            uniform int u_renderOnlyDistortion;

            #ifdef VERTEX
            attribute vec2 a_texCoord;
            
            varying vec2 v_texCoord;
            varying vec2 v_orgTexCoord;

            float distanceFromValidCenter()
            {
                float aspectRatio = faceFactors.y;
                float faceRoll = faceFactors.w;

                // faceRoll should be given in screen coordinate
                float roll = faceRoll * -1.0;
                float cosRoll = cos(roll);
                float sinRoll = sin(roll);

                float x = a_texCoord.x;
                float y = a_texCoord.y * aspectRatio;
                float a = validCR.x;
                float b = validCR.y * aspectRatio;
                float rx = validCR.z;
                float ry = validCR.w;

                float e1 = ((x - a) * cosRoll - (y - b) * sinRoll) / rx;
                float e2 = ((x - a) * sinRoll + (y - b) * cosRoll) / ry;
                float d = sqrt(e1 * e1 + e2 * e2);

                return d;
            }

            float weightFunction(float x, float t)
            {
                // cubic version
                return smoothstep(t, 1.0, x);
                // linear version
                // float y = x / (1.0 - t) + 1.0 - 1.0 / (1.0 - t);
                // return clamp(y, 0.0, 1.0);
            }

            vec2 distortion(vec2 uv, int i, float extraWeight) {

                vec2 textureCoordinateToUse = uv;
                
                // Face Factors
                float faceYFactor = faceFactors.x;
                float aspectRatio = faceFactors.y;
                float eyeDist = faceFactors.z;
                float faceRoll = faceFactors.w;

                // Circles
                vec2 center = circles[i].xy;
                vec2 radius = circles[i].zw;
                
                // Groups
                float scale = groups[i].x;
                float angle = groups[i].y + faceRoll;
                float u_min = groups[i].z;
                float u_max = groups[i].w;

                float x = textureCoordinateToUse.x;
                float y = textureCoordinateToUse.y;

                // faceRoll should be given in screen coordinate
                float roll = faceRoll * -1.0;
                float cosRoll = cos(roll);
                float sinRoll = sin(roll);

                float e1 = ((x - center.x) * cosRoll - (y - center.y) * sinRoll * aspectRatio) / radius.x;
                float e2 = ((x - center.x) * sinRoll + (y - center.y) * cosRoll * aspectRatio) / radius.y;
                float d  = (e1 * e1) + (e2 * e2);

                if (d < 1.0) {
                    if(types[i] == 1.0 || types[i] == 3.0) {

                        // bulge
                        vec2 dist = vec2(d * radius.x, d * radius.y);
                        textureCoordinateToUse -= center;

                        vec2 delta = ((radius - dist) / radius);
                        float deltaScale = scale * 0.04;
                        if(deltaScale > 0.0 && types[i] == 1.0) {
                            deltaScale = smoothstep(u_min, u_max, deltaScale);
                        }

                        vec2 percent = 1.0 - ((delta * deltaScale) * progress[i] * extraWeight);
                        textureCoordinateToUse = textureCoordinateToUse * percent;
                        uv = textureCoordinateToUse + center;
                    } else if(types[i] == 2.0) {

                        // shift
                        float dist = 1.0 - d;
                        float delta = (scale * 0.5 * eyeDist) * dist * progress[i] * extraWeight;

                        float deltaScale = smoothstep(u_min, u_max, dist);
                        float directionX = cos(angle) * deltaScale;
                        float directionY = sin(angle) * deltaScale / (faceYFactor * aspectRatio);

                        uv = vec2(textureCoordinateToUse.x - (delta * directionX), textureCoordinateToUse.y -  (delta * directionY));
                    }
                }

                return uv;
            }

            void main() {
                vec2 uv = a_texCoord;

                gl_Position = vec4(uv * 2.0 - 1.0, 0.0, 1.0);

                float w = valid_threshold >= 0.0 ? 1.0 - weightFunction(distanceFromValidCenter(), valid_threshold) : 1.0;
                if (w > 0.0) {
                    for (int i = 0; i < count; i++) {
                        uv = distortion(uv, i, w);
                    }
                }

                v_texCoord = uv;
                v_orgTexCoord = a_texCoord;
            }
            #endif

            #ifdef FRAGMENT

            #ifdef OPENGL_ES
            #ifdef GL_FRAGMENT_PRECISION_HIGH
            precision highp float;
            #else
            precision mediump float;
            #endif
            #endif


            varying vec2 v_texCoord;
            varying vec2 v_orgTexCoord;

            void main()
            {
                bool willDiscard = (u_renderOnlyDistortion == 1 && v_texCoord == v_orgTexCoord);
                float alpha = 1.0 - float(willDiscard);
                vec4 color = texture2D(u_texture, v_texCoord);
                gl_FragColor = vec4(color.rgb, color.a * alpha);
            }

            #endif
            ENDGLSL
        }
    }
    FallBack "Diffuse"
}
