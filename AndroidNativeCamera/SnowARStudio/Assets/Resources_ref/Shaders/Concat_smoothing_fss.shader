// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ARStudio/SkinSmoothing/Concat_smoothing_fss"
{
	Properties
	{
		_MainTex("_MainTex", 2D) = "white" {}
		_LUT0("_LUT0", 3D) = "white" {}
		_LUT1("_LUT1", 3D) = "white" {}
		_LUT2("_LUT2", 3D) = "white" {}
		_BlurTexture("_BlurTexture", 2D) = "white" {}
		_uOpacity("uOpacity", Range( 0 , 1)) = 0.5
		_LUTMaskTex("_LUTMaskTex", 2D) = "white" {}
		_weightLayer0("weightLayer0", Range( 0 , 1)) = 1
		_weightLayer1("weightLayer1", Range( 0 , 1)) = 1
		_SmoothingMaskTex("_SmoothingMaskTex", 2D) = "white" {}
		_useSmoothing("useSmoothing", Int) = 0
		_weightLayer2("weightLayer2", Range( 0 , 1)) = 1
		_useLUT("useLUT", Int) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows nodynlightmap nodirlightmap nofog nometa noforwardadd 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform int _useLUT;
		uniform sampler2D _LUTMaskTex;
		uniform float4 _LUTMaskTex_ST;
		uniform int _useSmoothing;
		uniform sampler2D _SmoothingMaskTex;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _uOpacity;
		uniform sampler2D _BlurTexture;
		uniform float4 _BlurTexture_ST;
		uniform half _weightLayer0;
		uniform sampler3D _LUT0;
		uniform half _weightLayer1;
		uniform sampler3D _LUT1;
		uniform half _weightLayer2;
		uniform sampler3D _LUT2;


		float3 RGBToHSV(float3 c)
		{
			float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
			float4 p = lerp( float4( c.bg, K.wz ), float4( c.gb, K.xy ), step( c.b, c.g ) );
			float4 q = lerp( float4( p.xyw, c.r ), float4( c.r, p.yzx ), step( p.x, c.r ) );
			float d = q.x - min( q.w, q.y );
			float e = 1.0e-10;
			return float3( abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_LUTMaskTex = i.uv_texcoord * _LUTMaskTex_ST.xy + _LUTMaskTex_ST.zw;
			float4 break49_g183 = tex2D( _LUTMaskTex, uv_LUTMaskTex );
			float temp_output_2_0_g189 = break49_g183.r;
			float ifLocalVar17_g189 = 0;
			if( temp_output_2_0_g189 <= 0.0 )
				ifLocalVar17_g189 = 0.0;
			else
				ifLocalVar17_g189 = 1.0;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode12 = tex2D( _MainTex, uv_MainTex );
			float3 hsvTorgb10 = RGBToHSV( tex2DNode12.rgb );
			float value132 = hsvTorgb10.z;
			float hue128 = hsvTorgb10.x;
			float ifLocalVar15 = 0;
			if( hue128 >= 0.18 )
				ifLocalVar15 = 0.0;
			else
				ifLocalVar15 = 1.0;
			float ifLocalVar18 = 0;
			if( hue128 <= 0.89 )
				ifLocalVar18 = ifLocalVar15;
			else
				ifLocalVar18 = 1.0;
			float ifLocalVar22 = 0;
			if( value132 <= 0.2 )
				ifLocalVar22 = 0.0;
			else
				ifLocalVar22 = ifLocalVar18;
			float ifLocalVar30 = 0;
			if( hue128 <= 0.16 )
				ifLocalVar30 = ifLocalVar22;
			else
				ifLocalVar30 = min( ifLocalVar22 , ( ( 0.18 - hue128 ) / 0.02 ) );
			float ifLocalVar33 = 0;
			if( hue128 >= 0.18 )
				ifLocalVar33 = ifLocalVar22;
			else
				ifLocalVar33 = ifLocalVar30;
			float ifLocalVar42 = 0;
			if( hue128 <= 0.89 )
				ifLocalVar42 = ifLocalVar33;
			else
				ifLocalVar42 = min( ifLocalVar33 , ( 1.0 - ( ( 0.91 - hue128 ) / 0.02 ) ) );
			float ifLocalVar51 = 0;
			if( hue128 >= 0.91 )
				ifLocalVar51 = ifLocalVar33;
			else
				ifLocalVar51 = ifLocalVar42;
			float ifLocalVar53 = 0;
			if( hue128 <= 0.2 )
				ifLocalVar53 = ifLocalVar51;
			else
				ifLocalVar53 = min( ifLocalVar51 , ( 1.0 - ( ( 0.3 - value132 ) / 0.1 ) ) );
			float ifLocalVar61 = 0;
			if( value132 >= 0.3 )
				ifLocalVar61 = ifLocalVar51;
			else
				ifLocalVar61 = ifLocalVar53;
			half uOpacity176 = _uOpacity;
			float factor7166 = 0.963;
			float2 uv_BlurTexture = i.uv_texcoord * _BlurTexture_ST.xy + _BlurTexture_ST.zw;
			float4 tex2DNode65 = tex2D( _BlurTexture, uv_BlurTexture );
			float4 temp_cast_2 = (0.0).xxxx;
			float factor1153 = 2.782;
			half cDistance157 = ( distance( float4( float3(0,0,0) , 0.0 ) , max( ( tex2DNode65 - tex2DNode12 ) , temp_cast_2 ) ) * factor1153 );
			float temp_output_1_0_g177 = cDistance157;
			float factor3159 = 1.158;
			float temp_output_1_0_g169 = factor3159;
			float factor2155 = 1.131;
			float3 temp_output_10_0_g167 = ( tex2DNode12.rgb * factor2155 );
			float dotResult11_g167 = dot( temp_output_10_0_g167 , float3(0.2125,0.7154,0.0721) );
			float3 appendResult28_g167 = (float3(dotResult11_g167 , dotResult11_g167 , dotResult11_g167));
			float temp_output_1_0_g168 = 1.0;
			float4 temp_cast_8 = (0.0).xxxx;
			float factor4163 = 2.901;
			half dDistance172 = ( distance( float4( float3(0,0,0) , 0.0 ) , max( ( tex2DNode12 - tex2DNode65 ) , temp_cast_8 ) ) * factor4163 );
			float temp_output_1_0_g178 = dDistance172;
			float factor6165 = 0.639;
			float temp_output_1_0_g176 = factor6165;
			float factor5164 = 0.979;
			float3 temp_output_10_0_g174 = ( tex2DNode12.rgb * factor5164 );
			float dotResult11_g174 = dot( temp_output_10_0_g174 , float3(0.2125,0.7154,0.0721) );
			float3 appendResult28_g174 = (float3(dotResult11_g174 , dotResult11_g174 , dotResult11_g174));
			float temp_output_1_0_g175 = 1.0;
			float4 temp_output_109_0 = ( ( ( ( tex2DNode12 * ( 1.0 - temp_output_1_0_g177 ) ) + ( ( ( float4( float3(0.5,0.5,0.5) , 0.0 ) * ( 1.0 - temp_output_1_0_g169 ) ) + ( ( ( float4( appendResult28_g167 , 0.0 ) * ( 1.0 - temp_output_1_0_g168 ) ) + ( float4( temp_output_10_0_g167 , 0.0 ) * temp_output_1_0_g168 ) ) * temp_output_1_0_g169 ) ) * temp_output_1_0_g177 ) ) * ( 1.0 - temp_output_1_0_g178 ) ) + ( ( ( float4( float3(0.5,0.5,0.5) , 0.0 ) * ( 1.0 - temp_output_1_0_g176 ) ) + ( ( ( float4( appendResult28_g174 , 0.0 ) * ( 1.0 - temp_output_1_0_g175 ) ) + ( float4( temp_output_10_0_g174 , 0.0 ) * temp_output_1_0_g175 ) ) * temp_output_1_0_g176 ) ) * temp_output_1_0_g178 ) );
			float opacityLimit117 = ( 0.46 * ifLocalVar61 );
			float temp_output_1_0_g181 = opacityLimit117;
			float4 temp_output_122_0 = ( ( temp_output_109_0 * ( 1.0 - temp_output_1_0_g181 ) ) + ( tex2DNode65 * temp_output_1_0_g181 ) );
			float temp_output_1_0_g179 = factor7166;
			float temp_output_1_0_g180 = opacityLimit117;
			float4 ifLocalVar111 = 0;
			if( factor7166 >= 0.999 )
				ifLocalVar111 = temp_output_122_0;
			else
				ifLocalVar111 = ( ( ( ( tex2DNode12 * ( 1.0 - temp_output_1_0_g179 ) ) + ( temp_output_109_0 * temp_output_1_0_g179 ) ) * ( 1.0 - temp_output_1_0_g180 ) ) + ( tex2DNode65 * temp_output_1_0_g180 ) );
			float temp_output_1_0_g182 = uOpacity176;
			float4 ifLocalVar123 = 0;
			if( uOpacity176 >= 0.999 )
				ifLocalVar123 = ifLocalVar111;
			else
				ifLocalVar123 = ( ( tex2DNode12 * ( 1.0 - temp_output_1_0_g182 ) ) + ( ifLocalVar111 * temp_output_1_0_g182 ) );
			float4 ifLocalVar149 = 0;
			if( ifLocalVar61 <= 0.0 )
				ifLocalVar149 = tex2DNode12;
			else
				ifLocalVar149 = ifLocalVar123;
			float4 temp_cast_13 = (1.0).xxxx;
			float4 ifLocalVar206 = 0;
			if( tex2D( _SmoothingMaskTex, uv_MainTex ).r <= 0.0 )
				ifLocalVar206 = tex2DNode12;
			else
				ifLocalVar206 = min( ifLocalVar149 , temp_cast_13 );
			float4 ifLocalVar216 = 0;
			if( _useSmoothing >= 1 )
				ifLocalVar216 = ifLocalVar206;
			else
				ifLocalVar216 = tex2DNode12;
			float4 temp_output_42_0_g183 = ifLocalVar216;
			float4 temp_output_12_0_g189 = temp_output_42_0_g183;
			float temp_output_1_0_g190 = _weightLayer0;
			float4 break47_g183 = temp_output_42_0_g183;
			float3 appendResult21_g183 = (float3(break47_g183.r , break47_g183.g , break47_g183.b));
			float4 ifLocalVar7_g189 = 0;
			if( temp_output_2_0_g189 <= 0.0 )
				ifLocalVar7_g189 = temp_output_12_0_g189;
			else
				ifLocalVar7_g189 = ( ( temp_output_12_0_g189 * ( 1.0 - temp_output_1_0_g190 ) ) + ( tex3D( _LUT0, appendResult21_g183 ) * temp_output_1_0_g190 ) );
			float4 temp_output_1_0_g188 = ifLocalVar7_g189;
			float temp_output_2_0_g186 = break49_g183.g;
			float ifLocalVar17_g186 = 0;
			if( temp_output_2_0_g186 <= 0.0 )
				ifLocalVar17_g186 = 0.0;
			else
				ifLocalVar17_g186 = 1.0;
			float4 temp_output_12_0_g186 = temp_output_42_0_g183;
			float temp_output_1_0_g187 = _weightLayer1;
			float4 ifLocalVar7_g186 = 0;
			if( temp_output_2_0_g186 <= 0.0 )
				ifLocalVar7_g186 = temp_output_12_0_g186;
			else
				ifLocalVar7_g186 = ( ( temp_output_12_0_g186 * ( 1.0 - temp_output_1_0_g187 ) ) + ( tex3D( _LUT1, appendResult21_g183 ) * temp_output_1_0_g187 ) );
			float4 temp_output_3_0_g188 = ifLocalVar7_g186;
			float temp_output_2_0_g184 = break49_g183.b;
			float4 temp_output_12_0_g184 = temp_output_42_0_g183;
			float temp_output_1_0_g185 = _weightLayer2;
			float4 ifLocalVar7_g184 = 0;
			if( temp_output_2_0_g184 <= 0.0 )
				ifLocalVar7_g184 = temp_output_12_0_g184;
			else
				ifLocalVar7_g184 = ( ( temp_output_12_0_g184 * ( 1.0 - temp_output_1_0_g185 ) ) + ( tex3D( _LUT2, appendResult21_g183 ) * temp_output_1_0_g185 ) );
			float4 ifLocalVar12_g188 = 0;
			if( ifLocalVar17_g186 >= 0.5 )
				ifLocalVar12_g188 = temp_output_3_0_g188;
			else
				ifLocalVar12_g188 = ifLocalVar7_g184;
			float4 ifLocalVar9_g188 = 0;
			if( ifLocalVar17_g189 >= 0.5 )
				ifLocalVar9_g188 = temp_output_1_0_g188;
			else
				ifLocalVar9_g188 = ifLocalVar12_g188;
			float4 temp_output_213_0 = ifLocalVar9_g188;
			float4 ifLocalVar196 = 0;
			if( _useLUT >= 1 )
				ifLocalVar196 = temp_output_213_0;
			else
				ifLocalVar196 = ifLocalVar216;
			o.Emission = ifLocalVar196.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15900
236;65;1877;1086;-332.9301;2079.621;1.926681;True;False
Node;AmplifyShaderEditor.SamplerNode;12;-1634.443,-602.0721;Float;True;Property;_MainTex;_MainTex;0;0;Create;True;0;0;False;0;None;;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RGBToHSVNode;10;-1057.079,-800.1544;Float;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;185;-885.6883,-3098.448;Float;False;2064.206;2223.976;Comment;53;38;136;40;37;45;138;39;134;31;46;44;36;48;135;41;30;60;33;142;50;57;43;59;137;47;139;56;52;42;58;51;141;54;55;53;140;62;61;9;152;117;19;129;20;16;17;130;15;18;25;133;23;22;DecisionOpacity;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;128;-782.9719,-793.0125;Float;False;hue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;129;-647.4297,-2896.808;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;16;-642.8728,-2814.684;Float;False;Constant;_Float0;Float 0;1;0;Create;True;0;0;False;0;0.18;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;20;-636.067,-2642.637;Float;False;Constant;_opacityLimit;opacityLimit;1;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-623.0834,-2726.668;Float;False;Constant;_Float2;Float 2;1;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-161.5621,-2955.019;Float;False;Constant;_Float1;Float 1;1;0;Create;True;0;0;False;0;0.89;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;130;-165.7598,-3048.448;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;136;-654.4077,-1813.297;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;38;-635.2399,-1903.713;Float;False;Constant;_Float6;Float 6;1;0;Create;True;0;0;False;0;0.18;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;15;-408.7903,-2890.638;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;132;-783.5764,-704.0079;Float;False;value;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;37;-424.3665,-1898.231;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;133;-152.808,-2545.264;Float;False;132;value;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25;-125.1741,-2345.37;Float;False;Constant;_Float4;Float 4;1;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;40;-433.0378,-1771.033;Float;False;Constant;_Float7;Float 7;1;0;Create;True;0;0;False;0;0.02;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;18;63.06669,-2806.31;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-148.5477,-2460.709;Float;False;Constant;_Float3;Float 3;1;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;45;-808.3595,-1610.333;Float;False;Constant;_Float11;Float 11;1;0;Create;True;0;0;False;0;0.91;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;22;69.6049,-2429.674;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;138;-818.8941,-1482.904;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;39;-271.3199,-1897.568;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;46;-620.187,-1500.645;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;134;-134.0084,-2088.988;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;36;-108.919,-1914.873;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-127.2969,-1997.69;Float;False;Constant;_Float5;Float 5;1;0;Create;True;0;0;False;0;0.16;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-626.5261,-1394.447;Float;False;Constant;_Float10;Float 10;1;0;Create;True;0;0;False;0;0.02;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;135;182.9738,-2182.179;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;30;100.483,-1992.905;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;41;198.5348,-2082.602;Float;False;Constant;_Float8;Float 8;1;0;Create;True;0;0;False;0;0.18;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;48;-428.351,-1499.238;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;186;-2385.714,-287.6429;Float;False;502.833;636.5507;Comment;14;2;153;155;3;159;4;163;5;164;6;165;7;166;8;Factors;1,1,1,1;0;0
Node;AmplifyShaderEditor.ConditionalIfNode;33;387.0239,-1982.788;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;142;-835.6883,-1127.263;Float;False;132;value;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;50;-283.998,-1500.752;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-807.8699,-1221.515;Float;False;Constant;_Float15;Float 15;1;0;Create;True;0;0;False;0;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-2335.714,-237.6429;Float;False;Constant;_factor1;factor1;0;0;Create;True;0;0;False;0;2.782;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;137;-65.92597,-1711.552;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-50.70588,-1621.242;Float;False;Constant;_Float9;Float 9;1;0;Create;True;0;0;False;0;0.89;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;57;-648.7676,-1105.158;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;47;-98.95028,-1524.733;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;59;-646.7808,-988.9722;Float;False;Constant;_Float14;Float 14;1;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;179;-1472.192,431.1859;Float;False;832.4617;211.2766;Comment;3;100;157;154;cDistance;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;223.0233,-1729.277;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;56;-469.601,-1102.42;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;52;240.5901,-1644.757;Float;False;Constant;_Float12;Float 12;1;0;Create;True;0;0;False;0;0.91;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;42;117.6469,-1564.328;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;153;-2116.381,-236.3528;Float;False;factor1;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;5;-2330.94,-1.810942;Float;False;Constant;_factor4;factor4;0;0;Create;True;0;0;False;0;2.901;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-2332.713,-157.8189;Float;False;Constant;_factor2;factor2;0;0;Create;True;0;0;False;0;1.131;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;65;-1629.425,87.74401;Float;True;Property;_BlurTexture;_BlurTexture;5;0;Create;True;0;0;False;0;None;;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;58;-300.0042,-1100.495;Float;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;163;-2120.04,-1.015776;Float;False;factor4;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;180;-1474.375,732.8526;Float;False;819.6359;212.2753;Comment;3;103;169;172;dDistance;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;154;-1422.192,481.1858;Float;False;153;factor1;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;4;-2332.552,-80.4808;Float;False;Constant;_factor3;factor3;0;0;Create;True;0;0;False;0;1.158;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;51;411.1126,-1563.757;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-2332.046,76.29796;Float;False;Constant;_factor5;factor5;0;0;Create;True;0;0;False;0;0.979;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-2332.074,155.744;Float;False;Constant;_factor6;factor6;0;0;Create;True;0;0;False;0;0.639;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;169;-1424.375,782.8526;Float;False;163;factor4;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;141;-107.9818,-1335.587;Float;False;128;hue;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;155;-2117.969,-157.0538;Float;False;factor2;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;54;-80.89707,-1244.558;Float;False;Constant;_Float13;Float 13;1;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;159;-2118.577,-79.72982;Float;False;factor3;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;181;-1194.881,-382.3789;Float;False;912.0398;436.0206;Comment;5;78;158;156;77;160;mix11Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.FunctionNode;100;-1166.807,487.9624;Float;False;CalculateDistance;-1;;165;84c8c70a88dda4d8aad76277895964e8;0;3;5;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;55;-95.85578,-1123.313;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;254.4252,-1239.046;Float;False;Constant;_Float16;Float 16;1;0;Create;True;0;0;False;0;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;53;89.01359,-1188.45;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;156;-1139.796,-332.3789;Float;False;155;factor2;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;77;-1126.404,-197.9088;Float;False;Constant;_Float19;Float 19;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;157;-873.2297,483.7473;Half;False;cDistance;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;183;-478.9005,113.0256;Float;False;941.9974;337.1226;Comment;5;171;161;170;173;109;mix115Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;164;-2117.848,76.19493;Float;False;factor5;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;140;241.6028,-1318.465;Float;False;132;value;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;160;-1144.881,-81.16052;Float;False;159;factor3;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;103;-1176.41,790.6279;Float;False;CalculateDistance;-1;;166;84c8c70a88dda4d8aad76277895964e8;0;3;5;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;165;-2118.449,156.859;Float;False;factor6;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;171;-417.9056,313.0388;Float;False;165;factor6;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;188;-918.9067,-142.545;Float;False;ContrastSaturationBrightness;-1;;167;166de50a9a2e04f82beadb86709c813c;0;4;26;FLOAT;0;False;27;FLOAT3;0,0,0;False;25;FLOAT;0;False;24;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;8;-2332.047,232.9889;Float;False;Constant;_factor7;factor7;0;0;Create;True;0;0;False;0;0.963;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;172;-888.239,786.6695;Half;False;dDistance;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;170;-428.9003,163.0256;Float;False;164;factor5;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;161;-393.3484,236.9271;Float;False;Constant;_Float21;Float 21;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;61;417.844,-1192.559;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;9;570.5408,-1281.862;Float;False;Constant;_blurOpacity;blurOpacity;0;0;Create;True;0;0;False;0;0.46;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;158;-800.8534,-260.2605;Float;False;157;cDistance;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;184;335.1992,516.9733;Float;False;1217.183;532.9853;Comment;9;167;113;114;120;112;122;174;168;111;mix12Color;1,1,1,1;0;0
Node;AmplifyShaderEditor.FunctionNode;78;-525.8416,-159.5732;Float;False;mix;-1;;177;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;152;740.3869,-1210.814;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;173;59.37257,335.6482;Float;False;172;dDistance;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;166;-2119.272,234.4078;Float;False;factor7;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;187;-193.5764,189.1857;Float;False;ContrastSaturationBrightness;-1;;174;166de50a9a2e04f82beadb86709c813c;0;4;26;FLOAT;0;False;27;FLOAT3;0,0,0;False;25;FLOAT;0;False;24;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;117;945.0178,-1213.992;Float;False;opacityLimit;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;109;220.0965,181.2212;Float;False;mix;-1;;178;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;167;385.1991,691.0956;Float;False;166;factor7;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;175;-2385.107,-538.5147;Float;False;Property;_uOpacity;uOpacity;6;0;Create;True;0;0;False;0;0.5;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;120;688.0811,771.9855;Float;False;117;opacityLimit;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;174;694.7087,935.4593;Float;False;117;opacityLimit;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;113;594.1264,649.8201;Float;False;mix;-1;;179;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;122;891.9977,852.2074;Float;False;mix;-1;;181;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;112;1145.097,648.2742;Float;False;Constant;_Float18;Float 18;2;0;Create;True;0;0;False;0;0.999;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;114;888.121,653.1581;Float;False;mix;-1;;180;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;176;-2052.645,-538.0143;Half;False;uOpacity;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;168;1134.246,566.9733;Float;False;166;factor7;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;124;1372.781,257.2984;Float;False;176;uOpacity;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;111;1355.382,632.3717;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;177;1584.286,-70.05064;Float;False;176;uOpacity;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;125;1592.629,19.96887;Float;False;Constant;_Float20;Float 20;2;0;Create;True;0;0;False;0;0.999;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;126;1593.972,217.0271;Float;False;mix;-1;;182;e06417962618e42f38e4f45b4697388e;0;3;3;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;209;1785.739,-1178.36;Float;False;897.9832;373.085;UseSkinSmootingMask;4;206;207;201;205;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ConditionalIfNode;123;1863.365,1.033381;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;14;1514.365,-1106.905;Float;False;0;12;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;151;1908.709,-674.6407;Float;False;Constant;_Float17;Float 17;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;201;1835.739,-1128.36;Float;True;Property;_SmoothingMaskTex;_SmoothingMaskTex;10;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;149;2129.007,-690.8326;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;200;2316.552,-612.2178;Float;False;Constant;_Float22;Float 22;9;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;199;2461.879,-689.3552;Float;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;207;2254.986,-919.7755;Float;False;Constant;_Float23;Float 23;10;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;218;2337.034,-1606.641;Float;False;540.3386;328.8939;Check Use Smoothing Flag;3;214;217;216;;1,1,1,1;0;0
Node;AmplifyShaderEditor.BreakToComponentsNode;205;2191.058,-1121.547;Float;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.IntNode;217;2436.092,-1462.367;Float;False;Constant;_Int1;Int 1;11;0;Create;True;0;0;False;0;1;0;0;1;INT;0
Node;AmplifyShaderEditor.ConditionalIfNode;206;2486.722,-1121.546;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;198;2719.747,-887.4241;Float;False;1275.986;838.2252;Comment;9;196;192;193;194;190;0;197;195;213;UseMaskedLUT;1,1,1,1;0;0
Node;AmplifyShaderEditor.IntNode;214;2387.034,-1556.641;Float;False;Property;_useSmoothing;useSmoothing;11;0;Create;True;0;0;False;0;0;1;0;1;INT;0
Node;AmplifyShaderEditor.ConditionalIfNode;216;2680.372,-1477.247;Float;False;False;5;0;INT;0;False;1;INT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;192;2775.534,-317.4988;Half;False;Property;_weightLayer0;weightLayer0;8;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;190;2765.747,-598.3329;Float;True;Property;_LUTMaskTex;_LUTMaskTex;7;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;193;2776.764,-238.753;Half;False;Property;_weightLayer1;weightLayer1;9;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;194;2777.995,-163.6988;Half;False;Property;_weightLayer2;weightLayer2;12;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.IntNode;195;3283.672,-839.5649;Float;False;Property;_useLUT;useLUT;13;0;Create;True;0;0;False;0;0;1;0;1;INT;0
Node;AmplifyShaderEditor.FunctionNode;213;3094.611,-590.7361;Float;False;MultiLayerLUTfn;1;;183;0e599d5410abb4481925cf79f13ea704;0;5;42;COLOR;0,0,0,0;False;48;COLOR;0,0,0,0;False;50;FLOAT;0;False;51;FLOAT;0;False;52;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.IntNode;197;3281.674,-747.8049;Float;False;Constant;_Int0;Int 0;9;0;Create;True;0;0;False;0;1;0;0;1;INT;0
Node;AmplifyShaderEditor.ConditionalIfNode;196;3475.311,-763.8083;Float;False;False;5;0;INT;0;False;1;INT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;3735.734,-809.2751;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ARStudio/SkinSmoothing/Concat_smoothing_fss;False;False;False;False;False;False;False;True;True;True;True;True;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;True;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;10;0;12;0
WireConnection;128;0;10;1
WireConnection;15;0;129;0
WireConnection;15;1;16;0
WireConnection;15;2;19;0
WireConnection;15;3;19;0
WireConnection;15;4;20;0
WireConnection;132;0;10;3
WireConnection;37;0;38;0
WireConnection;37;1;136;0
WireConnection;18;0;130;0
WireConnection;18;1;17;0
WireConnection;18;2;20;0
WireConnection;18;3;15;0
WireConnection;18;4;15;0
WireConnection;22;0;133;0
WireConnection;22;1;23;0
WireConnection;22;2;18;0
WireConnection;22;3;25;0
WireConnection;22;4;25;0
WireConnection;39;0;37;0
WireConnection;39;1;40;0
WireConnection;46;0;45;0
WireConnection;46;1;138;0
WireConnection;36;0;22;0
WireConnection;36;1;39;0
WireConnection;30;0;134;0
WireConnection;30;1;31;0
WireConnection;30;2;36;0
WireConnection;30;3;22;0
WireConnection;30;4;22;0
WireConnection;48;0;46;0
WireConnection;48;1;44;0
WireConnection;33;0;135;0
WireConnection;33;1;41;0
WireConnection;33;2;22;0
WireConnection;33;3;22;0
WireConnection;33;4;30;0
WireConnection;50;0;48;0
WireConnection;57;0;60;0
WireConnection;57;1;142;0
WireConnection;47;0;33;0
WireConnection;47;1;50;0
WireConnection;56;0;57;0
WireConnection;56;1;59;0
WireConnection;42;0;137;0
WireConnection;42;1;43;0
WireConnection;42;2;47;0
WireConnection;42;3;33;0
WireConnection;42;4;33;0
WireConnection;153;0;2;0
WireConnection;58;0;56;0
WireConnection;163;0;5;0
WireConnection;51;0;139;0
WireConnection;51;1;52;0
WireConnection;51;2;33;0
WireConnection;51;3;33;0
WireConnection;51;4;42;0
WireConnection;155;0;3;0
WireConnection;159;0;4;0
WireConnection;100;5;154;0
WireConnection;100;3;12;0
WireConnection;100;4;65;0
WireConnection;55;0;51;0
WireConnection;55;1;58;0
WireConnection;53;0;141;0
WireConnection;53;1;54;0
WireConnection;53;2;55;0
WireConnection;53;3;51;0
WireConnection;53;4;51;0
WireConnection;157;0;100;0
WireConnection;164;0;6;0
WireConnection;103;5;169;0
WireConnection;103;3;65;0
WireConnection;103;4;12;0
WireConnection;165;0;7;0
WireConnection;188;26;156;0
WireConnection;188;27;12;0
WireConnection;188;25;77;0
WireConnection;188;24;160;0
WireConnection;172;0;103;0
WireConnection;61;0;140;0
WireConnection;61;1;62;0
WireConnection;61;2;51;0
WireConnection;61;3;51;0
WireConnection;61;4;53;0
WireConnection;78;3;12;0
WireConnection;78;2;188;0
WireConnection;78;1;158;0
WireConnection;152;0;9;0
WireConnection;152;1;61;0
WireConnection;166;0;8;0
WireConnection;187;26;170;0
WireConnection;187;27;12;0
WireConnection;187;25;161;0
WireConnection;187;24;171;0
WireConnection;117;0;152;0
WireConnection;109;3;78;0
WireConnection;109;2;187;0
WireConnection;109;1;173;0
WireConnection;113;3;12;0
WireConnection;113;2;109;0
WireConnection;113;1;167;0
WireConnection;122;3;109;0
WireConnection;122;2;65;0
WireConnection;122;1;174;0
WireConnection;114;3;113;0
WireConnection;114;2;65;0
WireConnection;114;1;120;0
WireConnection;176;0;175;0
WireConnection;111;0;168;0
WireConnection;111;1;112;0
WireConnection;111;2;122;0
WireConnection;111;3;122;0
WireConnection;111;4;114;0
WireConnection;126;3;12;0
WireConnection;126;2;111;0
WireConnection;126;1;124;0
WireConnection;123;0;177;0
WireConnection;123;1;125;0
WireConnection;123;2;111;0
WireConnection;123;3;111;0
WireConnection;123;4;126;0
WireConnection;201;1;14;0
WireConnection;149;0;61;0
WireConnection;149;1;151;0
WireConnection;149;2;123;0
WireConnection;149;3;12;0
WireConnection;149;4;12;0
WireConnection;199;0;149;0
WireConnection;199;1;200;0
WireConnection;205;0;201;0
WireConnection;206;0;205;0
WireConnection;206;1;207;0
WireConnection;206;2;199;0
WireConnection;206;3;12;0
WireConnection;206;4;12;0
WireConnection;216;0;214;0
WireConnection;216;1;217;0
WireConnection;216;2;206;0
WireConnection;216;3;206;0
WireConnection;216;4;12;0
WireConnection;213;42;216;0
WireConnection;213;48;190;0
WireConnection;213;50;192;0
WireConnection;213;51;193;0
WireConnection;213;52;194;0
WireConnection;196;0;195;0
WireConnection;196;1;197;0
WireConnection;196;2;213;0
WireConnection;196;3;213;0
WireConnection;196;4;216;0
WireConnection;0;2;196;0
ASEEND*/
//CHKSM=8B9B90799DD6BE9F84F5848F4363F46B0CC5032A