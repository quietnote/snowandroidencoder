// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ARStudio/ColorTemperature"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_weight("weight", Range( -1 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _weight;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode1 = tex2D( _MainTex, uv_MainTex );
			float valueG11 = tex2DNode1.g;
			float valueR38 = tex2DNode1.r;
			float weightR5 = ( _weight + 1.0 );
			float ifLocalVar35 = 0;
			if( valueG11 <= 0.0 )
				ifLocalVar35 = valueR38;
			else
				ifLocalVar35 = min( ( ( valueR38 / valueG11 ) * weightR5 * valueG11 ) , 1.0 );
			float newR23 = ifLocalVar35;
			float valueB39 = tex2DNode1.b;
			float weightB14 = ( 1.0 - _weight );
			float ifLocalVar48 = 0;
			if( valueG11 <= 0.0 )
				ifLocalVar48 = valueB39;
			else
				ifLocalVar48 = min( ( ( valueB39 / valueG11 ) * weightB14 * valueG11 ) , 1.0 );
			float newB24 = ifLocalVar48;
			float valueA22 = tex2DNode1.a;
			float4 appendResult20 = (float4(newR23 , valueG11 , newB24 , valueA22));
			o.Emission = appendResult20.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15900
2621;41;2499;1357;1427.073;1922.872;2.161379;True;False
Node;AmplifyShaderEditor.CommentaryNode;43;-755.7401,-764.0314;Float;False;688.2904;427.7261;Input Texture;5;1;38;11;39;22;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;29;-767.4871,334.4879;Float;False;730.5001;312;Generate Weight for R, B;6;10;4;9;13;5;14;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;10;-585.9667,490.9278;Float;False;Constant;_Float0;Float 0;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-705.7401,-627.1721;Float;True;Property;_MainTex;MainTex;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;4;-717.4871,385.4879;Float;False;Property;_weight;weight;1;0;Create;True;0;0;False;0;0;0;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;9;-428.4859,389.4879;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;30;-772.2608,-265.5;Float;False;1722.326;550.9199;Change R/G, B/G with Weight;17;23;24;8;6;17;3;16;2;15;7;31;32;33;34;40;41;42;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;11;-305.839,-623.0756;Float;False;valueG;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;38;-302.8188,-714.0314;Float;False;valueR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;13;-428.4859,514.4879;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;39;-303.8188,-531.0314;Float;False;valueB;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;40;-603.8188,-198.0314;Float;False;38;valueR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;41;-602.8188,-108.0314;Float;False;11;valueG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;42;-603.8188,-13.03143;Float;False;39;valueB;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;14;-270.4862,509.4879;Float;False;weightB;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;5;-271.4861,384.4879;Float;False;weightR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;2;-331.7675,-204.3562;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;15;-14.5,105.5;Float;False;14;weightB;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;17;-13.5,184.5;Float;False;11;valueG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;3;-323.5996,9.020504;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;16;-15.5,-60.5;Float;False;11;valueG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;7;-16.5,-131.5;Float;False;5;weightR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;207.5,28.5;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;34;341.418,118.256;Float;False;Constant;_Float2;Float 2;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;204.5,-209.5;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;32;336.409,-108.1493;Float;False;Constant;_Float1;Float 1;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;33;490.3172,26.89739;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;36;388.4112,-454.0938;Float;False;Constant;_Float3;Float 3;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;47;357.7278,307.0177;Float;False;11;valueG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;37;365.9552,-525.8923;Float;False;11;valueG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;31;475.2282,-206.2279;Float;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;44;364.1812,-374.0314;Float;False;38;valueR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;45;380.1838,378.8162;Float;False;Constant;_Float4;Float 4;2;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;46;355.9539,458.8786;Float;False;39;valueB;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;35;591.0864,-521.7482;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ConditionalIfNode;48;582.8591,311.1617;Float;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;24;730.2523,24.01507;Float;False;newB;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;28;1201.036,-281.5178;Float;False;771.5;509.5;Make New Color;6;27;26;21;25;20;0;;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;23;728.912,-209.9493;Float;False;newR;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;22;-301.4497,-451.3054;Float;False;valueA;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;25;1251.036,-231.5178;Float;False;23;newR;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;26;1252.036,-84.51779;Float;False;24;newB;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;27;1252.036,-9.517803;Float;False;22;valueA;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;21;1253.036,-158.5178;Float;False;11;valueG;1;0;OBJECT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;20;1473.036,-175.5178;Float;True;COLOR;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1712.535,-219.0178;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ARStudio/ColorTemperature;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;9;0;4;0
WireConnection;9;1;10;0
WireConnection;11;0;1;2
WireConnection;38;0;1;1
WireConnection;13;0;10;0
WireConnection;13;1;4;0
WireConnection;39;0;1;3
WireConnection;14;0;13;0
WireConnection;5;0;9;0
WireConnection;2;0;40;0
WireConnection;2;1;41;0
WireConnection;3;0;42;0
WireConnection;3;1;41;0
WireConnection;8;0;3;0
WireConnection;8;1;15;0
WireConnection;8;2;17;0
WireConnection;6;0;2;0
WireConnection;6;1;7;0
WireConnection;6;2;16;0
WireConnection;33;0;8;0
WireConnection;33;1;34;0
WireConnection;31;0;6;0
WireConnection;31;1;32;0
WireConnection;35;0;37;0
WireConnection;35;1;36;0
WireConnection;35;2;31;0
WireConnection;35;3;44;0
WireConnection;35;4;44;0
WireConnection;48;0;47;0
WireConnection;48;1;45;0
WireConnection;48;2;33;0
WireConnection;48;3;46;0
WireConnection;48;4;46;0
WireConnection;24;0;48;0
WireConnection;23;0;35;0
WireConnection;22;0;1;4
WireConnection;20;0;25;0
WireConnection;20;1;21;0
WireConnection;20;2;26;0
WireConnection;20;3;27;0
WireConnection;0;2;20;0
ASEEND*/
//CHKSM=50BE75D6690D566E0B26DDB09756EF6B6D759763