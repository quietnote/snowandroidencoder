﻿Shader "Snow/Distort"
{
    Properties
    {
        // u_texture ("Texture", 2D) = "white" {}
        count("Distort_Count", int) = 0
        faceFactors("FaceFactor (Y,aspect,eyeDist,R)", Vector) = (0,0,0,0)
        validCR("center,radius", Vector) = (0,0,0,0)
        valid_threshold("threshold", float) = 1.0
        u_renderOnlyDistortion("OnlyDistortion", int) = 0
        rotateUV("uv mat", Vector) = (1,0,0,1)
        transUV("uv trans", Vector) = (0,0,0,0)
        yFlipCoef("Y Flip Coefficient", Vector) = (1,1,0,0)
    }
    SubShader
    {
        Tags {
        }
        LOD 400
        Cull Off 
        ZWrite Off
        ZTest Off

        GrabPass { }

        Pass
        {
            Name "Distort"
            Tags {
            }

            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            
            #define MAXPOINTS 30

            // circles = (float2 center, float2 radius)
            uniform float4 circles[MAXPOINTS];
            // groups = (scale, angle, u_min, u_max)
            uniform float4 groups[MAXPOINTS];
            uniform float types[MAXPOINTS];
            uniform float progress[MAXPOINTS];

            uniform int count;
            // faceFactors = (faceYFactor, aspectRatio, eyeDist, faceRoll)
            uniform float4 faceFactors;

            uniform float4 validCR;
            //uniform float2 valid_center;
            //uniform float2 valid_radius;
            uniform float valid_threshold;
            
            // uniform sampler2D u_texture; uniform float4 u_texture_ST;
            uniform int u_renderOnlyDistortion;
            uniform sampler2D _GrabTexture;
            // yFlipCoef(YPosFlipCoef,YUVFlipCoef,YUVFlipConst,None)
            uniform float4 yFlipCoef;


            struct VertexInput {
                float4 vertex : POSITION;
                float2 a_texCoord : TEXCOORD0;
            };

            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 v_texCoord : TEXCOORD0;
                float2 v_orgTexCoord : TEXCOORD1;
            };



            float distanceFromValidCenter(float2 uv)
            {
                float aspectRatio = faceFactors.y;
                float faceRoll = faceFactors.w;

                // faceRoll should be given in screen coordinate
                float roll = faceRoll * -1.0;
                float cosRoll = cos(roll);
                float sinRoll = sin(roll);

                float x = uv.x;
                float y = uv.y * aspectRatio;
                float a = validCR.x;
                float b = validCR.y * aspectRatio;
                float rx = validCR.z;
                float ry = validCR.w;

                float e1 = ((x - a) * cosRoll - (y - b) * sinRoll) / rx;
                float e2 = ((x - a) * sinRoll + (y - b) * cosRoll) / ry;
                float d = sqrt(e1 * e1 + e2 * e2);

                return d;
            }

            float weightFunction(float x, float t)
            {
                // cubic version
                return smoothstep(t, 1.0, x);
                // linear version
                // float y = x / (1.0 - t) + 1.0 - 1.0 / (1.0 - t);
                // return clamp(y, 0.0, 1.0);
            }

            float2 distortion(float2 uv, int i, float extraWeight) {

                float2 textureCoordinateToUse = uv;
                
                // Face Factors
                float faceYFactor = faceFactors.x;
                float aspectRatio = faceFactors.y;
                float eyeDist = faceFactors.z;
                float faceRoll = faceFactors.w;

                // Circles
                float2 center = circles[i].xy;
                float2 radius = circles[i].zw;
                
                // Groups
                float scale = groups[i].x;
                float angle = groups[i].y + faceRoll;
                float u_min = groups[i].z;
                float u_max = groups[i].w;

                float x = textureCoordinateToUse.x;
                float y = textureCoordinateToUse.y;

                // faceRoll should be given in screen coordinate
                float roll = faceRoll * -1.0;
                float cosRoll = cos(roll);
                float sinRoll = sin(roll);

                float e1 = ((x - center.x) * cosRoll - (y - center.y) * sinRoll * aspectRatio) / radius.x;
                float e2 = ((x - center.x) * sinRoll + (y - center.y) * cosRoll * aspectRatio) / radius.y;
                float d  = (e1 * e1) + (e2 * e2);

                if (d < 1.0) {
                    if((types[i] > 0.9 && types[i] < 1.1) || 
                    (types[i] > 2.9 && types[i] < 3.1)) {

                        // bulge
                        float2 dist = float2(d * radius.x, d * radius.y);
                        textureCoordinateToUse -= center;

                        float2 delta = ((radius - dist) / radius);
                        float deltaScale = scale * 0.04;
                        if(deltaScale > 0.0 && types[i] == 1.0) {
                            deltaScale = smoothstep(u_min, u_max, deltaScale);
                        }

                        float2 percent = 1.0 - ((delta * deltaScale) * progress[i] * extraWeight);
                        textureCoordinateToUse = textureCoordinateToUse * percent;
                        uv = textureCoordinateToUse + center;
                    } else if(types[i] > 1.9 && types[i] < 2.1) {

                        // shift
                        float dist = 1.0 - d;
                        float delta = (scale * 0.5 * eyeDist) * dist * progress[i] * extraWeight;

                        float deltaScale = smoothstep(u_min, u_max, dist);
                        float directionX = cos(angle) * deltaScale;
                        float directionY = sin(angle) * deltaScale / (faceYFactor * aspectRatio);

                        uv = float2(textureCoordinateToUse.x - (delta * directionX), textureCoordinateToUse.y -  (delta * directionY));
                    }
                }

                return uv;
            }

            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                float2 uv = v.a_texCoord;

                //o.pos = UnityObjectToClipPos( v.vertex );
                o.pos = float4(uv * 2.0 - 1.0, 0.0, 1.0);
                o.pos.y *= yFlipCoef.x;

                float w = valid_threshold >= 0.0 ? 1.0 - weightFunction(distanceFromValidCenter(v.a_texCoord), valid_threshold) : 1.0;
                if (w > 0.0) {
                    for (int i = 0; i < count; i++) {
                        uv = distortion(uv, i, w);
                    }
                }

                o.v_texCoord = float2(uv.x,yFlipCoef.z + yFlipCoef.y * uv.y);
                o.v_orgTexCoord = float2(v.a_texCoord.x,yFlipCoef.z + yFlipCoef.y * v.a_texCoord.y);
                return o;
            }

            float4 frag(VertexOutput i) : COLOR 
            {
                bool willDiscard = (u_renderOnlyDistortion > 1 && i.v_texCoord == i.v_orgTexCoord);
                float alpha = 1.0 - float(willDiscard);

                float4 color = tex2D(_GrabTexture, i.v_texCoord);

                float4 final = float4(color.rgb, color.a * alpha);

                return final;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
