// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ARStudio/BllomEffect"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_Blur0("Blur0", 2D) = "white" {}
		_Blur1("Blur1", 2D) = "white" {}
		_Blur2("Blur2", 2D) = "white" {}
		_Blur3("Blur3", 2D) = "white" {}
		_Blur4("Blur4", 2D) = "white" {}
		_weight("weight", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform sampler2D _Blur0;
		uniform float4 _Blur0_ST;
		uniform sampler2D _Blur1;
		uniform float4 _Blur1_ST;
		uniform sampler2D _Blur2;
		uniform float4 _Blur2_ST;
		uniform sampler2D _Blur3;
		uniform float4 _Blur3_ST;
		uniform sampler2D _Blur4;
		uniform float4 _Blur4_ST;
		uniform float _weight;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 uv_Blur0 = i.uv_texcoord * _Blur0_ST.xy + _Blur0_ST.zw;
			float2 uv_Blur1 = i.uv_texcoord * _Blur1_ST.xy + _Blur1_ST.zw;
			float2 uv_Blur2 = i.uv_texcoord * _Blur2_ST.xy + _Blur2_ST.zw;
			float2 uv_Blur3 = i.uv_texcoord * _Blur3_ST.xy + _Blur3_ST.zw;
			float2 uv_Blur4 = i.uv_texcoord * _Blur4_ST.xy + _Blur4_ST.zw;
			float4 lerpResult22 = lerp( tex2D( _MainTex, uv_MainTex ) , ( ( tex2D( _Blur0, uv_Blur0 ) * 0.1 ) + ( tex2D( _Blur1, uv_Blur1 ) * 0.35 ) + ( tex2D( _Blur2, uv_Blur2 ) * 0.35 ) + ( tex2D( _Blur3, uv_Blur3 ) * 0.35 ) + ( tex2D( _Blur4, uv_Blur4 ) * 0.1 ) ) , _weight);
			o.Emission = lerpResult22.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=15900
2621;58;2499;1360;1905.037;882.063;1;True;False
Node;AmplifyShaderEditor.SamplerNode;4;-1404.978,-638.4612;Float;True;Property;_Blur0;Blur0;1;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;35;-1118.037,-172.0632;Float;False;Constant;_Float3;Float 3;7;0;Create;True;0;0;False;0;0.35;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;5;-1402.978,-438.4611;Float;True;Property;_Blur1;Blur1;2;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;37;-1117.037,28.93677;Float;False;Constant;_Float4;Float 4;7;0;Create;True;0;0;False;0;0.35;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;7;-1398.978,-42.46115;Float;True;Property;_Blur3;Blur3;4;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;27;-1399.037,159.9368;Float;True;Property;_Blur4;Blur4;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;39;-1115.037,235.9368;Float;False;Constant;_Float0;Float 0;7;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;31;-1129.037,-556.0632;Float;False;Constant;_Float1;Float 1;7;0;Create;True;0;0;False;0;0.1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-1125.037,-365.0632;Float;False;Constant;_Float2;Float 2;7;0;Create;True;0;0;False;0;0.35;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-1399.978,-240.4611;Float;True;Property;_Blur2;Blur2;3;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-987.0375,-432.0632;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;36;-973.0375,-45.06323;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-965.0375,164.9368;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;30;-991.0375,-632.0632;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-973.0375,-240.0632;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;1;-1404.978,-854.4612;Float;True;Property;_MainTex;MainTex;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-885.2769,-756.4431;Float;False;Property;_weight;weight;6;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-673.9778,-545.4612;Float;False;5;5;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;22;-438.2769,-837.4431;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;29;-116.6177,-833.0491;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;ARStudio/BllomEffect;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;32;0;5;0
WireConnection;32;1;33;0
WireConnection;36;0;7;0
WireConnection;36;1;37;0
WireConnection;38;0;27;0
WireConnection;38;1;39;0
WireConnection;30;0;4;0
WireConnection;30;1;31;0
WireConnection;34;0;6;0
WireConnection;34;1;35;0
WireConnection;10;0;30;0
WireConnection;10;1;32;0
WireConnection;10;2;34;0
WireConnection;10;3;36;0
WireConnection;10;4;38;0
WireConnection;22;0;1;0
WireConnection;22;1;10;0
WireConnection;22;2;19;0
WireConnection;29;2;22;0
ASEEND*/
//CHKSM=3CC1F8127F7AB0718FD92F66686C6BD150DE134B