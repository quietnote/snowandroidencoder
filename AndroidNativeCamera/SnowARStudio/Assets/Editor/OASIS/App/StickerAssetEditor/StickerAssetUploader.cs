﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace OASIS
{
    using CS;
    public class StickerAssetUploader : EditorWindow
    {
        [MenuItem("SNOW/Sticker Uploader")]
        static void Init()
        {
            StickerAssetUploader window = (StickerAssetUploader)GetWindow(typeof(StickerAssetUploader));
            window.Show();
        }

        void OnEnable()
        {
            titleContent = new GUIContent("Sticker Uploader");
        }

        void OnGUI()
        {
            if (IsBusy())
            {
                GUILayout.Box("Busy Uploading... (blocking script recompile to keep AWS SDK static queue consistent)");
                GUILayout.Label("S3Region = " + m_S3Region);
                GUILayout.Label("Bucket = " + m_BucketName);

                for (int i = 0; i < m_Tasks.Length; ++i)
                {
                    GUILayout.Space(10.0f);
                    GUILayout.Label("Task Idx = " + i);
                    GUILayout.Label("Name = " + m_Tasks[i].stickerName);
                    GUILayout.Label("Path = " + m_Tasks[i].bundlePath);
                    GUILayout.Label("Platform = " + m_Tasks[i].bundlePlatform);
                }
            }
            else
            {
                GUILayout.Box("Ready To Upload...");
            }
        }

        public struct Task
        {
            public string stickerName;
            public string bundlePath;
            public BuildTarget bundlePlatform;
        }

        public bool UploadStickers(Task[] uploadTasks, ContentsServerSettings serverSettings)
        {
            if (IsBusy())
            {
                return false;
            }

            EditorApplication.LockReloadAssemblies();

            m_ManagerGO = new GameObject
            {
                name = "AWSManagerGO",
                hideFlags = HideFlags.HideAndDontSave
            };

            m_AWSMainThreadDispatcher = UnityInitializer.StartAWSSession(m_ManagerGO);
            m_AWSMainThreadDispatcher.runInEditMode = true;

            m_Credentials = new CognitoAWSCredentials(serverSettings.identityPoolId, serverSettings.cognitoIdentityRegion);
            m_S3Client = new AmazonS3Client(m_Credentials, serverSettings.s3Region);
            m_S3Region = serverSettings.s3Region;
            m_BucketName = serverSettings.bucketName;

            for (int i = 0; i < uploadTasks.Length; ++i)
            {
                string stickerName = uploadTasks[i].stickerName;
                string bundlePath = uploadTasks[i].bundlePath;
                BuildTarget bundlePlatform = uploadTasks[i].bundlePlatform;
                string S3FolderName = ARStickerRemoteStoragePolicy.GetFolderForBuildTarget(bundlePlatform);
                string S3Key = S3FolderName + "/" + stickerName;

                FileStream stream = new FileStream(bundlePath, FileMode.Open, FileAccess.Read, FileShare.Read);

                PostObjectRequest request = new PostObjectRequest()
                {
                    Bucket = m_BucketName,
                    Key = S3Key,
                    InputStream = stream,
                    CannedACL = S3CannedACL.Private,
                    Region = m_S3Region
                };

                m_S3Client.PostObjectAsync(request, HandleS3PostObject);
            }

            m_Tasks = (Task[])uploadTasks.Clone();

            return true;
        }

        public bool IsBusy()
        {
            return m_Tasks != null;
        }

        void HandleS3PostObject(AmazonServiceResult<PostObjectRequest, PostObjectResponse> responseObj)
        {
            if (responseObj.Exception != null)
            {
                Debug.LogError(responseObj.Exception.ToString());
            }

            CleanUpTasks();
        }

        void OnDisable()
        {
            CleanUpTasks();
        }

        void CleanUpTasks()
        {
            if (m_Tasks != null)
            {
                for (int i = 0; i < m_Tasks.Length; ++i)
                {
                    try
                    {
                        File.Delete(m_Tasks[i].bundlePath);
                    }
                    catch (IOException exception)
                    {
                        Debug.LogWarning(exception.ToString());
                    }
                }
                m_Tasks = null;

                m_BucketName = null;
                m_S3Region = null;
                m_S3Client.Dispose();
                m_S3Client = null;
                m_Credentials = null;

                UnityInitializer.EndAWSSession(m_AWSMainThreadDispatcher);
                m_AWSMainThreadDispatcher = null;

                DestroyImmediate(m_ManagerGO);
                m_ManagerGO = null;

                EditorApplication.UnlockReloadAssemblies();
            }
        }

        AWSCredentials m_Credentials;
        IAmazonS3 m_S3Client;
        RegionEndpoint m_S3Region;
        string m_BucketName;

        GameObject m_ManagerGO;
        Amazon.Runtime.Internal.UnityMainThreadDispatcher m_AWSMainThreadDispatcher;

        Task[] m_Tasks;
    }

}
