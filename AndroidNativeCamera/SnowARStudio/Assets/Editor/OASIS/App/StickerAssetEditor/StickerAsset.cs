﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace OASIS
{
    using CS;
    [CreateAssetMenu(fileName = "ARStickcer", menuName = "Snow/ARStickcer", order = 1)]
    public class StickerAsset : ScriptableObject
    {

#pragma warning disable CS0649 // Private SerializeField is set by Unity Editor

        [SerializeField]
        [Tooltip("Name for Sticker Data file")]
        string m_StickerName;
        public string stickerName
        {
            get { return m_StickerName; }
        }

        [SerializeField]
        [Tooltip("The scene to load when we load your sticker")]
        SceneAsset m_MainSceneAsset;
        public SceneAsset mainSceneAsset
        {
            get { return m_MainSceneAsset; }
        }

        [SerializeField]
        ContentsServerSettings m_ServerSettings;
        public ContentsServerSettings serverSettings
        {
            get { return m_ServerSettings; }
        }

#pragma warning restore CS0649

    }
}
