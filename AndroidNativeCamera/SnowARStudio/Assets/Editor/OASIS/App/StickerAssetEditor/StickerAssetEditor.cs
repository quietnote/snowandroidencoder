﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEditor;
using UnityEditor.Build.Pipeline;
using UnityEditor.Build.Pipeline.Interfaces;

using System;
using System.IO;

using Amazon;
using Amazon.S3;
using Amazon.Runtime;
using Amazon.CognitoIdentity;
using Amazon.S3.Model;

using Snow.G;

namespace OASIS
{
    using CS;
    [CustomEditor(typeof(StickerAsset))]
    public class StickerAssetEditor : Editor
    {
        SerializedProperty m_StickerName;
        SerializedProperty m_MainSceneAsset;
        SerializedProperty m_ServerSettings;

        void OnEnable()
        {
            m_StickerName = serializedObject.FindProperty("m_StickerName");
            m_MainSceneAsset = serializedObject.FindProperty("m_MainSceneAsset");
            m_ServerSettings = serializedObject.FindProperty("m_ServerSettings");
        }

        class ARStickerBuildContent : IBundleBuildContent, IBuildContent, IContextObject
        {
            public ARStickerBuildContent(string stickerName, SceneAsset mainSceneAsset)
            {
                string mainScenePath = AssetDatabase.GetAssetPath(mainSceneAsset);
                GUID mainSceneGUID = new GUID(AssetDatabase.AssetPathToGUID(mainScenePath));

                m_Assets = new List<GUID>();
                m_Scenes = new List<GUID>();
                m_Addresses = new Dictionary<GUID, string>();
                m_BundleLayout = new Dictionary<string, List<GUID>>();

                List<GUID> sceneBundleAssetList = new List<GUID>();
                sceneBundleAssetList.Add(mainSceneGUID);
                m_BundleLayout.Add(stickerName, sceneBundleAssetList);
                m_Addresses.Add(mainSceneGUID, stickerName); // TODO: This address need to be unique across all snow sticker authors
                Scenes.Add(mainSceneGUID);
            }

            List<GUID> m_Assets;
            public List<GUID> Assets
            {
                get
                {
                    return m_Assets;
                }
            }

            List<GUID> m_Scenes;
            public List<GUID> Scenes
            {
                get
                {
                    return m_Scenes;
                }
            }

            Dictionary<GUID, string> m_Addresses;
            public Dictionary<GUID, string> Addresses
            {
                get
                {
                    return m_Addresses;
                }
            }


            Dictionary<string, List<GUID>> m_BundleLayout;
            public Dictionary<string, List<GUID>> BundleLayout
            {
                get
                {
                    return m_BundleLayout;
                }
            }
        }

        void BuildAssetBundle(string stickerName, SceneAsset mainSceneAsset, BuildTarget bundleTarget, string targetDir, out IBundleBuildResults results)
        {
            AssetDatabase.SaveAssets();

            BuildTargetGroup bundleGroup;
            switch (bundleTarget)
            {
                case (BuildTarget.Android):
                    {
                        bundleGroup = BuildTargetGroup.Android;
                        break;
                    }
                case (BuildTarget.iOS):
                    {
                        bundleGroup = BuildTargetGroup.iOS;
                        break;
                    }
                case (BuildTarget.StandaloneWindows):
                case (BuildTarget.StandaloneWindows64):
                    {
                        bundleGroup = BuildTargetGroup.Standalone;
                        break;
                    }
                case (BuildTarget.StandaloneOSX):
                    {
                        bundleGroup = BuildTargetGroup.Standalone;
                        break;
                    }

                default:
                    {
                        bundleGroup = BuildTargetGroup.Unknown;
                        break;
                    }
            }

            BundleBuildParameters buildParameter = new BundleBuildParameters(bundleTarget, bundleGroup, targetDir);
            buildParameter.UseCache = false;

            ARStickerBuildContent buildContent = new ARStickerBuildContent(stickerName, mainSceneAsset);

            ContentPipeline.BuildAssetBundles(buildParameter, buildContent, out results);
        }

        GUIContent stickerNameLabel = new GUIContent("Sticker Name");
        GUIContent mainSceneAssetLabel = new GUIContent("Main Scene Asset");
        GUIContent serverSettingsLabel = new GUIContent("Server Settings");

        public override void OnInspectorGUI()
        {
            StickerAssetUploader stickerUploader = (StickerAssetUploader)EditorWindow.GetWindow(typeof(StickerAssetUploader), false, null, false);

            EditorGUILayout.PropertyField(m_StickerName, stickerNameLabel, GUILayout.Height(15));
            string stickerName = m_StickerName.stringValue;

            EditorGUILayout.PropertyField(m_MainSceneAsset, mainSceneAssetLabel, GUILayout.Height(15));
            SceneAsset mainSceneAsset = m_MainSceneAsset.objectReferenceValue as SceneAsset;

            EditorGUILayout.PropertyField(m_ServerSettings, serverSettingsLabel, GUILayout.Height(15));
            ContentsServerSettings serverSettings = m_ServerSettings.objectReferenceValue as ContentsServerSettings;

            GUILayout.Space(10.0f);

            if (GUILayout.Button("Save to StreamingAssets (All)"))
            {
                IBundleBuildResults results;

                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.Android, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.Android), out results);

                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.iOS, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.iOS), out results);

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneWindows, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.StandaloneWindows), out results);
                }
                else
                {
                    Debug.Assert(Application.platform == RuntimePlatform.OSXEditor);

                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneOSX, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.StandaloneOSX), out results);
                }
            }

            if (GUILayout.Button("Save to StreamingAssets (Android)"))
            {
                IBundleBuildResults results;
                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.Android, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.Android), out results);
            }

            if (GUILayout.Button("Save to StreamingAssets (iOS)"))
            {
                IBundleBuildResults results;
                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.iOS, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.iOS), out results);
            }

            if (GUILayout.Button("Save to StreamingAssets (Editor Platform)"))
            {
                IBundleBuildResults results;

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneWindows64, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.StandaloneWindows), out results);
                }
                else
                {
                    Debug.Assert(Application.platform == RuntimePlatform.OSXEditor);

                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneOSX, PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(BuildTarget.StandaloneOSX), out results);
                }
            }

            GUILayout.Space(10.0f);

            if (serverSettings == null || stickerUploader.IsBusy())
            {
                GUI.enabled = false;
            }

            if (GUILayout.Button("Upload to Cloud Server (All)"))
            {
                string tempDir = FileUtil.GetUniqueTempPathInProject();

                IBundleBuildResults results;

                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.Android, tempDir + "/Android", out results);

                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.iOS, tempDir + "/iOS", out results);

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneWindows, tempDir + "/Windows", out results);
                }
                else
                {
                    Debug.Assert(Application.platform == RuntimePlatform.OSXEditor);

                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneOSX, tempDir + "/OSX", out results);
                }

                StickerAssetUploader.Task[] tasks = new StickerAssetUploader.Task[3];

                tasks[0].stickerName = stickerName;
                tasks[0].bundlePath = Path.Combine(tempDir + "/Android", stickerName);
                tasks[0].bundlePlatform = BuildTarget.Android;

                tasks[1].stickerName = stickerName;
                tasks[1].bundlePath = Path.Combine(tempDir + "/iOS", stickerName);
                tasks[1].bundlePlatform = BuildTarget.iOS;

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    tasks[2].stickerName = stickerName;
                    tasks[2].bundlePath = Path.Combine(tempDir + "/Windows", stickerName);
                    tasks[2].bundlePlatform = BuildTarget.StandaloneWindows;
                }
                else
                {
                    Debug.Assert(Application.platform == RuntimePlatform.OSXEditor);

                    tasks[2].stickerName = stickerName;
                    tasks[2].bundlePath = Path.Combine(tempDir + "/OSX", stickerName);
                    tasks[2].bundlePlatform = BuildTarget.StandaloneOSX;
                }

                stickerUploader.UploadStickers(tasks, serverSettings);
                stickerUploader.Focus();
            }

            if (GUILayout.Button("Upload to Cloud Server (Android)"))
            {
                string tempDir = FileUtil.GetUniqueTempPathInProject();

                IBundleBuildResults results;
                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.Android, tempDir + "/Android", out results);

                StickerAssetUploader.Task[] tasks = new StickerAssetUploader.Task[1];

                tasks[0].stickerName = stickerName;
                tasks[0].bundlePath = Path.Combine(tempDir + "/Android", stickerName);
                tasks[0].bundlePlatform = BuildTarget.Android;

                stickerUploader.UploadStickers(tasks, serverSettings);
                stickerUploader.Focus();
            }

            if (GUILayout.Button("Upload to Cloud Server (iOS)"))
            {
                string tempDir = FileUtil.GetUniqueTempPathInProject();

                IBundleBuildResults results;
                BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.iOS, tempDir + "/iOS", out results);

                StickerAssetUploader.Task[] tasks = new StickerAssetUploader.Task[1];

                tasks[0].stickerName = stickerName;
                tasks[0].bundlePath = Path.Combine(tempDir + "/iOS", stickerName);
                tasks[0].bundlePlatform = BuildTarget.iOS;

                stickerUploader.UploadStickers(tasks, serverSettings);
                stickerUploader.Focus();
            }

            if (GUILayout.Button("Upload to Cloud Server (Editor Platform)"))
            {
                string tempDir = FileUtil.GetUniqueTempPathInProject();

                IBundleBuildResults results;

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneWindows, tempDir + "/Windows", out results);
                }
                else
                {
                    Debug.Assert(Application.platform == RuntimePlatform.OSXEditor);

                    BuildAssetBundle(stickerName, mainSceneAsset, BuildTarget.StandaloneOSX, tempDir + "/OSX", out results);
                }

                StickerAssetUploader.Task[] tasks = new StickerAssetUploader.Task[1];

                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    tasks[0].stickerName = stickerName;
                    tasks[0].bundlePath = Path.Combine(tempDir + "/Windows", stickerName);
                    tasks[0].bundlePlatform = BuildTarget.StandaloneWindows;
                }
                else
                {
                    Debug.Assert(Application.platform == RuntimePlatform.OSXEditor);

                    tasks[0].stickerName = stickerName;
                    tasks[0].bundlePath = Path.Combine(tempDir + "/OSX", stickerName);
                    tasks[0].bundlePlatform = BuildTarget.StandaloneOSX;
                }

                stickerUploader.UploadStickers(tasks, serverSettings);
                stickerUploader.Focus();
            }

            if (serverSettings == null || stickerUploader.IsBusy())
            {
                GUI.enabled = true;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}