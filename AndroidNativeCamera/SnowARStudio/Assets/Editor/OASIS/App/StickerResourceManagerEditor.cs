﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace OASIS
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(StickerResourceMananger))]
    public class StickerResourceManagerEditor : Editor
    {
        StickerResourceMananger castedTarget;
        string resourcePath;

        GUIContent createResourceFolderLabel;
        GUIContent resourceFolderLabel;
        GUIContent syncResourcesLabel;

        bool listAssets;
        GUIContent assetNameLabel;

        void OnEnable()
        {
            castedTarget = (StickerResourceMananger) target;
            resourcePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(castedTarget.gameObject.scene.path), "StickerResources");

            createResourceFolderLabel = new GUIContent("Create Resources Folder");
            resourceFolderLabel = new GUIContent("Resource Folder Path: ");
            syncResourcesLabel = new GUIContent("Sync Resources");

            listAssets = false;
            assetNameLabel = new GUIContent();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            Object resourseFolder = AssetDatabase.LoadAssetAtPath(resourcePath, typeof(Object));
            if (resourseFolder == null)
            {
                if (GUILayout.Button(createResourceFolderLabel))
                {
                    AssetDatabase.CreateFolder(System.IO.Path.GetDirectoryName(resourcePath), System.IO.Path.GetFileName(resourcePath));
                }
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.ObjectField(resourceFolderLabel, resourseFolder, typeof(Object), false);
                GUI.enabled = true;
            }

            if (GUILayout.Button(syncResourcesLabel))
            {
                string scenePath = castedTarget.gameObject.scene.path;
                string resourcePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(scenePath), "StickerResources");
                SyncFromResourcePath(resourcePath);
            }

            listAssets = EditorGUILayout.Foldout(listAssets, "Referenced Assets");
            if (listAssets)
            {
                GUI.enabled = false;
                for (int i = 0; i < castedTarget.referencedAssetNames.Count; ++i)
                {
                    assetNameLabel.text = castedTarget.referencedAssetNames[i];
                    EditorGUILayout.ObjectField(assetNameLabel, castedTarget.referencedAssets[i], typeof(Object), false);
                }
                GUI.enabled = true;
            }

            serializedObject.ApplyModifiedProperties();
        }

        void SyncFromResourcePath(string resourcePath)
        {
            string[] assetGUIDs = UnityEditor.AssetDatabase.FindAssets("", new string[] { resourcePath });

            HashSet<string> uniqueAssetPaths = new HashSet<string>();
            for (int i = 0; i < assetGUIDs.Length; ++i)
            {
                string assetPath = UnityEditor.AssetDatabase.GUIDToAssetPath(assetGUIDs[i]);
                uniqueAssetPaths.Add(assetPath);
            }

            castedTarget.referencedAssetNames.Clear();
            castedTarget.referencedAssets.Clear();

            foreach (string assetPath in uniqueAssetPaths)
            {
                string relativeAssetPath = assetPath.Substring(resourcePath.Length + 1);

                Object mainAsset = UnityEditor.AssetDatabase.LoadMainAssetAtPath(assetPath);
                if (mainAsset != null)
                {
                    // assetPath may be a directory which will load a DefaultAsset from LoadMainAssetAtPath
                    // we do not need to reference this kind of useless assets
                    if (mainAsset.GetType() != typeof(UnityEditor.DefaultAsset))
                    {
                        castedTarget.referencedAssetNames.Add(relativeAssetPath);
                        castedTarget.referencedAssets.Add(mainAsset);
                    }
                }

                Object[] objectsFromPath = UnityEditor.AssetDatabase.LoadAllAssetRepresentationsAtPath(assetPath);
                for (int i = 0; i < objectsFromPath.Length; ++i)
                {
                    castedTarget.referencedAssetNames.Add(relativeAssetPath + "/" + objectsFromPath[i].name);
                    castedTarget.referencedAssets.Add(objectsFromPath[i]);
                }
            }
        }
    }
}