﻿using UnityEditor;

namespace OASIS
{
    public class SceneFlowMenu
    {
        private const string mainMenu = "SNOW/";
        private const string flowMenu = mainMenu + "SceneFlow/";
        private const string DebugMenu = mainMenu + "Debug/";

        private const int sceneGroup = 0;

        private const int priority1 = 1000;

        [MenuItem(flowMenu + "SNOW Play Mode", true, priority = sceneGroup + priority1)]
        static bool ShowLoadStartSceneOnPlayMenu()
        {
            return (SceneFlowMenuEditor.CheckLoadStartSceneOnPlay () == false);
        }
        [MenuItem(flowMenu + "SNOW Play Mode", priority = sceneGroup + priority1)]
        private static void LoadStartSceneOnPlayEnable()
        {
            SceneFlowMenuEditor.SetLoadStartSceneOnPlay (true);
        }
        [MenuItem(flowMenu + "Legacy Play Mode", true, priority = sceneGroup + priority1)]
        static bool ShowDontLoadStartSceneOnPlayMenu()
        {
            return (SceneFlowMenuEditor.CheckLoadStartSceneOnPlay () == true);
        }
        [MenuItem(flowMenu + "Legacy Play Mode", priority = sceneGroup + priority1)]
        private static void LoadStartSceneOnPlayDisable()
        {
            SceneFlowMenuEditor.SetLoadStartSceneOnPlay (false);
        }

        
        [MenuItem(flowMenu + "STUDIO ON", true, priority = sceneGroup + priority1)]
        static bool ShowSnowStudioPlayMenu()
        {
            return (SceneFlowMenuEditor.CheckPlayForStudio () == false);
        }
        [MenuItem(flowMenu + "STUDIO ON", priority = sceneGroup + priority1)]
        private static void SnowStudioEnable()
        {
            SceneFlowMenuEditor.SetPlayForStudio (true);
        }
        [MenuItem(flowMenu + "STUDIO OFF", true, priority = sceneGroup + priority1)]
        static bool ShowSnowAppPlayMenu()
        {
            return (SceneFlowMenuEditor.CheckPlayForStudio () == true);
        }
        [MenuItem(flowMenu + "STUDIO OFF", priority = sceneGroup + priority1)]
        private static void SnowAppEnable()
        {
            SceneFlowMenuEditor.SetPlayForStudio (false);
        }


        

        private static bool _landmarkToggle = false;
        [MenuItem(DebugMenu + "Landmark ON")]     
        static bool ShowToggleLandmarkMenu()
        {
            return (SceneFlowMenuEditor.CheckLandmarkDebug() == true);
        }   
        [MenuItem(DebugMenu + "Landmark ON")]     
        private static void ToggleLandmarkMode()
        {   
            bool _landmarkToggle = !SceneFlowMenuEditor.CheckLandmarkDebug();
            Menu.SetChecked(DebugMenu + "Landmark ON", _landmarkToggle);
            SceneFlowMenuEditor.SetLandmarkDebug(_landmarkToggle);
        }

    }
}
