﻿using UnityEditor;
using UnityEditor.SceneManagement;

using UnityEngine;
using UnityEngine.SceneManagement;

// http://wiki.unity3d.com/index.php/SceneAutoLoader
namespace OASIS
{
    [InitializeOnLoad]
    public class SceneFlowMenuEditor : MonoBehaviour
    {
        private const string loadMasterOnPlayPrefName = "SceneAutoLoader.LoadMasterOnPlay";
        private const string previousScenePrefName = "SceneAutoLoader.PreviousScene";

        private const string playForStudioPrefName = "SceneAutoLoader.PlayForStudio";

        private const string landmarkDebugPrefName = "SceneAutoLoader.Landmark";

        private static bool loadMasterOnPlay
        {
            get
            {
                return EditorPrefs.GetBool(loadMasterOnPlayPrefName, true);
            }
            set
            {
                EditorPrefs.SetBool(loadMasterOnPlayPrefName, value);
            }
        }

        private static string previousScene
        {
            get
            {
                return EditorPrefs.GetString(previousScenePrefName, EditorSceneManager.GetActiveScene().path);
            }
            set
            {
                EditorPrefs.SetString(previousScenePrefName, value);
            }
        }

        private static bool playForStudio
        {
            get
            {
                return EditorPrefs.GetBool(playForStudioPrefName, false);
            }
            set
            {
                EditorPrefs.SetBool(playForStudioPrefName, value);
            }
        }

        private static string masterScene
        {
            get
            {
                return OASIS.ACConst.Path.SCENE + OASIS.ACConst.SceneName.STARTER + OASIS.ACConst.Extention.SCENE;
            }
        }

        static SceneFlowMenuEditor()
        {
            EditorApplication.playModeStateChanged += OnPlayModeChanged;
        }

        private static bool LoadScene(string sceneName, OpenSceneMode mode = OpenSceneMode.Single)
        {
            if (string.IsNullOrEmpty(sceneName))
            {
                return false;
            }
            try
            {
                EditorSceneManager.OpenScene(sceneName, mode);
            }
            catch
            {
                Debug.LogError(string.Format("error:Scene not found: {0}", sceneName));
                return false;
            }
            return true;
        }

        private static void OnPlayModeChanged(PlayModeStateChange state)
        {
            if (loadMasterOnPlay == false)
            {
                return;
            }
            string currentScene = EditorSceneManager.GetActiveScene().path;

            if (state == PlayModeStateChange.ExitingEditMode)
            {
                previousScene = currentScene;
                // 현재 씬의 변화를 저장할지 묻기
                if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                {
                    if (masterScene != previousScene)
                    {

                        if (!LoadScene(masterScene))
                        {
                            EditorApplication.isPlaying = false;
                        }
                        if (playForStudio)
                        {
                            LoadScene(previousScene, OpenSceneMode.Additive);
                        }
                    }
                }
                else
                {
                    EditorApplication.isPlaying = false;
                }
            }
            else if (state == PlayModeStateChange.EnteredEditMode)
            {
                if (masterScene != previousScene)
                {
                    LoadScene(previousScene);
                }
            }

        }

        public static void SetLoadStartSceneOnPlay(bool curState)
        {
            loadMasterOnPlay = curState;
        }

        public static bool CheckLoadStartSceneOnPlay()
        {
            return loadMasterOnPlay;
        }

        public static void SetPlayForStudio(bool curState)
        {
            playForStudio = curState;
        }

        public static bool CheckPlayForStudio()
        {
            return playForStudio;
        }


        // heejun landmark debug
        private static bool landmarkDebug
        {
            get
            {
                return EditorPrefs.GetBool(landmarkDebugPrefName, false);
            }
            set
            {
                EditorPrefs.SetBool(landmarkDebugPrefName, value);
            }
        }

        public static void SetLandmarkDebug(bool curState)
        {
            landmarkDebug = curState;
        }

        public static bool CheckLandmarkDebug()
        {
            return landmarkDebug;
        }
    }
}