﻿using System.Collections.Generic;
using System;
using XLua;
using System.Reflection;
using System.Linq;

namespace OASIS.Contents
{
    public static class LuaConfig
    {
        [LuaCallCSharp]
        public static IEnumerable<Type> LuaCallCSharp
        {
            get
            {
                return new List<Type>()
                {
                    // Fundamental struts and classes from Unity
                    typeof(UnityEngine.Vector2),
                    typeof(UnityEngine.Vector3),
                    typeof(UnityEngine.Vector4),
                    typeof(UnityEngine.Color),
                    typeof(UnityEngine.Quaternion),
                    typeof(UnityEngine.Ray),
                    typeof(UnityEngine.Bounds),
                    typeof(UnityEngine.Ray2D),

                    typeof(UnityEngine.Object),
                    typeof(UnityEngine.GameObject),
                    typeof(UnityEngine.ScriptableObject),
                    typeof(UnityEngine.MonoBehaviour),

                    typeof(UnityEngine.Debug),
                    typeof(UnityEngine.Mathf),

                    // Our defined Lua interface
                    typeof(OASIS.Lua.LuaBehaviour),
                    typeof(OASIS.Contents.LuaFaceBehaviour),
                    typeof(OASIS.Contents.LuaInterface),

                    // Including all classes from UnityEngine namespace  
                    // We excluded:
                    // Services like Analytics, InAppPurchase, Advertisements
                    // Unused platform like WSA, tvOS
                    // Testing API like TestTools
                    // "seriously" obsolete classes (can )
                    
                    // We kept the "not-seriously" obsolete classes, but xLua do not generate wrapper for them anyways
                    // If you want to use these "not-seriously" obsolete classes, change xLua's code-gen algorithm 
                    
                    // XLua can handle concrete instances of generic classes if you specify explicitly the generic parameters
                    
                    typeof(UnityEngine.Accessibility.VisionUtility),

                    typeof(UnityEngine.AI.NavMesh),
                    typeof(UnityEngine.AI.NavMeshAgent),
                    typeof(UnityEngine.AI.NavMeshBuildDebugSettings),
                    typeof(UnityEngine.AI.NavMeshBuilder),
                    typeof(UnityEngine.AI.NavMeshBuildMarkup),
                    typeof(UnityEngine.AI.NavMeshBuildSettings),
                    typeof(UnityEngine.AI.NavMeshBuildSource),
                    typeof(UnityEngine.AI.NavMeshData),
                    typeof(UnityEngine.AI.NavMeshDataInstance),
                    typeof(UnityEngine.AI.NavMeshHit),
                    typeof(UnityEngine.AI.NavMeshLinkData),
                    typeof(UnityEngine.AI.NavMeshLinkInstance),
                    typeof(UnityEngine.AI.NavMeshObstacle),
                    typeof(UnityEngine.AI.NavMeshPath),
                    typeof(UnityEngine.AI.NavMeshQueryFilter),
                    typeof(UnityEngine.AI.NavMeshTriangulation),
                    typeof(UnityEngine.AI.OffMeshLink),
                    typeof(UnityEngine.AI.OffMeshLinkData),

                    typeof(UnityEngine.AI.NavMeshBuildDebugFlags),
                    typeof(UnityEngine.AI.NavMeshBuildSourceShape),
                    typeof(UnityEngine.AI.NavMeshCollectGeometry),
                    typeof(UnityEngine.AI.NavMeshObstacleShape),
                    typeof(UnityEngine.AI.NavMeshPathStatus),
                    typeof(UnityEngine.AI.ObstacleAvoidanceType),
                    typeof(UnityEngine.AI.OffMeshLinkType),
                    
                    #if UNITY_ANDROID
                    typeof(UnityEngine.Android.Permission),
                    #endif

                    typeof(UnityEngine.Animations.AimConstraint),
                    typeof(UnityEngine.Animations.AnimationClipPlayable),
                    typeof(UnityEngine.Animations.AnimationLayerMixerPlayable),
                    typeof(UnityEngine.Animations.AnimationMixerPlayable),
                    typeof(UnityEngine.Animations.AnimationPlayableBinding),
                    typeof(UnityEngine.Animations.AnimationPlayableOutput),
                    typeof(UnityEngine.Animations.AnimatorControllerPlayable),
                    typeof(UnityEngine.Animations.ConstraintSource),
                    typeof(UnityEngine.Animations.LookAtConstraint),
                    typeof(UnityEngine.Animations.ParentConstraint),
                    typeof(UnityEngine.Animations.PositionConstraint),
                    typeof(UnityEngine.Animations.RotationConstraint),
                    typeof(UnityEngine.Animations.ScaleConstraint),
                    
                    typeof(UnityEngine.Animations.IConstraint),
                    
                    typeof(UnityEngine.Animations.Axis),

                    #if UNITY_IPHONE
                    typeof(UnityEngine.Apple.ReplayKit.ReplayKit),
                    #endif

                    typeof(UnityEngine.Assertions.Comparers.FloatComparer),
                    // typeof(UnityEngine.Assertions.Must.MustExtensions), // Comment out obsolete class
                    typeof(UnityEngine.Assertions.Assert),
                    typeof(UnityEngine.Assertions.AssertionException),

                    typeof(UnityEngine.Audio.AudioClipPlayable),
                    typeof(UnityEngine.Audio.AudioMixer),
                    typeof(UnityEngine.Audio.AudioMixerGroup),
                    typeof(UnityEngine.Audio.AudioMixerPlayable),
                    typeof(UnityEngine.Audio.AudioMixerSnapshot),
                    typeof(UnityEngine.Audio.AudioPlayableBinding),
                    typeof(UnityEngine.Audio.AudioPlayableOutput),
                    
                    typeof(UnityEngine.Audio.AudioMixerUpdateMode),

                    typeof(UnityEngine.CrashReportHandler.CrashReportHandler),
                    
                    typeof(UnityEngine.Diagnostics.Utils),
                    
                    typeof(UnityEngine.Diagnostics.ForcedCrashCategory),

                    typeof(UnityEngine.Events.UnityEvent),
                    typeof(UnityEngine.Events.UnityEvent<int>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Events.UnityEventBase),

                    typeof(UnityEngine.Events.PersistentListenerMode),
                    typeof(UnityEngine.Events.UnityEventCallState),

                    typeof(UnityEngine.EventSystems.AbstractEventData),
                    typeof(UnityEngine.EventSystems.AxisEventData),
                    typeof(UnityEngine.EventSystems.BaseEventData),
                    typeof(UnityEngine.EventSystems.BaseInput),
                    typeof(UnityEngine.EventSystems.BaseInputModule),
                    typeof(UnityEngine.EventSystems.BaseRaycaster),
                    typeof(UnityEngine.EventSystems.EventSystem),
                    typeof(UnityEngine.EventSystems.EventTrigger),
                    typeof(UnityEngine.EventSystems.ExecuteEvents),
                    typeof(UnityEngine.EventSystems.Physics2DRaycaster),
                    typeof(UnityEngine.EventSystems.PhysicsRaycaster),
                    typeof(UnityEngine.EventSystems.PointerEventData),
                    typeof(UnityEngine.EventSystems.PointerInputModule),
                    typeof(UnityEngine.EventSystems.RaycastResult),
                    typeof(UnityEngine.EventSystems.StandaloneInputModule),
                    typeof(UnityEngine.EventSystems.UIBehaviour),
                    
                    typeof(UnityEngine.EventSystems.IBeginDragHandler),
                    typeof(UnityEngine.EventSystems.ICancelHandler),
                    typeof(UnityEngine.EventSystems.IDeselectHandler),
                    typeof(UnityEngine.EventSystems.IDragHandler),
                    typeof(UnityEngine.EventSystems.IDropHandler),
                    typeof(UnityEngine.EventSystems.IEndDragHandler),
                    typeof(UnityEngine.EventSystems.IEventSystemHandler),
                    typeof(UnityEngine.EventSystems.IInitializePotentialDragHandler),
                    typeof(UnityEngine.EventSystems.IMoveHandler),
                    typeof(UnityEngine.EventSystems.IPointerClickHandler),
                    typeof(UnityEngine.EventSystems.IPointerDownHandler),
                    typeof(UnityEngine.EventSystems.IPointerEnterHandler),
                    typeof(UnityEngine.EventSystems.IPointerExitHandler),
                    typeof(UnityEngine.EventSystems.IPointerUpHandler),
                    typeof(UnityEngine.EventSystems.IScrollHandler),
                    typeof(UnityEngine.EventSystems.ISelectHandler),
                    typeof(UnityEngine.EventSystems.ISubmitHandler),
                    typeof(UnityEngine.EventSystems.IUpdateSelectedHandler),
                                       
                    typeof(UnityEngine.EventSystems.EventHandle),
                    typeof(UnityEngine.EventSystems.EventTriggerType),
                    typeof(UnityEngine.EventSystems.MoveDirection),

                    typeof(UnityEngine.Experimental.AI.NavMeshLocation),
                    typeof(UnityEngine.Experimental.AI.NavMeshQuery),
                    typeof(UnityEngine.Experimental.AI.NavMeshWorld),
                    typeof(UnityEngine.Experimental.AI.PolygonId),
                    
                    typeof(UnityEngine.Experimental.AI.NavMeshPolyTypes),
                    typeof(UnityEngine.Experimental.AI.PathQueryStatus),

                    typeof(UnityEngine.Experimental.Animations.AnimationHumanStream),
                    typeof(UnityEngine.Experimental.Animations.AnimationScriptPlayable),
                    typeof(UnityEngine.Experimental.Animations.AnimationStream),
                    typeof(UnityEngine.Experimental.Animations.AnimatorJobExtensions),
                    typeof(UnityEngine.Experimental.Animations.MuscleHandle),
                    typeof(UnityEngine.Experimental.Animations.PropertySceneHandle),
                    typeof(UnityEngine.Experimental.Animations.PropertyStreamHandle),
                    typeof(UnityEngine.Experimental.Animations.TransformSceneHandle),
                    typeof(UnityEngine.Experimental.Animations.TransformStreamHandle),
                    
                    typeof(UnityEngine.Experimental.Animations.IAnimationJob),
                    typeof(UnityEngine.Experimental.Animations.IAnimationJobPlayable),
          
                    typeof(UnityEngine.Experimental.Audio.AudioSampleProvider),

                    typeof(UnityEngine.Experimental.GlobalIllumination.DirectionalLight),
                    typeof(UnityEngine.Experimental.GlobalIllumination.DiscLight),
                    typeof(UnityEngine.Experimental.GlobalIllumination.LightDataGI),
                    typeof(UnityEngine.Experimental.GlobalIllumination.LightmapperUtils),
                    typeof(UnityEngine.Experimental.GlobalIllumination.Lightmapping),
                    typeof(UnityEngine.Experimental.GlobalIllumination.LinearColor),
                    typeof(UnityEngine.Experimental.GlobalIllumination.PointLight),
                    typeof(UnityEngine.Experimental.GlobalIllumination.RectangleLight),
                    typeof(UnityEngine.Experimental.GlobalIllumination.SpotLight),
                    
                    typeof(UnityEngine.Experimental.GlobalIllumination.FalloffType),
                    typeof(UnityEngine.Experimental.GlobalIllumination.LightMode),
                    typeof(UnityEngine.Experimental.GlobalIllumination.LightType),

                    typeof(UnityEngine.Experimental.LowLevel.PlayerLoop),
                    typeof(UnityEngine.Experimental.LowLevel.PlayerLoopSystem),

                    typeof(UnityEngine.Experimental.Playables.CameraPlayable),
                    typeof(UnityEngine.Experimental.Playables.MaterialEffectPlayable),
                    typeof(UnityEngine.Experimental.Playables.TextureMixerPlayable),
                    typeof(UnityEngine.Experimental.Playables.TexturePlayableBinding),
                    typeof(UnityEngine.Experimental.Playables.TexturePlayableOutput),

                    typeof(UnityEngine.Experimental.PlayerLoop.EarlyUpdate),
                    typeof(UnityEngine.Experimental.PlayerLoop.FixedUpdate),
                    typeof(UnityEngine.Experimental.PlayerLoop.Initialization),
                    typeof(UnityEngine.Experimental.PlayerLoop.PostLateUpdate),
                    typeof(UnityEngine.Experimental.PlayerLoop.PreLateUpdate),
                    typeof(UnityEngine.Experimental.PlayerLoop.PreUpdate),
                    typeof(UnityEngine.Experimental.PlayerLoop.Update),

                    typeof(UnityEngine.Experimental.Rendering.BlendState),
                    typeof(UnityEngine.Experimental.Rendering.CameraProperties),
                    typeof(UnityEngine.Experimental.Rendering.CoreCameraValues),
                    typeof(UnityEngine.Experimental.Rendering.CullResults),
                    typeof(UnityEngine.Experimental.Rendering.DepthState),
                    typeof(UnityEngine.Experimental.Rendering.DrawRendererSettings),
                    typeof(UnityEngine.Experimental.Rendering.DrawRendererSortSettings),
                    typeof(UnityEngine.Experimental.Rendering.DrawShadowsSettings),
                    typeof(UnityEngine.Experimental.Rendering.FilterRenderersSettings),
                    typeof(UnityEngine.Experimental.Rendering.FilterResults),
                    typeof(UnityEngine.Experimental.Rendering.LODParameters),
                    typeof(UnityEngine.Experimental.Rendering.RasterState),
                    typeof(UnityEngine.Experimental.Rendering.RenderPass),
                    typeof(UnityEngine.Experimental.Rendering.RenderPassAttachment),
                    typeof(UnityEngine.Experimental.Rendering.RenderPipeline),
                    typeof(UnityEngine.Experimental.Rendering.RenderPipelineAsset),
                    typeof(UnityEngine.Experimental.Rendering.RenderPipelineManager),
                    typeof(UnityEngine.Experimental.Rendering.RenderQueueRange),
                    typeof(UnityEngine.Experimental.Rendering.RenderStateBlock),
                    typeof(UnityEngine.Experimental.Rendering.RenderStateMapping),
                    typeof(UnityEngine.Experimental.Rendering.RenderTargetBlendState),
                    typeof(UnityEngine.Experimental.Rendering.ScriptableCullingParameters),
                    typeof(UnityEngine.Experimental.Rendering.ScriptableRenderContext),
                    typeof(UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystem),
                    typeof(UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings),
                    typeof(UnityEngine.Experimental.Rendering.ShaderPassName),
                    typeof(UnityEngine.Experimental.Rendering.ShadowSplitData),
                    typeof(UnityEngine.Experimental.Rendering.StencilState),
                    typeof(UnityEngine.Experimental.Rendering.SupportedRenderingFeatures),
                    typeof(UnityEngine.Experimental.Rendering.VisibleLight),
                    typeof(UnityEngine.Experimental.Rendering.VisibleReflectionProbe),

                    typeof(UnityEngine.Experimental.Rendering.IRenderPipeline),
                    typeof(UnityEngine.Experimental.Rendering.IRenderPipelineAsset),
                    typeof(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem),
                    
                    typeof(UnityEngine.Experimental.Rendering.DrawRendererFlags),
                    typeof(UnityEngine.Experimental.Rendering.DrawRendererSortMode),
                    typeof(UnityEngine.Experimental.Rendering.FormatUsage),
                    typeof(UnityEngine.Experimental.Rendering.GraphicsFormat),
                    typeof(UnityEngine.Experimental.Rendering.ReflectionProbeSortOptions),
                    typeof(UnityEngine.Experimental.Rendering.RendererConfiguration),
                    typeof(UnityEngine.Experimental.Rendering.RenderStateMask),
                    typeof(UnityEngine.Experimental.Rendering.SortFlags),
                    typeof(UnityEngine.Experimental.Rendering.VisibleLightFlags),

                    typeof(UnityEngine.Experimental.TerrainAPI.TerrainPaintUtility),
                    typeof(UnityEngine.Experimental.TerrainAPI.TerrainUtility),

                    typeof(UnityEngine.Experimental.U2D.AngleRangeInfo),
                    typeof(UnityEngine.Experimental.U2D.PixelPerfectRendering),
                    typeof(UnityEngine.Experimental.U2D.ShapeControlPoint),
                    typeof(UnityEngine.Experimental.U2D.SpriteBone),
                    typeof(UnityEngine.Experimental.U2D.SpriteDataAccessExtensions),
                    typeof(UnityEngine.Experimental.U2D.SpriteRendererDataAccessExtensions),
                    typeof(UnityEngine.Experimental.U2D.SpriteShapeMetaData),
                    typeof(UnityEngine.Experimental.U2D.SpriteShapeParameters),
                    typeof(UnityEngine.Experimental.U2D.SpriteShapeRenderer),
                    typeof(UnityEngine.Experimental.U2D.SpriteShapeUtility),

                    typeof(UnityEngine.Experimental.UIElements.StyleEnums.Align),
                    typeof(UnityEngine.Experimental.UIElements.StyleEnums.FlexDirection),
                    typeof(UnityEngine.Experimental.UIElements.StyleEnums.Justify),
                    typeof(UnityEngine.Experimental.UIElements.StyleEnums.PositionType),
                    typeof(UnityEngine.Experimental.UIElements.StyleEnums.Visibility),
                    typeof(UnityEngine.Experimental.UIElements.StyleEnums.Wrap),

                    typeof(UnityEngine.Experimental.UIElements.StyleSheets.StyleValue<int>), // Example of concrete generic class, expand as needed
                    
                    typeof(UnityEngine.Experimental.UIElements.StyleSheets.ICustomStyle),
                    
                    typeof(UnityEngine.Experimental.UIElements.AttachToPanelEvent),
                    typeof(UnityEngine.Experimental.UIElements.BaseField<int>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Experimental.UIElements.BaseSlider<int>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Experimental.UIElements.BindableElement),
                    typeof(UnityEngine.Experimental.UIElements.BlurEvent),
                    typeof(UnityEngine.Experimental.UIElements.Box),
                    typeof(UnityEngine.Experimental.UIElements.Button),
                    typeof(UnityEngine.Experimental.UIElements.CallbackEventHandler),
                    typeof(UnityEngine.Experimental.UIElements.ChangeEvent<int>),
                    // typeof(UnityEngine.Experimental.UIElements.CommandEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.ContextClickEvent),
                    typeof(UnityEngine.Experimental.UIElements.ContextualMenuManager),
                    typeof(UnityEngine.Experimental.UIElements.ContextualMenuManipulator),
                    typeof(UnityEngine.Experimental.UIElements.ContextualMenuPopulateEvent),
                    typeof(UnityEngine.Experimental.UIElements.CreationContext),
                    typeof(UnityEngine.Experimental.UIElements.CursorStyle),
                    typeof(UnityEngine.Experimental.UIElements.DetachFromPanelEvent),
                    // typeof(UnityEngine.Experimental.UIElements.DragAndDropEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.DragEnterEvent),
                    typeof(UnityEngine.Experimental.UIElements.DragExitedEvent),
                    typeof(UnityEngine.Experimental.UIElements.DragLeaveEvent),
                    typeof(UnityEngine.Experimental.UIElements.DragPerformEvent),
                    typeof(UnityEngine.Experimental.UIElements.DragUpdatedEvent),
                    typeof(UnityEngine.Experimental.UIElements.DropdownMenu),
                    typeof(UnityEngine.Experimental.UIElements.EventBase),
                    // typeof(UnityEngine.Experimental.UIElements.EventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.EventDispatcher),
                    typeof(UnityEngine.Experimental.UIElements.ExecuteCommandEvent),
                    typeof(UnityEngine.Experimental.UIElements.Flex),
                    typeof(UnityEngine.Experimental.UIElements.Focusable),
                    typeof(UnityEngine.Experimental.UIElements.FocusChangeDirection),
                    typeof(UnityEngine.Experimental.UIElements.FocusController),
                    typeof(UnityEngine.Experimental.UIElements.FocusEvent),
                    // typeof(UnityEngine.Experimental.UIElements.FocusEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.FocusInEvent),
                    typeof(UnityEngine.Experimental.UIElements.FocusOutEvent),
                    typeof(UnityEngine.Experimental.UIElements.Foldout),
                    typeof(UnityEngine.Experimental.UIElements.GeometryChangedEvent),
                    typeof(UnityEngine.Experimental.UIElements.IBindingExtensions),
                    typeof(UnityEngine.Experimental.UIElements.Image),
                    typeof(UnityEngine.Experimental.UIElements.IMGUIContainer),
                    typeof(UnityEngine.Experimental.UIElements.IMGUIEvent),
                    typeof(UnityEngine.Experimental.UIElements.InputEvent),
                    // typeof(UnityEngine.Experimental.UIElements.KeyboardEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.KeyDownEvent),
                    typeof(UnityEngine.Experimental.UIElements.KeyUpEvent),
                    typeof(UnityEngine.Experimental.UIElements.Label),
                    typeof(UnityEngine.Experimental.UIElements.ListView),
                    typeof(UnityEngine.Experimental.UIElements.Manipulator),
                    typeof(UnityEngine.Experimental.UIElements.ManipulatorActivationFilter),
                    typeof(UnityEngine.Experimental.UIElements.MinMaxSlider),
                    typeof(UnityEngine.Experimental.UIElements.MouseCaptureController),
                    typeof(UnityEngine.Experimental.UIElements.MouseCaptureEvent),
                    // typeof(UnityEngine.Experimental.UIElements.MouseCaptureEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.MouseCaptureOutEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseDownEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseEnterEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseEnterWindowEvent),
                    // typeof(UnityEngine.Experimental.UIElements.MouseEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.MouseLeaveEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseLeaveWindowEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseMoveEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseOutEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseOverEvent),
                    typeof(UnityEngine.Experimental.UIElements.MouseUpEvent),
                    // typeof(UnityEngine.Experimental.UIElements.PanelChangedEventBase<>), // Base generic class for child to inherit not used from user script
                    typeof(UnityEngine.Experimental.UIElements.PopupWindow),
                    typeof(UnityEngine.Experimental.UIElements.RepeatButton),
                    typeof(UnityEngine.Experimental.UIElements.Scroller),
                    typeof(UnityEngine.Experimental.UIElements.ScrollerButton),
                    typeof(UnityEngine.Experimental.UIElements.ScrollView),
                    typeof(UnityEngine.Experimental.UIElements.Slider),
                    typeof(UnityEngine.Experimental.UIElements.SliderInt),
                    typeof(UnityEngine.Experimental.UIElements.TemplateContainer),
                    typeof(UnityEngine.Experimental.UIElements.TextElement),
                    typeof(UnityEngine.Experimental.UIElements.TextField),
                    typeof(UnityEngine.Experimental.UIElements.TextInputFieldBase<int>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Experimental.UIElements.Toggle),
                    typeof(UnityEngine.Experimental.UIElements.TooltipEvent),
                    typeof(UnityEngine.Experimental.UIElements.UQuery),
                    typeof(UnityEngine.Experimental.UIElements.UQueryExtensions),
                    typeof(UnityEngine.Experimental.UIElements.UxmlAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlBoolAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlChildElementDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlColorAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlDoubleAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlEnumAttributeDescription<int>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Experimental.UIElements.UxmlEnumeration),
                    typeof(UnityEngine.Experimental.UIElements.UxmlFactory<UnityEngine.Experimental.UIElements.VisualElement>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Experimental.UIElements.UxmlFloatAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlIntAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlLongAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlRootElementFactory),
                    typeof(UnityEngine.Experimental.UIElements.UxmlRootElementTraits),
                    typeof(UnityEngine.Experimental.UIElements.UxmlStringAttributeDescription),
                    typeof(UnityEngine.Experimental.UIElements.UxmlTraits),
                    typeof(UnityEngine.Experimental.UIElements.UxmlTypeRestriction),
                    typeof(UnityEngine.Experimental.UIElements.UxmlValueBounds),
                    typeof(UnityEngine.Experimental.UIElements.UxmlValueMatches),
                    typeof(UnityEngine.Experimental.UIElements.ValidateCommandEvent),
                    typeof(UnityEngine.Experimental.UIElements.VisualContainer),
                    typeof(UnityEngine.Experimental.UIElements.VisualElement),
                    typeof(UnityEngine.Experimental.UIElements.VisualElementExtensions),
                    typeof(UnityEngine.Experimental.UIElements.VisualElementFocusChangeDirection),
                    typeof(UnityEngine.Experimental.UIElements.VisualElementFocusRing),
                    typeof(UnityEngine.Experimental.UIElements.VisualTreeAsset),
                    typeof(UnityEngine.Experimental.UIElements.WheelEvent),

                    typeof(UnityEngine.Experimental.UIElements.IBindable),
                    typeof(UnityEngine.Experimental.UIElements.IBinding),
                    typeof(UnityEngine.Experimental.UIElements.IChangeEvent),
                    typeof(UnityEngine.Experimental.UIElements.ICommandEvent),
                    typeof(UnityEngine.Experimental.UIElements.IDragAndDropEvent),
                    typeof(UnityEngine.Experimental.UIElements.IEventHandler),
                    typeof(UnityEngine.Experimental.UIElements.IFocusEvent),
                    typeof(UnityEngine.Experimental.UIElements.IFocusRing),
                    typeof(UnityEngine.Experimental.UIElements.IKeyboardEvent),
                    typeof(UnityEngine.Experimental.UIElements.IManipulator),
                    typeof(UnityEngine.Experimental.UIElements.IMouseCaptureEvent),
                    typeof(UnityEngine.Experimental.UIElements.IMouseEvent),
                    typeof(UnityEngine.Experimental.UIElements.INotifyValueChanged<int>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Experimental.UIElements.IPanel),
                    typeof(UnityEngine.Experimental.UIElements.IPanelChangedEvent),
                    typeof(UnityEngine.Experimental.UIElements.IScheduledItem),
                    typeof(UnityEngine.Experimental.UIElements.IScheduler),
                    typeof(UnityEngine.Experimental.UIElements.IStyle),
                    typeof(UnityEngine.Experimental.UIElements.ITransform),
                    typeof(UnityEngine.Experimental.UIElements.IUIElementDataWatch),
                    typeof(UnityEngine.Experimental.UIElements.IUIElementDataWatchRequest),
                    typeof(UnityEngine.Experimental.UIElements.IUxmlAttributes),
                    typeof(UnityEngine.Experimental.UIElements.IUxmlFactory),
                    typeof(UnityEngine.Experimental.UIElements.IVisualElementScheduledItem),
                    typeof(UnityEngine.Experimental.UIElements.IVisualElementScheduler),
                                    
                    typeof(UnityEngine.Experimental.UIElements.ChangeType),
                    typeof(UnityEngine.Experimental.UIElements.PropagationPhase),
                    typeof(UnityEngine.Experimental.UIElements.SelectionType),
                    typeof(UnityEngine.Experimental.UIElements.SliderDirection),
                    typeof(UnityEngine.Experimental.UIElements.TrickleDown),

                    typeof(UnityEngine.Experimental.VFX.VFXExpressionValues),
                    typeof(UnityEngine.Experimental.VFX.VFXManager),
                    typeof(UnityEngine.Experimental.VFX.VFXSpawnerCallbacks),
                    typeof(UnityEngine.Experimental.VFX.VFXSpawnerState),
                    typeof(UnityEngine.Experimental.VFX.VisualEffect),
                    typeof(UnityEngine.Experimental.VFX.VisualEffectAsset),

                    typeof(UnityEngine.Experimental.Video.VideoClipPlayable),
                    typeof(UnityEngine.Experimental.Video.VideoPlayerExtensions),

                    typeof(UnityEngine.Experimental.XR.Interaction.BaseArmModel),
                    typeof(UnityEngine.Experimental.XR.Interaction.BasePoseProvider),
                    
                    typeof(UnityEngine.Experimental.XR.BoundedPlane),
                    typeof(UnityEngine.Experimental.XR.FrameReceivedEventArgs),
                    typeof(UnityEngine.Experimental.XR.MeshGenerationResult),
                    typeof(UnityEngine.Experimental.XR.MeshInfo),
                    typeof(UnityEngine.Experimental.XR.PlaneAddedEventArgs),
                    typeof(UnityEngine.Experimental.XR.PlaneRemovedEventArgs),
                    typeof(UnityEngine.Experimental.XR.PlaneUpdatedEventArgs),
                    typeof(UnityEngine.Experimental.XR.PointCloudUpdatedEventArgs),
                    typeof(UnityEngine.Experimental.XR.ReferencePoint),
                    typeof(UnityEngine.Experimental.XR.ReferencePointUpdatedEventArgs),
                    typeof(UnityEngine.Experimental.XR.SessionTrackingStateChangedEventArgs),
                    typeof(UnityEngine.Experimental.XR.TrackableId),
                    typeof(UnityEngine.Experimental.XR.XRCameraSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRCameraSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRDepthSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRDepthSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRDisplaySubsystem),
                    typeof(UnityEngine.Experimental.XR.XRDisplaySubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRInputSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRInputSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRMeshSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRMeshSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRPlaneSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRPlaneSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRRaycastHit),
                    typeof(UnityEngine.Experimental.XR.XRRaycastSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRRaycastSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRReferencePointSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRReferencePointSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.XR.XRSessionSubsystem),
                    typeof(UnityEngine.Experimental.XR.XRSessionSubsystemDescriptor),

                    typeof(UnityEngine.Experimental.XR.MeshChangeState),
                    typeof(UnityEngine.Experimental.XR.MeshGenerationStatus),
                    typeof(UnityEngine.Experimental.XR.MeshVertexAttributes),
                    typeof(UnityEngine.Experimental.XR.PlaneAlignment),
                    typeof(UnityEngine.Experimental.XR.TrackableType),
                    typeof(UnityEngine.Experimental.XR.TrackingState),

                    typeof(UnityEngine.Experimental.IntegratedSubsystem),
                    typeof(UnityEngine.Experimental.IntegratedSubsystemDescriptor),
                    typeof(UnityEngine.Experimental.Subsystem),
                    typeof(UnityEngine.Experimental.SubsystemDescriptor),
                    typeof(UnityEngine.Experimental.SubsystemManager),

                    typeof(UnityEngine.Experimental.ISubsystem),
                    typeof(UnityEngine.Experimental.ISubsystemDescriptor),
                    
                    #if UNITY_IPHONE
                    typeof(UnityEngine.iOS.Device),
                    typeof(UnityEngine.iOS.LocalNotification),
                    // [181227] heejun - block iOS push support
                    // typeof(UnityEngine.iOS.NotificationServices),
                    typeof(UnityEngine.iOS.OnDemandResources),
                    typeof(UnityEngine.iOS.OnDemandResourcesRequest),
                    typeof(UnityEngine.iOS.RemoteNotification),

                    typeof(UnityEngine.iOS.ActivityIndicatorStyle),
                    typeof(UnityEngine.iOS.CalendarIdentifier),
                    typeof(UnityEngine.iOS.CalendarUnit),
                    typeof(UnityEngine.iOS.DeviceGeneration),
                    typeof(UnityEngine.iOS.NotificationType),
                    typeof(UnityEngine.iOS.SystemGestureDeferMode),          
                    #endif

                    typeof(UnityEngine.Jobs.IJobParallelForTransformExtensions),
                    typeof(UnityEngine.Jobs.TransformAccess),
                    typeof(UnityEngine.Jobs.TransformAccessArray),
                    
                    typeof(UnityEngine.Jobs.IJobParallelForTransform),

                    // typeof(UnityEngine.Networking.Match.MatchInfo), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.Match.MatchInfoSnapshot), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.Match.NetworkMatch), // Comment out obsolete class
                    
                    // typeof(UnityEngine.Networking.NetworkSystem.AddPlayerMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.EmptyMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.ErrorMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.IntegerMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.NotReadyMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.PeerAuthorityMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.PeerInfoMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.PeerInfoPlayer), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.PeerListMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.ReadyMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.ReconnectMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.RemovePlayerMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSystem.StringMessage),
                    
                    typeof(UnityEngine.Networking.PlayerConnection.MessageEventArgs),
                    typeof(UnityEngine.Networking.PlayerConnection.PlayerConnection),

                    typeof(UnityEngine.Networking.Types.NetworkAccessToken),

                    typeof(UnityEngine.Networking.Types.AppID),
                    typeof(UnityEngine.Networking.Types.HostPriority),
                    typeof(UnityEngine.Networking.Types.NetworkAccessLevel),
                    typeof(UnityEngine.Networking.Types.NetworkID),
                    typeof(UnityEngine.Networking.Types.NodeID),
                    typeof(UnityEngine.Networking.Types.SourceID),
                                        
                    typeof(UnityEngine.Networking.CertificateHandler),
                    // typeof(UnityEngine.Networking.ChannelQOS), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.Channels), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.ClientScene), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.ConnectionConfig), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.ConnectionSimulatorConfig), // Comment out obsolete class
                    typeof(UnityEngine.Networking.DownloadHandler),
                    typeof(UnityEngine.Networking.DownloadHandlerAssetBundle),
                    typeof(UnityEngine.Networking.DownloadHandlerAudioClip),
                    typeof(UnityEngine.Networking.DownloadHandlerBuffer),
                    typeof(UnityEngine.Networking.DownloadHandlerFile),
                    // typeof(UnityEngine.Networking.DownloadHandlerMovieTexture), // Comment out obsolete class
                    typeof(UnityEngine.Networking.DownloadHandlerScript),
                    typeof(UnityEngine.Networking.DownloadHandlerTexture),
                    // typeof(UnityEngine.Networking.GlobalConfig), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.HostTopology), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.LogFilter), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.MessageBase), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.MsgType), // Comment out obsolete class
                    typeof(UnityEngine.Networking.MultipartFormDataSection),
                    typeof(UnityEngine.Networking.MultipartFormFileSection),
                    // typeof(UnityEngine.Networking.NetworkAnimator), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkBehaviour), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkBroadcastResult), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkClient), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkConnection), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkCRC), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkDiscovery), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkHash128), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkIdentity), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkInstanceId), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkLobbyManager), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkLobbyPlayer), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkManager), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkManagerHUD), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkMessage), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkMigrationManager), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkProximityChecker), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkReader), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkSceneId), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkServer), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkServerSimple), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkStartPosition), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkTransform), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkTransformChild), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkTransformVisualizer), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkTransport), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.NetworkWriter), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.PlayerController), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncList<int>), // Example of concrete generic class, expand as needed  // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncListBool), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncListFloat) // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncListInt), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncListString), // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncListStruct<int>), // Example of concrete generic class, expand as needed  // Comment out obsolete class
                    // typeof(UnityEngine.Networking.SyncListUInt), // Comment out obsolete class
                    typeof(UnityEngine.Networking.UnityWebRequest),
                    typeof(UnityEngine.Networking.UnityWebRequestAssetBundle),
                    typeof(UnityEngine.Networking.UnityWebRequestAsyncOperation),
                    typeof(UnityEngine.Networking.UnityWebRequestMultimedia),
                    typeof(UnityEngine.Networking.UnityWebRequestTexture),
                    typeof(UnityEngine.Networking.UploadHandler),
                    typeof(UnityEngine.Networking.UploadHandlerFile),
                    typeof(UnityEngine.Networking.UploadHandlerRaw),
                    typeof(UnityEngine.Networking.Utility),

                    typeof(UnityEngine.Networking.IMultipartFormSection),
                    // typeof(UnityEngine.Networking.INetworkTransport), // Comment out obsolete class
                    
                    typeof(UnityEngine.Networking.ChannelOption),
                    typeof(UnityEngine.Networking.ConnectionAcksType),
                    typeof(UnityEngine.Networking.NetworkError),
                    typeof(UnityEngine.Networking.NetworkEventType),
                    typeof(UnityEngine.Networking.PlayerSpawnMethod),
                    typeof(UnityEngine.Networking.QosType),
                    typeof(UnityEngine.Networking.ReactorModel),
                    typeof(UnityEngine.Networking.Version),

                    typeof(UnityEngine.Playables.AnimationPlayableUtilities),
                    typeof(UnityEngine.Playables.FrameData),
                    typeof(UnityEngine.Playables.Notification),
                    typeof(UnityEngine.Playables.Playable),
                    typeof(UnityEngine.Playables.PlayableAsset),
                    typeof(UnityEngine.Playables.PlayableBehaviour),
                    typeof(UnityEngine.Playables.PlayableBinding),
                    typeof(UnityEngine.Playables.PlayableDirector),
                    typeof(UnityEngine.Playables.PlayableExtensions),
                    typeof(UnityEngine.Playables.PlayableGraph),
                    typeof(UnityEngine.Playables.PlayableOutput),
                    typeof(UnityEngine.Playables.PlayableOutputExtensions),
                    typeof(UnityEngine.Playables.ScriptPlayable<UnityEngine.Timeline.TimelinePlayable>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.Playables.ScriptPlayableBinding),
                    typeof(UnityEngine.Playables.ScriptPlayableOutput),

                    typeof(UnityEngine.Playables.INotification),
                    typeof(UnityEngine.Playables.INotificationReceiver),
                    typeof(UnityEngine.Playables.IPlayable),
                    typeof(UnityEngine.Playables.IPlayableAsset),
                    typeof(UnityEngine.Playables.IPlayableBehaviour),
                    typeof(UnityEngine.Playables.IPlayableOutput),
                    
                    // typeof(UnityEngine.Playables.DataStreamType), // Comment out obsolete class
                    typeof(UnityEngine.Playables.DirectorUpdateMode),
                    typeof(UnityEngine.Playables.DirectorWrapMode),
                    typeof(UnityEngine.Playables.PlayableTraversalMode),
                    typeof(UnityEngine.Playables.PlayState),

                    typeof(UnityEngine.Profiling.Memory.Experimental.MemoryProfiler),
                    typeof(UnityEngine.Profiling.Memory.Experimental.MetaData),
                    
                    typeof(UnityEngine.Profiling.Memory.Experimental.CaptureFlags),
                    
                    typeof(UnityEngine.Profiling.CustomSampler),
                    typeof(UnityEngine.Profiling.Profiler),
                    typeof(UnityEngine.Profiling.Recorder),
                    typeof(UnityEngine.Profiling.Sampler),
                    
                    typeof(UnityEngine.Profiling.ProfilerArea),

                    typeof(UnityEngine.Rendering.AsyncGPUReadback),
                    typeof(UnityEngine.Rendering.AsyncGPUReadbackRequest),
                    typeof(UnityEngine.Rendering.CommandBuffer),
                    typeof(UnityEngine.Rendering.GPUFence),
                    typeof(UnityEngine.Rendering.GraphicsSettings),
                    typeof(UnityEngine.Rendering.PlatformKeywordSet),
                    typeof(UnityEngine.Rendering.ReflectionProbeBlendInfo),
                    typeof(UnityEngine.Rendering.RenderTargetBinding),
                    typeof(UnityEngine.Rendering.RenderTargetIdentifier),
                    typeof(UnityEngine.Rendering.ShaderKeyword),
                    typeof(UnityEngine.Rendering.ShaderKeywordSet),
                    typeof(UnityEngine.Rendering.SortingGroup),
                    typeof(UnityEngine.Rendering.SphericalHarmonicsL2),
                    typeof(UnityEngine.Rendering.SplashScreen),

                    typeof(UnityEngine.Rendering.AmbientMode),
                    typeof(UnityEngine.Rendering.BlendMode),
                    typeof(UnityEngine.Rendering.BlendOp),
                    typeof(UnityEngine.Rendering.BuiltinRenderTextureType),
                    typeof(UnityEngine.Rendering.BuiltinShaderDefine),
                    typeof(UnityEngine.Rendering.BuiltinShaderMode),
                    typeof(UnityEngine.Rendering.BuiltinShaderType),
                    typeof(UnityEngine.Rendering.CameraEvent),
                    typeof(UnityEngine.Rendering.CameraHDRMode),
                    typeof(UnityEngine.Rendering.ColorWriteMask),
                    typeof(UnityEngine.Rendering.CompareFunction),
                    typeof(UnityEngine.Rendering.ComputeQueueType),
                    typeof(UnityEngine.Rendering.CopyTextureSupport),
                    typeof(UnityEngine.Rendering.CullMode),
                    typeof(UnityEngine.Rendering.DefaultReflectionMode),
                    typeof(UnityEngine.Rendering.GraphicsDeviceType),
                    typeof(UnityEngine.Rendering.GraphicsTier),
                    typeof(UnityEngine.Rendering.IndexFormat),
                    typeof(UnityEngine.Rendering.LightEvent),
                    typeof(UnityEngine.Rendering.LightProbeUsage),
                    typeof(UnityEngine.Rendering.LightShadowResolution),
                    typeof(UnityEngine.Rendering.OpaqueSortMode),
                    typeof(UnityEngine.Rendering.PassType),
                    typeof(UnityEngine.Rendering.RealtimeGICPUUsage),
                    typeof(UnityEngine.Rendering.ReflectionCubemapCompression),
                    typeof(UnityEngine.Rendering.ReflectionProbeClearFlags),
                    typeof(UnityEngine.Rendering.ReflectionProbeMode),
                    typeof(UnityEngine.Rendering.ReflectionProbeRefreshMode),
                    typeof(UnityEngine.Rendering.ReflectionProbeTimeSlicingMode),
                    typeof(UnityEngine.Rendering.ReflectionProbeUsage),
                    typeof(UnityEngine.Rendering.RenderBufferLoadAction),
                    typeof(UnityEngine.Rendering.RenderBufferStoreAction),
                    typeof(UnityEngine.Rendering.RenderQueue),
                    typeof(UnityEngine.Rendering.ShaderKeywordType),
                    typeof(UnityEngine.Rendering.ShadowCastingMode),
                    typeof(UnityEngine.Rendering.ShadowMapPass),
                    typeof(UnityEngine.Rendering.ShadowSamplingMode),
                    typeof(UnityEngine.Rendering.StencilOp),
                    typeof(UnityEngine.Rendering.SynchronisationStage),
                    typeof(UnityEngine.Rendering.TextureDimension),
                    typeof(UnityEngine.Rendering.UVChannelFlags),
                    typeof(UnityEngine.Rendering.VertexAttribute),

                    typeof(UnityEngine.SceneManagement.CreateSceneParameters),
                    typeof(UnityEngine.SceneManagement.LoadSceneParameters),
                    typeof(UnityEngine.SceneManagement.Scene),
                    typeof(UnityEngine.SceneManagement.SceneManager),
                    typeof(UnityEngine.SceneManagement.SceneUtility),

                    typeof(UnityEngine.SceneManagement.LoadSceneMode),
                    typeof(UnityEngine.SceneManagement.LocalPhysicsMode),

                    typeof(UnityEngine.Scripting.GarbageCollector),

                    // [181227] heejun - block iOS game center support
                    // #if UNITY_IPHONE
                    // typeof(UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform), 
                    // #endif

                    typeof(UnityEngine.SocialPlatforms.Range),
                    
                    typeof(UnityEngine.SocialPlatforms.IAchievement),
                    typeof(UnityEngine.SocialPlatforms.IAchievementDescription),
                    typeof(UnityEngine.SocialPlatforms.ILeaderboard),
                    typeof(UnityEngine.SocialPlatforms.ILocalUser),
                    typeof(UnityEngine.SocialPlatforms.IScore),
                    typeof(UnityEngine.SocialPlatforms.ISocialPlatform),
                    typeof(UnityEngine.SocialPlatforms.IUserProfile),
                    
                    typeof(UnityEngine.SocialPlatforms.TimeScope),
                    typeof(UnityEngine.SocialPlatforms.UserScope),
                    typeof(UnityEngine.SocialPlatforms.UserState),

                    typeof(UnityEngine.SpatialTracking.PoseDataSource),
                    typeof(UnityEngine.SpatialTracking.TrackedPoseDriver),

                    typeof(UnityEngine.Sprites.DataUtility),

                    typeof(UnityEngine.TextCore.LowLevel.FontEngine),
                    
                    typeof(UnityEngine.TextCore.LowLevel.FontEngineError),
                    typeof(UnityEngine.TextCore.LowLevel.GlyphLoadFlags),
                    typeof(UnityEngine.TextCore.LowLevel.GlyphPackingMode),
                    typeof(UnityEngine.TextCore.LowLevel.GlyphRenderMode),
                    
                    typeof(UnityEngine.TextCore.FaceInfo),
                    typeof(UnityEngine.TextCore.Glyph),
                    typeof(UnityEngine.TextCore.GlyphMetrics),
                    typeof(UnityEngine.TextCore.GlyphRect),

                    typeof(UnityEngine.Tilemaps.ITilemap),
                    typeof(UnityEngine.Tilemaps.Tile),
                    typeof(UnityEngine.Tilemaps.TileAnimationData),
                    typeof(UnityEngine.Tilemaps.TileBase),
                    typeof(UnityEngine.Tilemaps.TileData),
                    typeof(UnityEngine.Tilemaps.Tilemap),
                    typeof(UnityEngine.Tilemaps.TilemapCollider2D),
                    typeof(UnityEngine.Tilemaps.TilemapRenderer),
                    
                    typeof(UnityEngine.Tilemaps.TileFlags),

                    typeof(UnityEngine.Timeline.ActivationControlPlayable),
                    typeof(UnityEngine.Timeline.ActivationTrack),
                    typeof(UnityEngine.Timeline.AnimationPlayableAsset),
                    typeof(UnityEngine.Timeline.AnimationTrack),
                    typeof(UnityEngine.Timeline.AudioPlayableAsset),
                    typeof(UnityEngine.Timeline.AudioTrack),
                    typeof(UnityEngine.Timeline.ControlPlayableAsset),
                    typeof(UnityEngine.Timeline.ControlTrack),
                    typeof(UnityEngine.Timeline.DirectorControlPlayable),
                    typeof(UnityEngine.Timeline.GroupTrack),
                    typeof(UnityEngine.Timeline.ParticleControlPlayable),
                    typeof(UnityEngine.Timeline.PlayableTrack),
                    typeof(UnityEngine.Timeline.PrefabControlPlayable),
                    typeof(UnityEngine.Timeline.TimelineAsset),
                    typeof(UnityEngine.Timeline.TimelineClip),
                    typeof(UnityEngine.Timeline.TimelinePlayable),
                    typeof(UnityEngine.Timeline.TrackAsset),
                    typeof(UnityEngine.Timeline.TrackAssetExtensions),

                    typeof(UnityEngine.Timeline.IPropertyCollector),
                    typeof(UnityEngine.Timeline.IPropertyPreview),
                    typeof(UnityEngine.Timeline.ITimeControl),
                    typeof(UnityEngine.Timeline.ITimelineClipAsset),

                    typeof(UnityEngine.Timeline.ClipCaps),
                    typeof(UnityEngine.Timeline.TrackBindingFlags),
                    typeof(UnityEngine.Timeline.TrackOffset),

                    typeof(UnityEngine.U2D.SpriteAtlas),
                    typeof(UnityEngine.U2D.SpriteAtlasManager),

                    typeof(UnityEngine.UI.AnimationTriggers),
                    typeof(UnityEngine.UI.AspectRatioFitter),
                    typeof(UnityEngine.UI.BaseMeshEffect),
                    typeof(UnityEngine.UI.Button),
                    typeof(UnityEngine.UI.CanvasScaler),
                    typeof(UnityEngine.UI.CanvasUpdateRegistry),
                    typeof(UnityEngine.UI.ClipperRegistry),
                    typeof(UnityEngine.UI.Clipping),
                    typeof(UnityEngine.UI.ColorBlock),
                    typeof(UnityEngine.UI.ContentSizeFitter),
                    typeof(UnityEngine.UI.DefaultControls),
                    typeof(UnityEngine.UI.Dropdown),
                    typeof(UnityEngine.UI.FontData),
                    typeof(UnityEngine.UI.FontUpdateTracker),
                    typeof(UnityEngine.UI.Graphic),
                    typeof(UnityEngine.UI.GraphicRaycaster),
                    typeof(UnityEngine.UI.GraphicRegistry),
                    typeof(UnityEngine.UI.GridLayoutGroup),
                    typeof(UnityEngine.UI.HorizontalLayoutGroup),
                    typeof(UnityEngine.UI.HorizontalOrVerticalLayoutGroup),
                    typeof(UnityEngine.UI.Image),
                    typeof(UnityEngine.UI.InputField),
                    typeof(UnityEngine.UI.LayoutElement),
                    typeof(UnityEngine.UI.LayoutGroup),
                    typeof(UnityEngine.UI.LayoutRebuilder),
                    typeof(UnityEngine.UI.LayoutUtility),
                    typeof(UnityEngine.UI.Mask),
                    typeof(UnityEngine.UI.MaskableGraphic),
                    typeof(UnityEngine.UI.MaskUtilities),
                    typeof(UnityEngine.UI.Navigation),
                    typeof(UnityEngine.UI.Outline),
                    typeof(UnityEngine.UI.PositionAsUV1),
                    typeof(UnityEngine.UI.RawImage),
                    typeof(UnityEngine.UI.RectMask2D),
                    typeof(UnityEngine.UI.Scrollbar),
                    typeof(UnityEngine.UI.ScrollRect),
                    typeof(UnityEngine.UI.Selectable),
                    typeof(UnityEngine.UI.Shadow),
                    typeof(UnityEngine.UI.Slider),
                    typeof(UnityEngine.UI.SpriteState),
                    typeof(UnityEngine.UI.Text),
                    typeof(UnityEngine.UI.Toggle),
                    typeof(UnityEngine.UI.ToggleGroup),
                    typeof(UnityEngine.UI.VertexHelper),
                    typeof(UnityEngine.UI.VerticalLayoutGroup),
                    
                    typeof(UnityEngine.UI.ICanvasElement),
                    typeof(UnityEngine.UI.IClippable),
                    typeof(UnityEngine.UI.IClipper),
                    typeof(UnityEngine.UI.ILayoutController),
                    typeof(UnityEngine.UI.ILayoutElement),
                    typeof(UnityEngine.UI.ILayoutGroup),
                    typeof(UnityEngine.UI.ILayoutIgnorer),
                    typeof(UnityEngine.UI.ILayoutSelfController),
                    typeof(UnityEngine.UI.IMaskable),
                    typeof(UnityEngine.UI.IMaterialModifier),
                    typeof(UnityEngine.UI.IMeshModifier),

                    typeof(UnityEngine.UI.CanvasUpdate),

                    typeof(UnityEngine.Video.VideoClip),
                    typeof(UnityEngine.Video.VideoPlayer),
                    
                    typeof(UnityEngine.Video.Video3DLayout),
                    typeof(UnityEngine.Video.VideoAspectRatio),
                    typeof(UnityEngine.Video.VideoAudioOutputMode),
                    typeof(UnityEngine.Video.VideoRenderMode),
                    typeof(UnityEngine.Video.VideoSource),
                    typeof(UnityEngine.Video.VideoTimeReference),
                    typeof(UnityEngine.Video.VideoTimeSource),

                    typeof(UnityEngine.XR.ARBackgroundRenderer),
                    typeof(UnityEngine.XR.HapticCapabilities),
                    typeof(UnityEngine.XR.InputDevice),
                    typeof(UnityEngine.XR.InputDevices),
                    typeof(UnityEngine.XR.InputTracking),
                    typeof(UnityEngine.XR.XRDevice),
                    typeof(UnityEngine.XR.XRNodeState),
                    typeof(UnityEngine.XR.XRSettings),
                    typeof(UnityEngine.XR.XRStats),
                    
                    typeof(UnityEngine.XR.ARRenderMode),
                    typeof(UnityEngine.XR.GameViewRenderMode),
                    typeof(UnityEngine.XR.TrackingOriginMode),
                    typeof(UnityEngine.XR.TrackingSpaceType),
                    typeof(UnityEngine.XR.UserPresenceState),
                    typeof(UnityEngine.XR.XRNode),
                    
                    typeof(UnityEngine.AccelerationEvent),
                    typeof(UnityEngine.AnchoredJoint2D),
                                        
#if UNITY_ANDROID
                    typeof(UnityEngine.AndroidInput),
                    typeof(UnityEngine.AndroidJavaClass),
                    typeof(UnityEngine.AndroidJavaObject),
                    typeof(UnityEngine.AndroidJavaProxy),
                    typeof(UnityEngine.AndroidJNI),
                    typeof(UnityEngine.AndroidJNIHelper),
#endif
                    
                    typeof(UnityEngine.Animation),
                    typeof(UnityEngine.AnimationClip),
                    // typeof(UnityEngine.AnimationClipPair), // Comment out obsolete class
                    typeof(UnityEngine.AnimationCurve),
                    typeof(UnityEngine.AnimationEvent),
                    typeof(UnityEngine.AnimationState),
                    typeof(UnityEngine.Animator),
                    typeof(UnityEngine.AnimatorClipInfo),
                    typeof(UnityEngine.AnimatorControllerParameter),
                    typeof(UnityEngine.AnimatorOverrideController),
                    typeof(UnityEngine.AnimatorStateInfo),
                    typeof(UnityEngine.AnimatorTransitionInfo),
                    typeof(UnityEngine.AnimatorUtility),
                    typeof(UnityEngine.Application),
                    typeof(UnityEngine.AreaEffector2D),
                    typeof(UnityEngine.AssetBundle),
                    typeof(UnityEngine.AssetBundleCreateRequest),
                    typeof(UnityEngine.AssetBundleManifest),
                    typeof(UnityEngine.AssetBundleRecompressOperation),
                    typeof(UnityEngine.AssetBundleRequest),
                    typeof(UnityEngine.AsyncOperation),
                    typeof(UnityEngine.AudioChorusFilter),
                    typeof(UnityEngine.AudioClip),
                    typeof(UnityEngine.AudioConfiguration),
                    typeof(UnityEngine.AudioDistortionFilter),
                    typeof(UnityEngine.AudioEchoFilter),
                    typeof(UnityEngine.AudioHighPassFilter),
                    typeof(UnityEngine.AudioListener),
                    typeof(UnityEngine.AudioLowPassFilter),
                    typeof(UnityEngine.AudioRenderer),
                    typeof(UnityEngine.AudioReverbFilter),
                    typeof(UnityEngine.AudioReverbZone),
                    typeof(UnityEngine.AudioSettings),
                    typeof(UnityEngine.AudioSource),
                    typeof(UnityEngine.Avatar),
                    typeof(UnityEngine.AvatarBuilder),
                    typeof(UnityEngine.AvatarMask),
                    typeof(UnityEngine.Behaviour),
                    typeof(UnityEngine.BillboardAsset),
                    typeof(UnityEngine.BillboardRenderer),
                    typeof(UnityEngine.BoneWeight),
                    typeof(UnityEngine.BoundingSphere),
                    typeof(UnityEngine.Bounds),
                    typeof(UnityEngine.BoundsInt),
                    typeof(UnityEngine.BoxcastCommand),
                    typeof(UnityEngine.BoxCollider),
                    typeof(UnityEngine.BoxCollider2D),
                    typeof(UnityEngine.BuildCompression),
                    typeof(UnityEngine.BuoyancyEffector2D),
                    typeof(UnityEngine.Cache),
                    typeof(UnityEngine.CachedAssetBundle),
                    typeof(UnityEngine.Caching),
                    typeof(UnityEngine.Camera),
                    typeof(UnityEngine.Canvas),
                    typeof(UnityEngine.CanvasGroup),
                    typeof(UnityEngine.CanvasRenderer),
                    typeof(UnityEngine.CapsulecastCommand),
                    typeof(UnityEngine.CapsuleCollider),
                    typeof(UnityEngine.CapsuleCollider2D),
                    typeof(UnityEngine.CharacterController),
                    typeof(UnityEngine.CharacterInfo),
                    typeof(UnityEngine.CharacterJoint),
                    typeof(UnityEngine.CircleCollider2D),
                    typeof(UnityEngine.Cloth),
                    typeof(UnityEngine.ClothSkinningCoefficient),
                    typeof(UnityEngine.ClothSphereColliderPair),
                    typeof(UnityEngine.Collider),
                    typeof(UnityEngine.Collider2D),
                    typeof(UnityEngine.ColliderDistance2D),
                    typeof(UnityEngine.Collision),
                    typeof(UnityEngine.Collision2D),
                    typeof(UnityEngine.Color),
                    typeof(UnityEngine.Color32),
                    typeof(UnityEngine.ColorUtility),
                    typeof(UnityEngine.CombineInstance),
                    typeof(UnityEngine.Compass),
                    typeof(UnityEngine.Component),
                    typeof(UnityEngine.CompositeCollider2D),
                    typeof(UnityEngine.ComputeBuffer),
                    typeof(UnityEngine.ComputeShader),
                    typeof(UnityEngine.ConfigurableJoint),
                    typeof(UnityEngine.ConstantForce),
                    typeof(UnityEngine.ConstantForce2D),
                    typeof(UnityEngine.ContactFilter2D),
                    typeof(UnityEngine.ContactPoint),
                    typeof(UnityEngine.ContactPoint2D),
                    typeof(UnityEngine.ControllerColliderHit),
                    typeof(UnityEngine.Coroutine),
                    typeof(UnityEngine.CrashReport),
                    typeof(UnityEngine.Cubemap),
                    typeof(UnityEngine.CubemapArray),
                    typeof(UnityEngine.CullingGroup),
                    typeof(UnityEngine.CullingGroupEvent),
                    typeof(UnityEngine.Cursor),
                    typeof(UnityEngine.CustomRenderTexture),
                    typeof(UnityEngine.CustomRenderTextureUpdateZone),
                    typeof(UnityEngine.CustomYieldInstruction),
                    typeof(UnityEngine.Debug),
                    typeof(UnityEngine.DetailPrototype),
                    typeof(UnityEngine.Display),
                    typeof(UnityEngine.DistanceJoint2D),
                    typeof(UnityEngine.DrivenRectTransformTracker),
                    typeof(UnityEngine.DynamicGI),
                    typeof(UnityEngine.EdgeCollider2D),
                    typeof(UnityEngine.Effector2D),
                    typeof(UnityEngine.Event),
                    typeof(UnityEngine.ExitGUIException),
                    typeof(UnityEngine.ExposedPropertyResolver),
                    typeof(UnityEngine.ExposedReference<UnityEngine.Object>), // Example of concrete generic class, expand as needed
                    typeof(UnityEngine.FixedJoint),
                    typeof(UnityEngine.FixedJoint2D),
                    typeof(UnityEngine.Flare),
                    typeof(UnityEngine.FlareLayer),
                    typeof(UnityEngine.Font),
                    typeof(UnityEngine.FrameTiming),
                    typeof(UnityEngine.FrameTimingManager),
                    typeof(UnityEngine.FrictionJoint2D),
                    typeof(UnityEngine.FrustumPlanes),
                    typeof(UnityEngine.GameObject),
                    typeof(UnityEngine.GeometryUtility),
                    typeof(UnityEngine.Gizmos),
                    typeof(UnityEngine.GL),
                    typeof(UnityEngine.Gradient),
                    typeof(UnityEngine.GradientAlphaKey),
                    typeof(UnityEngine.GradientColorKey),
                    typeof(UnityEngine.Graphics),
                    typeof(UnityEngine.Grid),
                    typeof(UnityEngine.GridBrushBase),
                    typeof(UnityEngine.GridLayout),
                    typeof(UnityEngine.GUI),
                    typeof(UnityEngine.GUIContent),
                    typeof(UnityEngine.GUIElement),
                    // typeof(UnityEngine.GUILayer), // Comment out obsolete class
                    typeof(UnityEngine.GUILayout),
                    typeof(UnityEngine.GUILayoutOption),
                    typeof(UnityEngine.GUILayoutUtility),
                    typeof(UnityEngine.GUISettings),
                    typeof(UnityEngine.GUISkin),
                    typeof(UnityEngine.GUIStyle),
                    typeof(UnityEngine.GUIStyleState),
                    // typeof(UnityEngine.GUIText), // Comment out obsolete class
                    // typeof(UnityEngine.GUITexture), // Comment out obsolete class
                    typeof(UnityEngine.GUIUtility),
                    typeof(UnityEngine.Gyroscope),
#if UNITY_ANDROID || UNITY_IPHONE
                    typeof(UnityEngine.Handheld),
#endif
                    typeof(UnityEngine.Hash128),
                    typeof(UnityEngine.HashUnsafeUtilities),
                    typeof(UnityEngine.HashUtilities),
                    typeof(UnityEngine.HingeJoint),
                    typeof(UnityEngine.HingeJoint2D),
                    typeof(UnityEngine.HumanBone),
                    typeof(UnityEngine.HumanDescription),
                    typeof(UnityEngine.HumanLimit),
                    typeof(UnityEngine.HumanPose),
                    typeof(UnityEngine.HumanPoseHandler),
                    typeof(UnityEngine.HumanTrait),
                    typeof(UnityEngine.ImageConversion),
                    typeof(UnityEngine.Input),
                    typeof(UnityEngine.Joint),
                    typeof(UnityEngine.Joint2D),
                    typeof(UnityEngine.JointAngleLimits2D),
                    typeof(UnityEngine.JointDrive),
                    typeof(UnityEngine.JointLimits),
                    typeof(UnityEngine.JointMotor),
                    typeof(UnityEngine.JointMotor2D),
                    typeof(UnityEngine.JointSpring),
                    typeof(UnityEngine.JointSuspension2D),
                    typeof(UnityEngine.JointTranslationLimits2D),
                    typeof(UnityEngine.JsonUtility),
                    typeof(UnityEngine.Keyframe),
                    typeof(UnityEngine.LayerMask),
                    typeof(UnityEngine.LensFlare),
                    typeof(UnityEngine.Light),
                    typeof(UnityEngine.LightBakingOutput),
                    typeof(UnityEngine.LightmapData),
                    typeof(UnityEngine.LightmapSettings),
                    typeof(UnityEngine.LightProbeGroup),
                    typeof(UnityEngine.LightProbeProxyVolume),
                    typeof(UnityEngine.LightProbes),
                    typeof(UnityEngine.LineRenderer),
                    typeof(UnityEngine.LineUtility),
                    typeof(UnityEngine.LocalizationAsset),
                    typeof(UnityEngine.LocationInfo),
                    typeof(UnityEngine.LocationService),
                    typeof(UnityEngine.LOD),
                    typeof(UnityEngine.LODGroup),
                    typeof(UnityEngine.Logger),
                    typeof(UnityEngine.MatchTargetWeightMask),
                    typeof(UnityEngine.Material),
                    typeof(UnityEngine.MaterialPropertyBlock),
                    typeof(UnityEngine.Matrix4x4),
                    typeof(UnityEngine.Mesh),
                    typeof(UnityEngine.MeshCollider),
                    typeof(UnityEngine.MeshFilter),
                    typeof(UnityEngine.MeshRenderer),
                    typeof(UnityEngine.Microphone),
                    typeof(UnityEngine.MonoBehaviour),
                    typeof(UnityEngine.Motion),
                    // typeof(UnityEngine.MovieTexture), // Comment out obsolete class
                    typeof(UnityEngine.Object),
                    typeof(UnityEngine.OcclusionArea),
                    typeof(UnityEngine.OcclusionPortal),
                    typeof(UnityEngine.ParticleCollisionEvent),
                    typeof(UnityEngine.ParticlePhysicsExtensions),
                    typeof(UnityEngine.ParticleSystem),
                    typeof(UnityEngine.ParticleSystemForceField),
                    typeof(UnityEngine.ParticleSystemRenderer),
                    typeof(UnityEngine.PatchExtents),
                    typeof(UnityEngine.PhysicMaterial),
                    typeof(UnityEngine.Physics),
                    typeof(UnityEngine.Physics2D),
                    typeof(UnityEngine.PhysicsJobOptions2D),
                    typeof(UnityEngine.PhysicsMaterial2D),
                    typeof(UnityEngine.PhysicsScene),
                    typeof(UnityEngine.PhysicsScene2D),
                    typeof(UnityEngine.PhysicsSceneExtensions),
                    typeof(UnityEngine.PhysicsSceneExtensions2D),
                    typeof(UnityEngine.PhysicsUpdateBehaviour2D),
                    typeof(UnityEngine.Ping),
                    typeof(UnityEngine.Plane),
                    typeof(UnityEngine.PlatformEffector2D),
                    typeof(UnityEngine.PlayerPrefs),
                    typeof(UnityEngine.PlayerPrefsException),
                    typeof(UnityEngine.PointEffector2D),
                    typeof(UnityEngine.PolygonCollider2D),
                    typeof(UnityEngine.Pose),
                    typeof(UnityEngine.Projector),
                    typeof(UnityEngine.PropertyName),
                    typeof(UnityEngine.QualitySettings),
                    typeof(UnityEngine.Quaternion),
                    typeof(UnityEngine.Random),
                    typeof(UnityEngine.RangeInt),
                    typeof(UnityEngine.Ray),
                    typeof(UnityEngine.Ray2D),
                    typeof(UnityEngine.RaycastCommand),
                    typeof(UnityEngine.RaycastHit),
                    typeof(UnityEngine.RaycastHit2D),
                    typeof(UnityEngine.Rect),
                    typeof(UnityEngine.RectInt),
                    typeof(UnityEngine.RectOffset),
                    typeof(UnityEngine.RectTransform),
                    typeof(UnityEngine.RectTransformUtility),
                    typeof(UnityEngine.ReflectionProbe),
                    typeof(UnityEngine.RelativeJoint2D),
                    typeof(UnityEngine.RemoteSettings),
                    typeof(UnityEngine.RenderBuffer),
                    typeof(UnityEngine.Renderer),
                    typeof(UnityEngine.RendererExtensions),
                    typeof(UnityEngine.RenderSettings),
                    typeof(UnityEngine.RenderTargetSetup),
                    typeof(UnityEngine.RenderTexture),
                    typeof(UnityEngine.RenderTextureDescriptor),
                    typeof(UnityEngine.Resolution),
                    typeof(UnityEngine.ResourceRequest),
                    typeof(UnityEngine.Resources),
                    typeof(UnityEngine.Rigidbody),
                    typeof(UnityEngine.Rigidbody2D),
                    typeof(UnityEngine.RuntimeAnimatorController),
                    typeof(UnityEngine.ScalableBufferManager),
                    typeof(UnityEngine.Screen),
                    typeof(UnityEngine.ScreenCapture),
                    typeof(UnityEngine.ScriptableObject),
                    typeof(UnityEngine.Security),
                    typeof(UnityEngine.Shader),
                    typeof(UnityEngine.ShaderVariantCollection),
                    typeof(UnityEngine.ShaderVariantCollection.ShaderVariant),
                    typeof(UnityEngine.SkeletonBone),
                    typeof(UnityEngine.SkinnedMeshRenderer),
                    typeof(UnityEngine.Skybox),
                    typeof(UnityEngine.SleepTimeout),
                    typeof(UnityEngine.SliderJoint2D),
                    typeof(UnityEngine.Social),
                    typeof(UnityEngine.SoftJointLimit),
                    typeof(UnityEngine.SoftJointLimitSpring),
                    typeof(UnityEngine.SortingLayer),
                    typeof(UnityEngine.SparseTexture),
                    typeof(UnityEngine.SpherecastCommand),
                    typeof(UnityEngine.SphereCollider),
                    typeof(UnityEngine.SplatPrototype),
                    typeof(UnityEngine.SpringJoint),
                    typeof(UnityEngine.SpringJoint2D),
                    typeof(UnityEngine.Sprite),
                    typeof(UnityEngine.SpriteMask),
                    typeof(UnityEngine.SpriteRenderer),
                    typeof(UnityEngine.StateMachineBehaviour),
                    typeof(UnityEngine.StaticBatchingUtility),
                    typeof(UnityEngine.StreamingController),
                    typeof(UnityEngine.SurfaceEffector2D),
                    typeof(UnityEngine.SystemInfo),
                    typeof(UnityEngine.TargetJoint2D),
                    typeof(UnityEngine.Terrain),
                    typeof(UnityEngine.TerrainCollider),
                    typeof(UnityEngine.TerrainData),
                    typeof(UnityEngine.TerrainExtensions),
                    typeof(UnityEngine.TerrainLayer),
                    typeof(UnityEngine.TextAsset),
                    typeof(UnityEngine.TextGenerationSettings),
                    typeof(UnityEngine.TextGenerator),
                    typeof(UnityEngine.TextMesh),
                    typeof(UnityEngine.Texture),
                    typeof(UnityEngine.Texture2D),
                    typeof(UnityEngine.Texture2DArray),
                    typeof(UnityEngine.Texture3D),
                    typeof(UnityEngine.Time),
                    typeof(UnityEngine.Touch),
                    typeof(UnityEngine.TouchScreenKeyboard),
                    typeof(UnityEngine.TrailRenderer),
                    typeof(UnityEngine.Transform),
                    typeof(UnityEngine.Tree),
                    typeof(UnityEngine.TreeInstance),
                    typeof(UnityEngine.TreePrototype),
                    typeof(UnityEngine.UICharInfo),
                    typeof(UnityEngine.UILineInfo),
                    typeof(UnityEngine.UIVertex),                 
                    typeof(UnityEngine.Vector2),                  
                    typeof(UnityEngine.Vector2Int),                  
                    typeof(UnityEngine.Vector3),                  
                    typeof(UnityEngine.Vector3Int),
                    typeof(UnityEngine.Vector4),                  
                    typeof(UnityEngine.WaitForEndOfFrame),
                    typeof(UnityEngine.WaitForFixedUpdate),
                    typeof(UnityEngine.WaitForSeconds),
                    typeof(UnityEngine.WaitForSecondsRealtime),
                    typeof(UnityEngine.WaitUntil),
                    typeof(UnityEngine.WaitWhile),
                    typeof(UnityEngine.WebCamDevice),
                    typeof(UnityEngine.WebCamTexture),
                    typeof(UnityEngine.WheelCollider),
                    typeof(UnityEngine.WheelFrictionCurve),
                    typeof(UnityEngine.WheelHit),
                    typeof(UnityEngine.WheelJoint2D),
                    typeof(UnityEngine.WindZone),
                    // typeof(UnityEngine.WWW), // Comment out obsolete class
                    typeof(UnityEngine.WWWForm),
                    typeof(UnityEngine.YieldInstruction),

                    typeof(UnityEngine.IAnimationClipSource),
                    typeof(UnityEngine.ICanvasRaycastFilter),
                    typeof(UnityEngine.IExposedPropertyTable),
                    typeof(UnityEngine.ILogger),
                    typeof(UnityEngine.ILogHandler),
                    typeof(UnityEngine.ISerializationCallbackReceiver),

                    typeof(UnityEngine.AdditionalCanvasShaderChannels),
                    
#if UNITY_ANDROID
                    typeof(UnityEngine.AndroidActivityIndicatorStyle),
#endif
                    
                    typeof(UnityEngine.AnimationBlendMode),
                    typeof(UnityEngine.AnimationCullingType),
                    typeof(UnityEngine.AnimatorControllerParameterType),
                    typeof(UnityEngine.AnimatorCullingMode),
                    typeof(UnityEngine.AnimatorRecorderMode),
                    typeof(UnityEngine.AnimatorUpdateMode),
                    typeof(UnityEngine.AnisotropicFiltering),
                    typeof(UnityEngine.ApplicationInstallMode),
                    typeof(UnityEngine.ApplicationSandboxType),
                    typeof(UnityEngine.ArmDof),
                    typeof(UnityEngine.AssetBundleLoadResult),
                    typeof(UnityEngine.AudioClipLoadType),
                    typeof(UnityEngine.AudioCompressionFormat),
                    typeof(UnityEngine.AudioDataLoadState),
                    typeof(UnityEngine.AudioReverbPreset),
                    typeof(UnityEngine.AudioRolloffMode),
                    typeof(UnityEngine.AudioSourceCurveType),
                    typeof(UnityEngine.AudioSpeakerMode),
                    typeof(UnityEngine.AudioType),
                    typeof(UnityEngine.AudioVelocityUpdateMode),
                    typeof(UnityEngine.AvatarIKGoal),
                    typeof(UnityEngine.AvatarIKHint),
                    typeof(UnityEngine.AvatarMaskBodyPart),
                    typeof(UnityEngine.AvatarTarget),
                    typeof(UnityEngine.BatteryStatus),
                    typeof(UnityEngine.BlendWeights),
                    typeof(UnityEngine.BodyDof),
                    typeof(UnityEngine.CameraClearFlags),
                    typeof(UnityEngine.CameraType),
                    typeof(UnityEngine.CapsuleDirection2D),
                    typeof(UnityEngine.CollisionDetectionMode),
                    typeof(UnityEngine.CollisionDetectionMode2D),
                    typeof(UnityEngine.CollisionFlags),
                    typeof(UnityEngine.ColorGamut),
                    typeof(UnityEngine.ColorSpace),
                    typeof(UnityEngine.CompressionLevel),
                    typeof(UnityEngine.CompressionType),
                    typeof(UnityEngine.ComputeBufferType),
                    typeof(UnityEngine.ConfigurableJointMotion),
                    typeof(UnityEngine.CubemapFace),
                    typeof(UnityEngine.CursorLockMode),
                    typeof(UnityEngine.CursorMode),
                    typeof(UnityEngine.CustomRenderTextureInitializationSource),
                    typeof(UnityEngine.CustomRenderTextureUpdateMode),
                    typeof(UnityEngine.CustomRenderTextureUpdateZoneSpace),
                    typeof(UnityEngine.DepthTextureMode),
                    typeof(UnityEngine.DetailRenderMode),
                    typeof(UnityEngine.DeviceOrientation),
                    typeof(UnityEngine.DeviceType),
                    typeof(UnityEngine.DrivenTransformProperties),
                    typeof(UnityEngine.DurationUnit),
                    typeof(UnityEngine.EffectorForceMode2D),
                    typeof(UnityEngine.EffectorSelection2D),
                    typeof(UnityEngine.EventModifiers),
                    typeof(UnityEngine.EventType),
                    typeof(UnityEngine.FFTWindow),
                    typeof(UnityEngine.FilterMode),
                    typeof(UnityEngine.FingerDof),
                    typeof(UnityEngine.FocusType),
                    typeof(UnityEngine.FogMode),
                    typeof(UnityEngine.FontStyle),
                    typeof(UnityEngine.ForceMode),
                    typeof(UnityEngine.ForceMode2D),
                    typeof(UnityEngine.FullScreenMode),
#if UNITY_ANDROID || UNITY_IPHONE
                    typeof(UnityEngine.FullScreenMovieControlMode),
                    typeof(UnityEngine.FullScreenMovieScalingMode),
#endif
                    typeof(UnityEngine.GradientMode),
                    typeof(UnityEngine.HeadDof),
                    typeof(UnityEngine.HideFlags),
                    typeof(UnityEngine.HorizontalWrapMode),
                    typeof(UnityEngine.HumanBodyBones),
                    typeof(UnityEngine.HumanPartDof),
                    typeof(UnityEngine.ImagePosition),
                    typeof(UnityEngine.IMECompositionMode),
                    // typeof(UnityEngine.JointDriveMode), // Comment out obsolete class
                    typeof(UnityEngine.JointLimitState2D),
                    typeof(UnityEngine.JointProjectionMode),
                    typeof(UnityEngine.KeyCode),
                    typeof(UnityEngine.LegDof),
                    typeof(UnityEngine.LightmapBakeType),
                    typeof(UnityEngine.LightmapsMode),
                    typeof(UnityEngine.LightmapsModeLegacy),
                    typeof(UnityEngine.LightRenderMode),
                    typeof(UnityEngine.LightShadowCasterMode),
                    typeof(UnityEngine.LightShadows),
                    typeof(UnityEngine.LightType),
                    typeof(UnityEngine.LineAlignment),
                    typeof(UnityEngine.LineTextureMode),
                    typeof(UnityEngine.LocationServiceStatus),
                    typeof(UnityEngine.LODFadeMode),
                    typeof(UnityEngine.LogType),
                    typeof(UnityEngine.MaterialGlobalIlluminationFlags),
                    typeof(UnityEngine.MeshColliderCookingOptions),
                    typeof(UnityEngine.MeshTopology),
                    typeof(UnityEngine.MixedLightingMode),
                    typeof(UnityEngine.MotionVectorGenerationMode),
                    typeof(UnityEngine.NetworkReachability),
                    typeof(UnityEngine.NPOTSupport),
                    typeof(UnityEngine.OperatingSystemFamily),
                    typeof(UnityEngine.ParticleSystemAnimationMode),
                    typeof(UnityEngine.ParticleSystemAnimationTimeMode),
                    typeof(UnityEngine.ParticleSystemAnimationType),
                    typeof(UnityEngine.ParticleSystemCollisionMode),
                    typeof(UnityEngine.ParticleSystemCollisionQuality),
                    typeof(UnityEngine.ParticleSystemCollisionType),
                    typeof(UnityEngine.ParticleSystemCullingMode),
                    typeof(UnityEngine.ParticleSystemCurveMode),
                    typeof(UnityEngine.ParticleSystemCustomData),
                    typeof(UnityEngine.ParticleSystemCustomDataMode),
                    // typeof(UnityEngine.ParticleSystemEmissionType), // Comment out obsolete class
                    typeof(UnityEngine.ParticleSystemEmitterVelocityMode),
                    typeof(UnityEngine.ParticleSystemForceFieldShape),
                    typeof(UnityEngine.ParticleSystemGameObjectFilter),
                    typeof(UnityEngine.ParticleSystemGradientMode),
                    typeof(UnityEngine.ParticleSystemInheritVelocityMode),
                    typeof(UnityEngine.ParticleSystemMeshShapeType),
                    typeof(UnityEngine.ParticleSystemNoiseQuality),
                    typeof(UnityEngine.ParticleSystemOverlapAction),
                    typeof(UnityEngine.ParticleSystemRenderMode),
                    typeof(UnityEngine.ParticleSystemRenderSpace),
                    typeof(UnityEngine.ParticleSystemRingBufferMode),
                    typeof(UnityEngine.ParticleSystemScalingMode),
                    typeof(UnityEngine.ParticleSystemShapeMultiModeValue),
                    typeof(UnityEngine.ParticleSystemShapeTextureChannel),
                    typeof(UnityEngine.ParticleSystemShapeType),
                    typeof(UnityEngine.ParticleSystemSimulationSpace),
                    typeof(UnityEngine.ParticleSystemSortMode),
                    typeof(UnityEngine.ParticleSystemStopAction),
                    typeof(UnityEngine.ParticleSystemStopBehavior),
                    typeof(UnityEngine.ParticleSystemSubEmitterProperties),
                    typeof(UnityEngine.ParticleSystemSubEmitterType),
                    typeof(UnityEngine.ParticleSystemTrailMode),
                    typeof(UnityEngine.ParticleSystemTrailTextureMode),
                    typeof(UnityEngine.ParticleSystemTriggerEventType),
                    typeof(UnityEngine.ParticleSystemVertexStream),
                    typeof(UnityEngine.PhysicMaterialCombine),
                    typeof(UnityEngine.PlayMode),
                    typeof(UnityEngine.PrimitiveType),
                    typeof(UnityEngine.QueryTriggerInteraction),
                    typeof(UnityEngine.QueueMode),
                    typeof(UnityEngine.RenderingPath),
                    typeof(UnityEngine.RenderMode),
                    typeof(UnityEngine.RenderTextureCreationFlags),
                    typeof(UnityEngine.RenderTextureFormat),
                    typeof(UnityEngine.RenderTextureMemoryless),
                    typeof(UnityEngine.RenderTextureReadWrite),
                    typeof(UnityEngine.RigidbodyConstraints),
                    typeof(UnityEngine.RigidbodyConstraints2D),
                    typeof(UnityEngine.RigidbodyInterpolation),
                    typeof(UnityEngine.RigidbodyInterpolation2D),
                    typeof(UnityEngine.RigidbodySleepMode2D),
                    typeof(UnityEngine.RigidbodyType2D),
                    typeof(UnityEngine.RotationDriveMode),
                    typeof(UnityEngine.RuntimeInitializeLoadType),
                    typeof(UnityEngine.RuntimePlatform),
                    typeof(UnityEngine.ScaleMode),
                    typeof(UnityEngine.ScreenOrientation),
                    typeof(UnityEngine.SendMessageOptions),
                    typeof(UnityEngine.ShadowmaskMode),
                    typeof(UnityEngine.ShadowProjection),
                    typeof(UnityEngine.ShadowQuality),
                    typeof(UnityEngine.ShadowResolution),
                    typeof(UnityEngine.SkinQuality),
                    typeof(UnityEngine.Space),
                    typeof(UnityEngine.SpriteAlignment),
                    typeof(UnityEngine.SpriteDrawMode),
                    typeof(UnityEngine.SpriteMaskInteraction),
                    typeof(UnityEngine.SpriteMeshType),
                    typeof(UnityEngine.SpritePackingMode),
                    typeof(UnityEngine.SpritePackingRotation),
                    typeof(UnityEngine.SpriteSortPoint),
                    typeof(UnityEngine.SpriteTileMode),
                    typeof(UnityEngine.StackTraceLogType),
                    typeof(UnityEngine.StereoTargetEyeMask),
                    typeof(UnityEngine.SystemLanguage),
                    typeof(UnityEngine.TerrainChangedFlags),
                    typeof(UnityEngine.TerrainRenderFlags),
                    typeof(UnityEngine.TextAlignment),
                    typeof(UnityEngine.TextAnchor),
                    typeof(UnityEngine.TextClipping),
                    typeof(UnityEngine.TextureFormat),
                    typeof(UnityEngine.TextureWrapMode),
                    typeof(UnityEngine.ThreadPriority),
                    typeof(UnityEngine.TouchPhase),
                    typeof(UnityEngine.TouchScreenKeyboardType),
                    typeof(UnityEngine.TouchType),
                    typeof(UnityEngine.TransparencySortMode),
                    typeof(UnityEngine.UserAuthorization),
                    typeof(UnityEngine.VerticalWrapMode),
                    typeof(UnityEngine.VRTextureUsage),
                    typeof(UnityEngine.WebCamKind),
                    typeof(UnityEngine.WeightedMode),
                    typeof(UnityEngine.WindZoneMode),
                    typeof(UnityEngine.WrapMode),
                };
            }
        }

        [GCOptimize]
        static List<Type> GCOptimize
        {
            get
            {
                return new List<Type>()
                {
                    // Fundamental classes from Unity
                    typeof(UnityEngine.Vector2),
                    typeof(UnityEngine.Vector3),
                    typeof(UnityEngine.Vector4),
                    typeof(UnityEngine.Color),
                    typeof(UnityEngine.Quaternion),
                    typeof(UnityEngine.Ray),
                    typeof(UnityEngine.Bounds),
                    typeof(UnityEngine.Ray2D),

                    // Our defined Lua interface
                    typeof(OASIS.Data.FaceData),
                };
            }
        }

        [AdditionalProperties]
        static Dictionary<Type, List<string>> AdditionalProperties
        {
            get
            {
                return new Dictionary<Type, List<string>>()
                {
                    { typeof(UnityEngine.Ray), new List<string>() { "origin", "direction" } },
                    { typeof(UnityEngine.Ray2D), new List<string>() { "origin", "direction" } },
                    { typeof(UnityEngine.Bounds), new List<string>() { "center", "extents" } },
                };
            }
        }

        [CSharpCallLua]
        public static List<Type> CSharpCallLua
        {
            get
            {
                var lua_call_csharp = LuaCallCSharp;
                var delegate_types = new List<Type>();
                var flag = BindingFlags.Public | BindingFlags.Instance
                    | BindingFlags.Static | BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly;
                foreach (var field in (from type in lua_call_csharp select type).SelectMany(type => type.GetFields(flag)))
                {
                    if (typeof(Delegate).IsAssignableFrom(field.FieldType))
                    {
                        delegate_types.Add(field.FieldType);
                    }
                }

                foreach (var method in (from type in lua_call_csharp select type).SelectMany(type => type.GetMethods(flag)))
                {
                    if (typeof(Delegate).IsAssignableFrom(method.ReturnType))
                    {
                        delegate_types.Add(method.ReturnType);
                    }
                    foreach (var param in method.GetParameters())
                    {
                        var paramType = param.ParameterType.IsByRef ? param.ParameterType.GetElementType() : param.ParameterType;
                        if (typeof(Delegate).IsAssignableFrom(paramType))
                        {
                            delegate_types.Add(paramType);
                        }
                    }
                }

                delegate_types.Add(typeof(System.Collections.IEnumerator));

                return delegate_types.Distinct().ToList();
            }
        }

        [BlackList]
        public static List<List<string>> BlackList = new List<List<string>>()
        {   
            // Editor only method in runtime classes:
            
            new List<string>(){"System.IO.FileInfo", "GetAccessControl", "System.Security.AccessControl.AccessControlSections"},
            new List<string>(){"System.IO.FileInfo", "SetAccessControl", "System.Security.AccessControl.FileSecurity"},
            new List<string>(){"System.IO.DirectoryInfo", "GetAccessControl", "System.Security.AccessControl.AccessControlSections"},
            new List<string>(){"System.IO.DirectoryInfo", "SetAccessControl", "System.Security.AccessControl.DirectorySecurity"},
            new List<string>(){"System.IO.DirectoryInfo", "CreateSubdirectory", "System.String", "System.Security.AccessControl.DirectorySecurity"},
            new List<string>(){"System.IO.DirectoryInfo", "Create", "System.Security.AccessControl.DirectorySecurity"},

            new List<string>(){"UnityEngine.Application", "ExternalEval"},
            
            new List<string>(){"UnityEngine.GameObject", "networkView"},
            new List<string>(){"UnityEngine.Component", "networkView"},
            new List<string>(){"UnityEngine.MonoBehaviour", "runInEditMode"},
            
            new List<string>(){"UnityEngine.Light", "areaSize"},
            new List<string>(){"UnityEngine.Light", "shadowRadius"},
            new List<string>(){"UnityEngine.Light", "shadowAngle"},
            new List<string>(){"UnityEngine.Light", "lightmapBakeType"},
            new List<string>(){"UnityEngine.Light", "SetLightDirty"},
               
            new List<string>(){"UnityEngine.LightProbeGroup", "probePositions"},
            new List<string>(){"UnityEngine.LightProbeGroup", "dering"},
            
            new List<string>(){"UnityEngine.Experimental.Rendering.RenderPipelineAsset", "GetTerrainBrushPassIndex"},
            new List<string>(){"UnityEngine.Experimental.Rendering.IRenderPipelineAsset", "GetTerrainBrushPassIndex"},
            new List<string>(){"UnityEngine.Experimental.Rendering.ScriptableRenderContext", "EmitWorldGeometryForSceneView", "UnityEngine.Camera"},
                
            new List<string>(){"UnityEngine.ParticleSystemForceField", "FindAll"},
            
            new List<string>(){"UnityEngine.Playables.PlayableGraph", "GetEditorName"},
            new List<string>(){"UnityEngine.Timeline.AnimationPlayableAsset", "LiveLink"},
            new List<string>(){"UnityEngine.Timeline.AnimationTrack", "CreateRecordableClip", "System.String"},

            new List<string>(){"UnityEngine.Texture", "imageContentsHash"},
            new List<string>(){"UnityEngine.Texture2D", "alphaIsTransparency"},

            new List<string>(){"UnityEngine.Terrain", "bakeLightProbesForTrees"},
            new List<string>(){"UnityEngine.Terrain", "deringLightProbesForTrees"},
            
            new List<string>(){"UnityEngine.GUIStyleState", "scaledBackgrounds"},           
          
            new List<string>(){"UnityEngine.CanvasRenderer+OnRequestRebuild"},
            new List<string>(){"UnityEngine.CanvasRenderer", "onRequestRebuild"},
            
            new List<string>(){"UnityEngine.UI.Graphic", "OnRebuildRequested"},
            new List<string>(){"UnityEngine.UI.Text", "OnRebuildRequested"},
                
            new List<string>(){"UnityEngine.DrivenRectTransformTracker", "StopRecordingUndo"},
            new List<string>(){"UnityEngine.DrivenRectTransformTracker", "StartRecordingUndo"},
            
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "GetEditorPreviewTile", "UnityEngine.Vector3Int"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "SetEditorPreviewTile", "UnityEngine.Vector3Int", "UnityEngine.Tilemaps.TileBase"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "HasEditorPreviewTile", "UnityEngine.Vector3Int"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "GetEditorPreviewSprite", "UnityEngine.Vector3Int"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "GetEditorPreviewTransformMatrix", "UnityEngine.Vector3Int"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "SetEditorPreviewTransformMatrix", "UnityEngine.Vector3Int", "UnityEngine.Matrix4x4"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "GetEditorPreviewColor", "UnityEngine.Vector3Int"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "SetEditorPreviewColor", "UnityEngine.Vector3Int", "UnityEngine.Color"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "GetEditorPreviewTileFlags", "UnityEngine.Vector3Int"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "EditorPreviewFloodFill", "UnityEngine.Vector3Int", "UnityEngine.Tilemaps.TileBase"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "EditorPreviewBoxFill", "UnityEngine.Vector3Int", "UnityEngine.Object", "System.Int32", "System.Int32", "System.Int32", "System.Int32"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "ClearAllEditorPreviewTiles"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "editorPreviewOrigin"},
            new List<string>(){"UnityEngine.Tilemaps.Tilemap", "editorPreviewSize"},            
            
            new List<string>(){"UnityEngine.AudioSettings", "GetSpatializerPluginNames"},
            new List<string>(){"UnityEngine.AudioSettings", "SetSpatializerPluginName", "System.String"},
            
            new List<string>(){"UnityEngine.Input", "IsJoystickPreconfigured", "System.String"},

            // Property that read only in player and editable only in Editor:
            
            new List<string>(){"UnityEngine.QualitySettings", "set_streamingMipmapsRenderersPerFrame"},
            new List<string>(){"UnityEngine.AnimatorControllerParameter", "set_name"},            
            
            // Method that is valid only on Android/iOS
                        
#if UNITY_IPHONE
            new List<string>(){"UnityEngine.Handheld", "SetActivityIndicatorStyle", "UnityEngine.AndroidActivityIndicatorStyle"},
#elif UNITY_ANDROID
            new List<string>(){"UnityEngine.Handheld", "SetActivityIndicatorStyle", "UnityEngine.iOS.ActivityIndicatorStyle"},
#endif

#if UNITY_IPHONE
            new List<string>(){ "UnityEngine.TextureFormat", "DXT1Crunched"},
            new List<string>(){ "UnityEngine.TextureFormat", "DXT5Crunched"},
            new List<string>(){ "UnityEngine.TextureFormat", "ETC_RGB4Crunched"},
            new List<string>(){ "UnityEngine.TextureFormat", "ETC2_RGBA8Crunched"},
#endif

#if !UNITY_IPHONE
            new List<string>(){ "UnityEngine.Caching", "ResetNoBackupFlag", "UnityEngine.CachedAssetBundle"},
            new List<string>(){ "UnityEngine.Caching", "ResetNoBackupFlag", "System.String", "UnityEngine.Hash128"},
            new List<string>(){ "UnityEngine.Caching", "SetNoBackupFlag", "UnityEngine.CachedAssetBundle"},
            new List<string>(){ "UnityEngine.Caching", "SetNoBackupFlag", "System.String", "UnityEngine.Hash128"},
#endif
        };
    }
}