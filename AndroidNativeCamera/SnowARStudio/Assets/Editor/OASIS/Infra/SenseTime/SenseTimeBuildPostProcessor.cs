﻿using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEditor.Callbacks;
using System.Collections;

public class SenseTimeBuildPostProcessor
{
    [PostProcessBuild (0)]
    public static void OnPostprocessBuild (BuildTarget buildTarget, string pathToBuiltProject)
    {
        // Stop processing if target is NOT iOS
        if (buildTarget != BuildTarget.iOS)
            return;

        // Initialize PbxProject
        var projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
        PBXProject pbxProject = new PBXProject();
        pbxProject.ReadFromFile (projectPath);
        string targetGuid = pbxProject.TargetGuidByName ("Unity-iPhone");

        // Sensetime.a does not support bitcode
        pbxProject.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");

        // Sensetime.a links to these libraries
        pbxProject.AddFrameworkToProject(targetGuid, "Accelerate.framework", false);

        // Sensetime.a uses zlib
        pbxProject.UpdateBuildProperty(targetGuid, "OTHER_LDFLAGS", new string[]{"-lz"}, null);

        // Apply settings
        File.WriteAllText (projectPath, pbxProject.WriteToString ());        
    }
}