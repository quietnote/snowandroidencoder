﻿using OASIS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayDevicePosition : MonoBehaviour
{
    Text textUI;

    private void Start()
    {
        textUI = GetComponent<Text>();
    }

    void Update()
    {
        textUI.text = ACApp.instance.data.devicePosition.ToString();
    }
}
