﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using OASIS.VideoRecorder;

enum CameraStatus
{
    cameraStatusStop,
    cameraStatusPreview,
    cameraStatusPauseToSave,
    cameraStatusSave,
    cameraStatusStartRecording,
    cameraStatusRecording,
    cameraStatusStopRecording,
    cameraStatusFinishRecording
};

public class BtnCameraShot : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public VideoRecorder videoRecorder;
    public BtnCameraRatio cameraRatio;
    public GameObject buttonRatio;
    public GameObject recordMark;
	public Sprite imagePreview;
    public Sprite imageStopRecording;

    private bool isRecording;
    private float recordStartTime = 0;
    Text recordTime;

    private CameraStatus cameraStatus;
	Button myButton;

    // for checking long shot (recording)
    public float durationThreshold = 0.5f;
    private bool isPointerDown = false;
    private bool longPressTriggered = false;
    private float timePressStarted;

	// Use this for initialization
	void Start () {
        myButton = this.GetComponent<Button>();
        recordTime = recordMark.GetComponentInChildren<Text>();
        myButton.onClick.AddListener(TaskOnClick);
        cameraStatus = CameraStatus.cameraStatusPreview;
        longPressTriggered = false;
        recordMark.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if(isRecording)
        {
            float deltaTime = Time.time - recordStartTime;
            int minute = (int)deltaTime / 60;
            int second = (int)deltaTime - (minute * 60);
            int second1 = second;
            int second2 = (int)(deltaTime*100.0f - (int)deltaTime*100);
            recordTime.text = (minute.ToString("00") + ":" + second1.ToString("00") + "." + second2.ToString("00"));
        }
        if (isPointerDown && !longPressTriggered)
        {
            if (Time.time - timePressStarted > durationThreshold)
            {
                StartLongPress();
            }
        }
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        if (cameraStatus == CameraStatus.cameraStatusPreview)
        {
            timePressStarted = Time.time;
            isPointerDown = true;
            longPressTriggered = false;
        }
        else if(cameraStatus == CameraStatus.cameraStatusStartRecording)
        {
            // Do Nothing
        }
        else if (cameraStatus == CameraStatus.cameraStatusRecording)
        {
            // Do Nothing
            cameraStatus = CameraStatus.cameraStatusStopRecording;      // Move to stop recording
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (cameraStatus == CameraStatus.cameraStatusPreview)
        {
            longPressTriggered = false;
            isPointerDown = false;
        }
        else if (cameraStatus == CameraStatus.cameraStatusStartRecording)
        {
            longPressTriggered = false;
            isPointerDown = false;
            cameraStatus = CameraStatus.cameraStatusRecording;
        }
        else if (cameraStatus == CameraStatus.cameraStatusStopRecording)
        {
            longPressTriggered = false;
            isPointerDown = false;
            StopLongPress();
        }
    }

    void StartLongPress()
    {
        longPressTriggered = true;
        myButton.image.sprite = imageStopRecording;
        recordMark.SetActive(true);
        buttonRatio.SetActive(false);
        isRecording = true;
        cameraStatus = CameraStatus.cameraStatusStartRecording;
        videoRecorder.Record(cameraRatio.RatioIndex);  // ratioIndex = 0 (full) 1 (4:3) 2 (1:1)
        recordStartTime = Time.time;
    }

    void StopLongPress()
    {
        longPressTriggered = false;
        myButton.image.sprite = imagePreview;
        recordMark.SetActive(false);
        buttonRatio.SetActive(true);
        isRecording = false;
        cameraStatus = CameraStatus.cameraStatusFinishRecording;
        videoRecorder.StopRecord();
    }

    void TaskOnClick() {        // Task for Single Image Capture
        if(cameraStatus == CameraStatus.cameraStatusFinishRecording)        // After recording, reset preview status
        {
            cameraStatus = CameraStatus.cameraStatusPreview;
            return;
        }
        else if(cameraStatus == CameraStatus.cameraStatusPreview)
        {
            videoRecorder.Capture(cameraRatio.RatioIndex);
            return;
        }
        else if (cameraStatus == CameraStatus.cameraStatusPauseToSave)
        {
            return;
        }
    }
}
