﻿using UnityEngine;
using UnityEngine.UI;

public class BtnCameraRatio : MonoBehaviour {

    public Sprite imageFull;
    public Sprite image4to3;
    public Sprite image1to1;
    public Image guide4to3;
    public Image guide1to1;
    Button myButton;
    int ratioIndex = 0;

	// Use this for initialization
	void Start () {
        myButton = this.GetComponent<Button>();
        myButton.onClick.AddListener(ChangeRatio);
        guide4to3.enabled = false;
        guide1to1.enabled = false;
        ratioIndex = 0;
	}

    void ChangeRatio() {
        switch(++ratioIndex)
        {
            case 0: //full
                myButton.image.sprite = imageFull;
                guide4to3.enabled = false;
                guide1to1.enabled = false;
                break;
            case 1: //4:3
                myButton.image.sprite = image4to3;
                guide1to1.enabled = false;
                guide4to3.enabled = true;
                break;
            case 2: //1:1
                myButton.image.sprite = image1to1;
                guide4to3.enabled = false;
                guide1to1.enabled = true;
                break;
            default:
                myButton.image.sprite = imageFull;
                guide4to3.enabled = false;
                guide1to1.enabled = false;
                ratioIndex = 0;
                break;            
        }
    }

    public int RatioIndex { get { return ratioIndex; } } 
}
