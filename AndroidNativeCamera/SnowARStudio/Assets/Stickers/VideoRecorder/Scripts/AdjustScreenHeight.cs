﻿using UnityEngine;

public class AdjustScreenHeight : MonoBehaviour
{
    public float aspectRatio = 1f;
    RectTransform thisRect;

    void Start()
    {
        thisRect = GetComponent<RectTransform>();
        thisRect.sizeDelta = new Vector2(Screen.width, Screen.width * aspectRatio);
    }
}