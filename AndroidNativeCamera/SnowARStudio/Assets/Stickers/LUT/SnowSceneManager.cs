﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SnowSceneManager : MonoBehaviour
{
    public string[] layerScene;
    
#if !UNITY_EDITOR
    // Start is called before the first frame update
    void Start()
    {
        if(layerScene.Length > 0)
        {
            for(int i=0; i<layerScene.Length; i++)
            SceneManager.LoadScene(layerScene[i], LoadSceneMode.Additive);
        }        
    }
#endif
}
