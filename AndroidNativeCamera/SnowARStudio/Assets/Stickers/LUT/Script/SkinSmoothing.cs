﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OASIS.Contents;


public class SkinSmoothing : MonoBehaviour
{ 
    bool supportSnowLUT = true;
    //public Shader[] shaders;
    Material matHorizontal;
    Material matVertical;
    Material matSkinPostprocess;
    Material matDefault;
    public bool useLUT = false;
    bool useThis = true;
    Vector2Int uiBox = new Vector2Int(200, 100);

    // for LUT
    public Texture2D smoothingMaskImage;
    public RenderTexture smoothingMaskTexture;
    public Texture2D lutMaskImage;
    public RenderTexture lutMaskTexture;
    public Texture2D[] inputLUT = new Texture2D[3];
    string[] inputLUTname = new string[3];
    Texture3D[] converted3DLut = new Texture3D[3];
    int sizeLUT = 64;

    RenderTexture blur;
    RenderTexture blur2;
    RenderTexture resize;
 
    void Start ()
    {
        matHorizontal = Resources.Load<Material>("Materials/BilateralHorizontal");
        matVertical = Resources.Load<Material>("Materials/BilateralVertical");
        matSkinPostprocess = Resources.Load<Material>("Materials/Concat_smoothing_fss");
        matDefault = Resources.Load<Material>("Materials/Default");
        smoothingMaskImage = Resources.Load<Texture2D>("Textures/white");
        lutMaskImage = Resources.Load<Texture2D>("Textures/white");

        for(int i=0; i<inputLUT.Length; i++)
        {
            inputLUTname[i] = inputLUT[i].name;
        }  
        Prepare();
    }

    void Update()
    {
        if(CheckChangeLUT())
        {  
            Prepare();
        }
    }

    // render texture 생성
    void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        if(!useThis)
        {            
            matSkinPostprocess.SetTexture("_MainTex", source);

            if(blur2 && blur2.width != source.width)
            {
                blur2.Release();
                blur2 = RenderTexture.GetTemporary(source.width, source.height);
            }
            else if(blur2 == null)
            {
                blur2 = RenderTexture.GetTemporary(source.width, source.height);
            }
            matSkinPostprocess.SetTexture("_BlurTexture", source);

        }  
        else
        {
            if(resize == null)
                resize = RenderTexture.GetTemporary(source.width/2, source.height/2);            
            if(blur == null)
                blur = RenderTexture.GetTemporary(source.width/2, source.height/2);
            if(blur2 && blur2.width != source.width /2)
            {
                blur2.Release();
                blur2 = RenderTexture.GetTemporary(source.width/2, source.height/2);
            }
            else if(blur2 == null)
            {
                blur2 = RenderTexture.GetTemporary(source.width/2, source.height/2);
            }

            //Debug.Log("ULOG source size = " + source.width.ToString() + ", " + source.height.ToString());
            if(smoothingMaskTexture != null)        // render texture를 우선으로 한다.
                matHorizontal.SetTexture("_SmoothingMaskTex", smoothingMaskTexture); 
            else
                matHorizontal.SetTexture("_SmoothingMaskTex", smoothingMaskImage); 

            // Resize /2
            Graphics.Blit(source, resize, matDefault, 0);
            // Horizontal Process
            matHorizontal.SetTexture("_MainTex", resize);        
            Graphics.Blit(resize, blur, matHorizontal, 0);
            // Vertical Process
            matVertical.SetTexture("_MainTex", blur);
            Graphics.Blit(blur, blur2, matVertical, 0);        
            // Skin PostProcessing
            matSkinPostprocess.SetTexture("_MainTex", source);
            matSkinPostprocess.SetTexture("_BlurTexture", blur2);
        }
        if(useLUT)
        {
            if(matSkinPostprocess == null)      // nothing to do
                return;
            if(lutMaskTexture != null)              // render texture를 우선으로 한다.
                matSkinPostprocess.SetTexture("_LUTMaskTex", lutMaskTexture);
            else
                matSkinPostprocess.SetTexture("_LUTMaskTex", lutMaskImage);
            matSkinPostprocess.SetTexture("_LUT0", converted3DLut[0]);
            matSkinPostprocess.SetTexture("_LUT1", converted3DLut[1]);
            matSkinPostprocess.SetTexture("_LUT2", converted3DLut[2]);     
            matSkinPostprocess.SetInt("_useLUT", 1);
        }
        else
            matSkinPostprocess.SetInt("_useLUT", 0);
        Graphics.Blit(source, destination, matSkinPostprocess, 0);
    }

    void OnGUI()
    {
        if (GUI.Button (new Rect ((Screen.width-uiBox.x)/2, Screen.height*0.9f-uiBox.y, uiBox.x, uiBox.y), "SkinSmoothing : " + useThis.ToString()))
        {
            useThis = !useThis;
        }
        if (GUI.Button (new Rect ((Screen.width-uiBox.x)/2, Screen.height*0.8f-uiBox.y, uiBox.x, uiBox.y), "UseLUT : " + useLUT.ToString()))
        {
            useLUT = !useLUT;
        }       
    }

    void Prepare()
    {    
        LUTConverter3D lutConverter = new LUTConverter3D(); 
        for(int i=0; i<inputLUT.Length; i++)
        {            
            Texture3D temp = lutConverter.Convert(inputLUT[i]);
            sizeLUT = lutConverter.LutSize;
            converted3DLut[i] = new Texture3D (sizeLUT, sizeLUT, sizeLUT, TextureFormat.ARGB32, false);
            converted3DLut[i].wrapMode = TextureWrapMode.Clamp;
            if(converted3DLut[i] != null)
                Graphics.CopyTexture(temp, converted3DLut[i]);
        }
    }

    bool CheckChangeLUT()
    {      
        for(int i=0; i<inputLUT.Length; i++)
        {
            if(inputLUTname[i] != inputLUT[i].name)
            {
                inputLUTname[i] = inputLUT[i].name;
                return true;
            }
        } 
        return false; 
    }
}
