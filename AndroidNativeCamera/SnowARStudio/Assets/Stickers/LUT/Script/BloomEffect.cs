﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloomEffect : MonoBehaviour
{
    [SerializeField]
    float weight = 0;
    Material matBloomEffect;

    RenderTexture blur1;
    RenderTexture blur2;
    RenderTexture blur3;
    RenderTexture blur4;
    RenderTexture[] blur = new RenderTexture[5];

    RenderTexture resize;
    //RenderTexture blur;

    // Start is called before the first frame update
    void Start()
    {
        matBloomEffect = Resources.Load<Material>("Raws/Materials/BloomEffect");      
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnRenderImage (RenderTexture source, RenderTexture destination)
    {
        int blurStrength = 2;
        for(int i=0; i<blur.Length; i++)
        {
            if(blur[i] == null)
            {
                blur[i] = RenderTexture.GetTemporary(source.width/blurStrength, source.height/blurStrength);
                blurStrength *= 2;
            }
        }
        Graphics.Blit(source, blur[0], Vector2.one, Vector2.zero);
        Graphics.Blit(blur[0], blur[1], Vector2.one, Vector2.zero);
        Graphics.Blit(blur[1], blur[2], Vector2.one, Vector2.zero);
        Graphics.Blit(blur[2], blur[3], Vector2.one, Vector2.zero);
        Graphics.Blit(blur[3], blur[4], Vector2.one, Vector2.zero);
        matBloomEffect.SetTexture("_Blur0", blur[0]);
        matBloomEffect.SetTexture("_Blur1", blur[1]);
        matBloomEffect.SetTexture("_Blur2", blur[2]);
        matBloomEffect.SetTexture("_Blur3", blur[3]);
        matBloomEffect.SetTexture("_Blur4", blur[4]);
        matBloomEffect.SetFloat("_weight", weight);
        Graphics.Blit(source, destination, matBloomEffect, 0);
    }
}
