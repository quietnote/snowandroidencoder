﻿using System;
using UnityEngine;

namespace OASIS.Data
{
    
    [Serializable]
    public class HumanData
    {
        [SerializeField]
        protected int ID;
        [SerializeField]
        protected bool m_isValid;
        
        public int getDataID(){
            return ID;
        }

        public void setDataID(int id)
        {
            ID = id;
        }

        public bool isValidData()
        {
            return m_isValid;
        }

        public void setValidData(bool isValid)
        {
            m_isValid = isValid;
        }
    }
}
