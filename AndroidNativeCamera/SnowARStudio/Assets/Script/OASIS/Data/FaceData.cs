﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS.Data
{
    using ST;
    [Serializable]
    public class FaceData : HumanData
    {
        public SenseTimeLib.FaceData sourceData = SenseTimeLib.FaceData.Create();

        public float yaw
        {
            get { return sourceData.yaw; }
            set { sourceData.yaw = value; }
        }

        public float pitch
        {
            get { return sourceData.pitch; }
            set { sourceData.pitch = value; }
        }

        public float roll
        {
            get { return sourceData.roll; }
            set { sourceData.roll = value; }
        }

        public Vector2[] keyPoints
        {
            get { return sourceData.keyPointsArray; }
            set { sourceData.keyPointsArray = value; }
        }

        public Vector2[] verticesArray = new Vector2[ACConst.FACE_SENSETIME_SIZE];
        public float ulseeYaw;
        public float ulseePitch;
        public float ulseeRoll;
        public float ulseeCX;
        public float ulseeCY;
        public float ulseeFaceScale;

        public float relativeYaw;
        public float relativePitch;
        public float relativeRoll;

        public float compensatedYaw;
        public float compensatedPitch;
        public float legacyRoll;
        public float[] ulseePose = new float[ACConst.FACE_ULSEE_POSE_SIZE];
        public float[] ulseePoseEx = new float[ACConst.FACE_ULSEE_POSE_SIZE];

        public Vector2[] ulseeShape = new Vector2[ACConst.FACE_ULSEE_SIZE];
        public Vector3[] ulseeShape3d = new Vector3[ACConst.FACE_ULSEE_SIZE];
        public Vector2[] ulseeVertexShape = new Vector2[ACConst.FACE_ULSEE_SIZE];

        public Vector2[] ulseeNormalizedVertexShape = new Vector2[ACConst.FACE_ULSEE_SIZE];

        public Vector2[] ulseeShapePortrait = new Vector2[ACConst.FACE_ULSEE_SIZE];

        public Vector3[] featurePositions = new Vector3[ACConst.FACE_ULSEE_SIZE];

        public Vector3[] extraFeaturePositions = new Vector3[ACConst.FACE_EXTRA_FEATURE_SIZE];

        public Vector2 center;

        public float scale;

        public Vector3 faceLocation;

        public Quaternion faceRotation;

    }

}