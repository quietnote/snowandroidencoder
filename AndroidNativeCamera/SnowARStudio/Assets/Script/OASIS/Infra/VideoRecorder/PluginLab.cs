﻿using UnityEngine;
using System.Runtime.InteropServices;
using System;

namespace OASIS.VideoRecorder
{
    public class PluginLab
    {
        // NativeVideoRecorder    
        [DllImport("__Internal")]
        public static extern long CreateSnowNativeVideoRecorder(int width, int height, int startX, int startY, int audioChannels, int audioFrequency);

        [DllImport("__Internal")]
        public static extern IntPtr GetSnowNativeVideoRecorderRenderEventFunc();

        [DllImport("__Internal")]
        public static extern void SnowNativeVideoRecorder_FillSoundData(long token, float[] array, int length);

        [DllImport("__Internal")]
        public static extern void DestroySnowNativeVideoRecorder(long token);

        // Native Image Capture
        [DllImport("__Internal")]
        public static extern long CreateSnowNativeCapture(int width, int height, int startX, int startY);

        [DllImport("__Internal")]
        public static extern IntPtr GetSnowNativeVideoCaptureRenderEventFunc();

        [DllImport("__Internal")]
        public static extern void DestroySnowNativeVideoCapture(long token);

        [DllImport("__Internal")]
        public static extern long NativeCapture(byte[] pixels, int width, int height);

        [DllImport("__Internal")]
        public static extern long NativeStartVideoRecord(int width, int height, int audioChannels, int audioFrequency);

        [DllImport("__Internal")]
        public static extern void NativeFillVideoData(long token, byte[] pixels, int width, int height);
    }
}