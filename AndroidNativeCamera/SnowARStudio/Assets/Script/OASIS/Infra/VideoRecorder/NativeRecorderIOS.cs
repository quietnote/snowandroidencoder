﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using System;
using UnityEngine.UI;

namespace OASIS.VideoRecorder
{
    public class NativeRecorderIOS : MonoBehaviour
    {
        public Camera m_TargetCamera;
#if UNITY_IOS && !UNITY_EDITOR
        CommandBuffer m_CommandBuffer;
        string m_MicString;
        AudioClip m_MicAudioClip;
        int m_MicReadHead;
        long m_RecorderToken = 0;
        bool m_IsRecording = false;
        int m_CapturingCount = 0;
        Coroutine m_FlushAudioRoutine = null;
#endif

#if UNITY_IOS && !UNITY_EDITOR
        void Update()
        {
            if(m_CapturingCount == 1)
            {
                m_CapturingCount++;
            }
            else if(m_CapturingCount > 1)
            {
                m_TargetCamera.RemoveCommandBuffer(CameraEvent.AfterImageEffects, m_CommandBuffer);
                m_CommandBuffer = null;
                PluginLab.DestroySnowNativeVideoCapture(m_RecorderToken);
                m_CapturingCount = 0;
            }
            else
            {
                m_CapturingCount = 0;
            }
        }
#endif

        public void Capture(int type)
        {
#if UNITY_IOS && !UNITY_EDITOR
            switch (type)
            {
                case 0:
                    m_RecorderToken = PluginLab.CreateSnowNativeCapture(Screen.width, Screen.height, 0, 0);
                    break;
                case 1:
                    m_RecorderToken = PluginLab.CreateSnowNativeCapture(Screen.width, (int)(Screen.width * 1.3333f), 0, 404);
                    break;
                case 2:
                    m_RecorderToken = PluginLab.CreateSnowNativeCapture(Screen.width, Screen.width, 0, 431);
                    break;
            }

            RefreshCommandBuffer();
            m_CapturingCount = 1;
#endif
        }

        public void Record(int type)
        {
#if UNITY_IOS && !UNITY_EDITOR
            // Microphone always single channeled; 44100 is a typical frequency to record PCM stream
            switch (type)
            {
                case 0:
                    m_RecorderToken = PluginLab.CreateSnowNativeVideoRecorder(Screen.width, Screen.height, 0, 0, 1, 44100);
                    break;
                case 1:
                    m_RecorderToken = PluginLab.CreateSnowNativeVideoRecorder(Screen.width, (int)(Screen.width*1.3333f), 0, 404, 1, 44100);
                    break;
                case 2:
                    m_RecorderToken = PluginLab.CreateSnowNativeVideoRecorder(Screen.width, Screen.width, 0, 431, 1, 44100);     
                    break;
            }

		    RefreshCommandBuffer();

    		m_MicString = Microphone.devices[0];
	    	m_MicAudioClip = Microphone.Start(m_MicString, true, 1, 44100);
		    m_MicReadHead = 0;
		    m_FlushAudioRoutine = StartCoroutine(FlushAudio());

		    m_IsRecording = true;
#endif
        }

        public void StopRecord()
        {
#if UNITY_IOS && !UNITY_EDITOR
    		if (!m_IsRecording)
	    		return;
		    m_IsRecording = false;

		    StopCoroutine(m_FlushAudioRoutine);
    		m_MicReadHead = 0;
	    	m_MicAudioClip = null;
		    m_MicString = null;
    		Microphone.End(m_MicString);

	    	m_TargetCamera.RemoveCommandBuffer(CameraEvent.AfterImageEffects, m_CommandBuffer);
		    m_CommandBuffer = null;

		    PluginLab.DestroySnowNativeVideoRecorder(m_RecorderToken);
#endif
        }

#if UNITY_IOS && !UNITY_EDITOR
        void RefreshCommandBuffer()
        {
            m_CommandBuffer = new CommandBuffer();
            m_CommandBuffer.IssuePluginEventAndData(PluginLab.GetSnowNativeVideoRecorderRenderEventFunc(), 0, (IntPtr)m_RecorderToken);
            m_TargetCamera.AddCommandBuffer(CameraEvent.AfterImageEffects, m_CommandBuffer);
        }
#endif

#if UNITY_IOS && !UNITY_EDITOR
    	IEnumerator FlushAudio()
	    {
    		WaitForSeconds waitingTime = new WaitForSeconds(0.1f); // Flush data to native every 0.1 s
	    	int ClipSampleSize = m_MicAudioClip.samples;
            // m_audioSource.clip = m_MicAudioClip;
            // m_audioSource.pitch = 2.0f;
		
		    while (true)
		    {
    			int writeHead = Microphone.GetPosition(m_MicString);
	    		int nFloatsToGet = (ClipSampleSize + writeHead - m_MicReadHead) % ClipSampleSize;
		    	float[] buffer = new float[nFloatsToGet];
    			m_MicAudioClip.GetData(buffer, m_MicReadHead);
	    		PluginLab.SnowNativeVideoRecorder_FillSoundData(m_RecorderToken, buffer, buffer.Length);											
		    	m_MicReadHead = writeHead;
    			yield return waitingTime;	
	    	}
	    }
#endif
    }
}