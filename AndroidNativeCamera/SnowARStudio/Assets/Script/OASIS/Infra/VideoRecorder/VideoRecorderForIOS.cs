﻿using UnityEngine;

namespace OASIS.VideoRecorder
{
    public class VideoRecorderForIOS : MonoBehaviour
    {
        [SerializeField]
        int previewWidth = 720;
#if UNITY_IOS && !UNITY_EDITOR
        long m_RecorderToken = 0;
        VideoRecorder.Bitmap bitmap = new VideoRecorder.Bitmap();

        void Awake()
        {
            VideoRecorder.Init(previewWidth);
            VideoRecorder.OnCapture += OnCapture;
            VideoRecorder.OnStartRecord += OnStartRecord;
            VideoRecorder.OnStopRecord += OnStopRecord;
            VideoRecorder.OnReceiveVideoData += OnReceiveVideoData;
            VideoRecorder.OnReceiveAudioData += OnReceiveAudioData;
        }

        void OnDestroy()
        {
            VideoRecorder.OnCapture -= OnCapture;
            VideoRecorder.OnStartRecord -= OnStartRecord;
            VideoRecorder.OnStopRecord -= OnStopRecord;
            VideoRecorder.OnReceiveVideoData -= OnReceiveVideoData;
            VideoRecorder.OnReceiveAudioData -= OnReceiveAudioData;
        }

        public void OnCapture(Texture2D tex)
        {
            bitmap.colors = tex.GetPixels32();
            m_RecorderToken = PluginLab.NativeCapture(bitmap.pixels, tex.width, tex.height);
        }

        public void OnStartRecord(int videoWidth, int videoHeight, int audioChannels, int audioFreq)
        {
            m_RecorderToken = PluginLab.NativeStartVideoRecord(videoWidth, videoHeight, audioChannels, audioFreq);
        }
        
        public void OnStopRecord()
        {
            PluginLab.DestroySnowNativeVideoRecorder(m_RecorderToken);
        }

        public void OnReceiveVideoData(Texture2D tex)
        {
            bitmap.colors = tex.GetPixels32();
            PluginLab.NativeFillVideoData(m_RecorderToken, bitmap.pixels, tex.width, tex.height);
        }

        public void OnReceiveAudioData(float[] data)
        {
            PluginLab.SnowNativeVideoRecorder_FillSoundData(m_RecorderToken, data, data.Length);
        }
#endif
    }
}