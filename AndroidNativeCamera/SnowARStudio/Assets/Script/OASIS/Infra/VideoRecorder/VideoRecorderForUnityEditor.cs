﻿using UnityEngine;
#if UNITY_EDITOR
using System.IO;
#endif

namespace OASIS.VideoRecorder
{
    public class VideoRecorderForUnityEditor : MonoBehaviour
    {
        [SerializeField]
        int previewWidth = 720;
#if UNITY_EDITOR
        string filepath;

        void Awake()
        {
            filepath = Application.persistentDataPath + "/capture.png";
            VideoRecorder.Init(previewWidth);
            VideoRecorder.OnCapture += OnCapture;
        }

        void OnDestroy()
        {
            VideoRecorder.OnCapture -= OnCapture;
        }

        public void OnCapture(Texture2D tex)
        {
            File.WriteAllBytes(filepath, tex.EncodeToPNG());
            Debug.Log("Captured at " + filepath);
        }
#endif
    }
}