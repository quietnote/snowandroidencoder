﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace OASIS.VideoRecorder
{
    public class VideoRecorder : MonoBehaviour
    {
        public enum AspectRatioType
        {
            FullScreen = 0,
            Ratio4_3,
            Ratio1_1
        }

        [StructLayout(LayoutKind.Explicit)]
        public struct Bitmap
        {
            [FieldOffset(0)]
            public byte[] pixels;

            [FieldOffset(0)]
            public Color32[] colors;
        }

        enum State
        {
            Preview,
            Capture,
            Record,
            StopRecord
        }
        State currentState = State.Preview;

        //Video
        static Rect[] captureRects;
        Rect captureRect;
        RenderTexture renderTexture;
        Texture2D texture;

        //Audio
        string m_MicString;
        AudioClip m_MicAudioClip;
        int m_MicReadHead;
        Coroutine m_FlushAudioRoutine = null;

        bool started = false;

        void Start()
        {
            if (captureRects == null)
            {
                Init(Screen.width);    
            }
        }

        public static void Init(int previewWidth)
        {
            float aspectRatio = (float)Screen.height / (float)Screen.width;
            float previewHeight = previewWidth * aspectRatio;
            captureRects = new Rect[] {
                new Rect(0, 0, previewWidth, previewHeight),
                new Rect(0, (previewHeight - previewWidth * 1.3333f) * 0.5f, previewWidth, previewWidth * 1.3333f),
                new Rect(0, (previewHeight - previewWidth) * 0.5f, previewWidth, previewWidth)
            };
        }

        public void Capture(int type)
        {
            Capture((AspectRatioType)type);
        }

        public void Capture(AspectRatioType type)
        {
            if (State.Preview != currentState) {
                return;
            }
            captureRect = captureRects[(int)type];

            SetState(State.Capture);
        }

        public void Record(int type)
        {
            Record((AspectRatioType)type);
        }

        public void Record(AspectRatioType type)
        {
            if (State.Preview != currentState) {
                return;
            }
            captureRect = captureRects[(int)type];

            SetState(State.Record);
            Debug.LogError("VideoRecorder:Record()");

            if (OnStartRecord != null) {
                //Video width, Video height, Audio channel count, Audio frequency
                OnStartRecord((int)captureRect.width, (int)captureRect.height, 1, 44100);
            }
            m_MicString = Microphone.devices[0];
		    m_MicAudioClip = Microphone.Start(m_MicString, true, 1, 44100);
		    m_MicReadHead = 0;
		    m_FlushAudioRoutine = StartCoroutine(FlushAudio());
        }

        public void StopRecord()
        {
            Debug.LogError("VideoRecorder:StopRecord(): currentState = " + currentState);
            if (State.Record != currentState)
            {
                return;
            }

            SetState(State.StopRecord);

            StopCoroutine(m_FlushAudioRoutine);
            m_MicReadHead = 0;
            m_MicAudioClip = null;
            m_MicString = null;
            Microphone.End(m_MicString);
        }

        void SetState(State state)
        {
            currentState = state;
        }

        protected Texture2D GetReadPixelsToScaledTexture(Rect rect)
        {
            float scale = (float)RenderTexture.active.width / rect.width;
            renderTexture = CheckValidRenderTexture(renderTexture, Screen.width, Screen.height);
            Graphics.Blit(RenderTexture.active, renderTexture, Vector2.one * scale, Vector2.zero);

            return GetReadPixelsToTexture(rect);
        }

        protected Texture2D GetReadPixelsToTexture(Rect rect)
        {
            texture = CheckValidTexture(texture, (int)rect.width, (int)rect.height);
            texture.ReadPixels(rect, 0, 0);
            texture.Apply();

            return texture;
        }

        protected Texture2D CheckValidTexture(Texture2D tex, int width, int height)
        {
            if (tex == null || tex.width != width || texture.height != height) {
                tex = new Texture2D(width, height);
            }
            return tex;
        }

        protected RenderTexture CheckValidRenderTexture(RenderTexture tex, int width, int height)
        {
            if (tex == null || tex.width != width || texture.height != height) {
                tex = new RenderTexture(width, height, 0);
            }
            return tex;
        }

        IEnumerator FlushAudio()
	    {
		    WaitForSeconds waitingTime = new WaitForSeconds(0.1f); // Flush data to native every 0.1 s
		    int ClipSampleSize = m_MicAudioClip.samples;
            // m_audioSource.clip = m_MicAudioClip;
            // m_audioSource.pitch = 2.0f;
		
		    while (true) {
			    int writeHead = Microphone.GetPosition(m_MicString);
			    int nFloatsToGet = (ClipSampleSize + writeHead - m_MicReadHead) % ClipSampleSize;
			    float[] buffer = new float[nFloatsToGet];
			    m_MicAudioClip.GetData(buffer, m_MicReadHead);
                if (OnReceiveAudioData != null) {
			        OnReceiveAudioData(buffer);
                }
			    m_MicReadHead = writeHead;
			    yield return waitingTime;	
		    }
	    }

        void OnPostRender()
        {
            Debug.LogWarning("VideoRecorder:OnPostRender(): currentState = " + currentState);
            if (State.Preview == currentState)
            {
                return;
            }

            Texture2D tex = GetReadPixelsToScaledTexture(captureRect);
            if (State.Capture == currentState)
            {
                if (OnCapture != null) {
                    OnCapture(tex);
                }
                SetState(State.Preview);
                RelaseTexturesInvoke();
            }
            else if (State.Record == currentState)
            {
                if (OnReceiveVideoData != null) {
//#if !UNITY_EDITOR && UNITY_ANDROID
//                    OnReceiveVideoData(RenderTexture.active.GetNativeTexturePtr());
//#else
                    OnReceiveVideoData(tex);
//#endif
                }

            }
            else if (State.StopRecord == currentState)
            {
                if (OnStopRecord != null) {
                    Debug.LogWarning("VideoRecorder:OnPostRender(): currentState = " + currentState);
                    OnStopRecord();
                    started = false;
                }
                SetState(State.Preview);
                RelaseTexturesInvoke();
            }
        }

        void OnDestroy()
        {
            RelaseTextures();
        }

        void RelaseTexturesInvoke()
        {
            if (IsInvoking()) {
                CancelInvoke();
            }
            Invoke("RelaseTextures", 1);
        }

        void RelaseTextures()
        {
            if (renderTexture != null) {
                renderTexture.Release();
                renderTexture = null;
            }
            if (texture != null) {
                DestroyImmediate(texture);
                texture = null;
            }
        }

        public static System.Action<Texture2D> OnCapture;
        public static System.Action OnStopRecord;
        public static System.Action<float[]> OnReceiveAudioData;
        public static System.Action<int, int, int, int> OnStartRecord;
        public static System.Action<Texture2D> OnReceiveVideoData;
    }
}
