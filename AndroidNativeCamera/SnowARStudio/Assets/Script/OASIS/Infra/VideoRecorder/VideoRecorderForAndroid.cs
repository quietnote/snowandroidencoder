﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;

namespace OASIS.VideoRecorder
{
#if UNITY_ANDROID && !UNITY_EDITOR
    using NC;
#endif

    public class VideoRecorderForAndroid : MonoBehaviour
    {
        [SerializeField]
        int previewWidth = 720;
#if UNITY_ANDROID && !UNITY_EDITOR
        // Structure to cast Color32[] to byte[]
        [StructLayout(LayoutKind.Explicit)]
        private struct Color32Array
        {
            [FieldOffset(0)]
            public byte[] byteArray;

            [FieldOffset(0)]
            public Color32[] colors;
        }
        Color32Array m_colorBuffer = new Color32Array();


        AndroidNativeCamera m_AndroidCameraLib = null;
        int m_Preferredidx = 0;

        void Awake()
        {
            VideoRecorder.Init(previewWidth);
            VideoRecorder.OnCapture += OnCapture;
            VideoRecorder.OnStartRecord += OnStartRecord;
            VideoRecorder.OnStopRecord += OnStopRecord;
            VideoRecorder.OnReceiveVideoData += OnReceiveVideoData;
            VideoRecorder.OnReceiveAudioData += OnReceiveAudioData;

            if (m_AndroidCameraLib == null)
            {
                m_AndroidCameraLib = AndroidNativeCamera.GetInstance();
            }
        }

        void OnDestroy()
        {
            VideoRecorder.OnCapture -= OnCapture;
            VideoRecorder.OnStartRecord -= OnStartRecord;
            VideoRecorder.OnStopRecord -= OnStopRecord;
            VideoRecorder.OnReceiveVideoData -= OnReceiveVideoData;
            VideoRecorder.OnReceiveAudioData -= OnReceiveAudioData;

            if (m_AndroidCameraLib == null)
            {
                m_AndroidCameraLib.Dispose();
            }
            m_AndroidCameraLib = null;
        }

        public void OnCapture(Texture2D tex)
        {
           // bitmap.colors = tex.GetPixels32();
           // m_RecorderToken = PluginLab.NativeCapture(bitmap.pixels, tex.width, tex.height);
        }

        public void OnStartRecord(int videoWidth, int videoHeight, int audioChannels, int audioFreq)
        {
            Debug.LogError("VideoRecorderFroAndroid:OnStartRecord()");
            bool cameraAvailable = m_AndroidCameraLib.OnStartRecord(videoWidth, videoHeight, audioChannels, audioFreq);
            Debug.LogError("VideoRecorderFroAndroid:OnStartRecord(): cameraAvailable = " + cameraAvailable);
        }
        
        public void OnStopRecord()
        {
            Debug.LogError("VideoRecorderFroAndroid:OnStopRecord()");
            m_AndroidCameraLib.OnStopRecord();
        }

        public void OnReceiveVideoData(Texture2D tex)
        {
            Debug.LogError("VideoRecorderFroAndroid:OnReceiveVideoData()");
            m_colorBuffer.colors = tex.GetPixels32();
            m_AndroidCameraLib.OnReceiveVideoData(m_colorBuffer.byteArray, m_colorBuffer.byteArray.Length);
        }

        public void OnReceiveAudioData(float[] data)
        {
            m_AndroidCameraLib.OnReceiveAudioData(data, data.Length);
        }
        
#endif
    }
}