﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.AR
{
    using ST;
    public struct CameraSessionConfig
    {
        public bool preferFrontFacing;
        public int targetWidth;
        public int targetHeight;
        public int targetRefreshRate;

        public CameraSessionConfig(bool inPreferFrontFacing, int inTargetWidth, int inTargetHeight, int inTargetRefreshRate)
        {
            preferFrontFacing = inPreferFrontFacing;
            targetWidth = inTargetWidth;
            targetHeight = inTargetHeight;
            targetRefreshRate = inTargetRefreshRate;
        }
    }

    public enum TextureRotationCommand
    {
        kNoRotation = 0,
        kClockwise90 = 90,
        kClockwise180 = 180,
        kClockwise270 = 270
    }

    [Serializable]
    public struct CameraFrame : IEquatable<CameraFrame>
    {
        public Texture cameraTextureRef;
        public Texture cameraFitTextureRef;

        // 1. Maybe flip top and bottom (texture data layout may be upside-down)
        // 2. Maybe rotate (app and device camera orientation may not match)
        // 3. Maybe flip left and right (may want mirroring)
        public bool shouldUpDownFlip;
        public TextureRotationCommand shouldRotateBy;
        public bool shouldLeftRightFlip;

        public Vector2Int renderResolution;

        public float virtualWidth;
        public float virtualHeight;
        public float virtualDistance;

        // Actually this can be calculated from virtual height and virtual distance
        // However caching it here is good because then we prevent doing maths to-and-fro
        public float fov;

        public int lastUpdateFrameCount;
        public double lastUpdateTime;

        public bool Equals(CameraFrame other)
        {
            return EqualityComparer<Texture>.Default.Equals(cameraTextureRef, other.cameraTextureRef) &&
                   shouldUpDownFlip == other.shouldUpDownFlip &&
                   shouldRotateBy == other.shouldRotateBy &&
                   shouldLeftRightFlip == other.shouldLeftRightFlip &&
                   renderResolution == other.renderResolution &&
                   virtualWidth == other.virtualWidth &&
                   virtualHeight == other.virtualHeight &&
                   virtualDistance == other.virtualDistance &&
                   fov == other.fov &&
                   lastUpdateFrameCount == other.lastUpdateFrameCount &&
                   lastUpdateTime == other.lastUpdateTime;
        }

        [Obsolete] // Temporary property for FaceBuilder
        public TextureRotationCommand videoRotation
        {
            get
            {
                switch(shouldRotateBy)
                {
                    case TextureRotationCommand.kClockwise270:
                        return TextureRotationCommand.kClockwise90;

                    case TextureRotationCommand.kClockwise90:
                        return TextureRotationCommand.kClockwise270;

                    default:
                        return shouldRotateBy;
                }
            }
        }
    }

    public interface IARSession : IDisposable
    {
        void UpdateSessionConfig(CameraSessionConfig cameraConfig);

        void UpdateAnalysisConfig(SenseTimeLib.Config config);

        void SetAnalysisPaused(bool value);

        void SessionBegin();

        void SetNextCamera();

		void Tick();

        void SessionEnd();

        bool GetFrame(ref CameraFrame frame);

        bool GetPose(ref Vector3 location, ref Quaternion rotation);

        int GetFaceCount();

        void GetFaceAt(int idx, ref SenseTimeLib.FaceData data);

        int GetHandCount();

        void GetHandAt(int idx, ref SenseTimeLib.HandData data);

        int GetBodyCount();

        void GetBodyAt(int idx, ref SenseTimeLib.BodyData data);
    }
}
