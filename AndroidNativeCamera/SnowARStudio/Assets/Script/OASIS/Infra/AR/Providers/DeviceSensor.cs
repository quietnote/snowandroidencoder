﻿using UnityEngine;

namespace OASIS.AR
{
    public static class DeviceSensor
    {
        public static void EnsureSensorIsOn()
        {
            // Make sure we enable gyro
            Input.gyro.enabled = true;
        }

        // The real world gravity points towards this direction in Unity space
        public static bool GetGravityVector(ref Vector3 value)
        {
            // Direction for z axis on iOS and Android are both opposite to that of unity
            // https://developer.android.com/reference/android/hardware/SensorEvent#values
            // https://developer.apple.com/documentation/coremotion/getting_raw_gyroscope_events
            value = Input.gyro.gravity;
            value.z = -value.z;

            // On PC/OSX Unity tells us we have gyro enabled but gravity vector is zero
            // We return the downward pointing vector
            if (value.x < Mathf.Epsilon && value.x > -Mathf.Epsilon &&
                value.y < Mathf.Epsilon && value.y > -Mathf.Epsilon &&
                value.z < Mathf.Epsilon && value.z > -Mathf.Epsilon)
            {
                value = Vector3.down;
            }

            return true;
        }

        // Currently the raw sensor getters only works when our app is in portrait orientation
        // This app has Input.compensateSensors == true while Sensetime expects Input.compensateSensors == false
        // Only in portrait orientation, value of Input.compensateSensors does not matter

        public static bool GetRawGravityVector(ref Vector3 value)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                // Let's assume the app is in portrait orientation, then:
                // From unity 2018.3 android platform source code Sensors.cpp:
                // const float ooMinusG = -1.f / ASENSOR_STANDARD_GRAVITY;
                // mGravity.x =  ooMinusG * event.data[0];
                // mGravity.y =  ooMinusG * event.data[1];
                // mGravity.z =  ooMinusG * event.data[2];
                // From https://android.googlesource.com/platform/frameworks/native/+/jb-mr1-dev/include/android/sensor.h:
                // #define ASENSOR_STANDARD_GRAVITY            (9.80665f)
                value = Input.gyro.gravity * -1.0f * 9.80665f;

                return true;
            }
            else if (Application.platform == RuntimePlatform.IPhonePlayer)
            {
                value = Input.gyro.gravity;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool GetRawUserAcceleration(ref Vector3 value)
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                // This assumes the app is in portrait orientation
                value = Input.gyro.userAcceleration;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool GetRawRotationRate(ref Vector3 value)
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                // This assumes the app is in portrait orientation
                value = Input.gyro.rotationRate;
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool GetRawAttitude(ref Quaternion value)
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                // This assumes the app is in portrait orientation
                value = Input.gyro.attitude;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}