using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;

namespace OASIS.AR
{
    using ST;
	using NC;

    public class WebCamProvider : IARSession
    {
        // Temp - use this until we obtain actual device FOV
        public float m_UnityCameraVerticalFOV = 45.0f;

        // Structure to cast Color32[] to byte[]
        [StructLayout(LayoutKind.Explicit)]
        private struct Color32Array
        {
            [FieldOffset(0)]
            public byte[] byteArray;

            [FieldOffset(0)]
            public Color32[] colors;
        }

        CameraSessionConfig m_SessionConfig;
        bool m_AnalysisPaused = false;
        bool m_ShouldAnalyzeSLAM = false;
        bool m_ShouldAnalyzeHumanAction = false;
        SenseTimeLib m_SenseTimeLib = null;
        bool m_webCamIsFrontFacing = false;
#if !UNITY_EDITOR && UNITY_ANDROID
        Texture2D m_BackgroundTex = null;	
        Texture2D m_ScreenYTex;
		Texture2D m_ScreenUVTex;
#else
        WebCamTexture m_BackgroundTex = null;
#endif
        Color32Array m_colorBuffer = new Color32Array();
        Color32Array m_flippedColorBuffer = new Color32Array();
        int m_LastCamFrameFrameCount;
        double m_LastCamFrameTimeStamp;

        Func<Vector3> m_GetGravityVector = null;
        Func<Vector3> m_GetRawGravity = null;
        Func<Vector3> m_GetRawUserAcceleration = null;
        Func<Vector3> m_GetRawRotationRate = null;
        Func<Quaternion> m_GetRawAttitude = null;

#if !UNITY_EDITOR && UNITY_ANDROID
        AndroidNativeCamera m_AndroidCameraLib = null;
        int m_Preferredidx = 0;		
#endif

        public WebCamProvider(Func<Vector3> getGravityVector,
            Func<Vector3> getRawGravity, Func<Vector3> getRawUserAcceleration, Func<Vector3> getRawRotationRate, Func<Quaternion> getRawAttitude)
        {
            m_GetGravityVector = getGravityVector;
            m_GetRawGravity = getRawGravity;
            m_GetRawUserAcceleration = getRawUserAcceleration;
            m_GetRawRotationRate = getRawRotationRate;
            m_GetRawAttitude = getRawAttitude;

            if (m_SenseTimeLib == null)
            {
                m_SenseTimeLib = SenseTimeLib.GetInstance();
            }

#if !UNITY_EDITOR && UNITY_ANDROID
            if (m_AndroidCameraLib == null)
            {
                m_AndroidCameraLib = AndroidNativeCamera.GetInstance();
            }
#endif			

            // Init with some default value in case user of this class does not specify
            m_SessionConfig = new CameraSessionConfig(true, 1280, 720, 30);
        }

        public void Dispose()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            if (m_AndroidCameraLib == null)
            {
                m_AndroidCameraLib.Dispose();
            }
            m_AndroidCameraLib = null;
#endif
            if (m_SenseTimeLib != null)
            {
                m_SenseTimeLib.Dispose();
            }

            m_SenseTimeLib = null;
        }

        public void UpdateSessionConfig(CameraSessionConfig cameraConfig)
        {
            m_SessionConfig = cameraConfig;
        }

        public void UpdateAnalysisConfig(SenseTimeLib.Config config)
        {
            if (m_SenseTimeLib == null)
            {
                return;
            }

            m_SenseTimeLib.UpdateConfig(config);

            m_ShouldAnalyzeSLAM = config.useSLAM;
            m_ShouldAnalyzeHumanAction = (config.maxFaceCount > 0 || config.maxHandCount > 0 || config.maxBodyCount > 0 || config.segmetationFlags.useBackground);
        }

        public void SetAnalysisPaused(bool value)
        {
            m_AnalysisPaused = value;
        }

        public void SessionBegin()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            if (m_BackgroundTex == null)
            {
                int numberOfCameras = m_AndroidCameraLib.GetCamera2Count();

                m_Preferredidx = 0;
                for (int i = 0; i < numberOfCameras; ++i)
                {
                    if (m_AndroidCameraLib.IsCamera2FrontFacing(i) == m_SessionConfig.preferFrontFacing)
                    {
                        m_Preferredidx = i;
                        break;
                    }
                }

                m_webCamIsFrontFacing = m_AndroidCameraLib.IsCamera2FrontFacing(m_Preferredidx);

                bool cameraAvailable = m_AndroidCameraLib.InitializeCamera2(m_Preferredidx, m_SessionConfig.targetWidth, m_SessionConfig.targetHeight, m_SessionConfig.targetRefreshRate);
                Debug.LogError("WebCamProvider.SessionBegin:cameraAvailable = " + cameraAvailable);

                Rect frameSize = m_AndroidCameraLib.GetFrameSizeCamera2();
                Debug.LogError("WebCamProvider.SessionBegin:frameSize = " + frameSize);

               if (m_ScreenYTex == null || m_ScreenYTex.width != m_SessionConfig.targetWidth || m_ScreenYTex.height != m_SessionConfig.targetHeight) {
                    m_ScreenYTex = new Texture2D (m_SessionConfig.targetWidth, m_SessionConfig.targetHeight, TextureFormat.R8, false, true);
	    		}
		    	if (m_ScreenUVTex == null || m_ScreenUVTex.width != m_SessionConfig.targetWidth || m_ScreenUVTex.height != m_ScreenYTex.height) {
                    m_ScreenUVTex = new Texture2D (m_SessionConfig.targetWidth, m_ScreenYTex.height, TextureFormat.RG16, false, true);
			    }

                m_AndroidCameraLib.Play(); // --> StartCamera2();
                m_BackgroundTex = new Texture2D(m_SessionConfig.targetWidth, m_SessionConfig.targetHeight, TextureFormat.BGRA32, false, false);      

                m_LastCamFrameFrameCount = Time.frameCount;
                m_LastCamFrameTimeStamp = Time.unscaledTime;
                Debug.LogError("WebCamProvider.SessionBegin:m_BackgroundTex = " + m_BackgroundTex);
            }
#else
            if (m_BackgroundTex == null)
            {
                WebCamDevice[] devices = WebCamTexture.devices;

                // Prefer front facing camera
                int preferredidx = 0;
                for (int i = 0; i < devices.Length; ++i)
                {
                    if (devices[i].isFrontFacing == m_SessionConfig.preferFrontFacing)
                    {
                        preferredidx = i;
                        break;
                    }
                }

                m_webCamIsFrontFacing = devices[preferredidx].isFrontFacing;

                m_BackgroundTex = new WebCamTexture(devices[preferredidx].name, m_SessionConfig.targetWidth, m_SessionConfig.targetHeight, m_SessionConfig.targetRefreshRate);
                m_BackgroundTex.Play();

                m_LastCamFrameFrameCount = Time.frameCount;
                m_LastCamFrameTimeStamp = Time.unscaledTime;
            }
#endif		
        }

        public void Tick()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            m_LastCamFrameFrameCount = Time.frameCount;
            m_LastCamFrameTimeStamp = Time.unscaledTime;
#else
            if (m_BackgroundTex && m_BackgroundTex.didUpdateThisFrame)
            {
                m_LastCamFrameFrameCount = Time.frameCount;
                m_LastCamFrameTimeStamp = Time.unscaledTime;
            }
#endif
        }

        public void SetNextCamera()
        {
            SessionEnd();
            m_SessionConfig.preferFrontFacing = !m_SessionConfig.preferFrontFacing;
            if (m_SenseTimeLib != null)
            {
                m_SenseTimeLib.ResetHumanAnalyzer();
                m_SenseTimeLib.ResetSLAMAnalyzer();
            }
            SessionBegin();
        }

        public void SessionEnd()
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            if (m_AndroidCameraLib != null)
            {
                m_AndroidCameraLib.Stop(); // --> StopCamera2();
            }
            if (m_BackgroundTex != null)
            {
                m_BackgroundTex = null;
            }
#else
            if (m_BackgroundTex != null)
            {
                m_BackgroundTex.Stop();
                m_BackgroundTex = null;
            }
#endif
        }

        public bool GetFrame(ref CameraFrame frame)
        {
            if (!FillFrame(ref frame))
                return false;

            if (m_AnalysisPaused)
                return true;

            if (frame.lastUpdateFrameCount == Time.frameCount)
            {
                // Note we only do SLAM analysis if WebCam-texture is back-facing
                // If we want to support front facing slam, we need to up-down flip the image we show to SLAM
                // Currently, front-facing SLAM is not a common use case that deserves this extra complexity
                if (m_ShouldAnalyzeSLAM && !m_webCamIsFrontFacing)
                {
                    // Sense-time counts texture buffer differently from unity
                    // For correct SLAM calculation, we flip the texture array upside-down

                    // We can use trick to avoid this copy (flip sensor data + flip output instead of flip image)
                    // However doing this trick is not so easy
                    // Since we have more efficient implementation with native camera providers on mobile platforms anyway
                    // Let's stick with simpler implementation here

                    GetPixel(true);
                }
                else if (m_ShouldAnalyzeHumanAction)
                {
                    GetPixel(false);
                }

                if (m_ShouldAnalyzeSLAM && !m_webCamIsFrontFacing)
                {
                    AnalyzeSLAM(ref frame);
                }

                if (m_ShouldAnalyzeHumanAction)
                {
                    AnalyzeHumanAction(ref frame);
                }
            }

            return true;
        }

        private bool FillFrame(ref CameraFrame frame)
        {
#if !UNITY_EDITOR && UNITY_ANDROID
            if (m_AndroidCameraLib == null)
            {
                return false;
            }

            byte[] rawData = m_AndroidCameraLib.GetRawData();
            if(rawData == null){
                Debug.LogError("WebCamProvider.GetFrame(): rawData is null");
                return false;
            }
            
            //Debug.LogWarning("WebCamProvider.GetFrame(): m_BackgroundTex.format = " + m_BackgroundTex.format);
            //Debug.LogWarning("WebCamProvider.GetFrame(): Texture set " + m_BackgroundTex.width + " x " + m_BackgroundTex.height + " = " + m_BackgroundTex.GetPixels32().Length + ", rawData.Length = " + rawData.Length);

            m_BackgroundTex.LoadRawTextureData(rawData);
            m_BackgroundTex.Apply();
            
#endif		
		
            if (m_BackgroundTex != null)
            {
                frame.cameraTextureRef = m_BackgroundTex;

#if !UNITY_EDITOR && UNITY_ANDROID
                frame.shouldUpDownFlip = true;
                switch (m_AndroidCameraLib.GetCamera2SensorOrientation(m_Preferredidx))
                {
                    case 90:
                        frame.shouldRotateBy = TextureRotationCommand.kClockwise270;
                        break;

                    case 180:
                        frame.shouldRotateBy = TextureRotationCommand.kClockwise180;
                        break;

                    case 270:
                        frame.shouldRotateBy = TextureRotationCommand.kClockwise90;
                        break;

                    default:
                        frame.shouldRotateBy = TextureRotationCommand.kNoRotation;
                        break;
                }
                //Debug.LogError("WebCamProvider.GetFrame(): GetCamera2SensorOrientation(m_Preferredidx) =" + m_AndroidCameraLib.GetCamera2SensorOrientation(m_Preferredidx));

                //Debug.LogError("WebCamProvider.GetFrame(): frame.shouldRotateBy =" + frame.shouldRotateBy);
                frame.shouldLeftRightFlip = m_webCamIsFrontFacing;
#else
                frame.shouldUpDownFlip = m_BackgroundTex.videoVerticallyMirrored;
                switch (m_BackgroundTex.videoRotationAngle)
                {
                    case 90:
                        frame.shouldRotateBy = TextureRotationCommand.kClockwise270;
                        break;

                    case 180:
                        frame.shouldRotateBy = TextureRotationCommand.kClockwise180;
                        break;

                    case 270:
                        frame.shouldRotateBy = TextureRotationCommand.kClockwise90;
                        break;

                    default:
                        frame.shouldRotateBy = TextureRotationCommand.kNoRotation;
                        break;
                }
                frame.shouldLeftRightFlip = m_webCamIsFrontFacing;
#endif
                // to do : 꽉찬화면이 아닐때 계산 필요. (ex. 1:1, 3:4 )
                frame.renderResolution.x = Screen.width;
                frame.renderResolution.y = Screen.height;

                frame.virtualDistance = 1;
                frame.virtualHeight = frame.virtualDistance * (2.0f * Mathf.Tan(Mathf.Deg2Rad * (m_UnityCameraVerticalFOV / 2)));
                frame.virtualWidth = (frame.shouldRotateBy == TextureRotationCommand.kClockwise180 || frame.shouldRotateBy == TextureRotationCommand.kNoRotation) ?
                    frame.virtualHeight * (float)m_BackgroundTex.width / (float)m_BackgroundTex.height :
                    frame.virtualHeight * (float)m_BackgroundTex.height / (float)m_BackgroundTex.width;

                // Normalize so virtualHeight is 1, should be easiest format to understand
                frame.virtualDistance /= frame.virtualHeight;
                frame.virtualWidth /= frame.virtualHeight;
                frame.virtualHeight = 1.0f; // Normalize virtualHeight;

                frame.fov = m_UnityCameraVerticalFOV;

                frame.lastUpdateFrameCount = m_LastCamFrameFrameCount;
                frame.lastUpdateTime = m_LastCamFrameTimeStamp;

                return true;
            }

            return false;
        }

        private void GetPixel(bool updateFlippedBuffer)
        {
            int colorBufferSize = m_BackgroundTex.width * m_BackgroundTex.height;
            if (m_colorBuffer.colors == null || colorBufferSize != m_colorBuffer.colors.Length)
            {
                m_colorBuffer.colors = new Color32[colorBufferSize];
            }
			
#if !UNITY_EDITOR && UNITY_ANDROID
                m_colorBuffer.colors = m_BackgroundTex.GetPixels32();
#else
            m_BackgroundTex.GetPixels32(m_colorBuffer.colors);
#endif			


            if (updateFlippedBuffer)
            {
                if (m_flippedColorBuffer.colors == null || colorBufferSize != m_flippedColorBuffer.colors.Length)
                {
                    m_flippedColorBuffer.colors = new Color32[colorBufferSize];
                }
                for (int i = 0; i < m_BackgroundTex.height; ++i)
                {
                    int sourceOffset = i * m_BackgroundTex.width;
                    int destOffset = (m_BackgroundTex.height - 1 - i) * m_BackgroundTex.width;

                    Array.Copy(m_colorBuffer.colors, sourceOffset, m_flippedColorBuffer.colors, destOffset, m_BackgroundTex.width);
                }
            }
        }

        private void AnalyzeSLAM(ref CameraFrame frame)
        {
            // frame.fov is the vertical fov in unity space
            // Sensetime wants to know the horizontal fov from device camera point of view
            float cameraDeviceFOVX = 45.0f;
            if (frame.shouldRotateBy == TextureRotationCommand.kNoRotation || frame.shouldRotateBy == TextureRotationCommand.kClockwise180)
            {
                cameraDeviceFOVX = 2 * Mathf.Rad2Deg * Mathf.Atan2(frame.virtualWidth / 2.0f, frame.virtualDistance);
            }
            else
            {
                cameraDeviceFOVX = 2 * Mathf.Rad2Deg * Mathf.Atan2(frame.virtualHeight / 2.0f, frame.virtualDistance);
            }

            // For SLAM, we want rotation of camera device relative to device in portrait orientation
            SenseTimeLib.RotateCommand rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate0;
            switch (frame.shouldRotateBy)
            {
                case (TextureRotationCommand.kNoRotation):
                    rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate0;
                    break;

                case (TextureRotationCommand.kClockwise90):
                    rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate270;
                    break;

                case (TextureRotationCommand.kClockwise180):
                    rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate180;
                    break;

                case (TextureRotationCommand.kClockwise270):
                    rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate90;
                    break;
            }

            double lastUpdateTime = frame.lastUpdateTime; 

            if (m_SenseTimeLib != null)
            {
                SenseTimeLib.PixelFormat pixFormat = SenseTimeLib.PixelFormat.kBGRA8888;

                m_SenseTimeLib.AnalyzeSLAM(
                    m_flippedColorBuffer.byteArray, pixFormat,
                    m_BackgroundTex.width, m_BackgroundTex.height, m_BackgroundTex.width * 4,
                    cameraDeviceFOVX, rotateCommand, lastUpdateTime,
                    m_GetRawGravity(), m_GetRawUserAcceleration(), m_GetRawRotationRate(), m_GetRawAttitude());
            }
        }

        private void AnalyzeHumanAction(ref CameraFrame frame)
        {
            // For Human Action, we want to rotate to orientation where face is facing upwards

            Vector3 gravityVector = m_GetGravityVector();

            // 1. Get frame space gravity vector
            Vector2 frameSpaceGravityVector = new Vector2(gravityVector.x, gravityVector.y);
            if (!frame.shouldLeftRightFlip)
            {
                switch (frame.shouldRotateBy)
                {
                    case TextureRotationCommand.kClockwise90:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(0, 1);
                            Vector2 textureSpaceYAxis = new Vector2(-1, 0);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;

                    case TextureRotationCommand.kClockwise180:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(-1, 0);
                            Vector2 textureSpaceYAxis = new Vector2(0, -1);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;

                    case TextureRotationCommand.kClockwise270:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(0, -1);
                            Vector2 textureSpaceYAxis = new Vector2(1, 0);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (frame.shouldRotateBy)
                {
                    case TextureRotationCommand.kClockwise90:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(0, 1);
                            Vector2 textureSpaceYAxis = new Vector2(1, 0);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;

                    case TextureRotationCommand.kClockwise180:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(1, 0);
                            Vector2 textureSpaceYAxis = new Vector2(0, -1);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;

                    case TextureRotationCommand.kClockwise270:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(0, -1);
                            Vector2 textureSpaceYAxis = new Vector2(-1, 0);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;

                    default:
                        {
                            Vector2 textureSpaceXAxis = new Vector2(-1, 0);
                            Vector2 textureSpaceYAxis = new Vector2(0, 1);

                            frameSpaceGravityVector = new Vector2(
                                Vector2.Dot(textureSpaceXAxis, frameSpaceGravityVector),
                                Vector2.Dot(textureSpaceYAxis, frameSpaceGravityVector));
                        }
                        break;
                }
            }

            // 2. Obtain rotate command
            SenseTimeLib.RotateCommand rotateCommand;
            {
                Vector2 clockwise45XAxis = new Vector2(1, -1);
                Vector2 clockwise45YAxis = new Vector2(1, 1);

                Vector2 clockwise45FrameSpaceGravityVector = new Vector2(
                    Vector2.Dot(clockwise45XAxis, frameSpaceGravityVector),
                    Vector2.Dot(clockwise45YAxis, frameSpaceGravityVector));

                if (clockwise45FrameSpaceGravityVector.x > 0)
                {
                    if (clockwise45FrameSpaceGravityVector.y > 0)
                    {
                        rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate90;
                    }
                    else
                    {
                        rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate0;
                    }
                }
                else
                {
                    if (clockwise45FrameSpaceGravityVector.y > 0)
                    {
                        rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate180;
                    }
                    else
                    {
                        rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate270;
                    }
                }
            }

            // Sense-time counts texture buffer differently from unity
            // To make it detect without extra copy we do this:
            switch (rotateCommand)
            {
                case SenseTimeLib.RotateCommand.kClockwiseRotate0:
                    rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate180;
                    break;

                case SenseTimeLib.RotateCommand.kClockwiseRotate180:
                    rotateCommand = SenseTimeLib.RotateCommand.kClockwiseRotate0;
                    break;

                default:
                    break;
            }

            if (m_SenseTimeLib != null)
            {
                m_SenseTimeLib.AnalyzeHuman(
                    m_colorBuffer.byteArray, SenseTimeLib.PixelFormat.kRGBA8888,
                    m_BackgroundTex.width, m_BackgroundTex.height, m_BackgroundTex.width * 4,
                    rotateCommand);
            }
        }

        public bool GetPose(ref Vector3 location, ref Quaternion rotation)
        {
            location = Vector3.zero;
            rotation = Quaternion.identity;

            if (m_SenseTimeLib != null)
            {
                if (m_SenseTimeLib.GetCameraPose(ref rotation, ref location))
                {
                    // Sensetime quaternion has a different coordinate system
                    Quaternion senseTimeOriginalRotation = new Quaternion(-0.5f, 0.5f, 0.5f, 0.5f);
                    Quaternion remappedSensetimeQuat = new Quaternion(rotation.y, rotation.x, -rotation.z, rotation.w);
                    rotation = senseTimeOriginalRotation * remappedSensetimeQuat;

                    float temp = location.y;
                    location.y = location.z;
                    location.z = temp;
                }
            }

            return true;
        }

        public int GetFaceCount()
        {
            if (m_SenseTimeLib == null)
            {
                return 0;
            }

            return m_SenseTimeLib.GetFaceCount();
        }

        public void GetFaceAt(int idx, ref SenseTimeLib.FaceData data)
        {
            if (m_SenseTimeLib == null)
            {
                return;
            }

            m_SenseTimeLib.GetFace(idx, ref data);
        }

        public int GetHandCount()
        {
            if (m_SenseTimeLib == null)
            {
                return 0;
            }

            return m_SenseTimeLib.GetHandCount();
        }

        public void GetHandAt(int idx, ref SenseTimeLib.HandData data)
        {
            if (m_SenseTimeLib == null)
            {
                return;
            }

            m_SenseTimeLib.GetHand(idx, ref data);
        }

        public int GetBodyCount()
        {
            if (m_SenseTimeLib == null)
            {
                return 0;
            }

            return m_SenseTimeLib.GetBodyCount();
        }

        public void GetBodyAt(int idx, ref SenseTimeLib.BodyData data)
        {
            if (m_SenseTimeLib == null)
            {
                return;
            }

            m_SenseTimeLib.GetBody(idx, ref data);
        }
    }
}