using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace OASIS.AR
{
    using Data;
    using AR;
    public static class FaceBuilder
    {
        static readonly int[] ULS_TO_SENSETIME_MAPPING_POINTS = {
        //start , end position
        //start와 end가 같을 경우에는 해당 인덱스의 점을 사용
        //output이 uls이므로 uls기준으로 sensetime index얻는다
        //start, end
        0, 1, //0
        2, 3,
        4, 5,
        6, 7,
        8, 9,
        10, 11,
        12, 13,
        14, 15,
        16, 16, //8
        17, 18,
        19, 20,
        21, 22,
        23, 24,
        25, 26,
        27, 28,
        29, 30,
        31, 32, //얼굴 윤곽
        33, 33, //눈썹
        34, 34,
        35, 35,
        36, 36,
        37, 37,
        38, 38,
        39, 39,
        40, 40,
        41, 41,
        42, 42, //눈썹end
        43, 44, //코
        44, 45,
        45, 46,
        46, 45,
        47, 48,
        48, 49,
        49, 46,
        50, 49,
        51, 50,
        52, 52, //눈
        53, 72,
        54, 72,
        55, 55,
        56, 73,
        57, 73, //
        58, 58,
        59, 75,
        60, 75,
        61, 61,
        62, 76,
        63, 76, //눈end
        84, 84, //입
        85, 85,
        86, 86,
        87, 87,
        88, 88,
        89, 89,
        90, 90,
        91, 91,
        92, 92,
        93, 93,
        94, 94,
        95, 95,
        97, 97,
        98, 98,
        99, 99,
        101, 101,
        102, 102,
        103, 103
        };

        //ulsee base xyz-value
        static readonly float[] ulsValues = {-23.684328f, -7.5030265f, 21.151945f, -22.743355f, -2.352349f,
                           20.729406f, -21.28244f, 2.7714372f, 20.433535f, -19.357058f, 7.7446876f,
                           18.395956f, -16.979641f, 12.234822f,
                           13.498581f, -13.297601f, 16.219484f, 8.050449f, -8.888856f, 19.294376f,
                           3.360281f, -4.104984f, 21.33326f, -0.8176205f, 1.4264171f, 21.961548f,
                           -4.4754696f, 6.2517343f, 20.722843f, -0.75115836f, 10.2072315f,
                           17.980856f, 3.448203f, 13.073993f, 14.5341015f, 8.110507f, 15.283839f,
                           10.570417f, 13.549948f, 16.505222f, 5.671674f, 18.4879f, 17.172918f,
                           0.325271f, 20.628374f, 17.736547f, -5.0506f, 21.04707f, 17.960403f,
                           -9.962412f, 21.519855f, -19.026081f, -14.49633f, 0.18237412f, -16.14302f,
                           -16.870047f, -1.2203207f, -12.455539f, -17.490206f, -2.512623f,
                           -8.735617f, -17.00354f, -3.8880906f, -5.1353946f, -15.809142f, -5.25465f,
                           2.9213192f, -16.439379f, -6.2477603f, 6.222433f, -18.135796f,
                           -4.6411886f, 9.796997f, -19.174116f, -3.0252457f, 13.429057f,
                           -18.890236f, -1.3130267f, 16.385036f, -16.919764f, 0.35320044f,
                           -0.69031984f, -11.804127f,
                           -5.4693127f, -0.38651842f, -8.198722f, -6.9395804f, -0.057146907f,
                           -4.7297044f, -8.532129f, 0.20553249f, -1.4248734f, -10.186896f,
                           -3.8282077f, 1.4998801f, -6.2585497f, -1.8313558f, 1.7851435f,
                           -7.3205633f,
                           0.15915912f, 1.8905184f, -8.101802f, 2.1086688f, 1.5760608f, -7.2766805f,
                           3.897988f, 1.0507314f, -6.177949f, -14.223483f, -9.570363f, -0.15699407f,
                           -11.638964f, -11.090296f, -1.0474982f, -8.594836f, -11.033765f,
                           -1.5557873f, -6.1133566f, -9.674134f, -1.4680414f, -8.850451f, -8.8381f,
                           -1.1174908f, -11.6796665f, -8.650951f, -0.837483f, 4.7793856f,
                           -10.614662f, -1.5378315f, 7.222945f, -12.54055f, -1.8606932f, 10.293077f,
                           -13.057469f, -1.6976964f, 12.871711f, -11.6373415f, -0.7199476f,
                           10.451457f, -10.370583f, -1.3619153f, 7.579549f, -10.209447f,
                           -1.3533037f, -7.8983192f, 8.119694f, -2.3265998f, -5.411434f, 6.104798f,
                           -4.7880306f,
                           -2.4979076f, 4.8396435f, -6.386176f, 0.34118456f, 5.075428f, -7.667663f,
                           3.007718f, 4.448647f, -6.525061f, 5.773514f, 5.3901825f, -4.766304f,
                           8.225148f, 7.0640645f, -2.0105326f, 6.400966f, 9.704597f, -4.1426888f,
                           3.7945108f, 11.412959f, -6.0430975f, 0.90303415f, 12.030219f, -7.226813f,
                           -2.3213723f, 11.828053f, -6.190255f, -5.452242f, 10.536071f, -4.4230504f,
                           -2.684339f, 7.2377167f, -5.84795f, 0.5439383f, 7.169256f,
                           -7.222515f, 3.6394577f, 6.736197f, -6.10996f, 3.753714f, 7.670824f,
                           -5.6237245f, 0.61079997f, 8.120463f, -6.849585f, -2.7877262f, 8.092708f,
                           -5.529465f};

        static readonly float[] senseToUlsWeightsLinear = {0.58339596f, 0.5623896f, 0.668031f, 0.7293982f, 0.589574f,
                                         0.441773f, 0.37740266f, 0.21745974f, -1.0f, 0.9258794f,
                                         0.6939929f, 0.7616484f, 0.58085364f, 0.6928158f,
                                         0.76723164f, 0.7960624f, 0.37936506f, -1.0f, -1.0f, -1.0f,
                                         -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
                                         0.12961674f, 0.17581567f, 0.21055037f, -0.11383193f,
                                         0.37593174f, 0.17197669f, -0.0490232f, 0.13441548f,
                                         0.3367588f, -1.0f, 0.4523768f, 0.28808856f, -1.0f,
                                         0.19460048f, 0.57797194f, -1.0f, 0.2662191f, 0.49642673f,
                                         -1.0f, 0.66089183f, 0.09253002f, -1.0f, -1.0f, -1.0f,
                                         -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
                                         -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f};

        const float InverseEyeDistanceScaleFactor = 0.68068478f;
        const float ZOffset = 0.60f;
        const float ScaleFactor = 0.057f;
        const float AdjustYawScaleFactor = 1.35f;
        const float AdjustRollGapFactor = 1.0f;
        const float EyeScaleFactor = 0.86f;
        const float AdjustRoll = -4.0f;

#if UNITY_IOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_EDITOR_WIN
        const float AdjustFaceCenterX = 0.0f;
        const float AdjustFaceCenterY = -0.6f;
        const float AdjustPitchScaleFactor = 1.0f;
#else
        const float AdjustFaceCenterX = 0.15f;
        const float AdjustFaceCenterY = 0.0f;
        const float AdjustPitchScaleFactor = 1.4f;
#endif

        public static void Build(ref FaceData fd, ref CameraFrame frame)
        {
            ConvertSensetimeToUlsee(ref fd, ref frame);

            fd.center.x = fd.ulseePoseEx[3];
            fd.center.y = fd.ulseePoseEx[4];
            fd.scale = fd.ulseePoseEx[5];
            fd.pitch = -fd.ulseePoseEx[0];
            fd.yaw = -fd.ulseePoseEx[1];
            fd.roll = -fd.ulseePoseEx[2];
            BuildVertex(ref fd, ref frame);
        }

        static void ConvertSenseToUlsPointsLineFitting(ref FaceData fd, ref CameraFrame frame)
        {
            int startIdx, endIdx;

            float t, x1, x2, y1, y2;
            float eyeCenterX, eyeCenterY;
            int senseTimeEyeCenterIndex;
            const int pointCount = ACConst.FACE_ULSEE_SIZE;

            for (int i = 0; i < pointCount; i++)
            {
                startIdx = ULS_TO_SENSETIME_MAPPING_POINTS[i * 2];
                endIdx = ULS_TO_SENSETIME_MAPPING_POINTS[i * 2 + 1];

                if (startIdx == endIdx)
                {
                    fd.ulseeShape[i].x = fd.keyPoints[startIdx].x;
                    fd.ulseeShape[i].y = fd.keyPoints[startIdx].y;

                    //eye size scale..(left,right)
                    if (i >= 36 && i <= 41)
                    {
                        senseTimeEyeCenterIndex = 104;
                        eyeCenterX = fd.keyPoints[senseTimeEyeCenterIndex].x;
                        eyeCenterY = fd.keyPoints[senseTimeEyeCenterIndex].y;

                        fd.ulseeShape[i].x = (fd.ulseeShape[i].x - eyeCenterX) * EyeScaleFactor + eyeCenterX;
                        fd.ulseeShape[i].y = (fd.ulseeShape[i].y - eyeCenterY) * EyeScaleFactor + eyeCenterY;
                    }

                    if (i >= 42 && i <= 47)
                    {
                        senseTimeEyeCenterIndex = 105;
                        eyeCenterX = fd.keyPoints[senseTimeEyeCenterIndex].x;
                        eyeCenterY = fd.keyPoints[senseTimeEyeCenterIndex].y;

                        fd.ulseeShape[i].x = (fd.ulseeShape[i].x - eyeCenterX) * EyeScaleFactor + eyeCenterX;
                        fd.ulseeShape[i].y = (fd.ulseeShape[i].y - eyeCenterY) * EyeScaleFactor + eyeCenterY;
                    }

                }
                else
                {
                    //( (x2-x1)t + x1 , (y2-y1)t + y1 )
                    t = senseToUlsWeightsLinear[i];
                    x1 = fd.keyPoints[startIdx].x;
                    y1 = fd.keyPoints[startIdx].y;
                    x2 = fd.keyPoints[endIdx].x;
                    y2 = fd.keyPoints[endIdx].y;
                    fd.ulseeShape[i].x = x1 + (x2 - x1) * t;
                    fd.ulseeShape[i].y = y1 + (y2 - y1) * t;

                    //eye size scale..
                    if (i >= 36 && i <= 41)
                    {
                        senseTimeEyeCenterIndex = 104;
                        eyeCenterX = fd.keyPoints[senseTimeEyeCenterIndex].x;
                        eyeCenterY = fd.keyPoints[senseTimeEyeCenterIndex].y;

                        fd.ulseeShape[i].x = (fd.ulseeShape[i].x - eyeCenterX) * EyeScaleFactor + eyeCenterX;
                        fd.ulseeShape[i].y = (fd.ulseeShape[i].y - eyeCenterY) * EyeScaleFactor + eyeCenterY;
                    }

                    if (i >= 42 && i <= 47)
                    {
                        senseTimeEyeCenterIndex = 105;
                        eyeCenterX = fd.keyPoints[senseTimeEyeCenterIndex].x;
                        eyeCenterY = fd.keyPoints[senseTimeEyeCenterIndex].y;

                        fd.ulseeShape[i].x = (fd.ulseeShape[i].x - eyeCenterX) * EyeScaleFactor + eyeCenterX;
                        fd.ulseeShape[i].y = (fd.ulseeShape[i].y - eyeCenterY) * EyeScaleFactor + eyeCenterY;
                    }
                }
            }
        }

        static float ComputeSensetimeCenterDepth(ref FaceData fd, ref CameraFrame frame)
        {
            //얼굴 가로 크기에 따른 비율로 계산하자 3차원 상의 눈 거리를 기준으로 잡자
            return ComputeSensetimeInverseEyeDistance(ref fd, ref frame) * ZOffset;
        }

        static float ComputeSensetimeInverseEyeDistance(ref FaceData fd, ref CameraFrame frame)
        {
            float[] leftEyePos = new float[4];//x,y,z,w
            float[] rightEyePos = new float[4];
            float[] rotationMat3rdRow = new float[3];

            float pitch, yaw, roll;
            pitch = fd.pitch;
            yaw = fd.yaw;
            roll = frame.shouldLeftRightFlip ? -fd.roll : fd.roll;

            //face rotation mat
            Matrix4x4 rotationMat;
            rotationMat = Matrix4x4.Rotate(Quaternion.AngleAxis(pitch,Vector3.right));
            rotationMat = Matrix4x4.Rotate(Quaternion.AngleAxis(yaw,Vector3.up)) * rotationMat;
            rotationMat = Matrix4x4.Rotate(Quaternion.AngleAxis(roll,Vector3.forward)) * rotationMat;

            rotationMat = Matrix4x4.Inverse(rotationMat);

            //rotation inverse와 변환후의 점을 곱한 결과의 z값을 0으로 계산하자
            rotationMat3rdRow[0] = rotationMat.m20;
            rotationMat3rdRow[1] = rotationMat.m21;
            rotationMat3rdRow[2] = rotationMat.m22;

            //lEyeX * row0 + lEyeY * row1 + lEyeZ * row2 = 0
            //lEyeZ = -(lEyeX * row0 + lEyeY * row1) / row2
            //left eye, right eye는 중심좌표에 의해 이동된 좌표상에서 계산하자

            leftEyePos[0] = fd.keyPoints[52].x - (fd.keyPoints[46].x + fd.keyPoints[49].x) / 2.0f;
            leftEyePos[1] = fd.keyPoints[52].y - (fd.keyPoints[46].y + fd.keyPoints[49].y) / 2.0f;

            rightEyePos[0] = fd.keyPoints[61].x - (fd.keyPoints[46].x + fd.keyPoints[49].x) / 2.0f;
            rightEyePos[1] = fd.keyPoints[61].y - (fd.keyPoints[46].y + fd.keyPoints[49].y) / 2.0f;

            if (rotationMat3rdRow[2] != 0.0f)
            {
                leftEyePos[2] = -(rotationMat3rdRow[0] * leftEyePos[0] + rotationMat3rdRow[1] * leftEyePos[1]) / rotationMat3rdRow[2];
                rightEyePos[2] = -(rotationMat3rdRow[0] * rightEyePos[0] + rotationMat3rdRow[1] * rightEyePos[1]) / rotationMat3rdRow[2];
            }
            else
            {
                leftEyePos[2] = 0.0f; //delta계산시 같은 값으로 되어서 dz는 어차피 0이 된다
                rightEyePos[2] = 0.0f;
            }
            leftEyePos[3] = 1.0f;
            rightEyePos[3] = 1.0f;

            //계산된 z값을 이용하여 거리값 계산.
            float dx = leftEyePos[0] - rightEyePos[0];
            float dy = leftEyePos[1] - rightEyePos[1];
            float dz = leftEyePos[2] - rightEyePos[2];

            return Mathf.Sqrt(dx * dx + dy * dy + dz * dz) * InverseEyeDistanceScaleFactor;
        }

        static float ComputeScaleSenseToUls(ref FaceData fd, ref CameraFrame frame)
        {
            //face scale factor 계산 -> 3차원 상의 눈의 거리를 기준으로 uls값을 근사(sensetime scale factor)
            return ComputeSensetimeInverseEyeDistance(ref fd, ref frame) * ScaleFactor;
        }

        static void ComputeSensetimeCenterPoint(ref FaceData fd, ref CameraFrame frame, float faceScale)
        {

            fd.ulseeCX = (fd.keyPoints[46].x + fd.keyPoints[49].x) / 2.0f;
            fd.ulseeCY = (fd.keyPoints[46].y + fd.keyPoints[49].y) / 2.0f;
            //0,0,z,1의 좌표를 회전변환에 의해 변환한 후 delta를 center Position에 더한다
            //    float centerPos[4];// x,y,z,w
            Vector3 centerPos;
            bool reverse = false;
#if UNITY_ANDROID
            //Nexus6emd의 경우 compensatedCameraRotation가 뒤집어져 있기 때문에 위치를 변경 해야한다
            int compensatedCameraRotation = (int)frame.videoRotation;
            bool backReverted = (!frame.shouldLeftRightFlip && compensatedCameraRotation == 90);
            bool frontReverted = (frame.shouldLeftRightFlip && compensatedCameraRotation == 270);
            reverse = (backReverted || frontReverted);
#endif

            float pitch, yaw, roll;
            pitch = fd.pitch;
            yaw = fd.yaw;
            roll = frame.shouldLeftRightFlip ? -fd.roll : fd.roll;

            //face rotation mat
            Matrix4x4 rotationMat;
            rotationMat = Matrix4x4.Rotate(Quaternion.AngleAxis(frame.shouldLeftRightFlip?pitch:-pitch,Vector3.right));
            rotationMat = Matrix4x4.Rotate(Quaternion.AngleAxis(yaw,Vector3.up)) * rotationMat;
            rotationMat = Matrix4x4.Rotate(Quaternion.AngleAxis(roll,Vector3.forward)) * rotationMat;

            centerPos.x = 0;
            centerPos.y = 0;
            centerPos.z = ComputeSensetimeCenterDepth(ref fd, ref frame);//depth 잡는 기준을 해상도에 관계없이 가야함.

            centerPos = rotationMat.MultiplyPoint3x4(centerPos);

            float sensetimeAdjustFaceCenterX = (frame.shouldLeftRightFlip ? 1 : -1) * (reverse ? -1 : 1) * AdjustFaceCenterX;
            float sensetimeAdjustFaceCenterY = (frame.shouldLeftRightFlip ? 1 : -1) * (reverse ? -1 : 1) * AdjustFaceCenterY;

            switch ((int)frame.videoRotation)
            {
                case 0: //PORTRAIT_0:
                    fd.ulseeCX += (centerPos.x - sensetimeAdjustFaceCenterY * faceScale);
                    fd.ulseeCY += (frame.shouldLeftRightFlip ? 1 : -1) * (centerPos.y - sensetimeAdjustFaceCenterX * faceScale);
                    break;
                case 180: //PORTRAIT_180:
                    fd.ulseeCX += (centerPos.x + sensetimeAdjustFaceCenterY * faceScale);
                    fd.ulseeCY += (frame.shouldLeftRightFlip ? 1 : -1) * (centerPos.y + sensetimeAdjustFaceCenterX * faceScale);
                    break;
                case 270: //LANDSCAPE_270:
                    fd.ulseeCX += (centerPos.x - sensetimeAdjustFaceCenterX * faceScale);
                    fd.ulseeCY += (frame.shouldLeftRightFlip ? 1 : -1) * (centerPos.y + sensetimeAdjustFaceCenterY * faceScale);
                    break;
                case 90: //LANDSCAPE_90:
                    fd.ulseeCX += (centerPos.x + sensetimeAdjustFaceCenterX * faceScale);
                    fd.ulseeCY += (frame.shouldLeftRightFlip ? 1 : -1) * (centerPos.y - sensetimeAdjustFaceCenterY * faceScale);
                    break;
            }
        }



        static float GetNormalizedOrientation(float orientation)
        {
            orientation = orientation % 360.0f;
            if (orientation < 0)
            {
                orientation = 360.0f + orientation;
            }
            return orientation;
        }

        static float GetAngle(float x1, float y1, float x2, float y2)
        {
            float dx = x1 - x2;
            float dy = y1 - y2;
            float degree = Mathf.Rad2Deg * (Mathf.Atan2(dx, dy));

            if (degree < 0)
            {
                degree += 360;
            }
            return (float)degree;
        }


        static void ConvertSensetimeToUlsee(ref FaceData fd, ref CameraFrame frame)
        {
            Matrix4x4 sensetimeM = Matrix4x4.Scale(new Vector3(1.0f / frame.cameraTextureRef.width, 1.0f / frame.cameraTextureRef.height, 1.0f));

            int cameraRotation = (360 - (int)frame.videoRotation) % 360;

            sensetimeM = Matrix4x4.Translate(new Vector3(-0.5f, -0.5f, 0.0f)) * sensetimeM;
            if (frame.shouldLeftRightFlip)
            {
                sensetimeM = Matrix4x4.Scale(new Vector3(1.0f, -1.0f, 1.0f)) * sensetimeM;
            }

            sensetimeM = Matrix4x4.Rotate(Quaternion.AngleAxis(cameraRotation + 180.0f, Vector3.forward)) * sensetimeM;
            sensetimeM = Matrix4x4.Translate(new Vector3(0.5f, 0.5f, 0.0f)) * sensetimeM;

            sensetimeM = Matrix4x4.Scale(new Vector3(frame.cameraTextureRef.height, frame.cameraTextureRef.width, 1.0f)) * sensetimeM;

            ConvertSenseToUlsPointsLineFitting(ref fd,ref frame);

            fd.ulseePose[0] = -(fd.yaw * AdjustYawScaleFactor);
            fd.ulseePose[1] = (fd.pitch * AdjustPitchScaleFactor);
            fd.ulseePose[2] = -(fd.roll + AdjustRollGapFactor);

            fd.ulseePose[5] = ComputeScaleSenseToUls(ref fd, ref frame);
            ComputeSensetimeCenterPoint(ref fd, ref frame, fd.ulseePose[5]);
            fd.ulseePose[3] = fd.ulseeCX;
            fd.ulseePose[4] = fd.ulseeCY;

            fd.ulseeYaw = fd.ulseePose[0];
            fd.ulseePitch = fd.ulseePose[1];
            fd.ulseeRoll = fd.ulseePose[2];
            fd.ulseeFaceScale = fd.ulseePose[5];

            const int L_EYE_OUT = 36;
            const int R_EYE_OUT = 45;

            //-- preview space에서 계산해야 한다.따라서 dstPts에서 계산하면 안됨
            float x1 = fd.ulseeShape[L_EYE_OUT].x;
            float y1 = fd.ulseeShape[L_EYE_OUT].y;
            float x2 = fd.ulseeShape[R_EYE_OUT].x;
            float y2 = fd.ulseeShape[R_EYE_OUT].y;


            fd.legacyRoll = GetNormalizedOrientation((GetAngle(x1, y1, x2, y2)) +
                                                     ((int)frame.videoRotation + 90));


            fd.relativeRoll = GetNormalizedOrientation(frame.shouldLeftRightFlip ? -fd.legacyRoll : fd.legacyRoll);

            fd.relativeYaw = (frame.shouldLeftRightFlip ? 1 : -1) * fd.ulseePose[0];
            fd.relativePitch = fd.ulseePose[1];

            fd.compensatedPitch = (float)((int)frame.videoRotation + 90) + fd.relativePitch;
            fd.compensatedYaw = (float)((int)frame.videoRotation - 90) + fd.relativeYaw;
            fd.relativeYaw = -fd.relativeYaw;

#if UNITY_IOS || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            fd.compensatedYaw = frame.shouldLeftRightFlip ? -fd.compensatedYaw : fd.compensatedYaw;
#endif
            const int pointCount = ACConst.FACE_ULSEE_SIZE;

            Vector3 tempPt;
            for (int i = 0; i < pointCount; i++)
            {
                tempPt.x = fd.ulseeShape[i].x;
                tempPt.y = fd.ulseeShape[i].y;
                tempPt.z = 0.0f;

                tempPt = sensetimeM.MultiplyPoint3x4(tempPt);
                fd.ulseeShapePortrait[i].x = tempPt.x;
                fd.ulseeShapePortrait[i].y = tempPt.y;
            }

            fd.legacyRoll += AdjustRoll;

            //make 3d data
            Convert2DTo3D(ref fd, ref frame);

            //adjust mPose (for compute poseEX)
            fd.ulseePoseEx[5] = fd.ulseePose[5];

            //poseEx용 pitch조정..
            float tempPitch = fd.ulseePose[1];

#if UNITY_ANDROID
            bool reverse = false;
            //Nexus6emd의 경우 compensatedCameraRotation가 뒤집어져 있기 때문에 위치를 변경 해야한다
            int compensatedCameraRotation = (int)frame.videoRotation;
            bool backReverted = (!frame.shouldLeftRightFlip && compensatedCameraRotation == 90);
            bool frontReverted = (frame.shouldLeftRightFlip && compensatedCameraRotation == 270);
            reverse = (backReverted || frontReverted);
#endif

            //
            // if (DebugConfig::instance().enableFaceRecalculate && EngineStatus::instance().getCountValue(kuru::EngineStatus::CountType::ACCURATE_FACE_POSE) > 0)
            // {
            //     recalculateFaceTransform();
            // }
            // else
            // {
            fd.ulseePoseEx[0] = tempPitch;
            fd.ulseePoseEx[1] = (frame.shouldLeftRightFlip ? 1 : -1) * fd.ulseePose[0];
            fd.ulseePoseEx[2] = (frame.shouldLeftRightFlip ? 1 : -1) * fd.ulseePose[2] - (int)frame.videoRotation;


            fd.ulseePoseEx[3] = frame.cameraTextureRef.height - fd.ulseePose[4];
            fd.ulseePoseEx[4] = frame.shouldLeftRightFlip ? frame.cameraTextureRef.width - fd.ulseePose[3]
                                               : fd.ulseePose[3];

            //}

#if UNITY_ANDROID
            if (reverse)
            {
                fd.ulseePoseEx[3] = frame.cameraTextureRef.height - fd.ulseePoseEx[3];
                fd.ulseePoseEx[4] = frame.cameraTextureRef.width - fd.ulseePoseEx[4];
            }
#endif
        }

        static void Convert2DTo3D(ref FaceData fd, ref CameraFrame frame)
        {
            Matrix4x4 mat;
            mat = Matrix4x4.Rotate(Quaternion.AngleAxis(fd.compensatedPitch,Vector3.right));
            mat = Matrix4x4.Rotate(Quaternion.AngleAxis(fd.compensatedYaw,Vector3.up)) * mat;
            mat = Matrix4x4.Rotate(Quaternion.AngleAxis(fd.legacyRoll,Vector3.forward)) * mat;
            
            float focal = 15500.0f;
            Matrix4x4 diag = Matrix4x4.identity;
            diag.m00 = focal / (frame.cameraTextureRef.height / 2.0f);
            diag.m11 = focal / (frame.cameraTextureRef.width / 2.0f);

            mat = diag * mat;
 
            float scale = focal / fd.ulseePose[5];
            float[] trans = new float[3];
            trans[0] = scale * (fd.ulseePose[4] - frame.cameraTextureRef.height / 2.0f) / (frame.cameraTextureRef.height / 2.0f);
            trans[1] = scale * (fd.ulseePose[3] - frame.cameraTextureRef.width / 2.0f) / (frame.cameraTextureRef.width / 2.0f);
            trans[2] = scale;

            Matrix4x4 matrix = Matrix4x4.identity;
            if (frame.shouldLeftRightFlip)
            {
                matrix = Matrix4x4.Scale(new Vector3(-1.0f, 1.0f, 1.0f)) * matrix;
            }
            matrix = Matrix4x4.Rotate(Quaternion.AngleAxis((float)((int)frame.videoRotation - 90), Vector3.forward))*matrix;
         
            Matrix4x4 invMat = matrix.inverse;
            Vector3 tempPt;
            float[] xyConvert = new float[2];

            Matrix4x4 invertM = mat.inverse;
            Vector3 vecConvert;
            Vector3 matRowVector;

            Vector3 xyzw;

            const int SHAPE_SIZE = ACConst.FACE_ULSEE_SIZE;

            for (int index = 0; index < SHAPE_SIZE; index++)
            {

                float xx = fd.ulseeShapePortrait[index].x;
                float yy = fd.ulseeShapePortrait[index].y;
                float baseX = ulsValues[index * 3];
                float baseY = ulsValues[index * 3 + 1];
                float baseZ = ulsValues[index * 3 + 2];

                //to xy
                xx = 1.0f - 2.0f * xx / frame.cameraTextureRef.height;
                yy = -1.0f + 2.0f * yy / frame.cameraTextureRef.width;

                //to vec xyz..
                tempPt.x = xx;
                tempPt.y = yy;
                tempPt.z = 0;
                tempPt = invMat.MultiplyPoint3x4(tempPt);

                xyConvert[0] = tempPt.x;
                xyConvert[1] = tempPt.y;

                vecConvert.x = baseX;
                vecConvert.y = baseY;
                vecConvert.z = baseZ;

                matRowVector.x = mat.m20;
                matRowVector.y = mat.m21;
                matRowVector.z = mat.m22;

                float zConvert = Vector3.Dot(vecConvert, matRowVector) + trans[2];
                xx = xyConvert[0];
                yy = xyConvert[1];
                xx = (xx * zConvert) - trans[0];
                yy = (yy * zConvert) - trans[1];

                //inverse
                xyzw.x = xx;
                xyzw.y = yy;
                xyzw.z = Vector3.Dot(vecConvert, matRowVector);

                xyzw = invertM.MultiplyPoint3x4(xyzw);

                fd.ulseeShape3d[index].x = xyzw.x;
                fd.ulseeShape3d[index].y = xyzw.y;
                fd.ulseeShape3d[index].z = xyzw.z;
            }
        }

        static Matrix4x4 getPointToVertexMatrix(ref CameraFrame frame)
        {
            Matrix4x4 m;
            m = Matrix4x4.Scale(new Vector3(1.0f / frame.cameraTextureRef.width, 1.0f / frame.cameraTextureRef.height, 1.0f));

            m = Matrix4x4.Translate(new Vector3(-0.5f, -0.5f, 0.0f)) * m;
            if (frame.shouldLeftRightFlip)
            {
                m = Matrix4x4.Scale(new Vector3(1.0f, -1.0f, 1.0f)) * m;
            }

            Quaternion rotQ;
            if (frame.videoRotation == 0 && !frame.shouldLeftRightFlip)
            {
                rotQ = Quaternion.AngleAxis(180.0f, Vector3.forward);
                m = Matrix4x4.Rotate(rotQ);
            }

            rotQ = Quaternion.AngleAxis((float) frame.videoRotation, Vector3.forward);
            m = Matrix4x4.Rotate(rotQ) * m;

            m = Matrix4x4.Scale(new Vector3(-frame.virtualWidth, -frame.virtualHeight, 1.0f)) * m;

            return m;
        }

        static Matrix4x4 getPointToNormalizedVertexMatrix(ref CameraFrame frame)
        {
            Matrix4x4 m;
            m = Matrix4x4.Scale(new Vector3(1.0f / frame.cameraTextureRef.width, 1.0f / frame.cameraTextureRef.height, 1.0f));

            m = Matrix4x4.Translate(new Vector3(-0.5f, -0.5f, 0.0f)) * m;
            if (frame.shouldLeftRightFlip)
            {
                m = Matrix4x4.Scale(new Vector3(1.0f, -1.0f, 1.0f)) * m;
            }

            Quaternion rotQ;
            if (frame.videoRotation == 0 && !frame.shouldLeftRightFlip)
            {
                rotQ = Quaternion.AngleAxis(180.0f, Vector3.forward);
                m = Matrix4x4.Rotate(rotQ);
            }

            rotQ = Quaternion.AngleAxis((float) frame.videoRotation, Vector3.forward);
            m = Matrix4x4.Rotate(rotQ) * m;

#if UNITY_EDITOR
            float scale = frame.virtualWidth * (float)frame.renderResolution.y / frame.virtualHeight/(float)frame.renderResolution.x;
#else
            float scale = 1.0f; 
#endif

            m = Matrix4x4.Scale(new Vector3(-2.0f * scale, -2.0f, 1.0f)) * m;

            return m;
        }

        static void BuildVertex(ref FaceData fd, ref CameraFrame frame)
        {
            Matrix4x4 m = getPointToVertexMatrix(ref frame);
            Matrix4x4 m_normal = getPointToNormalizedVertexMatrix(ref frame);

            for (int i = 0; i < ACConst.FACE_SENSETIME_SIZE; ++i)
            {
                Vector3 src, dst;
                src = new Vector3(fd.keyPoints[i].x, fd.keyPoints[i].y, 1.0f);
                dst = m.MultiplyPoint3x4(src);
                fd.verticesArray[i].x = dst.x;
                fd.verticesArray[i].y = dst.y;
            }

            for(int i = 0; i < ACConst.FACE_ULSEE_SIZE; ++i)
            {
                Vector3 src, dst;
                src = new Vector3(fd.ulseeShape[i].x, fd.ulseeShape[i].y, 1.0f);
                dst = m.MultiplyPoint3x4(src);
                fd.ulseeVertexShape[i].x = dst.x;
                fd.ulseeVertexShape[i].y = dst.y;

                dst = m_normal.MultiplyPoint3x4(src);
                fd.ulseeNormalizedVertexShape[i].x = dst.x;
                fd.ulseeNormalizedVertexShape[i].y = dst.y;
            }
            //fd.build(frame);
            FlipIdx(ref fd, ref frame);
        }


        static void swapPt<T>(ref T lhs,ref T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }
        static void FlipIdx(ref FaceData fd, ref CameraFrame frame)
        {
            // unity 에서는 idx가 camera와 반대로 flip
            if (!frame.shouldLeftRightFlip) {
                for (int i = 0; i < SENSETIME_FACE_FLIP_IDX.Length; i += 2) {
                    swapPt<Vector2>(ref fd.verticesArray[SENSETIME_FACE_FLIP_IDX[i]],ref fd.verticesArray[SENSETIME_FACE_FLIP_IDX[i+1]]);
                }
            } else {
                for (int i = 0; i < ULSEE_FACE_FLIP_IDX.Length; i += 2) {
                    swapPt<Vector2>(ref fd.ulseeVertexShape[ULSEE_FACE_FLIP_IDX[i]],ref fd.ulseeVertexShape[ULSEE_FACE_FLIP_IDX[i + 1]]);
                    swapPt<Vector2>(ref fd.ulseeNormalizedVertexShape[ULSEE_FACE_FLIP_IDX[i]],ref fd.ulseeNormalizedVertexShape[ULSEE_FACE_FLIP_IDX[i + 1]]);
                }
            }
        }


        static readonly byte[] ULSEE_FACE_FLIP_IDX = {
                26, 17,
                25, 18,
                24, 19,
                23, 20,
                22, 21,

                42, 39,
                43, 38,
                44, 37,
                45, 36,
                46, 41,
                47, 40,

                34, 32,
                35, 31,

                52, 50,
                53, 49,
                54, 48,
                55, 59,
                56, 58,

                62, 60,
                63, 65,

                9, 7,
                10, 6,
                11, 5,
                12, 4,
                13, 3,
                14, 2,
                15, 1,
                16, 0
        };
        static readonly byte[] SENSETIME_FACE_FLIP_IDX = {
                //-- left, right
                //-- brow
                33, 42,
                34, 41,
                35, 40,
                36, 39,
                37, 38,

                67, 68,
                66, 69,
                65, 70,
                64, 71,

                //-- eye
                78, 79,
                55, 58,
                56, 63,
                57, 62,
                52, 61,
                53, 60,
                54, 59,
                72, 75,
                73, 76,
                74, 77,

                104, 105,

                //-- nose
                80, 81,
                82, 83,
                47, 51,
                48, 50,

                //-- lip
                84, 90,
                85, 89,
                86, 88,
                96, 100,

                97, 99,
                103, 101,

                95, 91,
                94, 92,

                //-- chin
                0, 32,
                1, 31,
                2, 30,
                3, 29,
                4, 28,
                5, 27,
                6, 26,
                7, 25,
                8, 24,
                9, 23,
                10, 22,
                11, 21,
                12, 20,
                13, 19,
                14, 18,
                15, 17,
        };
    }
}