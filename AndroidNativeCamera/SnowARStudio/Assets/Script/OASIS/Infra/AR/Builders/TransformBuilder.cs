﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS.AR
{
    using Data;

    public static class TransformBuilder
    {
        public static void Build(CameraFrame frame, ref FaceData fd)
        {

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            float cy = ((float) fd.center.y) / ((float) frame.cameraTextureRef.width) - 0.5f;
            float cx = ((float) fd.center.x) / ((float) frame.cameraTextureRef.height) - 0.5f;
#else
            float cy = ((float) fd.center.x) / ((float) frame.cameraTextureRef.height) - 0.5f;
            float cx = ((float) fd.center.y) / ((float) frame.cameraTextureRef.width) - 0.5f;
#endif

            float w = frame.cameraTextureRef.height * 0.75f;
            float scale = (140.0f / (480.0f / w)) / (fd.scale / 5.0f) * Mathf.Tan(45.0f / 2.0f * Mathf.Deg2Rad) / Mathf.Tan(frame.fov / 2.0f * Mathf.Deg2Rad);

            float aspect = (float) frame.cameraTextureRef.width / (float) frame.cameraTextureRef.height;

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            scale *= aspect;
            cx = cx / aspect;
#else
            cx = cx * aspect;
            cy = -cy;
#endif

            float cdepth = 0.5f / Mathf.Tan(frame.fov / 2.0f * Mathf.Deg2Rad);

            float epAngleYaw = Mathf.Atan2(cx, cdepth) * Mathf.Rad2Deg;
            float epAnglePitch = Mathf.Atan2(cy, cdepth) * Mathf.Rad2Deg;

            cy = cy / cdepth * scale;
            cx = cx / cdepth * scale;

            fd.faceLocation = new Vector3(cx, cy, scale);
            
            Quaternion qunity = Quaternion.AngleAxis(180, Vector2.right);
            Quaternion qx = Quaternion.AngleAxis(-fd.pitch + epAnglePitch, Vector3.right);
            Quaternion qy = Quaternion.AngleAxis(-fd.yaw - epAngleYaw, Vector3.up);
            Quaternion qz = Quaternion.AngleAxis(frame.shouldLeftRightFlip?fd.roll:fd.roll + 180.0f, Vector3.forward);
            Quaternion rot = qz * qy * qx * qunity;
            fd.faceRotation = rot;
        }
        public static void MakeFeaturePoints(ref FaceData fd)
        {
            for (int i = 0; i < ACConst.FACE_ULSEE_SIZE; ++i)
            {
                fd.featurePositions[i].x = fd.ulseeVertexShape[i].x;
                fd.featurePositions[i].y = fd.ulseeVertexShape[i].y;
                fd.featurePositions[i].z = 0.0f;
            }
        }
    }

}