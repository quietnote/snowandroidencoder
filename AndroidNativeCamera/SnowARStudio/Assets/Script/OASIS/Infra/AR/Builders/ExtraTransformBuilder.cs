﻿using UnityEngine;
using System.Collections.Generic;

namespace OASIS.AR
{
    using Data;
    public class ExtraTransformBuilder
    {
        static SkinEx skinEx = new SkinEx();

        public static void Build(ref FaceData fd, float aspectRatio, float scale)
        {
            skinEx.scale(scale);
            Build(ref fd, aspectRatio);
        }

        static void Build(ref FaceData fd, float aspectRatio)
        {
            System.Array.ConstrainedCopy(fd.featurePositions, 0, fd.extraFeaturePositions, 0, fd.featurePositions.Length);

            Vector3 eyeTangentUnit = fd.extraFeaturePositions[36] - fd.extraFeaturePositions[45];
            eyeTangentUnit.y *= aspectRatio;
            eyeTangentUnit = Rotate(eyeTangentUnit, 90f);
            eyeTangentUnit.Normalize();
            Vector3 eyeTangentUnitWithRotation = eyeTangentUnit;
            eyeTangentUnitWithRotation = Rotate(eyeTangentUnitWithRotation, fd.relativeYaw * skinEx._yawRatio);
            eyeTangentUnitWithRotation.Normalize();

            for (int itemIndex = 0; itemIndex < featureTable.Length; ++itemIndex) {
                Item s = featureTable[itemIndex];
                Vector3 trVec = fd.extraFeaturePositions[s.p._begin] - fd.extraFeaturePositions[s.p._end];
                trVec.y *= aspectRatio;
                Vector3 unit;
                if (s.p._useEyeTangent) {
                    unit = eyeTangentUnit;
                    if (s.p._applyRotation) {
                        unit = eyeTangentUnitWithRotation;
                    }
                } else {
                    unit = trVec;
                    unit.Normalize();
                }

                float extraScale = (s.p._faceScale ? skinEx._scale - 1f : 0);
                if (s.p._vec2Weight > 0 && extraScale > 0) {
                    Vector3 unit2 = fd.extraFeaturePositions[s.p._begin2] - fd.extraFeaturePositions[s.p._end2];
                    unit2.y *= aspectRatio;
                    unit2.Normalize();
                    unit2 = Vector3.Scale(unit2, Vector3.one * vec2Weight * Mathf.Min(extraScale, 1f));
                    unit += unit2;
                    unit.Normalize();
                }

                float scale = s.p._basicScale;
                if (skinEx._extendForSkinSmooth) {
                    switch (s.p._skinSmoothExtension) {
                        case Property.SkinSmoothExtension.NOTHING:
                            scale += s.p._extraScale * extraScale;
                            break;
                        case Property.SkinSmoothExtension.EXTEND:
                            scale += s.p._extraScale * extraScale + 0.5f;
                            break;
                        case Property.SkinSmoothExtension.SHRINK:
                            scale += s.p._extraScale * extraScale * 0.5f;
                            break;
                    }
                } else {
                    scale += s.p._extraScale * extraScale;
                }
                scale *= trVec.magnitude;
                fd.extraFeaturePositions[s.targetIdx].x = fd.extraFeaturePositions[s.refIdx].x + unit.x * scale;
                fd.extraFeaturePositions[s.targetIdx].y = fd.extraFeaturePositions[s.refIdx].y + unit.y * scale / aspectRatio;
            }

            // if (mouthClosed && skinEx._forceToCloseMouth)
            // {
            //     /**
            //      *  	mouth point 처리
            //      * 		62--61--60
            //      * 		63--64--65
            //      */
            //     for (int idx = 60; idx <= 62; idx++)
            //     {
            //         int pairIdx = 65 - idx + 60;
            //         Vector3 point = fd.extraFeaturePositions[idx];
            //         Vector3 pairPoint = fd.extraFeaturePositions[pairIdx];
            //         fd.extraFeaturePositions[pairIdx] = fd.extraFeaturePositions[idx] = (point + pairPoint) / 2f;
            //     }
            // }
        }

        static Vector3 Rotate(Vector3 v, float degree)
        {
            return Quaternion.Euler(0f, 0f, degree) * v;;
        }

        static Property getBrowBasic() {
            return new Property().begin(27).end(30).extraScale(1.2f).useEyeTangent(true);
        }

        const float vec2Weight = 0.12f;

        static Item[] featureTable = {
            //-- brow center
            new Item(66, 27, getBrowBasic().basicScale(1.0f).faceScale(false)),

            //-- brow left
            new Item(67, 16, getBrowBasic().basicScale(0.6f).faceScale(false)),
            new Item(68, 26, getBrowBasic().basicScale(0.7f).faceScale(false)),
            new Item(69, 25, getBrowBasic().basicScale(0.7f).faceScale(false)),
            new Item(70, 24, getBrowBasic().basicScale(0.8f).faceScale(false)),
            new Item(71, 23, getBrowBasic().basicScale(1f).faceScale(false)),
            new Item(72, 22, getBrowBasic().basicScale(1.2f).faceScale(false)),

            //-- brow right
            new Item(78, 0, getBrowBasic().basicScale(0.6f).faceScale(false)),
            new Item(77, 17, getBrowBasic().basicScale(0.7f).faceScale(false)),
            new Item(76, 18, getBrowBasic().basicScale(0.7f).faceScale(false)),
            new Item(75, 19, getBrowBasic().basicScale(0.8f).faceScale(false)),
            new Item(74, 20, getBrowBasic().basicScale(1f).faceScale(false)),
            new Item(73, 21, getBrowBasic().basicScale(1.2f).faceScale(false)),

            //-- bottom out left
            new Item(79, 0, new Property().begin(0).end(27).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(80, 1, new Property().begin(1).end(28).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(81, 2, new Property().begin(2).end(29).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(82, 3, new Property().begin(3).end(30).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(83, 4, new Property().begin(4).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(84, 5, new Property().begin(5).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),
            new Item(85, 6, new Property().begin(6).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),
            new Item(86, 7, new Property().begin(7).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),

            //-- bottom out center
            new Item(87, 8, new Property().begin(8).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),

            //-- bottom out right
            new Item(95, 16, new Property().begin(16).end(27).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(94, 15, new Property().begin(15).end(28).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(93, 14, new Property().begin(14).end(29).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(92, 13, new Property().begin(13).end(30).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(91, 12, new Property().begin(12).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.SHRINK)),
            new Item(90, 11, new Property().begin(11).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),
            new Item(89, 10, new Property().begin(10).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),
            new Item(88, 9, new Property().begin(9).end(33).extraScale(0.5f).applySkinSmoothExtension(Property.SkinSmoothExtension.EXTEND)),

            //-- brow out left
            new Item(96, 16, getBrowBasic().basicScale(0.6f).vec2Weight(vec2Weight).begin2(45).end2(36)),
            new Item(97, 26, getBrowBasic().basicScale(0.7f).applyRotation(true)),
            new Item(98, 25, getBrowBasic().basicScale(0.7f).applyRotation(true)),
            new Item(99, 24, getBrowBasic().basicScale(0.8f).applyRotation(true)),
            new Item(100, 23, getBrowBasic().basicScale(1f).applyRotation(true)),
            new Item(101, 22, getBrowBasic().basicScale(1.2f).applyRotation(true)),

            //-- brow out right
            new Item(107, 0, getBrowBasic().basicScale(0.6f).vec2Weight(vec2Weight).begin2(36).end2(45)),
            new Item(106, 17, getBrowBasic().basicScale(0.7f).applyRotation(true)),
            new Item(105, 18, getBrowBasic().basicScale(0.7f).applyRotation(true)),
            new Item(104, 19, getBrowBasic().basicScale(0.8f).applyRotation(true)),
            new Item(103, 20, getBrowBasic().basicScale(1f).applyRotation(true)),
            new Item(102, 21, getBrowBasic().basicScale(1.2f).applyRotation(true)),
        };

        class Item {
            public int targetIdx = 0;
            public int refIdx = 0;
            public Property p;

            public Item(int targetIdx, int refIdx, Property p) {
                this.targetIdx = targetIdx;
                this.refIdx = refIdx;
                this.p = p;
            }
        }

        class Property
        {
            public enum SkinSmoothExtension : int
            {
                NOTHING = 0,
                EXTEND = 1,
                SHRINK = 2
            };
            public bool _useEyeTangent = false;
            public bool _applyRotation = false;
            public int _begin = 0;
            public int _end = 0;
            public int _begin2 = 0;
            public int _end2 = 0;
            public float _vec2Weight = 0;
            public float _basicScale = 0;
            public float _extraScale = 1;
            public bool _faceScale = true;
            public SkinSmoothExtension _skinSmoothExtension = SkinSmoothExtension.NOTHING;

            public Property useEyeTangent(bool v)
            {
                _useEyeTangent = v;
                return this;
            }

            public Property applyRotation(bool v)
            {
                _applyRotation = v;
                return this;
            }

            public Property applySkinSmoothExtension(SkinSmoothExtension v)
            {
                _skinSmoothExtension = v;
                return this;
            }

            public Property begin(int v)
            {
                _begin = v;
                return this;
            }

            public Property end(int v)
            {
                _end = v;
                return this;
            }

            public Property begin2(int v)
            {
                _begin2 = v;
                return this;
            }

            public Property end2(int v)
            {
                _end2 = v;
                return this;
            }

            public Property vec2Weight(float v)
            {
                _vec2Weight = v;
                return this;
            }

            public Property basicScale(float v)
            {
                _basicScale = v;
                return this;
            }

            public Property extraScale(float v)
            {
                _extraScale = v;
                return this;
            }

            public Property faceScale(bool v)
            {
                _faceScale = v;
                return this;
            }
        }

        public class SkinEx
        {
            public SkinEx scale(float v) { _scale = v; return this; }
            public SkinEx yawRatio(float v) { _yawRatio = v; return this; }
            public SkinEx fillInMouth(bool v) { _fillInMouth = v; return this; }
            public SkinEx extendForSkinSmooth(bool v) { _extendForSkinSmooth = v; return this; }
            public SkinEx forceToCloseMouth(bool v) { _forceToCloseMouth = v; return this; }

            public bool _fillInMouth = true;
            public float _scale = 2.5f;
            public float _yawRatio = 0.5f;
            public bool _extendForSkinSmooth = false;
            public bool _forceToCloseMouth = true;
        }
    }
}
