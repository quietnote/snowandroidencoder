﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;
using UnityEngine.Rendering;
using System.Text;
using System.IO;
using UnityEngine.Networking;

namespace OASIS.ST
{
    public class SenseTimeLib : IDisposable
    {
#if (UNITY_IPHONE || UNITY_WEBGL) && !UNITY_EDITOR
        const string DLL = "__Internal";
#else
        const string DLL = "SnowSenseTime";
#endif

        // SenseTime defined types

        private enum StResult
        {
            kOK = 0,
            kInvalidArg = -1,
            kHandle = -2,
            kOutOfMemory = -3,
            kFail = -4,
            kDelNotFound = -5,
            kInvalidPixelFormat = -6,
            kFileNotFound = -7,
            kInvalidFileFormat = -8,
            kFileExpire = -9,

            kInvalidAuth = -13,
            kInvalidAppID = -14,
            kAuthExpire = -15,
            kUuidMismatch = -16,
            kOnlineAuthConnectFail = -17,
            kOnlineAuthTimeout = -18,
            kOnlineAuthInvalid = -19,
            kLicenseIsNotActivatable = -20,
            kActivationFail = -21,
            kActivationCodeInvalid = -22,
            kNoCapability = -23,
            kPlatformNotSupported = -24,
            kSubmodelNotExist = -26,
            kOnlineActivateNoNeed = -27,
            kOnlineActivateFail = -28,
            kOnlineActivateCodeInvalid = -29,
            kOnlineActivateConnectFail = -30,

            kModelNotInMemory = -31
        }

        public enum PixelFormat
        {
            kGRAY8,
            kYUV420P,
            kNV12,
            kNV21,
            kBGRA8888,
            kBGR888,
            kRGBA8888,
            kRGB888
        }

        public enum RotateCommand
        {
            kClockwiseRotate0 = 0,
            kClockwiseRotate90 = 1,
            kClockwiseRotate180 = 2,
            kClockwiseRotate270 = 3
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct StRect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        };

        [StructLayout(LayoutKind.Sequential)]
        private struct StImage
        {
            public IntPtr data;
            public PixelFormat format;
            public int width;
            public int height;
            public int stride;
            public double timeStamp;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct StIMU
        {
            public double accelerationX;
            public double accelerationY;
            public double accelerationZ;
            public double gyroScopeX;
            public double gyroScopeY;
            public double gyroScopeZ;
            public double timeStamp;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct StAttitude
        {
            public double quaternionX;
            public double quaternionY;
            public double quaternionZ;
            public double quaternionW;
            public double gravityX;
            public double gravityY;
            public double gravityZ;
            public double timeStamp;
        }

        // Our natively defined types

        private const int FACE_FEATURE_POINTS_ARRAY_SIZE = 106;
        private const int FACE_EXTRA_POINTS_ARRAY_MAX_SIZE = 134;
        private const int IRIS_CENTER_POINTS_ARRAY_MAX_SIZE = 2;
        private const int IRIS_CONTOUR_POINTS_ARRAY_MAX_SIZE = 38;
        private const int HAND_KEY_POINTS_ARRAY_MAX_SIZE = 20;
        private const int BODY_KEYPOINTS_ARRAY_MAX_SIZE = 14;
        private const int BODY_CONTOUR_ARRAY_MAX_SIZE = 59;

        private enum DetectType
        {
            kFace,
            kFaceExtra,
            kEyeballCenter,
            kEyeballContour,
            kHairSegment,
            kHand,
            kBodyKeyPoints,
            kBodyContour,
            kBackgroundSegment,
        }

        public enum FaceAction
        {
            kEyeBlink,
            kMouthAh,
            kHeadYaw,
            kHeadPitch,
            kBrowJump,
            kCount
        };

        public enum HandAction
        {
            kOK,
            kScissor,
            kGood,
            kPalm,
            kPistol,
            kLove,
            kHoldup,
            kCongratulate,
            kFingerHeart,
            kFingerIndex,
            kFist,
            kCount
        };

        private enum HeadExpression
        {
            kHeadNormal,
            kSideFaceLeft,
            kSideFaceRight,
            kTiltedFaceLeft,
            kTiltedFaceRight,
            kHeadRise,
            kHeadLower,
        };

        private enum EyeExpression
        {
            kTwoEyeClose,
            kTwoEyeOpen,
            kLeftEyeOpenRightEyeClose,
            kLeftEyeCloseRightEyeOpen,
        };

        private enum MouthExpression
        {
            kMouthOpen,
            kMouthClose,
            kLipsUpward,
            kLipsPouted,
            kLipsCurlLeft,
            kLipsCurlRight,
        };

        // bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible. either 1 (true) or 0 (false).
        [StructLayout(LayoutKind.Sequential)]
        public struct FaceActionData
        {
            public byte eyeBlink;
            public byte mouthAh;
            public byte headYaw;
            public byte headPitch;
            public byte browJump;

            public bool GetState(FaceAction action)
            {
                switch (action)
                {
                    case FaceAction.kEyeBlink:
                        return eyeBlink != 0;

                    case FaceAction.kMouthAh:
                        return mouthAh != 0;

                    case FaceAction.kHeadYaw:
                        return headYaw != 0;

                    case FaceAction.kHeadPitch:
                        return headPitch != 0;

                    case FaceAction.kBrowJump:
                        return browJump != 0;
                }

                return false;
            }
        };

        // bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible. either 1 (true) or 0 (false).
        public struct HandActionData
        {
            public byte ok;
            public byte scissor;
            public byte good;
            public byte palm;
            public byte pistol;
            public byte love;
            public byte holdUp;
            public byte congratulate;
            public byte fingerHeart;
            public byte fingerIndex;
            public byte fist;

            public bool GetState(HandAction action)
            {
                switch (action)
                {
                    case HandAction.kOK:
                        return ok != 0;

                    case HandAction.kScissor:
                        return scissor != 0;

                    case HandAction.kGood:
                        return good != 0;

                    case HandAction.kPalm:
                        return palm != 0;

                    case HandAction.kPistol:
                        return pistol != 0;

                    case HandAction.kLove:
                        return love != 0;

                    case HandAction.kHoldup:
                        return holdUp != 0;

                    case HandAction.kCongratulate:
                        return congratulate != 0;

                    case HandAction.kFingerHeart:
                        return fingerHeart != 0;

                    case HandAction.kFingerIndex:
                        return fingerIndex != 0;

                    case HandAction.kFist:
                        return fist != 0;
                }

                return false;
            }
        };

        // bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible. either 1 (true) or 0 (false).
        [StructLayout(LayoutKind.Sequential)]
        public struct HeadExpressionData
        {
            public byte headNormal;
            public byte sideFaceLeft;
            public byte sideFaceRight;
            public byte tiltedFaceLeft;
            public byte tiltedFaceRight;
            public byte headRise;
            public byte headLower;
        };

        // bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible. either 1 (true) or 0 (false).
        [StructLayout(LayoutKind.Sequential)]
        public struct EyeExpressionData
        {
            public byte twoEyeClose;
            public byte twoEyeOpen;
            public byte leftEyeOpenRightEyeClose;
            public byte leftEyeCloseRightEyeOpen;
        };

        // bool is not blittable type. We use byte (unsigned char) to keep marshaling as simple as possible. either 1 (true) or 0 (false).
        [StructLayout(LayoutKind.Sequential)]
        public struct MouthExpressionData
        {
            public byte mouthOpen;
            public byte mouthClose;
            public byte lipsUpward;
            public byte lipsPouted;
            public byte lipsCurlLeft;
            public byte lipsCurlRight;
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct ExpressionData
        {
            public HeadExpressionData head;
            public EyeExpressionData eye;
            public MouthExpressionData mouth;
        };

        // Our native API

#if UNITY_ANDROID && !UNITY_EDITOR
        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GenerateActivationCode(IntPtr context, IntPtr licenseBuffer, int licenseBufferSize, IntPtr activationCodeBuffer, ref int activationCodeBufferSize);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_CheckActivationCode(IntPtr context, IntPtr licenseBuffer, int licenseBufferSize, IntPtr activationCodeBuffer, int activationCodeBufferSize);
#else
        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GenerateActivationCode(IntPtr licenseBuffer, int licenseBufferSize, IntPtr activationCodeBuffer, ref int activationCodeBufferSize);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_CheckActivationCode(IntPtr licenseBuffer, int licenseBufferSize, IntPtr activationCodeBuffer, int activationCodeBufferSize);

#endif
        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_InitHumanAction();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_DestroyHumanAction();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_LoadHumanActionModel(IntPtr buffer, int bufferSize);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_UnloadHumanActionModel(DetectType type);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetDetectionMask(DetectType type, byte setToOn);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetFaceActionMask(FaceAction faceAction, byte setToOn);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetFaceActionThreshold(FaceAction faceAction, float threshold);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHandActionMask(HandAction handAction, byte setToOn);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHandActionThreshold(HandAction handAction, float threshold);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetExpressionDetect(byte turnOn);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHeadExpressionThreshold(HeadExpression headExpression, float threshold);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetEyeExpressionThreshold(EyeExpression eyeExpression, float threshold);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetMouthExpressionThreshold(MouthExpression mouthExpression, float threshold);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetSmoothThreshold(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHeadposeThreshold(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetFaceLimit(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetFaceDetectInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetFaceProcessInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHandLimit(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHandDetectInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHandProcessInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetBodyLimit(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetBodyDetectInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetBodyProcessInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetBackgroundMaxSize(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetBackgroundBlurStrength(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetBackgroundProcessInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHairMaxSize(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHairBlurStrength(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_SetHairProcessInterval(float value);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_UpdateHumanAction(
            IntPtr image, PixelFormat pixelFormat, int width, int height, int stride, RotateCommand orientation);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_ResetHumanAction();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int SnowSenseTime_GetFaceCount();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetFaceData(int index,
            ref int ID, ref float score, ref StRect rect, 
            Vector2[] keyPointsArray, float[] keyPointsVisibilityArray,
            ref float yaw, ref float pitch, ref float roll,
            ref int validExtraPointsCount, Vector2[] extraPointsArray,
            ref int validIrisCenterPointsCount, Vector2[] irisCenterPointsArray,
            ref int validIrisContourPointsCount, Vector2[] irisContourPointsArray,
            ref FaceActionData faceActionData);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int SnowSenseTime_GetHandCount();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetHandData(int index,
            ref int ID, ref float score, ref StRect rect,
            ref int validKeyPointsCount, Vector2[] keyPointsArray,
            ref HandActionData handActionData);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int SnowSenseTime_GetBodyCount();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetBodyData(int index,
            ref int ID,
            ref int validKeyPointsCount, Vector2[] keyPointsArray, float[] keyPointsScoreArray,
            ref int validContourPointsCount, Vector2[] contourPointsArray, float[] contourPointsScoreArray);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetExpressionData(ref ExpressionData expressionToFill);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int SnowSenseTime_GetBackgroundSegmentationBufferSize();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetBackgroundSegmentationBuffer(IntPtr buffer, int bufferSize, ref int width, ref int height, ref int stride);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern int SnowSenseTime_GetHairSegmentationBufferSize();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetHairSegmentationBuffer(IntPtr buffer, int bufferSize, ref int width, ref int height, ref int stride);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_CreateSLAMSession(int width, int height, float fovx, RotateCommand toRotateBy);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_DestroySLAMSession();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_UpdateSLAMSession(
                        StImage image,
                        StIMU[] IMUArray, int IMUArraySize,
                        StAttitude attitude);

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_ResetSLAM();

        [DllImport(DLL, CallingConvention = CallingConvention.Cdecl)]
        private static extern StResult SnowSenseTime_GetCameraPose(ref Quaternion rotation, ref Vector3 center);

        // Nested class/struct definitions

        [Serializable]
        public class Config
        {
            [Serializable]
            public struct FaceFeatureFlags
            {
                public bool useFace; // 106 points
                public bool useFaceExtra; // 240 points (106+134)
                public bool useEyeballCenter;
                public bool useEyeballContour;
            }

            [Serializable]
            public struct FaceTriggerFlags
            {
                public bool useEyeBlink;
                public bool useMouthOpen;
                public bool useHeadNod;
                public bool useHeadShake;
                public bool useBrowJump;
            }

            [Serializable]
            public struct HandFeatureFlags
            {
                public bool useHand;
            }

            [Serializable]
            public struct HandTriggerFlags
            {
                public bool useOK;
                public bool useScissor;
                public bool useGood;
                public bool usePalm;
                public bool usePistol;
                public bool useLove;
                public bool useHoldup;
                public bool useCongratulate;
                public bool useFingerHeart;
                public bool useFingerIndex;
                public bool useFist;
            }

            [Serializable]
            public struct BodyFeatureFlags
            {
                public bool useBody; // 4 points
                public bool useBodyExtra; // 14 points
            }

            [Serializable]
            public struct SegmetationFlags
            {
                public bool useBackground;
            }

            public int maxFaceCount = 0;
            public FaceFeatureFlags faceFeatureFlags;
            public FaceTriggerFlags faceTriggerFlags;

            public int maxHandCount = 0;
            public HandFeatureFlags handFeatureFlags;
            public HandTriggerFlags handTriggerFlags;

            public int maxBodyCount = 0;
            public BodyFeatureFlags bodyFeatureFlags;

            public SegmetationFlags segmetationFlags;

            public bool useSLAM;
        }

        public struct FaceData
        {
            public int ID;
            public float score;

            public StRect rect;

            public Vector2[] keyPointsArray;
            public float[] keyPointsVisibilityArray;

            public float yaw;
            public float pitch;
            public float roll;

            public int validExtraPointsCount;
            public Vector2[] extraPointsArray;

            public int validIrisCenterPointsCount;
            public Vector2[] irisCenterPointsArray;

            public int validIrisContourPointsCount;
            public Vector2[] irisContourPointsArray;

            public FaceActionData faceActionData;

            public static FaceData Create()
            {
                FaceData ret = new FaceData();

                ret.keyPointsArray = new Vector2[FACE_FEATURE_POINTS_ARRAY_SIZE];
                ret.keyPointsVisibilityArray = new float[FACE_FEATURE_POINTS_ARRAY_SIZE];
                ret.extraPointsArray = new Vector2[FACE_EXTRA_POINTS_ARRAY_MAX_SIZE];
                ret.irisCenterPointsArray = new Vector2[IRIS_CENTER_POINTS_ARRAY_MAX_SIZE];
                ret.irisContourPointsArray = new Vector2[IRIS_CONTOUR_POINTS_ARRAY_MAX_SIZE];

                return ret;
            }
        }

        public struct HandData
        {
            public int ID;
            public float score;

            public StRect rect;

            public int validKeyPointsCount;
            public Vector2[] keyPointsArray;

            public HandActionData handActionData;

            public static HandData Create()
            {
                HandData ret = new HandData();

                ret.keyPointsArray = new Vector2[HAND_KEY_POINTS_ARRAY_MAX_SIZE];

                return ret;
            }
        }

        public struct BodyData
        {
            public int ID;

            public int validKeyPointsCount;
            public Vector2[] keyPointsArray;
            public float[] keyPointsScoreArray;

            public int validContourPointsCount;
            public Vector2[] contourArray;
            public float[] contourScoreArray;

            public static BodyData Create()
            {
                BodyData ret = new BodyData();

                ret.keyPointsArray = new Vector2[BODY_KEYPOINTS_ARRAY_MAX_SIZE];
                ret.keyPointsScoreArray = new float[BODY_KEYPOINTS_ARRAY_MAX_SIZE];
                ret.contourArray = new Vector2[BODY_CONTOUR_ARRAY_MAX_SIZE];
                ret.contourScoreArray = new float[BODY_CONTOUR_ARRAY_MAX_SIZE];

                return ret;
            }
        }

        // Internal constants

#if UNITY_EDITOR
        const string g_licenseName = "license_online_PC.lic";
#else
        const string g_licenseName = "license_online.lic";
#endif

        const string g_Face106ModelName = "M_SenseME_Face_Video_5.3.3.model";
        const string g_Face240ModelName = "M_SenseME_Face_Extra_5.1.0.model";
        const string g_IrisModelName = "M_SenseME_Iris_1.7.0.model";
        const string g_HandModelName = "M_SenseME_Hand_5.0.0.model";
        const string g_Body4ModelName = "M_SenseME_Body_Four_1.0.0.model";
        const string g_Body14ModelName = "M_SenseME_Body_Fourteen_1.3.1.model";
        const string g_BackgroundSegModelName = "M_SenseME_Segment_1.11.1.model";

        const int m_FeatureDetectInterval = 1;

        // Internal variables

        bool m_ShouldAnalyzeHuman = false;

        bool m_Face106ModelLoaded = false;
        bool m_Face240ModelLoaded = false;
        bool m_EyeballModelLoaded = false;
        bool m_HandModelLoaded = false;
        bool m_Body4ModelLoaded = false;
        bool m_Body14ModelLoaded = false;
        bool m_BackgroundSegModelLoaded = false;

        bool m_ShouldAnalyzeSLAM = false;

        bool m_SLAMSessionStarted = false;

        // Internal functions (and variables deeply coupled with the function)

        private static string g_ActivationCodeCachePath = null;
        private static string GetActivationCodeCachePath()
        {
            if (g_ActivationCodeCachePath == null)
            {
                g_ActivationCodeCachePath = Path.Combine(Application.persistentDataPath, "ActivationCodeCache");
            }
            return g_ActivationCodeCachePath;
        }

        private static byte[] GetDataFromStreamingAssets(string fileName)
        {
            string streamingAssetPath = Application.streamingAssetsPath;
            string path = Path.Combine(streamingAssetPath, fileName);

            // Workaround for reading from streaming asset folder on android
            // In app we ship the license will not be stored in streaming asset so this quality is fine
            if (Application.platform == RuntimePlatform.Android)
            {
                WWW reader = new WWW(path);
                while (!reader.isDone) { }
                return reader.bytes;
            }
            else
            {
                return File.ReadAllBytes(path);
            }
        }

        private void LoadModel(string modelFileName)
        {
            byte[] modelData = null;
            try
            {
                modelData = GetDataFromStreamingAssets(modelFileName);
            }
            catch (IOException ex)
            {
                Debug.LogError("SenseTimeLib.LoadModel fail: " + ex.ToString());
                return;
            }

            if (modelData == null)
            {
                Debug.LogError("SenseTimeLib.LoadModel fail: No model data loaded");
                return;
            }

            GCHandle pinnedArray = GCHandle.Alloc(modelData, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();
            StResult errCode = SnowSenseTime_LoadHumanActionModel(pointer, modelData.Length);
            pinnedArray.Free();
            
            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.LoadModel fail: " + errCode.ToString());
                return;
            }
        }

        // Public API

        public static SenseTimeLib GetInstance()
        {

#if !UNITY_EDITOR_WIN && !UNITY_EDITOR_OSX && !UNITY_ANDROID && !UNITY_IOS
            return null;
#endif

            StResult errCode;

            byte[] licenseFileData = null;
            try
            {
                licenseFileData = GetDataFromStreamingAssets(g_licenseName);
            }
            catch (IOException ex)
            {
                Debug.LogError("SenseTimeLib.GetInstance fail: " + ex.ToString());
                return null;
            }

#if UNITY_ANDROID && !UNITY_EDITOR
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");         
            IntPtr context = activity.GetRawObject();
#endif

            try
            {
                byte[] activationCodeData = null;
                activationCodeData = File.ReadAllBytes(GetActivationCodeCachePath());
                if (activationCodeData == null)
                {
                    throw new IOException();
                }
                
                GCHandle pinnedLicenseArray = GCHandle.Alloc(licenseFileData, GCHandleType.Pinned);
                GCHandle pinnedActivationArray = GCHandle.Alloc(activationCodeData, GCHandleType.Pinned);
                IntPtr licensePtr = pinnedLicenseArray.AddrOfPinnedObject();               
                IntPtr activationPtr = pinnedActivationArray.AddrOfPinnedObject();
                
#if UNITY_ANDROID && !UNITY_EDITOR
                errCode = SnowSenseTime_CheckActivationCode(context, licensePtr, licenseFileData.Length, activationPtr, activationCodeData.Length);
#else
                errCode = SnowSenseTime_CheckActivationCode(licensePtr, licenseFileData.Length, activationPtr, activationCodeData.Length);
#endif
                
                pinnedLicenseArray.Free();
                pinnedActivationArray.Free();
                
                if (errCode != StResult.kOK)
                {
                    throw new IOException();
                }
            }
            catch (IOException ex)
            {
                // This is not fatal, just need to refresh activation code cache
                Debug.Log("Fail to get activation code from cache: " + ex.ToString());

                int activationCodeBufferLength = 1024;
                byte[] activationCodeBuffer = new byte[1024];

                GCHandle pinnedLicenseArray = GCHandle.Alloc(licenseFileData, GCHandleType.Pinned);
                GCHandle pinnedActivationArray = GCHandle.Alloc(activationCodeBuffer, GCHandleType.Pinned);
                IntPtr licensePtr = pinnedLicenseArray.AddrOfPinnedObject();               
                IntPtr activationPtr = pinnedActivationArray.AddrOfPinnedObject();
                
#if UNITY_ANDROID && !UNITY_EDITOR
                errCode = SnowSenseTime_GenerateActivationCode(context, licensePtr, licenseFileData.Length, activationPtr, ref activationCodeBufferLength);
#else
                errCode = SnowSenseTime_GenerateActivationCode(licensePtr, licenseFileData.Length, activationPtr, ref activationCodeBufferLength);
#endif

                pinnedLicenseArray.Free();
                pinnedActivationArray.Free();
                
                if (errCode != StResult.kOK)
                {
                    Debug.LogError("SenseTimeLib.GetInstance fail: " + errCode.ToString());

#if UNITY_ANDROID && !UNITY_EDITOR
                    unityPlayer.Dispose();
                    activity.Dispose();
#endif

                    return null;
                }
                else if (activationCodeBufferLength > activationCodeBuffer.Length)
                {
                    Debug.Log("Fail to cache activation file, not fatal: memory buffer not large enough");
                }
                else
                {
                    try
                    {
                        if (File.Exists(GetActivationCodeCachePath()))
                        {
                            File.Delete(GetActivationCodeCachePath());
                        }

                        FileStream fileStream = File.Create(GetActivationCodeCachePath());
                        fileStream.Write(activationCodeBuffer, 0, activationCodeBufferLength);
                        fileStream.Close();
                    }
                    catch (IOException innerEx)
                    {
                        Debug.Log("Fail to cache activation file, not fatal: " + innerEx.ToString());
                    }
                }
            }

#if UNITY_ANDROID && !UNITY_EDITOR
            unityPlayer.Dispose();
            activity.Dispose();
#endif

            errCode = SnowSenseTime_InitHumanAction();

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.GetInstance fail: " + errCode.ToString());
                return null;
            }

            return new SenseTimeLib();
        }

        public void Dispose()
        {
            SnowSenseTime_DestroyHumanAction();
        }

        public void UpdateConfig(Config config)
        {
            if (config.maxFaceCount > 0 || config.maxHandCount > 0 || config.maxBodyCount > 0 || config.segmetationFlags.useBackground == true)
            {
                m_ShouldAnalyzeHuman = true;
            }
            else
            {
                m_ShouldAnalyzeHuman = false;
            }

            if (config.useSLAM)
            {
                if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    // Currently only android has SLAM support from native plugin
                    m_ShouldAnalyzeSLAM = true;
                }
            }
            else
            {
                if (m_SLAMSessionStarted)
                {
                    SnowSenseTime_ResetSLAM();
                    SnowSenseTime_DestroySLAMSession();
                    m_SLAMSessionStarted = false;
                }

                m_ShouldAnalyzeSLAM = false;
            }

            // Face settings

            if (config.faceFeatureFlags.useFace || config.faceFeatureFlags.useFaceExtra || config.faceFeatureFlags.useEyeballCenter || config.faceFeatureFlags.useEyeballContour)
            {
                if (!m_Face106ModelLoaded)
                {
                    LoadModel(g_Face106ModelName);
                    m_Face106ModelLoaded = true;
                }
            }
            else
            {
                if (m_Face106ModelLoaded)
                {
                    SnowSenseTime_UnloadHumanActionModel(DetectType.kFace);
                    m_Face106ModelLoaded = false;
                }
            }

            if (config.faceFeatureFlags.useFaceExtra)
            {
                if (!m_Face240ModelLoaded)
                {
                    LoadModel(g_Face240ModelName);
                    m_Face240ModelLoaded = true;
                }
            }
            else
            {
                if (m_Face240ModelLoaded)
                {
                    SnowSenseTime_UnloadHumanActionModel(DetectType.kFaceExtra);
                    m_Face240ModelLoaded = false;
                }
            }

            if (config.faceFeatureFlags.useEyeballCenter || config.faceFeatureFlags.useEyeballContour)
            {
                if (!m_EyeballModelLoaded)
                {
                    LoadModel(g_IrisModelName);
                    m_EyeballModelLoaded = true;
                }
            }
            else
            {
                if (m_EyeballModelLoaded)
                {
                    SnowSenseTime_UnloadHumanActionModel(DetectType.kEyeballCenter);
                    SnowSenseTime_UnloadHumanActionModel(DetectType.kEyeballContour);
                    m_EyeballModelLoaded = false;
                }
            }

            if (config.maxFaceCount > 0)
            {
                SnowSenseTime_SetFaceLimit(config.maxFaceCount);
            }
            SnowSenseTime_SetFaceDetectInterval(m_FeatureDetectInterval);

            SnowSenseTime_SetDetectionMask(DetectType.kFace, config.faceFeatureFlags.useFace ? (byte)1 : (byte)0);
            SnowSenseTime_SetDetectionMask(DetectType.kFaceExtra, config.faceFeatureFlags.useFaceExtra ? (byte)1 : (byte)0);
            SnowSenseTime_SetDetectionMask(DetectType.kEyeballCenter, config.faceFeatureFlags.useEyeballCenter ? (byte)1 : (byte)0);
            SnowSenseTime_SetDetectionMask(DetectType.kEyeballContour, config.faceFeatureFlags.useEyeballContour ? (byte)1 : (byte)0);

            SnowSenseTime_SetFaceActionMask(FaceAction.kEyeBlink, config.faceTriggerFlags.useEyeBlink ? (byte)1 : (byte)0);
            SnowSenseTime_SetFaceActionMask(FaceAction.kMouthAh, config.faceTriggerFlags.useMouthOpen ? (byte)1 : (byte)0);
            SnowSenseTime_SetFaceActionMask(FaceAction.kHeadYaw, config.faceTriggerFlags.useHeadShake ? (byte)1 : (byte)0);
            SnowSenseTime_SetFaceActionMask(FaceAction.kHeadPitch, config.faceTriggerFlags.useHeadNod ? (byte)1 : (byte)0);
            SnowSenseTime_SetFaceActionMask(FaceAction.kBrowJump, config.faceTriggerFlags.useBrowJump ? (byte)1 : (byte)0);

            // Hand settings

            if (config.handFeatureFlags.useHand)
            {
                if (!m_HandModelLoaded)
                {
                    LoadModel(g_HandModelName);
                    m_HandModelLoaded = true;
                }
            }
            else
            {
                if (m_HandModelLoaded)
                {
                    SnowSenseTime_UnloadHumanActionModel(DetectType.kFaceExtra);
                    m_HandModelLoaded = false;
                }
            }

            if (config.maxHandCount > 0)
            {
                SnowSenseTime_SetHandLimit(config.maxHandCount);
            }
            SnowSenseTime_SetHandDetectInterval(m_FeatureDetectInterval);

            SnowSenseTime_SetDetectionMask(DetectType.kHand, config.handFeatureFlags.useHand ? (byte)1 : (byte)0);

            SnowSenseTime_SetHandActionMask(HandAction.kOK, config.handTriggerFlags.useOK ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kScissor, config.handTriggerFlags.useScissor ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kGood, config.handTriggerFlags.useGood ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kPalm, config.handTriggerFlags.usePalm ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kPistol, config.handTriggerFlags.usePistol ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kLove, config.handTriggerFlags.useLove ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kHoldup, config.handTriggerFlags.useHoldup ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kCongratulate, config.handTriggerFlags.useCongratulate ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kFingerHeart, config.handTriggerFlags.useFingerHeart ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kFingerIndex, config.handTriggerFlags.useFingerIndex ? (byte)1 : (byte)0);
            SnowSenseTime_SetHandActionMask(HandAction.kFist, config.handTriggerFlags.useFist ? (byte)1 : (byte)0);

            // Body settings

            if (config.bodyFeatureFlags.useBodyExtra)
            {
                if (!m_Body14ModelLoaded)
                {
                    LoadModel(g_Body14ModelName);
                    m_Body14ModelLoaded = true;
                    m_Body4ModelLoaded = false;
                }
            }
            else if (config.bodyFeatureFlags.useBody)
            {
                if (!m_Body4ModelLoaded)
                {
                    LoadModel(g_Body4ModelName);
                    m_Body4ModelLoaded = true;
                    m_Body14ModelLoaded = false;
                }
            }
            else
            {
                if (m_Body4ModelLoaded || m_Body14ModelLoaded)
                {
                    SnowSenseTime_UnloadHumanActionModel(DetectType.kBodyKeyPoints);
                    m_Body4ModelLoaded = false;
                    m_Body14ModelLoaded = false;
                }
            }

            if (config.maxBodyCount > 0)
            {
                SnowSenseTime_SetBodyLimit(config.maxBodyCount);
            }
            SnowSenseTime_SetBodyDetectInterval(m_FeatureDetectInterval);

            SnowSenseTime_SetDetectionMask(DetectType.kBodyKeyPoints, config.bodyFeatureFlags.useBodyExtra || config.bodyFeatureFlags.useBody ? (byte)1 : (byte)0);

            // Background segment

            if (config.segmetationFlags.useBackground)
            {
                if (!m_BackgroundSegModelLoaded)
                {
                    LoadModel(g_BackgroundSegModelName);
                    m_BackgroundSegModelLoaded = true;
                }
                else
                {
                    if (m_BackgroundSegModelLoaded)
                    {
                        SnowSenseTime_UnloadHumanActionModel(DetectType.kBackgroundSegment);
                        m_BackgroundSegModelLoaded = false;
                    }
                }
            }
        }       

        public bool AnalyzeHuman(byte[] array, PixelFormat format, int width, int height, int stride, RotateCommand textureShouldBeRotatedBy)
        {
            if (!m_ShouldAnalyzeHuman)
                return true;

            StResult errCode;
            
            GCHandle pinnedArray = GCHandle.Alloc(array, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();
            {
                errCode = SnowSenseTime_UpdateHumanAction(
                    pointer, format, width, height, stride, textureShouldBeRotatedBy);
            }
            pinnedArray.Free();
            
            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.AnalyzeHuman fail: " + errCode.ToString());
                return false;
            }

            return true;
        }

        public bool ResetHumanAnalyzer()
        {
            StResult errCode = SnowSenseTime_ResetHumanAction();

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.ResetHumanAnalyzer fail: " + errCode.ToString());
                return false;
            }

            return true;
        }

        public int GetFaceCount()
        {
            if (!m_ShouldAnalyzeHuman)
                return 0;

            return SnowSenseTime_GetFaceCount();
        }

        public bool GetFace(int idx, ref FaceData face)
        {
            StResult errCode = SnowSenseTime_GetFaceData(idx, 
                ref face.ID, ref face.score, ref face.rect,
                face.keyPointsArray, face.keyPointsVisibilityArray,
                ref face.yaw, ref face.pitch, ref face.roll,
                ref face.validExtraPointsCount, face.extraPointsArray,
                ref face.validIrisCenterPointsCount, face.irisCenterPointsArray,
                ref face.validIrisContourPointsCount, face.irisContourPointsArray,
                ref face.faceActionData);

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.GetFace fail: " + errCode.ToString());
                return false;
            }
            return true;
        }

        public int GetHandCount()
        {
            if (!m_ShouldAnalyzeHuman)
                return 0;

            return SnowSenseTime_GetHandCount();
        }

        public bool GetHand(int idx, ref HandData hand)
        {
            StResult errCode = SnowSenseTime_GetHandData(idx,
                ref hand.ID, ref hand.score, ref hand.rect,
                ref hand.validKeyPointsCount, hand.keyPointsArray,
                ref hand.handActionData);

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.GetHand fail: " + errCode.ToString());
                return false;
            }
            return true;
        }

        public int GetBodyCount()
        {
            if (!m_ShouldAnalyzeHuman)
                return 0;

            return SnowSenseTime_GetBodyCount();
        }

        public bool GetBody(int idx, ref BodyData body)
        {
            StResult errCode = SnowSenseTime_GetBodyData(idx,
                ref body.ID,
                ref body.validKeyPointsCount, body.keyPointsArray, body.keyPointsScoreArray,
                ref body.validContourPointsCount, body.contourArray, body.contourScoreArray);

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.GetBody fail: " + errCode.ToString());
                return false;
            }
            return true;
        }

        public int GetBackgroundSegImageSize()
        {
            if (!m_ShouldAnalyzeHuman)
                return 0;

            return SnowSenseTime_GetBackgroundSegmentationBufferSize();
        }

        public bool GetBackgroundSegImage(byte[] buffer, ref int width, ref int height, ref int stride)
        {
            if (buffer == null)
                return false;

            GCHandle pinnedArray = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();
            StResult errCode = SnowSenseTime_GetBackgroundSegmentationBuffer(pointer, buffer.Length, ref width, ref height, ref stride);
            pinnedArray.Free();
            
            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.GetBackgroundSegImage fail: " + errCode.ToString());
                return false;
            }
            return true;
        }

        // Sensetime SLAM only supports back camera, if front camera image wants to do SLAM, you need to flip it upside down
        public bool AnalyzeSLAM(byte[] array, PixelFormat format, int width, int height, int stride, float fovx, RotateCommand textureShouldBeRotatedBy, double imageTimeStamp,
            Vector3 rawGravity, Vector3 rawUserAcceleration, Vector3 rawRotationRate, Quaternion rawAttitude)
        {
            StResult errCode;

            if (!m_ShouldAnalyzeSLAM)
                return true;

            if (!m_SLAMSessionStarted)
            {
                errCode = SnowSenseTime_CreateSLAMSession(width, height, fovx, textureShouldBeRotatedBy);

                if (errCode != StResult.kOK)
                {
                    Debug.LogError("SenseTimeLib.AnalyzeSLAM fail to create session: " + errCode.ToString());
                    return false;
                }

                m_SLAMSessionStarted = true;
            }
            
            GCHandle pinnedArray = GCHandle.Alloc(array, GCHandleType.Pinned);
            IntPtr pointer = pinnedArray.AddrOfPinnedObject();
            {
                StImage image = new StImage();
                image.data = pointer;
                image.format = format;
                image.width = width;
                image.height = height;
                image.stride = stride;
                image.timeStamp = imageTimeStamp;

                // Unity does not provide us with sensor timestamp, we cannot do better than this
                // To improve accuracy here, consider making a native plugin that provides more detailed sensor data
                float timeThisFrame = Time.unscaledTime;

                StIMU[] IMUArray = new StIMU[1];
                IMUArray[0].accelerationX = rawUserAcceleration.x;
                IMUArray[0].accelerationY = rawUserAcceleration.y;
                IMUArray[0].accelerationZ = rawUserAcceleration.z;
                IMUArray[0].gyroScopeX = rawRotationRate.x;
                IMUArray[0].gyroScopeY = rawRotationRate.y;
                IMUArray[0].gyroScopeZ = rawRotationRate.z;
                IMUArray[0].timeStamp = timeThisFrame;

                StAttitude stAttitude = new StAttitude();
                stAttitude.quaternionX = rawAttitude.x;
                stAttitude.quaternionY = rawAttitude.y;
                stAttitude.quaternionZ = rawAttitude.z;
                stAttitude.quaternionW = rawAttitude.w;
                stAttitude.gravityX = rawGravity.x;
                stAttitude.gravityY = rawGravity.y;
                stAttitude.gravityZ = rawGravity.z;
                stAttitude.timeStamp = timeThisFrame;

                // Rotation vector on some android devices may have problem (according to sample project from Sensetime)
                // So we generate a replacement using the rawGravity vector
                if (Application.platform == RuntimePlatform.Android)
                {
                    float[,] R = new float[3, 3];

                    // 1. make R as I

                    R[0, 0] = 1.0f;
                    R[0, 1] = 0.0f;
                    R[0, 2] = 0.0f;
                    R[1, 0] = 0.0f;
                    R[1, 1] = 1.0f;
                    R[1, 2] = 0.0f;
                    R[2, 0] = 0.0f;
                    R[2, 1] = 0.0f;
                    R[2, 2] = 1.0f;

                    // 2. Gram–Schmidt process

                    // row Z
                    Vector3 Z = rawGravity.normalized;

                    R[2, 0] = Z.x;
                    R[2, 1] = Z.y;
                    R[2, 2] = Z.z;

                    // row X
                    Vector3 original = new Vector3(R[0, 0], R[0, 1], R[0, 2]);
                    Vector3 X = original - Z * Vector3.Dot(Z, original);
                    X = X.normalized;

                    R[0, 0] = X.x;
                    R[0, 1] = X.y;
                    R[0, 2] = X.z;

                    // row Y
                    Vector3 Y = Vector3.Cross(Z, X);

                    R[1, 0] = Y.x;
                    R[1, 1] = Y.y;
                    R[1, 2] = Y.z;

                    // 3. rotation matrix to quaternion

                    stAttitude.quaternionW = Mathf.Sqrt((float) (1.0 + R[0, 0] + R[1, 1] + R[2, 2])) * 0.5f;
                    double inv_w4 = (1.0f / (4.0 * stAttitude.quaternionW));
                    stAttitude.quaternionX = (R[2, 1] - R[1, 2]) * inv_w4;
                    stAttitude.quaternionY = (R[0, 2] - R[2, 0]) * inv_w4;
                    stAttitude.quaternionZ = (R[1, 0] - R[0, 1]) * inv_w4;
                }

                errCode = SnowSenseTime_UpdateSLAMSession(image, IMUArray, IMUArray.Length, stAttitude);
            }
            pinnedArray.Free();         
            
            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.AnalyzeSLAM fail to update session: " + errCode.ToString());
                return false;
            }

            return true;
        }

        public bool ResetSLAMAnalyzer()
        {
            if (!m_SLAMSessionStarted)
                return true;

            StResult errCode = SnowSenseTime_ResetSLAM();

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.ResetSLAMAnalyzer fail: " + errCode.ToString());
                return false;
            }

            return true;
        }

        public bool GetCameraPose(ref Quaternion rotation, ref Vector3 center)
        {
            if (!m_ShouldAnalyzeSLAM)
                return false;

            if (!m_SLAMSessionStarted)
                return false;

            StResult errCode = SnowSenseTime_GetCameraPose(ref rotation, ref center);

            if (errCode != StResult.kOK)
            {
                Debug.LogError("SenseTimeLib.GetCameraPose fail: " + errCode.ToString());
                return false;
            }

            return true;
        }

    }
}