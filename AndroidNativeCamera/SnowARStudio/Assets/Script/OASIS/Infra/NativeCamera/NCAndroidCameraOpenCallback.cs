﻿
using System.Threading;
using UnityEngine;

namespace OASIS.NC
{
	class NCAndroidCameraOpenCallback : AndroidJavaProxy
	{
//#if !UNITY_EDITOR && UNITY_ANDROID

        private object threadLock;
        public bool mResult;
		

		public NCAndroidCameraOpenCallback( object threadLock ) : base( "com.unity.androidnativecamera.CameraInterface" )
		{
			//Path = string.Empty;
			this.threadLock = threadLock;
		}

	    public void onCameraDeviceOpened(bool result)
        {
            mResult = result;
        }
//#endif
    }
}
