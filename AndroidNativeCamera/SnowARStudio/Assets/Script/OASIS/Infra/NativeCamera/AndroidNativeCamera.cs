﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Android;
using Object = UnityEngine.Object;


namespace OASIS.NC
{

    public class AndroidNativeCamera : IDisposable
    {

#if !UNITY_EDITOR && UNITY_ANDROID

        private static AndroidJavaObject mContext = null;
        private static AndroidJavaObject mCameraWrapper = null;

        private int mRequestedWidth;
        private int mRequestedHeight;
        NCAndroidCameraCallback mNativeCallback;

        public static AndroidNativeCamera GetInstance()
        {
            if (mContext == null)
            {
                Debug.LogError("AndroidNativeCamera:GetInstance()");
                using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                {
                    mContext = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
                }
            }

            if (mCameraWrapper == null)
            {
                mCameraWrapper = new AndroidJavaObject("com.unity.androidnativecamera.Camera2Wrapper", mContext);
                mCameraWrapper.Set("mContext", mContext);
            }

            return new AndroidNativeCamera();
        }

        public bool InitializeCamera2(int index, int requestedWidth, int requestedHeight, int requestedFps)
        {
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }
            if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                Permission.RequestUserPermission(Permission.Camera);
            }


            Debug.LogError("AndroidNativeCamera:InitializeCamera2()");
            mRequestedWidth = requestedWidth;
            mRequestedHeight = requestedHeight;
            bool result = false;

            object threadLock = new object();
            lock (threadLock)
            {
                NCAndroidCameraOpenCallback nativeOpenCallback = new NCAndroidCameraOpenCallback(threadLock);
                result = mCameraWrapper.Call<Boolean>("initializeCamera2", index, requestedWidth, requestedHeight, requestedFps, nativeOpenCallback);
                Debug.LogError("AndroidNativeCamera:InitializeCamera2(): result = " + result);

                result = nativeOpenCallback.mResult;
                Debug.LogError("AndroidNativeCamera:InitializeCamera2(): result2 = " + result);
            }
            return result;
        }

        public int GetCamera2Count()
        {
            return mCameraWrapper.Call<int>("getCamera2Count");
        }

        public int GetCamera2SensorOrientation(int index)
        {
            return mCameraWrapper.Call<int>("getCamera2SensorOrientation", index);
        }

        public bool IsCamera2FrontFacing(int index)
        {
            return mCameraWrapper.Call<Boolean>("isCamera2FrontFacing", index);
        }

        public String[] getCameraIds()
        {
            return mCameraWrapper.Call<String[]>("getCameraIds");
        }

        public String getCameraId(int index)
        {
            return mCameraWrapper.Call<String>("getCameraId", index);
        }

        int[] GetCamera2Resolutions(int index)
        {
            return mCameraWrapper.Call<int[]>("getCamera2Resolutions", index);
        }

        public Rect GetFrameSizeCamera2()
        {
            int width = mCameraWrapper.Call<int>("getFrameSizeCamera2Width");
            int height = mCameraWrapper.Call<int>("getFrameSizeCamera2Height");

            return new Rect(0, 0, width, height);
        }

        public void CloseCamera2()
        {
            mCameraWrapper.Call("closeCamera2");
        }

        public void Play()
        {
            object threadLock = new object();
            lock (threadLock)
            {
                mNativeCallback = new NCAndroidCameraCallback(threadLock);
                Debug.LogError("AndroidNativeCamera:Play()");
                mCameraWrapper.Call("startCamera2", mNativeCallback);
            }
        }

        struct YuvFrame
        {
            public byte[] y;
            public byte[] u;
            public byte[] v;
            public int width;
            public int height;
            public int y_stride;
            public int uv_stride;
            public int offset_x;
            public int offset_y;
            public int uv_step;
        };

        YuvFrame m_YUV;

        public byte[] GetRawData()
        {
            if (mNativeCallback != null && mNativeCallback.m_rgbBuffer != null)
            {
                //Debug.LogError("AndroidNativeCamera.GetRawData(): m_rgbBuffer.Length = " + mNativeCallback.m_rgbBuffer.Length);
                return mNativeCallback.m_rgbBuffer;
            }
            return null;
        }

        /*public byte[] Get_YTexRawData()
        {
            if (mNativeCallback != null && mNativeCallback.m_yBuffer != null)
            {
                Debug.LogError("AndroidNativeCamera.GetRawData(): m_yBuffer.Length = " + mNativeCallback.m_yBuffer.Length);
                return mNativeCallback.m_yBuffer;
            }
            return null;
        }

        public byte[] Get_UTexRawData()
        {
            if (mNativeCallback != null && mNativeCallback.m_rgbBuffer != null)
            {
                Debug.LogError("AndroidNativeCamera.GetRawData(): m_uBuffer.Length = " + mNativeCallback.m_uBuffer.Length);
                return mNativeCallback.m_uBuffer;
            }
            return null;
        }

        public byte[] Get_VTexRawData()
        {
            if (mNativeCallback != null && mNativeCallback.m_vBuffer != null)
            {
                Debug.LogError("AndroidNativeCamera.GetRawData(): m_vBuffer.Length = " + mNativeCallback.m_vBuffer.Length);
                return mNativeCallback.m_vBuffer;
            }
            return null;
        }*/

               
        /*public byte[] ConvertIntArrayToByteArray(int[] inputElements)
        {
            if (inputElements == null)
            {
                return null;
            }
        
            byte[] myFinalBytes = new byte[inputElements.Length * 4];
            for (int cnt = 0; cnt < inputElements.Length; cnt++)
            {
                byte[] myBytes = BitConverter.GetBytes(inputElements[cnt]);
                Array.Copy(myBytes, 0, myFinalBytes, cnt * 4, 4);
            }
            return myFinalBytes;
        }*/

        public void Stop()
        {
            mCameraWrapper.Call("stopCamera2");
        }

        public void PauseCamera2()
        {
            mCameraWrapper.Call<AndroidJavaObject>("pauseCamera2");
        }

        public bool IsCamera2AutoFocusPointSupported(int index)
        {
            return mCameraWrapper.Call<Boolean>("isCamera2AutoFocusPointSupported", index);
        }

        public bool SetAutoFocusPoint(float x, float y)
        {
            return mCameraWrapper.Call<Boolean>("setAutoFocusPoint", x, y);
        }


        public bool OnStartRecord(int videoWidth, int videoHeight, int audioChannels, int audioFreq)
        {
            Debug.LogError("AndroidNativeCamera.OnStartRecord()");
            return mCameraWrapper.Call<Boolean>("startRecordingVideo", videoWidth, videoHeight, audioChannels, audioFreq, mContext);
        }
        public void OnStopRecord()
        {
            mCameraWrapper.Call<Boolean>("stopRecordingVideo");
        }

        public void OnReceiveVideoData(byte[] data, int length)
        {
            mCameraWrapper.Call("OnReceiveVideoData", data);
        }
		
		public void OnReceiveAudioData(float[] data, int length)
        {
            mCameraWrapper.Call("OnReceiveAudioData", data);
        }
		

        public bool isRecordingVideo()
        {
            bool result = mCameraWrapper.Call<Boolean>("isRecordingVideo");
            Debug.LogError("AndroidNativeCamera.isRecordingVideo(): result = " + result);
            return result;
        }
#endif

        public void Dispose()
        {
#if UNITY_ANDROID && !UNITY_EDITOR
            throw new NotImplementedException();
#endif
        }

    }
}
