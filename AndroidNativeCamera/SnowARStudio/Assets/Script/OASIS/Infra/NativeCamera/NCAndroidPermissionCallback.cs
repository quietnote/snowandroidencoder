﻿#if !UNITY_EDITOR && UNITY_ANDROID
using System.Threading;
using UnityEngine;

namespace  OASIS.NC
{
	public class NCAndroidPermissionCallback : AndroidJavaProxy
	{
		private object threadLock;
		public int Result { get; private set; }

		public NCAndroidPermissionCallback( object threadLock ) : base( "com.unity.androidnativecamera.RequestPermissionCallback" )
		{
			Result = -1;
			this.threadLock = threadLock;
		}

		public void requestPermissionResult( int result )
		{
			Result = result;

			lock( threadLock )
			{
				Monitor.Pulse( threadLock );
			}
		}
	}
}
#endif