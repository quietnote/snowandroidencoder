﻿
using System.Threading;
using UnityEngine;

namespace OASIS.NC
{
	class NCAndroidCameraCallback : AndroidJavaProxy
	{
//#if !UNITY_EDITOR && UNITY_ANDROID

        private object threadLock;
        //public byte[] m_yuvBuffer { get; private set; }
        
		public byte[] m_yBuffer { get; private set; }
		public byte[] m_uBuffer { get; private set; }
		public byte[] m_vBuffer { get; private set; }
        public byte[] m_rgbBuffer { get; private set; }


        public int m_width;
        public int m_height;

        public int m_yStride { get; private set; }
		public int m_uvStride { get; private set; }		
		public int m_uvStep { get; private set; }	
		

		public NCAndroidCameraCallback( object threadLock ) : base( "com.unity.androidnativecamera.IFrameReady" )
		{
			//Path = string.Empty;
			this.threadLock = threadLock;
		}


        public void onFrameUpdate(AndroidJavaObject frameData, int yStride, int uvStride, int uvStep )
		{
            /*AndroidJavaObject yuvBufferObject = frameData.Get<AndroidJavaObject>("mBufferYUV");
            m_yuvBuffer = AndroidJNIHelper.ConvertFromJNIArray<byte[]>(yuvBufferObject.GetRawObject());*/

			AndroidJavaObject yBufferObject = frameData.Get<AndroidJavaObject>("mBufferY");
            m_yBuffer = AndroidJNIHelper.ConvertFromJNIArray<byte[]>(yBufferObject.GetRawObject());

            AndroidJavaObject uBufferObject = frameData.Get<AndroidJavaObject>("mBufferU");
            m_uBuffer = AndroidJNIHelper.ConvertFromJNIArray<byte[]>(uBufferObject.GetRawObject());
			
            AndroidJavaObject vBufferObject = frameData.Get<AndroidJavaObject>("mBufferV");
            m_vBuffer = AndroidJNIHelper.ConvertFromJNIArray<byte[]>(vBufferObject.GetRawObject());

            AndroidJavaObject rgbBufferObject = frameData.Get<AndroidJavaObject>("mRGB");
            m_rgbBuffer = AndroidJNIHelper.ConvertFromJNIArray<byte[]>(rgbBufferObject.GetRawObject());


            m_width = frameData.Get<int>("mWidth");
            m_height = frameData.Get<int>("mHeight");


            m_yStride = yStride;
			m_uvStride = uvStride;
			m_uvStep = uvStep;

            //Debug.LogWarning("NCAndroidCameraCallback.onFrameUpdate() : m_yBuffer.Length = " + m_yBuffer.Length + ", m_uBuffer.Length = " + m_uBuffer.Length + ", m_vBuffer.Length = " + m_vBuffer.Length + ", m_rgbBuffer = " + m_rgbBuffer + ", m_width = " + m_width + ", m_height = " + m_height + ", m_yStride = " + m_yStride + ", m_uvStride = " + m_uvStride + ", m_uvStep = " + m_uvStep);

            lock ( threadLock )
			{
				Monitor.Pulse( threadLock );
			}
		}
//#endif
    }
}
