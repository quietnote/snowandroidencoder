﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XLua;
using System;

namespace OASIS.Lua
{
    [System.Serializable]
    public class Injection
    {
        public string name;
        public GameObject value;
    }

    public class LuaBehaviour : MonoBehaviour
    {
        public TextAsset luaScript;
        public Injection[] injections;

        private string init_coroutine =
            @"
            function lua_to_cs_coroutine(func)

                local move_end = {}

                local generator_mt = 
                    {
                        __index = 
                            {
                                MoveNext = 
                                    function(self)
                                        self.Current = self.co()
                                        if self.Current == move_end then
                                            self.Current = nil
                                            return false
                                        else
                                            return true
                                        end
                                    end;

                                Reset = 
                                    function(self)
                                        self.co = coroutine.wrap(self.wrapped_func)
                                    end
                            }
                    }

                local generator = 
                        setmetatable(
                            {
                                wrapped_func = 
                                    function()
                                        func()
                                        return move_end
                                    end
                            },
                            generator_mt)

                generator:Reset()

                return generator
            end
            ";

        internal static LuaEnv luaEnv = new LuaEnv(); // All Lua behavior shared one luaenv only
        internal static float lastGCTime = 0;
        internal const float GCInterval = 1; // 1 second 

        private LuaFunction luaStart;
        private LuaFunction luaUpdate;
        private LuaFunction luaOnDestroy;

        private LuaTable scriptEnv;

        virtual protected void BindLuaFunctions(LuaTable scriptEnv)
        {

        }

        protected void Awake()
        {
            scriptEnv = luaEnv.NewTable();

            LuaTable meta = luaEnv.NewTable();
            meta.Set("__index", luaEnv.Global);
            scriptEnv.SetMetaTable(meta);
            meta.Dispose();

            scriptEnv.Set("self", this);
            foreach (var injection in injections)
            {
                scriptEnv.Set(injection.name, injection.value);
            }

            luaEnv.DoString(init_coroutine, "InitCoroutine", scriptEnv);
            luaEnv.DoString(luaScript.text, "LuaBehaviour", scriptEnv);

            LuaFunction luaAwake = scriptEnv.Get<LuaFunction>("awake");
            scriptEnv.Get("start", out luaStart);
            scriptEnv.Get("update", out luaUpdate);
            scriptEnv.Get("ondestroy", out luaOnDestroy);
            BindLuaFunctions(scriptEnv);

            scriptEnv.Dispose();

            if (luaAwake != null)
            {
                luaAwake.Call();
            }
        }

        protected void Start()
        {
            luaStart?.Call();
        }

        protected void Update()
        {
            luaUpdate?.Call();

            if (Time.time - lastGCTime > GCInterval)
            {
                luaEnv.Tick();
                lastGCTime = Time.time;
            }
        }

        protected void OnDestroy()
        {
            luaOnDestroy?.Call();
        }
    }
}