﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.S3;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.CS
{
    [CreateAssetMenu(fileName = "ContentsServerSettings", menuName = "Snow/ContentsServerSettings", order = 1)]
    public class ContentsServerSettings : ScriptableObject
    {

#pragma warning disable CS0649 // Private SerializeField is set by Unity Editor

        [SerializeField]
        string m_CognitoIdentityRegion = RegionEndpoint.USEast1.SystemName;
        public RegionEndpoint cognitoIdentityRegion
        {
            get { return RegionEndpoint.GetBySystemName(m_CognitoIdentityRegion); }
        }

        [SerializeField]
        private string m_IdentityPoolId;
        public string identityPoolId
        {
            get { return m_IdentityPoolId; }
        }

        [SerializeField]
        private string m_S3Region = RegionEndpoint.USEast1.SystemName;
        public RegionEndpoint s3Region
        {
            get { return RegionEndpoint.GetBySystemName(m_S3Region); }
        }

        [SerializeField]
        private string m_BucketName;
        public string bucketName
        {
            get { return m_BucketName; }
        }

#pragma warning restore CS0649

    }
}