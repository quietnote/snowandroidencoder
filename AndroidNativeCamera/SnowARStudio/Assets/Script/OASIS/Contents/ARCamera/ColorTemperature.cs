﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    public class ColorTemperature : MonoBehaviour
    {
        [SerializeField]
        float weight = 0;

        Material matColorTemperature;


        // Start is called before the first frame update
        void Start()
        {
            matColorTemperature = Resources.Load<Material>("Raws/Materials/ColorTemperature");        
        }

        // Update is called once per frame
        void Update()
        {
            
        }

        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            matColorTemperature.SetFloat("_weight", weight);
            Graphics.Blit(source, destination, matColorTemperature, 0);
        }

        public float Weight{
            get { return weight; }
            set { weight = value; }
        }
    }
}
