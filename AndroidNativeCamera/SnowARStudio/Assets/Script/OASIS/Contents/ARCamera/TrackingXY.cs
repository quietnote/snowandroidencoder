﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    public class TrackingXY : MonoBehaviour
    {
        public Transform target;

        void Update()
        {
            float scale = transform.position.z / target.position.z;
            transform.position = new Vector3(target.position.x * scale, target.position.y * scale, transform.position.z);
        }
    }
}