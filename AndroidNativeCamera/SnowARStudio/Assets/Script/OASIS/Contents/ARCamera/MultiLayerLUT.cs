﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    public class MultiLayerLUT
    {
        Material matLUT;
        Texture2D maskImage;
        RenderTexture maskTexture;
        Texture2D[] inputLUT = new Texture2D[3];
        string[] inputLUTname = new string[3];
        Vector3 weight = new Vector3(0, 0, 0);
        Texture3D[] converted3DLut = new Texture3D[3];        
        int sizeLUT = 64;       // depends on lut images (64/32/16), it is changable

        public MultiLayerLUT()  // 생성자
        {
            matLUT = Resources.Load<Material>("Raws/Materials/MultiLayerLUT");
            maskImage = Resources.Load<Texture2D>("Raws/Textures/red");
            inputLUT[0] = Resources.Load<Texture2D>("Raws/Textures/lut_original");
            inputLUT[1] = Resources.Load<Texture2D>("Raws/Textures/lut_original");
            inputLUT[2] = Resources.Load<Texture2D>("Raws/Textures/lut_original");
            Update3DLUT();
        }

        public void UpdateLUT()
        {
            Update3DLUT();
        }

        public void Render(RenderTexture source, RenderTexture destination)
        {
            if(matLUT == null)      // nothing to do
                return;
            if(maskImage != null)
                matLUT.SetTexture("_MaskTex", maskImage);
            else
                matLUT.SetTexture("_MaskTex", maskTexture);
            matLUT.SetTexture("_Screen", source);
            matLUT.SetTexture("_lut0Tex", converted3DLut[0]);
            matLUT.SetTexture("_lut1Tex", converted3DLut[1]);
            matLUT.SetTexture("_lut2Tex", converted3DLut[2]);     
            matLUT.SetFloat("_weightLayer0", weight.x);   
            matLUT.SetFloat("_weightLayer1", weight.y);   
            matLUT.SetFloat("_weightLayer2", weight.z);   
            Graphics.Blit(source, destination, matLUT, 0);
        }
        
        void Update3DLUT()
        {    
            LUTConverter3D lutConverter = new LUTConverter3D(); 
            for(int i=0; i<inputLUT.Length; i++)
            {            
                Texture3D temp = lutConverter.Convert(inputLUT[i]);
                sizeLUT = lutConverter.LutSize;
                converted3DLut[i] = new Texture3D (sizeLUT, sizeLUT, sizeLUT, TextureFormat.ARGB32, false);
                converted3DLut[i].wrapMode = TextureWrapMode.Clamp;
                if(converted3DLut[i] != null)
                    Graphics.CopyTexture(temp, converted3DLut[i]);
            }
        }

        bool CheckChangeLUT()
        {      
            for(int i=0; i<inputLUT.Length; i++)
            {
                if(inputLUTname[i] != inputLUT[i].name)
                {
                    inputLUTname[i] = inputLUT[i].name;
                    return true;
                }
            } 
            return false; 
        }
        
        public Material MatLUT
        {
            get { return matLUT; }
        }
        public Texture2D MaskImage
        {
            get { return maskImage; }
            set { maskImage = value; }
        }
        public RenderTexture MaskRenderTexture
        {
            get { return maskTexture; }
            set { maskTexture = value; }        
        }
        public Texture2D InputLUT0
        {
            get { return inputLUT[0]; }
            set { inputLUT[0] = value; }            
        }
        public Texture2D InputLUT1
        {
            get { return inputLUT[1]; }
            set { inputLUT[1] = value; }            
        }
        public Texture2D InputLUT2
        {
            get { return inputLUT[2]; }
            set { inputLUT[2] = value; }            
        }
        public Vector3 Weight
        {
            get { return weight; }
            set { weight = value; }           
        }
    }
}