﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace OASIS.Contents
{
    [RequireComponent(typeof(Camera))]
    public class ARCameraBackground : MonoBehaviour
    {
        public struct UVMatrix
        {
            float a11;
            float a12;
            float a13;
            float a21;
            float a22;
            float a23;
            float a31;
            float a32;
            float a33;

            public Vector4 GetUVTransform()
            {
                return new Vector4(a11, a12, a21, a22);
            }

            public float GetUOffset()
            {
                return a13;
            }

            public float GetVOffset()
            {
                return a23;
            }

            public void LeftMultiply(UVMatrix other)
            {
                UVMatrix result = new UVMatrix();
                result.a11 = other.a11 * a11 + other.a12 * a21 + other.a13 * a31;
                result.a12 = other.a11 * a12 + other.a12 * a22 + other.a13 * a32;
                result.a13 = other.a11 * a13 + other.a12 * a23 + other.a13 * a33;
                result.a21 = other.a21 * a11 + other.a22 * a21 + other.a23 * a31;
                result.a22 = other.a21 * a12 + other.a22 * a22 + other.a23 * a32;
                result.a23 = other.a21 * a13 + other.a22 * a23 + other.a23 * a33;
                result.a31 = other.a31 * a11 + other.a32 * a21 + other.a33 * a31;
                result.a32 = other.a31 * a12 + other.a32 * a22 + other.a33 * a32;
                result.a33 = other.a31 * a13 + other.a32 * a23 + other.a33 * a33;
                this = result;
            }

            public static UVMatrix identity
            {
                get
                {
                    UVMatrix result = new UVMatrix();
                    result.a11 = 1;
                    result.a22 = 1;
                    result.a33 = 1;
                    return result;
                }
            }

            public static UVMatrix rotate270Clockwise
            {
                get
                {
                    UVMatrix result = new UVMatrix();
                    result.a12 = -1;
                    result.a13 = 1;
                    result.a21 = 1;
                    result.a33 = 1;
                    return result;
                }
            }

            public static UVMatrix rotate180Clockwise
            {
                get
                {
                    UVMatrix result = new UVMatrix();
                    result.a11 = -1;
                    result.a13 = 1;
                    result.a22 = -1;
                    result.a23 = 1;
                    result.a33 = 1;
                    return result;
                }
            }

            public static UVMatrix rotate90Clockwise
            {
                get
                {
                    UVMatrix result = new UVMatrix();
                    result.a12 = 1;
                    result.a21 = -1;
                    result.a23 = 1;
                    result.a33 = 1;
                    return result;
                }
            }

            public static UVMatrix leftRightFlip
            {
                get
                {
                    UVMatrix result = new UVMatrix();
                    result.a11 = -1;
                    result.a13 = 1;
                    result.a22 = 1;
                    result.a33 = 1;
                    return result;
                }
            }

            public static UVMatrix upDownFlip
            {
                get
                {
                    UVMatrix result = new UVMatrix();
                    result.a11 = 1;
                    result.a22 = -1;
                    result.a23 = 1;
                    result.a33 = 1;
                    return result;
                }
            }

            public static UVMatrix verticalRescale(float scale)
            {
                Debug.Assert(scale > 0 && scale <= 1);

                UVMatrix result = new UVMatrix();
                result.a11 = 1;
                result.a22 = scale;
                result.a23 = (1.0f - scale) / 2.0f;
                result.a33 = 1;
                return result;
            }

            public static UVMatrix horizontalRescale(float scale)
            {
                Debug.Assert(scale > 0 && scale <= 1);

                UVMatrix result = new UVMatrix();
                result.a11 = scale;
                result.a13 = (1.0f - scale) / 2.0f;
                result.a22 = 1;
                result.a33 = 1;
                return result;
            }
        }

        Camera m_TargetCamera = null;
        Material m_BlitMaterial = null;

        float m_CurrentScreenAspectRatio = 0;
        AR.CameraFrame m_CurrentFrame = new AR.CameraFrame();
        AR.CameraFrame m_LastFrame = new AR.CameraFrame();

        CameraClearFlags m_CameraClearFlags = CameraClearFlags.Nothing;
        CommandBuffer m_CommandBuffer = null;

        void Awake()
        {
            m_TargetCamera = GetComponent<Camera>();

            Shader blitShader = Shader.Find("Hidden/BackgroundShader");
            m_BlitMaterial = new Material(blitShader);

            UVMatrix transform = UVMatrix.identity;
            m_BlitMaterial.SetVector("_UVTransform", transform.GetUVTransform());
            m_BlitMaterial.SetFloat("_UOffset", transform.GetUOffset());
            m_BlitMaterial.SetFloat("_VOffset", transform.GetVOffset());            
        }

        void Update()
        {
            bool ShouldReset = false;

            float screenAspect = (float)Screen.width / (float)Screen.height;
            if (Mathf.Abs(m_CurrentScreenAspectRatio - screenAspect) > float.Epsilon)
            {
                m_CurrentScreenAspectRatio = screenAspect;
                ShouldReset = true;
            }

            if (ACApp.instance)
            {
                m_CurrentFrame = ACApp.instance.GetFrame();
                if (m_CurrentFrame.cameraTextureRef != null && m_CommandBuffer == null)
                {
                    ShouldReset = true;
                }
                else if (!m_LastFrame.Equals(m_CurrentFrame))
                {
                    m_LastFrame = m_CurrentFrame;
                    ShouldReset = true;
                }
            }

            if (ShouldReset)
            {
                DisableARBackgroundRendering();
                EnableARBackgroundRendering();
            }
        }

        private void OnEnable()
        {
            float screenAspect = (float)Screen.width / (float)Screen.height;
            m_CurrentScreenAspectRatio = screenAspect;
            if (ACApp.instance)
            {
                m_CurrentFrame = ACApp.instance.GetFrame();
                EnableARBackgroundRendering();
            }
        }

        void OnDisable()
        {
            DisableARBackgroundRendering();
        }

        bool EnableARBackgroundRendering()
        {
            if (m_CurrentFrame.cameraTextureRef == null)
                return false;

            float backgroundAspect = m_CurrentFrame.virtualWidth / m_CurrentFrame.virtualHeight;

            // 1. Set the camera to have simple clearing behavior
            m_CameraClearFlags = m_TargetCamera.clearFlags;
            m_TargetCamera.clearFlags = CameraClearFlags.Depth;

            // 2. Make sure the camera has the correct field of view value given the AR camera frame
            // float virtualWidthIncludedInCamera = m_CurrentFrame.virtualWidth;
            float virtualHeightIncludedInCamera = m_CurrentFrame.virtualHeight;
            if (m_CurrentScreenAspectRatio > backgroundAspect)
            {
                // Make camera see less of height
                virtualHeightIncludedInCamera = m_CurrentFrame.virtualHeight * (backgroundAspect / m_CurrentScreenAspectRatio);
            }
            else
            {
                // Make camera see less of width
                // virtualWidthIncludedInCamera = m_CurrentFrame.virtualHeight *  (m_CurrentScreenAspectRatio / backgroundAspect);
            }
            m_TargetCamera.fieldOfView = 2 * Mathf.Rad2Deg * Mathf.Atan2(virtualHeightIncludedInCamera / 2.0f, m_CurrentFrame.virtualDistance);


            // 3. Perform background rendering
            // We are transforming sampling UV vector, so the order is REVERSE of what we do when transforming input UV pixels
            UVMatrix transform = UVMatrix.identity;

            if (m_CurrentScreenAspectRatio > backgroundAspect)
            {
                transform.LeftMultiply(UVMatrix.verticalRescale(backgroundAspect / m_CurrentScreenAspectRatio));
            }
            else
            {
                transform.LeftMultiply(UVMatrix.horizontalRescale(m_CurrentScreenAspectRatio / backgroundAspect));
            }

            if (m_CurrentFrame.shouldLeftRightFlip)
            {
                transform.LeftMultiply(UVMatrix.leftRightFlip);
            }
                        
            switch (m_CurrentFrame.shouldRotateBy)
            {
                case (AR.TextureRotationCommand.kClockwise90):
                    transform.LeftMultiply(UVMatrix.rotate90Clockwise);
                    break;
                case (AR.TextureRotationCommand.kClockwise180):
                    transform.LeftMultiply(UVMatrix.rotate180Clockwise);
                    break;
                case (AR.TextureRotationCommand.kClockwise270):
                    transform.LeftMultiply(UVMatrix.rotate270Clockwise);
                    break;
                default:
                    break;
            }

            if (m_CurrentFrame.shouldUpDownFlip)
            {
                transform.LeftMultiply(UVMatrix.upDownFlip);
            }

            m_BlitMaterial.SetVector("_UVTransform", transform.GetUVTransform());
            m_BlitMaterial.SetFloat("_UOffset", transform.GetUOffset());
            m_BlitMaterial.SetFloat("_VOffset", transform.GetVOffset());

            m_CommandBuffer = new CommandBuffer();
            m_CommandBuffer.Blit(m_CurrentFrame.cameraTextureRef, BuiltinRenderTextureType.CameraTarget, m_BlitMaterial);

            m_TargetCamera.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_CommandBuffer);
            m_TargetCamera.AddCommandBuffer(CameraEvent.BeforeGBuffer, m_CommandBuffer);
            return true;
        }

        void DisableARBackgroundRendering()
        {
            if (null == m_CommandBuffer)
                return;

            m_TargetCamera.RemoveCommandBuffer(CameraEvent.BeforeGBuffer, m_CommandBuffer);
            m_TargetCamera.RemoveCommandBuffer(CameraEvent.BeforeForwardOpaque, m_CommandBuffer);
            m_CommandBuffer = null;

            m_TargetCamera.clearFlags = m_CameraClearFlags;
        }

        public float CurrentScreenAspectRatio
        {
            get{return m_CurrentScreenAspectRatio;}
        }
    }
}
