using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    enum LutMode {
        LutMode_Snow,
        LutMode_UnityNormal,
        LutMode_UnityExtend,
        LutMode_Custom,
        LutMode_NotSupported
    };

    public class LUTConverter3D
    {
        private Texture3D texture3d;
        int sizeLUT = 64;
        LutMode lutMode;
        int lutSize = 0;        

        public Texture3D Convert(Texture2D texture2d)
        {
            switch(DecisionLutMode(texture2d))
            {
                case LutMode.LutMode_Snow:
                    texture3d = MakeTexture3DSnowLUT(texture2d);
                    break;
                case LutMode.LutMode_UnityNormal:
                    texture3d = MakeTexture3DUnityLUT(texture2d, 16);
                    break;
                case LutMode.LutMode_UnityExtend:
                    texture3d = MakeTexture3DUnityLUT(texture2d, 32);
                    break;    
                default:
                    texture3d = MakeIdentityLut();
                    break;
            }
            return texture3d;
        }

        LutMode DecisionLutMode(Texture2D texture2d)
        {
            LutMode lutmode;

            if(texture2d.width == texture2d.height)
            {
                lutmode = LutMode.LutMode_Snow;
            }
            else if(texture2d.width == 256 && texture2d.height == 16)
            {
                lutmode = LutMode.LutMode_UnityNormal;
            }
            else if(texture2d.width == 1024 && texture2d.height == 32)
            {
                lutmode = LutMode.LutMode_UnityExtend;
            }
            else
            {
                lutmode = LutMode.LutMode_NotSupported;
            }
            return lutmode;
        }

        Texture3D MakeTexture3DSnowLUT(Texture2D texture2d)
        {
            lutSize = 64;
            Texture3D texture3d = new Texture3D(lutSize, lutSize, lutSize, TextureFormat.ARGB32, false);

            int width = texture2d.width;
            var c = texture2d.GetPixels();
            var newC = new Color[c.Length];        

            for(int i = 0; i < lutSize; i++) {          // R = 64, dim = 64
                for(int j = 0; j < lutSize; j++) {      // G = 64
                    for(int k = 0; k < lutSize; k++) {  // B = 64
                        int j_ = lutSize - j - 1;
                        int k_ = lutSize - k - 1;
                        newC[i + (j*lutSize) + (k*lutSize*lutSize)] = c[i+j_*width+k%8*lutSize+(k_/8)*width*lutSize];
                    }
                }
            }
            texture3d.SetPixels(newC);
            texture3d.Apply();
            return texture3d;
        }

        Texture3D MakeTexture3DUnityLUT(Texture2D texture2d, int dim)
        {
            lutSize = dim;
            Texture3D texture3d = new Texture3D(lutSize, lutSize, lutSize, TextureFormat.ARGB32, false);
            int width = texture2d.width;
            var c = texture2d.GetPixels();
            var newC = new Color[c.Length];

            for(int i = 0; i < lutSize; i++) {
                for(int j = 0; j < lutSize; j++) {
                    for(int k = 0; k < lutSize; k++) {
                        int j_ = lutSize-j-1;
                        newC[i + (j*lutSize) + (k*lutSize*lutSize)] = c[k*lutSize+i+j_*lutSize*lutSize];
                    }
                }
            }
            texture3d.SetPixels(newC);
            texture3d.Apply();
            return texture3d;
        }

        Texture3D MakeIdentityLut() {
            lutSize = 16;
            var newC = new Color[lutSize*lutSize*lutSize];
            float oneOverDim = 1.0f / (1.0f * lutSize - 1.0f);
            Texture3D texture3d = new Texture3D(sizeLUT, sizeLUT, sizeLUT, TextureFormat.ARGB32, false);
            
            for(int i = 0; i < lutSize; i++) {
                for(int j = 0; j < lutSize; j++) {
                    for(int k = 0; k < lutSize; k++) {
                        newC[i + (j*lutSize) + (k*lutSize*lutSize)] = new Color((i*1.0f)*oneOverDim, (j*1.0f)*oneOverDim, (k*1.0f)*oneOverDim, 1.0f);
                    }
                }
            }
            texture3d.SetPixels (newC);
            texture3d.Apply ();
            return texture3d;
        }

        public int LutSize
        {
            get{
                return lutSize;
            }
        }
    }
}