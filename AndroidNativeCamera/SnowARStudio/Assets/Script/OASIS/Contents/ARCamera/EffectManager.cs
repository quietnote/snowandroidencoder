﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{ 
    public class EffectManager : MonoBehaviour
    {
        [SerializeField]
        public bool useSmoothing;
        public float skinSmoothingWeight;
        public bool useLUT;
        public Texture2D lutFile;
        public float lutWeight;
        public float colorTemperatureWeight;

        ARCameraPreprocessing cameraPreprocessing;
        ColorTemperature colorTemperature;
        bool needUpdate = false;

        // Start is called before the first frame update
        void Start()
        {            
            cameraPreprocessing = GetComponent<ARCameraPreprocessing>();
            colorTemperature = GetComponent<ColorTemperature>();
            needUpdate = true;
        }

        // Update is called once per frame
        void Update()
        {
            if(needUpdate == false)
            {
                return;
            }

            if(cameraPreprocessing != null)
            {
                cameraPreprocessing.UseSmoothing = useSmoothing;
                cameraPreprocessing.SmoothingWeight = skinSmoothingWeight;
                cameraPreprocessing.UseLUT =  useLUT;
                cameraPreprocessing.InputLUT = lutFile;
                cameraPreprocessing.LUTWeight = lutWeight;
            }
            if(colorTemperature != null)
            {
                colorTemperature.Weight = colorTemperatureWeight;            
            }

            //needUpdate = false;    // 나중에 필요하면 활성화 예정, 파라메터가 변경 됐을 때만 update가 되도록...
        }

        public bool UseSmoothing
        {
            get { return useSmoothing; }
            set { needUpdate = true; useSmoothing = value; }
        }

        public float SkinSmoothingWeight
        {
            get { return skinSmoothingWeight; }
            set { needUpdate = true; skinSmoothingWeight = value; }           
        }

        public bool UseLUT
        {
            get { return useLUT; }
            set { needUpdate = true; useLUT = value; }
        }  

        public Texture2D LUTFile
        {
            get { return lutFile; }
            set { needUpdate = true; lutFile = value; }
        } 

        public float LutWeight
        {
            get { return lutWeight; }
            set { needUpdate = true; lutWeight = value; }           
        }

        public float ColorTemperatureWeight
        {
            get { return colorTemperatureWeight; }
            set { needUpdate = true; colorTemperatureWeight = value; }           
        }

    } 
}
