﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

namespace OASIS.Contents
{
    public class PostProcessingLUT : MonoBehaviour
    {
        [SerializeField]
        Texture2D maskImage;
        [SerializeField]
        Texture2D[] lutImage;
        [SerializeField]
        Vector3 weight = new Vector3(1.0f, 0, 0);

        MultiLayerLUT multiLayerLUT;
        String filterName;        

        int currentProfile = 0;
        Vector2Int uiBox = new Vector2Int(Screen.width/5, Screen.height/20);

        void Start()
        {
            multiLayerLUT = new MultiLayerLUT();            
            if(maskImage != null)
                multiLayerLUT.MaskImage = maskImage;
            if(lutImage[0] != null)
            {
                multiLayerLUT.InputLUT0 = lutImage[0];
            }
            multiLayerLUT.Weight = weight;
            filterName = multiLayerLUT.InputLUT0.name;
        }

        void Update()
        {
            multiLayerLUT.Weight = weight;
        }

        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            multiLayerLUT.Render(source, destination);
        }

        void OnGUI()        // for Test
        {      
            GUIStyle customButton = new GUIStyle("button");
            customButton.fontSize = Screen.width/30;
            if (GUI.Button (new Rect ((Screen.width-uiBox.x)/2, Screen.height*0.9f-uiBox.y, uiBox.x, uiBox.y), filterName, customButton))
            {
                SetProfile(++currentProfile);
            }
        }

        void SetProfile(int index)
        {
            if(lutImage.Length <= index)
            {   
                currentProfile = index = 0;
            }        
            multiLayerLUT.InputLUT0 = lutImage[index];
            filterName = lutImage[index].name;
            multiLayerLUT.UpdateLUT();
        }
    }
}