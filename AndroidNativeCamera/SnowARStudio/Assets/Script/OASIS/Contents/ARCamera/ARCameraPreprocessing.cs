﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace OASIS.Contents
{
    [RequireComponent(typeof(Camera))]
    public class ARCameraPreprocessing : MonoBehaviour
    {
        Camera m_TargetCamera = null;
        AR.CameraFrame m_CurrentFrame = new AR.CameraFrame();
        AR.CameraFrame m_LastFrame = new AR.CameraFrame();
        ARCameraBackground m_arCameraBackground;

        Material m_BlitGeometric = null;
        [SerializeField]
        bool m_UpdateCamera = false;
        bool m_CurrentUpdateCamera = false;

        // for SkinSmoothing and LUT for pre-processing by Cloud
        [SerializeField]
        bool m_UseSkinsmoothing = true;
        [SerializeField]
        RenderTexture m_rtSmoothingMask;
        [SerializeField]
        float m_SmoothingWeight = 0.8f;
        Texture2D m_imgSmoothingMask;

        [SerializeField]
        bool m_UseLUT = true;
        [SerializeField]
        RenderTexture m_rtLutMask;
        [SerializeField]
        Texture2D m_InputLUT;
        [SerializeField]
        float m_LUTWeight = 1.0f;

        Texture2D m_imgLutMask;
        string m_InputLUTname;
        Texture3D m_Converted3DLut;
        int m_DefaultLUTSize = 64;

        Material m_matHorizontal;
        Material m_matVertical;
        Material m_matSkinPostprocess;

        RenderTexture m_rtBlur;
        RenderTexture m_rtBlur2;
        RenderTexture m_rtResize;
        RenderTexture m_rtInputCopy;
        RenderTexture m_rtOutput;

        CommandBuffer m_CommandBuffer;

        int textureWidth = 0;
        int textureHeight = 0;

        float m_CurrentSmoothingWeight = 0;
        float m_CurrentLUTWeight = 0;
        bool m_CurrentSkinsmoothing;
        bool m_CurrentLUT;        
        bool m_Reset = false;
        bool flagSwap = false;

        bool flagNoCommandBuffer = false;

        void Awake()
        {
            m_TargetCamera = GetComponent<Camera>();
            m_arCameraBackground = GetComponent<ARCameraBackground>();

            m_matHorizontal = Resources.Load<Material>("Raws/Materials/BilateralHorizontal");
            m_matVertical = Resources.Load<Material>("Raws/Materials/BilateralVertical");
            m_matSkinPostprocess = Resources.Load<Material>("Raws/Materials/Concat_smoothing_fss");
            m_imgSmoothingMask = Resources.Load<Texture2D>("Raws/Textures/white");
            m_imgLutMask = Resources.Load<Texture2D>("Raws/Textures/white");

            if(m_InputLUT == null)
                m_InputLUT = Resources.Load<Texture2D>("Raws/Textures/lut_original");

            m_CurrentSkinsmoothing = m_UseSkinsmoothing;
            m_CurrentLUT = m_UseLUT;
            m_CurrentSmoothingWeight = m_SmoothingWeight;
            m_CurrentLUTWeight = m_LUTWeight;
            m_InputLUTname = m_InputLUT.name;
            PrepareLUT();

            Shader blitShader = Shader.Find("Hidden/BackgroundShader");
            m_BlitGeometric = new Material(blitShader);
            ARCameraBackground.UVMatrix transform = ARCameraBackground.UVMatrix.identity;
            m_BlitGeometric.SetVector("_UVTransform", transform.GetUVTransform());
            m_BlitGeometric.SetFloat("_UOffset", transform.GetUOffset());
            m_BlitGeometric.SetFloat("_VOffset", transform.GetVOffset());            
        }

        private void OnEnable()
        {
            float screenAspect = (float)Screen.width / (float)Screen.height;
            if (ACApp.instance && ACApp.instance.data)
            {
                m_CurrentFrame = ACApp.instance.GetFrame();
                m_Reset = true;
            }
        }

        private void OnDisable()
        {
            RemovePreProcessingCommandBuffer();
        }

        // Update is called once per frame
        void Update()
        {
            bool ShouldReset = false;

            if(m_arCameraBackground == null)
            {
                return;
            }

            if (ACApp.instance)
            {
                //int faceCount = ACApp.instance.GetFaceCount();
               // Debug.Log("faceCount = " + faceCount);
                
                m_CurrentFrame = ACApp.instance.GetFrame();
                if (m_CurrentFrame.cameraTextureRef != null && m_CommandBuffer == null)
                {
                    ShouldReset = true;
                }
                else if (!m_LastFrame.Equals(m_CurrentFrame))
                {
                    m_LastFrame = m_CurrentFrame;
                    ShouldReset = true;
                }

                if(!flagSwap && (textureWidth != m_CurrentFrame.cameraTextureRef.width || textureHeight != m_CurrentFrame.cameraTextureRef.height))
                {                    
                    ShouldReset = true;                    
                }
                else if(flagSwap && (textureWidth != m_CurrentFrame.cameraTextureRef.height || textureHeight != m_CurrentFrame.cameraTextureRef.width))
                {
                    ShouldReset = true;
                }

                if(m_UseSkinsmoothing != m_CurrentSkinsmoothing || m_UseLUT != m_CurrentLUT)
                {
                    m_CurrentSkinsmoothing = m_UseSkinsmoothing;
                    m_CurrentLUT = m_UseLUT;

                    ShouldReset = true;
                }
                if(m_CurrentUpdateCamera != m_UpdateCamera)
                {
                    m_CurrentUpdateCamera = m_UpdateCamera;
                    ShouldReset = true;
                }

                CheckAndUpdateLUTProperty();
                CheckAndUpdateSkinSmoothingProperty();
            }

            // for testing, update command buffer everytime, shoud be changed soon.
            if(ShouldReset || m_Reset)
            { 
                if(m_InputLUTname != m_InputLUT.name)
                {
                    m_InputLUTname = m_InputLUT.name;
                    PrepareLUT();
                }
                RemovePreProcessingCommandBuffer();
                if(m_CommandBuffer == null)
                {   
                    m_CommandBuffer = new CommandBuffer();
                }
                MakePreProcessingCommandBuffer();
                m_TargetCamera.AddCommandBuffer(CameraEvent.BeforeForwardOpaque, m_CommandBuffer);
                m_TargetCamera.AddCommandBuffer(CameraEvent.BeforeGBuffer, m_CommandBuffer);

                m_Reset = false;
            }

            if(flagNoCommandBuffer)
            {
                RenderNewFitTexture();
            }

            if(m_rtOutput != null)
            {
                ACApp.instance.data.frame.cameraFitTextureRef = m_rtOutput;
            }
        }


        void RemovePreProcessingCommandBuffer()
        {
            if(m_CommandBuffer == null)
                return;

            m_TargetCamera.RemoveCommandBuffer(CameraEvent.BeforeForwardOpaque, m_CommandBuffer);
            m_TargetCamera.RemoveCommandBuffer(CameraEvent.BeforeGBuffer, m_CommandBuffer);
            m_CommandBuffer = null;
        }

        void RenderNewFitTexture()
        {
            if(flagSwap)
            {
                textureWidth = m_CurrentFrame.cameraTextureRef.height;
                textureHeight = m_CurrentFrame.cameraTextureRef.width;
            }
            else
            {
                textureWidth = m_CurrentFrame.cameraTextureRef.width;
                textureHeight = m_CurrentFrame.cameraTextureRef.height;
            }
            ResetTextures();
            MakeGeometricTransform();

            if(m_CommandBuffer == null)
                return;
            m_CommandBuffer.Blit(m_CurrentFrame.cameraTextureRef, m_rtInputCopy, m_BlitGeometric);

            if(m_rtSmoothingMask != null)        // render texture를 우선으로 한다.
                m_matHorizontal.SetTexture("_SmoothingMaskTex", m_rtSmoothingMask); 
            else
            {
                m_matHorizontal.SetTexture("_SmoothingMaskTex", m_imgSmoothingMask);
            }
            // Resize to half resolution
            m_CommandBuffer.Blit(m_rtInputCopy, m_rtResize, Vector2.one, Vector2.zero);
            // Horizontal Bilateral Blur Process
            m_matHorizontal.SetTexture("_MainTex", m_rtResize);        
            m_CommandBuffer.Blit(m_rtResize, m_rtBlur, m_matHorizontal, 0);
            // Vertical Bilateral Blur Process
            m_matVertical.SetTexture("_MainTex", m_rtBlur);
            m_CommandBuffer.Blit(m_rtBlur, m_rtBlur2, m_matVertical, 0);  
            // Skin PostProcessing
            m_matSkinPostprocess.SetTexture("_MainTex", m_rtInputCopy);
            m_matSkinPostprocess.SetTexture("_BlurTexture", m_rtBlur2);
            m_matSkinPostprocess.SetTexture("_LUTMaskTex", m_rtLutMask);
            m_matSkinPostprocess.SetFloat("_uOpacity", m_SmoothingWeight);
            m_matSkinPostprocess.SetInt("_useSmoothing", m_UseSkinsmoothing?1:0);

            // LUT Process
            m_matSkinPostprocess.SetTexture("_LUTMaskTex", m_rtLutMask);
            m_matSkinPostprocess.SetTexture("_LUT0", m_Converted3DLut);                
            m_matSkinPostprocess.SetTexture("_LUT1", m_Converted3DLut);
            m_matSkinPostprocess.SetTexture("_LUT2", m_Converted3DLut);  
            m_matSkinPostprocess.SetFloat("_weightLayer0", m_LUTWeight);  
            m_matSkinPostprocess.SetFloat("_weightLayer1", m_LUTWeight);
            m_matSkinPostprocess.SetFloat("_weightLayer2", m_LUTWeight);  
            m_matSkinPostprocess.SetInt("_useLUT", m_UseLUT?1:0);

            m_CommandBuffer.Blit(m_rtInputCopy, m_rtOutput, m_matSkinPostprocess, 0);
            if(m_UpdateCamera)
                m_CommandBuffer.Blit(m_rtOutput, BuiltinRenderTextureType.CameraTarget, Vector2.one, Vector2.zero);
        }

        void MakePreProcessingCommandBuffer()
        {
            if(flagSwap)
            {
                textureWidth = m_CurrentFrame.cameraTextureRef.height;
                textureHeight = m_CurrentFrame.cameraTextureRef.width;
            }
            else
            {
                textureWidth = m_CurrentFrame.cameraTextureRef.width;
                textureHeight = m_CurrentFrame.cameraTextureRef.height;
            }
            ResetTextures();
            MakeGeometricTransform();

            if(m_CommandBuffer == null)
                return;
            m_CommandBuffer.Blit(m_CurrentFrame.cameraTextureRef, m_rtInputCopy, m_BlitGeometric);

            if(m_rtSmoothingMask != null)        // render texture를 우선으로 한다.
                m_matHorizontal.SetTexture("_SmoothingMaskTex", m_rtSmoothingMask); 
            else
            {
                m_matHorizontal.SetTexture("_SmoothingMaskTex", m_imgSmoothingMask);
            }
            // Resize to half resolution
            m_CommandBuffer.Blit(m_rtInputCopy, m_rtResize, Vector2.one, Vector2.zero);
            // Horizontal Bilateral Blur Process
            m_matHorizontal.SetTexture("_MainTex", m_rtResize);        
            m_CommandBuffer.Blit(m_rtResize, m_rtBlur, m_matHorizontal, 0);
            // Vertical Bilateral Blur Process
            m_matVertical.SetTexture("_MainTex", m_rtBlur);
            m_CommandBuffer.Blit(m_rtBlur, m_rtBlur2, m_matVertical, 0);  
            // Skin PostProcessing
            m_matSkinPostprocess.SetTexture("_MainTex", m_rtInputCopy);
            m_matSkinPostprocess.SetTexture("_BlurTexture", m_rtBlur2);
            m_matSkinPostprocess.SetTexture("_LUTMaskTex", m_rtLutMask);
            m_matSkinPostprocess.SetFloat("_uOpacity", m_SmoothingWeight);
            m_matSkinPostprocess.SetInt("_useSmoothing", m_UseSkinsmoothing?1:0);

            // LUT Process
            m_matSkinPostprocess.SetTexture("_LUTMaskTex", m_rtLutMask);
            m_matSkinPostprocess.SetTexture("_LUT0", m_Converted3DLut);                
            m_matSkinPostprocess.SetTexture("_LUT1", m_Converted3DLut);
            m_matSkinPostprocess.SetTexture("_LUT2", m_Converted3DLut);  
            m_matSkinPostprocess.SetFloat("_weightLayer0", m_LUTWeight);  
            m_matSkinPostprocess.SetFloat("_weightLayer1", m_LUTWeight);
            m_matSkinPostprocess.SetFloat("_weightLayer2", m_LUTWeight);  
            m_matSkinPostprocess.SetInt("_useLUT", m_UseLUT?1:0);

            m_CommandBuffer.Blit(m_rtInputCopy, m_rtOutput, m_matSkinPostprocess, 0);
            if(m_UpdateCamera)
                m_CommandBuffer.Blit(m_rtOutput, BuiltinRenderTextureType.CameraTarget, Vector2.one, Vector2.zero);
        }

        void MakeGeometricTransform()
        {
            ARCameraBackground.UVMatrix transform = ARCameraBackground.UVMatrix.identity;
            float backgroundAspect = m_CurrentFrame.virtualWidth / m_CurrentFrame.virtualHeight;
            flagSwap = false;

            if (m_arCameraBackground.CurrentScreenAspectRatio > backgroundAspect)
            {
                transform.LeftMultiply(ARCameraBackground.UVMatrix.verticalRescale(backgroundAspect / m_arCameraBackground.CurrentScreenAspectRatio));
            }
            else
            {
                transform.LeftMultiply(ARCameraBackground.UVMatrix.horizontalRescale(m_arCameraBackground.CurrentScreenAspectRatio / backgroundAspect));
            }

            if (m_CurrentFrame.shouldLeftRightFlip)
            {
                transform.LeftMultiply(ARCameraBackground.UVMatrix.leftRightFlip);
            }
            switch (m_CurrentFrame.shouldRotateBy)
            {
                case (AR.TextureRotationCommand.kClockwise90):
                    flagSwap = true;
                    transform.LeftMultiply(ARCameraBackground.UVMatrix.rotate90Clockwise);
                    break;
                case (AR.TextureRotationCommand.kClockwise180):
                    transform.LeftMultiply(ARCameraBackground.UVMatrix.rotate180Clockwise);
                    break;
                case (AR.TextureRotationCommand.kClockwise270):
                    flagSwap = true;
                    transform.LeftMultiply(ARCameraBackground.UVMatrix.rotate270Clockwise);
                    break;
                default:
                    break;
            }
            if (m_CurrentFrame.shouldUpDownFlip)
            {
                transform.LeftMultiply(ARCameraBackground.UVMatrix.upDownFlip);
            }
            if (!m_UpdateCamera && m_CurrentFrame.shouldLeftRightFlip && !m_CurrentFrame.shouldUpDownFlip)     // flip for fitting sensetime input buffer direction
            {
                transform.LeftMultiply(ARCameraBackground.UVMatrix.leftRightFlip);
            }

            m_BlitGeometric.SetVector("_UVTransform", transform.GetUVTransform());

#if !UNITY_EDITOR
            m_BlitGeometric.SetFloat("_UOffset", transform.GetUOffset());
            m_BlitGeometric.SetFloat("_VOffset", transform.GetVOffset());
#else
            if(m_UpdateCamera)
            {
                m_BlitGeometric.SetFloat("_UOffset", transform.GetUOffset());
                m_BlitGeometric.SetFloat("_VOffset", transform.GetVOffset());
            }
            else
            {
                m_BlitGeometric.SetFloat("_UOffset", 0);
                m_BlitGeometric.SetFloat("_VOffset", 0);
            }
#endif
        }

        void PrepareLUT()
        {    
            LUTConverter3D lutConverter = new LUTConverter3D(); 
          
            Texture3D temp = lutConverter.Convert(m_InputLUT);
            m_DefaultLUTSize = lutConverter.LutSize;
            if(m_Converted3DLut != null)
                Destroy(m_Converted3DLut);                    
            m_Converted3DLut = new Texture3D (m_DefaultLUTSize, m_DefaultLUTSize, m_DefaultLUTSize, TextureFormat.ARGB32, false);
            m_Converted3DLut.wrapMode = TextureWrapMode.Clamp;
            if(m_Converted3DLut != null)
                Graphics.CopyTexture(temp, m_Converted3DLut);

        }

        void CheckAndUpdateLUTProperty()
        {
            if(m_InputLUTname != m_InputLUT.name)
            {
                m_InputLUTname = m_InputLUT.name;
                PrepareLUT();

                if(m_matSkinPostprocess != null)
                {
                    m_matSkinPostprocess.SetTexture("_LUT0", m_Converted3DLut);                
                    m_matSkinPostprocess.SetTexture("_LUT1", m_Converted3DLut);
                    m_matSkinPostprocess.SetTexture("_LUT2", m_Converted3DLut);  
                }
            }

            if(m_CurrentLUTWeight != m_LUTWeight)
            {
                if(m_matSkinPostprocess != null)
                {                    
                    m_CurrentLUTWeight = m_LUTWeight;
                    m_matSkinPostprocess.SetFloat("_weightLayer0", m_LUTWeight);  
                    m_matSkinPostprocess.SetFloat("_weightLayer1", m_LUTWeight);
                    m_matSkinPostprocess.SetFloat("_weightLayer2", m_LUTWeight);  
                }               
            }
        }

        void CheckAndUpdateSkinSmoothingProperty()
        {
            if(m_CurrentSmoothingWeight != m_SmoothingWeight)
            {
                m_CurrentSmoothingWeight = m_SmoothingWeight;                   
                if(m_matSkinPostprocess != null)
                    m_matSkinPostprocess.SetFloat("_uOpacity", m_SmoothingWeight);
            }
        }

        void ResetTextures()
        {
            if(m_rtInputCopy != null)
            {
                RenderTexture.ReleaseTemporary(m_rtInputCopy);
                m_rtInputCopy = null;
            }
            m_rtInputCopy = RenderTexture.GetTemporary(textureWidth, textureHeight);

            if(m_rtOutput != null)
            {
                m_rtOutput.Release();
                m_rtOutput = null;
            }
            m_rtOutput = new RenderTexture(textureWidth, textureHeight, 0, RenderTextureFormat.ARGB32);

            if(m_rtResize != null)
            {
                RenderTexture.ReleaseTemporary(m_rtResize);
                m_rtResize = null;
            }
            m_rtResize = RenderTexture.GetTemporary(textureWidth/2, textureHeight/2);

            if(m_rtBlur && (m_rtBlur.width != textureWidth/2 || m_rtBlur.height != textureHeight/2))
            {
                RenderTexture.ReleaseTemporary(m_rtBlur);
                m_rtBlur = RenderTexture.GetTemporary(textureWidth/2, textureHeight/2);
            }
            else if(m_rtBlur == null)
            {
                m_rtBlur = RenderTexture.GetTemporary(textureWidth/2, textureHeight/2);
            }

            if(m_rtBlur2 && (m_rtBlur2.width != textureWidth/2 || m_rtBlur2.height != textureHeight/2))
            {
                RenderTexture.ReleaseTemporary(m_rtBlur2);
                m_rtBlur2 = RenderTexture.GetTemporary(textureWidth/2, textureHeight/2);
            }
            else if(m_rtBlur2 == null)
            {
                m_rtBlur2 = RenderTexture.GetTemporary(textureWidth/2, textureHeight/2);
            }
        }

        public bool UseSmoothing
        {
            get { return m_UseSkinsmoothing; }
            set { m_UseSkinsmoothing = value; }
        }

        public float SmoothingWeight
        {
            get { return m_SmoothingWeight; }
            set { m_SmoothingWeight = value; }
        }

        public bool UseLUT{
            get { return m_UseLUT; }
            set { m_UseLUT = value; }
        }

        public Texture2D InputLUT
        {
            get { return m_InputLUT; }
            set { m_InputLUT = value; }
        }

        public float LUTWeight{
            get { return m_LUTWeight; }
            set { m_LUTWeight = value; }
        }
    }
}
