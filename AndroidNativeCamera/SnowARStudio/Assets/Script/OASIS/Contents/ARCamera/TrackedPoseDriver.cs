﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    public class TrackedPoseDriver : MonoBehaviour
    {
        Transform targetTransform;

        private void Awake()
        {
            targetTransform = GetComponent<Transform>();
        }

        Vector3 m_LastLocation = Vector3.zero;
        Vector3 m_CurrentLocation = Vector3.zero;
        Quaternion m_LastRotation = Quaternion.identity;
        Quaternion m_CurrentRotation = Quaternion.identity;

        void Update()
        {
            if (ACApp.instance)
            {
                m_CurrentLocation = ACApp.instance.GetDevicePosition();
                m_CurrentRotation = ACApp.instance.GetDeviceRotation();

                if (m_CurrentLocation != m_LastLocation || m_CurrentRotation != m_LastRotation)
                {
                    // Setting transform is not cheap since it is accessed by many subsystems possibly from jobs
                    targetTransform.localPosition = m_CurrentLocation;
                    targetTransform.localRotation = m_CurrentRotation;
                    m_LastLocation = m_CurrentLocation;
                    m_LastRotation = m_CurrentRotation;
                }
            }
        }
    }
}