﻿using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    public class BindingTargetProperty : MonoBehaviour
    {
        [SerializeField]
        TransformPoint[] points = null;
        Dictionary<TransformPoint.PointType, TransformPoint> alignedPoints = new Dictionary<TransformPoint.PointType, TransformPoint>();

        void Awake()
        {
            foreach (TransformPoint point in points) {
                alignedPoints.Add(point.type, point);
            }
            MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer) {
                meshRenderer.enabled = false;
            }
        }

        public float GetWidth() {
            return Vector3.Distance(alignedPoints[TransformPoint.PointType.Left].transform.position, alignedPoints[TransformPoint.PointType.Right].transform.position);
        }

        public float GetHeight()
        {
            return Vector3.Distance(alignedPoints[TransformPoint.PointType.Top].transform.position, alignedPoints[TransformPoint.PointType.Bottom].transform.position);
        }

        public Vector3 Center {
            get {
                return transform.position;
            }
        }
    }
}
