﻿using UnityEngine;
using Snow.G;

namespace OASIS.Contents
{
    public class JsonParser : MonoBehaviour
    {
        [SerializeField]
        TextAsset inJson = null;

        public void ReadJson()
        {
            ResultContainer<DownloadedSticker> downloadedSticker = ParsingJson(inJson.text);
            if (downloadedSticker.IsEmpty()) {
                return;
            }
            foreach (StickerItem item in downloadedSticker.result.items) {
                RendererFactory.Create(item);
            }
        }

        ResultContainer<DownloadedSticker> ParsingJson(string json)
        {
            return JsonUtils.FromJson<ResultContainer<DownloadedSticker>>(json);
        }
    }
}