﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace OASIS.Contents
{
    public class DevSceneLoader : MonoBehaviour
    {
        void Awake()
        {
#if UNITY_EDITOR
            if (SceneManager.sceneCount == 1)
            {
                SceneManager.LoadScene("Temp Dev Env", LoadSceneMode.Additive);
            }
#endif
        }
    }
}