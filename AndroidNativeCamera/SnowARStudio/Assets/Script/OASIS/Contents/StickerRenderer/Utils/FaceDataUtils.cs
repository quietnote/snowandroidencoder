﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    public class FaceDataUtils
    {
        public enum Options {
            None        = 0x00,
            Others      = 0x01,
            Lips        = 0x02,
            Mouth       = 0x04,
            Eyes        = 0x08,
            Face        = 0x0F,
            FaceSkin    = 0x0B,
            ExtraFaceInside   = 0x10,
            ExtraFaceOutside  = 0x20,
            ExtraFace         = 0x40,
            ExtraFull         = 0xF0,
        }

        static int[] lastTriangles;
        static Options lastOption;

        public static void SetFaceIndexTriangles(out int[] triangles, Options option)
        {
            if (lastOption == option) {
                triangles = lastTriangles;
                return;
            }
            triangles = null;
            List<int> triangleList = new List<int>();
            if ((((int)option) & ((int)Options.Others)) > 0) {
                int[] points = {
                    28, 39, 29, 40, 29, 39, 29, 40, 31, 41, 31, 40, 31, 41,  1, 36,  1, 41,
                     1, 36,  0,  0, 36, 17, 17, 36, 18, 37, 18, 36, 18, 37, 19, 38, 19, 37,
                    19, 38, 20, 39, 20, 38, 20, 39, 21, 39, 28, 27, 39, 27, 21, 16, 45, 15,
                    46, 15, 45, 15, 46, 35, 47, 35, 46, 35, 47, 29, 42, 29, 47, 29, 42, 28,
                    22, 27, 42, 28, 42, 27, 22, 42, 23, 43, 23, 42, 23, 43, 24, 44, 24, 43,
                    24, 44, 25, 45, 25, 44, 25, 45, 26, 26, 45, 16, 27, 22, 21, 29, 31, 30,
                    32, 30, 31, 32, 33, 30, 33, 34, 30, 34, 35, 30, 30, 35, 29, 53, 54, 35,
                    53, 35, 52, 34, 52, 35, 52, 34, 51, 33, 51, 34, 51, 33, 50, 32, 50, 33,
                    50, 32, 49, 31, 49, 32, 49, 31, 48,  8, 57,  7, 58,  7, 57,  7, 58,  6,
                    59,  6, 58,  6, 59,  5, 48,  5, 59,  5, 48,  4, 31,  4, 48,  4, 31,  3,
                    13, 35, 12, 54, 12, 35, 12, 54, 11, 55, 11, 54, 11, 55, 10, 56, 10, 55,
                    10, 56,  9, 57,  9, 56,  9, 57,  8, 35, 13, 14, 35, 14, 15, 31,  2,  3,
                    31,  1,  2
                };
                AddArrayAtList(triangleList, points);
            }
            if ((((int)option) & (((int)Options.Lips) | ((int)Options.Mouth))) > 0) {
                int[] points = {
                    48, 59, 65, 58, 65, 59, 65, 58, 64, 57, 64, 58, 54, 63, 55, 64, 55, 63,
                    55, 64, 56, 57, 56, 64, 48, 60, 49, 61, 49, 60, 49, 61, 50, 51, 50, 61,
                    54, 53, 62, 52, 62, 53, 62, 52, 61, 51, 61, 52
                };
                AddArrayAtList(triangleList, points);
            }
            if ((((int)option) & ((int)Options.Mouth)) > 0) {
                int[] points = {
                    54, 62, 63, 62, 61, 63, 63, 61, 64, 64, 61, 65, 61, 60, 65, 60, 48, 65,
                };
                AddArrayAtList(triangleList, points);
            }
            if ((((int)option) & ((int)Options.Eyes)) > 0) {
                int[] points = {
                    //Left
                    42, 47, 43, 46, 43, 47, 43, 46, 44, 45, 44, 46,
                    //Right
                    36, 41, 37, 40, 37, 41, 37, 40, 38, 39, 38, 40         
                };
                AddArrayAtList(triangleList, points);
            }
            if ((((int)option) & ((int)Options.ExtraFaceInside)) > 0) {
                int[] points = {
                    72, 21, 22,
                    72, 73, 21,
                    //-- 왼쪽 이마 10
                    20, 21, 73, 20, 73, 74, 19, 20, 74,
                    19, 74, 75, 18, 19, 75, 18, 75, 76,
                    18, 76, 77, 17, 18, 77, 17, 77, 78,

                    //-- 오른쪽 이마 10
                    22, 23, 72, 23, 71, 72, 23, 24, 71,
                    24, 70, 71, 24, 25, 70, 25, 69, 70,
                    25, 26, 68, 25, 68, 69, 26, 67, 68,

                    //-- 왼쪽눈
                    39, 38, 40, 40, 38, 37, 40, 37, 41, 41, 37, 36,

                    //-- 오른쪽눈
                    42, 47, 43, 43, 47, 44, 44, 47, 46, 44, 46, 45,

                    22, 21, 27,
            
                    //-- 눈섭
                    //-- 왼쪽 눈섭 위 11
                    27, 21, 39, 21, 38, 39, 21, 20, 38,
                    20, 19, 38, 19, 37, 38, 19, 18, 37,
                    18, 36, 37, 18, 17, 36, 17, 0, 36,
                    0, 17, 78,

                    //-- 오른쪽 눈섭 위 11
                    22, 27, 42, 22, 42, 43, 22, 43, 23,
                    23, 43, 44, 24, 23, 44, 25, 24, 44,
                    25, 44, 45, 26, 25, 45, 16, 26, 45,
                    26, 16, 67,

                    //-- 왼쪽 눈섭 아래   13
                    28, 27, 39, 29, 28, 31, 30, 29, 31,
                    30, 31, 32, 30, 32, 33, 28, 39, 31,
                    31, 39, 40, 31, 40, 41, 1, 31, 41,
                    36, 1, 41, 0, 1, 36, 1, 2, 31,
                    2, 3, 31,

                    //-- 오른쪽 눈섭 아래   13
                    27, 28, 42, 28, 29, 35, 29, 30, 35,
                    28, 35, 42, 30, 34, 35, 30, 33, 34,
                    42, 35, 47, 35, 46, 47, 15, 16, 45,
                    35, 15, 46, 15, 45, 46, 14, 15, 35,
                    13, 14, 35,

                    //-- 왼쪽 코 밑 7
                    3, 4, 31, 31, 4, 48, 31, 48, 49, 32, 31, 49, 32, 49, 50, 33, 32, 50, 33, 50, 51,

                    //-- 오른쪽 코 밑 7
                    12, 13, 35, 54, 12, 35, 35, 53, 54, 35, 34, 53, 34, 52, 53, 34, 33, 52, 33, 51, 52,
    
                    //-- 왼쪽 입술 8
                    51, 50, 61, 50, 60, 61, 50, 49, 60, 49, 48, 60, 48, 59, 65, 59, 58, 65, 58, 64, 65, 58, 57, 64,
    
                    //-- 오른쪽 입술 8
                    52, 51, 61, 52, 61, 62, 53, 52, 62, 54, 53, 62, 55, 54, 63, 56, 55, 63, 56, 63, 64, 57, 56, 64,

                    //-- 왼쪽 턱 7
                    4, 5, 48, 48, 5, 59, 5, 6, 59, 6, 58, 59, 6, 7, 58, 7, 57, 58, 7, 8, 57,
                    //-- 오른쪽 턱 7
                    11, 12, 54, 11, 54, 55, 10, 11, 55, 10, 55, 56, 9, 10, 56, 9, 56, 57, 8, 9, 57,
                    //-- 입안
                    54, 62, 63, 62, 61, 63, 63, 61, 64, 64, 61, 65, 61, 60, 65, 60, 48, 65,
                };
                AddArrayAtList(triangleList, points);
            }
            if ((((int)option) & ((int)Options.ExtraFaceOutside)) > 0) {
                int[] points = {
                    72, 101, 73, 73, 101, 102, 73, 102, 103,
                    73, 103, 74, 74, 103, 104, 74, 104, 75,
                    75, 104, 105, 75, 105, 76, 76, 105, 106,
                    76, 106, 77, 77, 106, 107, 77, 107, 78,
                    78, 107, 79, 78, 79, 0, 0, 79, 80,
                    0, 80, 1, 1, 80, 81, 1, 81, 2,
                    2, 81, 82, 2, 82, 3, 3, 82, 83,
                    3, 83, 4, 4, 83, 84, 4, 84, 5,
                    5, 84, 85, 5, 85, 6, 6, 85, 86,
                    6, 86, 7, 7, 86, 87, 7, 87, 8,
                    8, 87, 9, 9, 87, 88, 9, 88, 10,
                    10, 88, 89, 10, 89, 11, 11, 89, 90,
                    11, 90, 12, 12, 90, 91, 12, 91, 13,
                    13, 91, 92, 13, 92, 14, 14, 92, 93,
                    14, 93, 15, 15, 93, 94, 15, 94, 16,
                    16, 94, 95, 16, 95, 67, 67, 95, 96,
                    67, 96, 68, 68, 96, 97, 68, 97, 69,
                    69, 97, 98, 69, 98, 70, 70, 98, 99,
                    70, 99, 71, 71, 99, 100, 71, 100, 72,
                    72, 100, 101,
                };
                AddArrayAtList(triangleList, points);
            }
            if (option != Options.ExtraFull && (((int)option) & ((int)Options.ExtraFace)) > 0) {
                int[] points = {
                    66, 77, 78, 66, 78, 0, 66, 0, 1,
                    66, 1, 2, 66, 2, 3, 66, 3, 4,
                    66, 4, 5, 66, 5, 6, 66, 6, 7,
                    66, 7, 8, 66, 8, 9, 66, 9, 10,
                    66, 10, 11, 66, 11, 12, 66, 12, 13,
                    66, 13, 14, 66, 14, 15, 66, 15, 16,
                    66, 16, 67, 66, 67, 68,
                };
                AddArrayAtList(triangleList, points);
            }

            triangles = triangleList.ToArray();
            lastTriangles = triangles;
            lastOption = option;
        }

        static void AddArrayAtList(List<int> list, int[] values)
        {
            foreach (int value in values) {
                list.Add(value);
            }
        }

        public static int[] ConvertTrianglesStripToTriangles(int[] vertices)
        {
            List<int> triangleVertices = new List<int>();

            triangleVertices.Add(vertices[0]);
            triangleVertices.Add(vertices[1]);
            triangleVertices.Add(vertices[2]);
            for (int i = 3; i < vertices.Length; ++i)
            {
                triangleVertices.Add(vertices[i - 2]);
                triangleVertices.Add(vertices[i - 1]);
                triangleVertices.Add(vertices[i]);
            }

            //Change Clock wise -> Clock
            List<int> output = new List<int>();
            int count = 0;
            for (int i = 0; i < triangleVertices.Count; i = i + 3)
            {
                if ((triangleVertices[i] == triangleVertices[i + 1]) || (triangleVertices[i + 1] == triangleVertices[i + 2]))
                {
                    count = 0;
                    continue;
                }
                if (++count % 2 == 1)
                {
                    output.Add(triangleVertices[i]);
                    output.Add(triangleVertices[i + 1]);
                    output.Add(triangleVertices[i + 2]);
                }
                else
                {
                    output.Add(triangleVertices[i + 2]);
                    output.Add(triangleVertices[i + 1]);
                    output.Add(triangleVertices[i]);
                }
            }

            // DUMP
            // string dump = "";
            // for (int i = 0; i < output.Count; ++i)
            // {
            //     dump += string.Format("{0}, ", output[i]);
            // }
            // FileUtils.WriteString("CoordinatesIndex.txt", dump);

            return output.ToArray();
        }

        public static Vector2[] ConvertCoordsToVector2(float[] coords)
        {
            Vector2[] output = new Vector2[coords.Length / 2];
            for (int i = 0; i < output.Length; ++i) {
                output[i] = new Vector2(coords[2 * i], coords[2 * i + 1]);
            }

            //DUMP
            // string dump = "";
            // for (int i = 0; i < output.Length; ++i)
            // {
            //     dump += string.Format("new Vector2({0}f,{1}f), ", output[i].x, output[i].y);
            // }
            // FileUtils.WriteString("Coordinates.txt", dump);

            return output;
        }

        public static Vector2[] ConvertCoordsToVector2(string[] coords)
        {
            Vector2[] output = new Vector2[coords.Length / 2];
            for (int i = 0; i < output.Length; ++i) {
                output[i] = new Vector2(float.Parse(coords[2 * i]), float.Parse(coords[2 * i + 1]));
            }

            //DUMP
            // string dump = "";
            // for (int i = 0; i < output.Length; ++i)
            // {
            //     dump += string.Format("new Vector2({0}f,{1}f), ", output[i].x, output[i].y);
            // }
            // FileUtils.WriteString("Coordinates.txt", dump);

            return output;
        }

        public static Vector2[] GetTextureUV(Options option)
        {
            return (((int)option) & ((int)Options.Face)) > 0 ? DefaultTextureUV : ExtraTextureUV;
        }

        public static Vector2[] DefaultTextureUV {
            get {
                return ConvertCoordsToVector2(DEFAULT_TEXTURE_COORDS);
            }
        }

        public static Vector2[] ExtraTextureUV {
            get {
                return ConvertCoordsToVector2(EXTRA_TEXTURE_COORDS);
            }
        }

        static float[] DEFAULT_TEXTURE_COORDS = {
            0.056f, 0.437f, 0.06f, 0.525f, 0.08133333f, 0.615f, 0.1133333f, 0.705f, 0.176f, 0.795f, 0.248f, 0.862f,
            0.3413333f, 0.912f, 0.4146667f, 0.942f, 0.5f, 0.957f, 0.5893334f, 0.943f, 0.661698f, 0.900046f,
            0.7666667f, 0.844f, 0.8386667f, 0.777f, 0.8813334f, 0.696f, 0.9146667f, 0.599f, 0.9386666f, 0.512f,
            0.9493333f, 0.448f, 0.1053333f, 0.16f, 0.1826667f, 0.042f, 0.248f, 0.047f, 0.3f, 0.051f, 0.368f, 0.057f,
            0.6013333f, 0.069f, 0.6666667f, 0.062f, 0.7373334f, 0.06f, 0.7986667f, 0.062f, 0.876f, 0.176f,
            0.5013334f, 0.485f, 0.5f, 0.537f, 0.5013334f, 0.589f, 0.504f, 0.635f, 0.4346667f, 0.689f, 0.4693333f,
            0.694f, 0.5066667f, 0.699f, 0.54f, 0.697f, 0.5773333f, 0.687f, 0.2293333f, 0.458f, 0.2626667f, 0.441f,
            0.34f, 0.444f, 0.3773333f, 0.464f, 0.3373333f, 0.481f, 0.264f, 0.482f, 0.624f, 0.466f, 0.6573333f,
            0.443f, 0.7386667f, 0.443f, 0.7746667f, 0.46f, 0.7373334f, 0.482f, 0.6626667f, 0.484f, 0.344f, 0.777f,
            0.408f, 0.755f, 0.456f, 0.737f, 0.5066667f, 0.741f, 0.548f, 0.735f, 0.592f, 0.749f, 0.6626667f, 0.779f,
            0.6266667f, 0.804f, 0.576f, 0.825f, 0.5106667f, 0.831f, 0.448f, 0.828f, 0.3906667f, 0.811f, 0.436f,
            0.777f, 0.5066667f, 0.774f, 0.58f, 0.776f, 0.58f, 0.778f, 0.5053333f, 0.78f, 0.4346667f, 0.779f
        };

        static float[] EXTRA_TEXTURE_COORDS = {
            0.777081f, 0.448287f, 0.770503f, 0.497328f, 0.758565f, 0.549944f, 0.741059f, 0.603929f, 0.716066f, 0.652402f, 0.67545f,
            0.6958f, 0.628222f, 0.731864f, 0.567817f, 0.757642f, 0.500068f, 0.768601f, 0.434709f, 0.757746f, 0.378234f, 0.732235f, 0.328728f,
            0.695581f, 0.291487f, 0.650034f, 0.270483f, 0.602766f, 0.253359f, 0.552101f, 0.2388f, 0.500292f, 0.230518f, 0.448921f, 0.732594f,
            0.395368f, 0.693102f, 0.373687f, 0.643714f, 0.369861f, 0.594113f, 0.379007f, 0.547749f, 0.394198f, 0.441357f, 0.394975f, 0.396994f,
            0.379916f, 0.350578f, 0.371753f, 0.30379f, 0.375066f, 0.266478f, 0.394459f, 0.496406f, 0.438513f, 0.496094f, 0.474346f, 0.496056f,
            0.509281f, 0.495391f, 0.543378f, 0.547616f, 0.569477f, 0.522129f, 0.574867f, 0.495437f, 0.577221f, 0.470134f, 0.57411f, 0.446047f,
            0.568674f, 0.679175f, 0.444705f, 0.645228f, 0.429871f, 0.604254f, 0.432571f, 0.571215f, 0.449363f, 0.607337f, 0.455707f, 0.645507f,
            0.455962f, 0.43096f, 0.451751f, 0.399179f, 0.4359f, 0.358764f, 0.433189f, 0.324038f, 0.446672f, 0.356493f, 0.457636f, 0.393954f,
            0.458028f, 0.605363f, 0.627482f, 0.570426f, 0.610753f, 0.532439f, 0.600497f, 0.496806f, 0.606648f, 0.462576f, 0.600757f, 0.425982f,
            0.610754f, 0.394711f, 0.627339f, 0.423259f, 0.655804f, 0.460736f, 0.672416f, 0.499678f, 0.677299f, 0.538384f, 0.672372f, 0.576334f,
            0.655473f, 0.537972f, 0.624149f, 0.498249f, 0.627739f, 0.460952f, 0.624843f, 0.459943f, 0.632768f, 0.49994f, 0.63618f, 0.540315f,
            0.633068f, 0.50179f, 0.333722f, 0.233748f, 0.386047f, 0.270247f, 0.321105f, 0.307559f, 0.301712f, 0.354885f, 0.28792f, 0.402378f,
            0.275126f, 0.447818f, 0.269226f, 0.554209f, 0.268449f, 0.599497f, 0.274216f, 0.648022f, 0.286028f, 0.69687f, 0.300334f, 0.736363f,
            0.322014f, 0.780312f, 0.385413f, 0.987588f, 0.455618f, 0.976309f, 0.514565f, 0.955447f, 0.580441f, 0.92531f, 0.649342f, 0.881538f,
            0.708787f, 0.810461f, 0.784734f, 0.727811f, 0.847847f, 0.622102f, 0.892957f, 0.50354f, 0.912135f, 0.389163f, 0.893139f, 0.290331f,
            0.848496f, 0.203697f, 0.78435f, 0.138524f, 0.704643f, 0.101801f, 0.647307f, 0.071336f, 0.584215f, 0.045829f, 0.519751f, 0.031102f,
            0.456728f, 0.203171f, 0.197647f, 0.279938f, 0.132482f, 0.317251f, 0.113089f, 0.364576f, 0.099297f, 0.412069f, 0.086502f, 0.457509f,
            0.080603f, 0.563901f, 0.079825f, 0.609188f, 0.085593f, 0.657713f, 0.097405f, 0.706562f, 0.11171f, 0.746054f, 0.133391f, 0.830271f, 0.196566f
        };
    }
}