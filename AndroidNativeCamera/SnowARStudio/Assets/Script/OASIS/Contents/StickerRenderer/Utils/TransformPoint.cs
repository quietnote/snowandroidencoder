﻿using UnityEngine;

namespace OASIS.Contents
{
    public class TransformPoint : MonoBehaviour
    {
        public enum PointType {
            Left,
            Top,
            Right,
            Bottom
        }
        public PointType type;
    }
}