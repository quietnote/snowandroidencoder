﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using Snow.G;

namespace OASIS.Contents
{
    using AR;
    using Data;
    public enum pType : int
    {
        INVALID = 0,
        MOUTH,
        NOSE,
        JAW,
        CENTER_OF_EYES,
        TOP_MOUTH,
        BOTTOM_MOUTH,

        LEFT_EYE,
        RIGHT_EYE,
        LEFT_NOSTRIL,
        RIGHT_NOSTRIL,
        LEFT_EAR,
        RIGHT_EAR,
        LEFT_JAW,
        RIGHT_JAW,
        LEFT_CHEEK,
        RIGHT_CHEEK,

        BROW,

        LEFT_BROW,
        RIGHT_BROW,
        LEFT_BOTTOM_BROW,
        RIGHT_BOTTOM_BROW
    };

    public enum DistortionGroup : int
    {
        NONE = 0,
        SLIM = 1,
        LIFT = 2,
        FACE_SHORT = 3,
        CHIN_SHORT = 4,
        CHIN_LONG = 5,
        EYE_ENLARGE = 6,
        EYE_TAIL = 7
    };

    public enum DistoritionType :int
    {
        inValid = 0,
        bulge = 1,
        shift = 2,
        linearBulge = 3
    };

    [System.Serializable]
    public class DistortionJsonContainer
    {
        public List<DistortionInfo> distortions = new List<DistortionInfo>();
    }

    [System.Serializable]
    public class DistortionInfo
    {
        [System.NonSerialized]
        public DistortionGroup group;
        public string faceAnchor; // json 적혀있는방식 legacy 내용의 대소문자가 이상함(Eye에만 대문자)
        public pType anchor;
        public DistoritionType type;
        public float scale = 1.0f;
        public float angle;
        public float offsetX;
        public float offsetY;
        public float min = 0.0f;
        public float max = 1.0f;
        public float radiusX = 1.0f;
        public float radiusY = 1.0f;
    }

    public class ARDistort : FaceStickerItemRenderer
    {

        [SerializeField]
        Material distort = null;

        const int PREVIEW_GRID_COUNT = 72;

        static readonly Dictionary<pType, pType> anchorTypeStrategy = new Dictionary<pType, pType>
        { { pType.INVALID, pType.INVALID },
            { pType.MOUTH, pType.MOUTH },
            { pType.NOSE, pType.NOSE },
            { pType.JAW, pType.JAW },
            { pType.CENTER_OF_EYES, pType.CENTER_OF_EYES },
            { pType.TOP_MOUTH, pType.TOP_MOUTH },
            { pType.BOTTOM_MOUTH, pType.BOTTOM_MOUTH },

            { pType.LEFT_EYE, pType.RIGHT_EYE },
            { pType.RIGHT_EYE, pType.LEFT_EYE },
            { pType.LEFT_NOSTRIL, pType.RIGHT_NOSTRIL },
            { pType.RIGHT_NOSTRIL, pType.LEFT_NOSTRIL },
            { pType.LEFT_EAR, pType.RIGHT_EAR },
            { pType.RIGHT_EAR, pType.LEFT_EAR },
            { pType.LEFT_JAW, pType.RIGHT_JAW },
            { pType.RIGHT_JAW, pType.LEFT_JAW },
            { pType.LEFT_CHEEK, pType.RIGHT_CHEEK },
            { pType.RIGHT_CHEEK, pType.LEFT_CHEEK },

            { pType.BROW, pType.BROW },

            { pType.LEFT_BROW, pType.LEFT_BROW },
            { pType.RIGHT_BROW, pType.RIGHT_BROW },
            { pType.LEFT_BOTTOM_BROW, pType.LEFT_BOTTOM_BROW },
            { pType.RIGHT_BOTTOM_BROW, pType.RIGHT_BOTTOM_BROW },
        };

        static readonly Dictionary<pType, int> anchorTypeToIndex = new Dictionary<pType, int>
        { { pType.INVALID, -1 },
            { pType.MOUTH, 51 },
            { pType.NOSE, 30 },
            { pType.JAW, 8 },
            { pType.CENTER_OF_EYES, 27 },
            { pType.TOP_MOUTH, 51 },
            { pType.BOTTOM_MOUTH, 57 },

            { pType.LEFT_EYE, 36 },
            { pType.RIGHT_EYE, 45 },
            { pType.LEFT_NOSTRIL, 31 },
            { pType.RIGHT_NOSTRIL, 35 },
            { pType.LEFT_EAR, 0 },
            { pType.RIGHT_EAR, 16 },
            { pType.LEFT_JAW, 6 },
            { pType.RIGHT_JAW, 10 },
            { pType.LEFT_CHEEK, 3 },
            { pType.RIGHT_CHEEK, 13 },

            { pType.BROW, 27 },

            { pType.LEFT_BROW, 17 },
            { pType.RIGHT_BROW, 26 },
            { pType.LEFT_BOTTOM_BROW, 0 },
            { pType.RIGHT_BOTTOM_BROW, 16 },
        };

        static readonly Dictionary<string, pType> anchorStringToType = new Dictionary<string, pType>
        { { "", pType.INVALID },
            { "mouth", pType.MOUTH },
            { "nose", pType.NOSE },
            { "jaw", pType.JAW },
            { "centerofeyes", pType.CENTER_OF_EYES },
            { "topmouth", pType.TOP_MOUTH },
            { "bottommouth", pType.BOTTOM_MOUTH },

            { "leftEye", pType.LEFT_EYE },
            { "rightEye", pType.RIGHT_EYE },
            { "leftnostril", pType.LEFT_NOSTRIL },
            { "rightnostril", pType.RIGHT_NOSTRIL },
            { "leftear", pType.LEFT_EAR },
            { "rightear", pType.RIGHT_EAR },
            { "leftjaw", pType.LEFT_JAW },
            { "rightjaw", pType.RIGHT_JAW },
            { "leftcheek", pType.LEFT_CHEEK },
            { "rightcheek", pType.RIGHT_CHEEK },

            { "brow", pType.BROW },

            { "leftbrow", pType.LEFT_BROW },
            { "rightbrow", pType.RIGHT_BROW },
            { "leftbottombrow", pType.LEFT_BOTTOM_BROW },
            { "rightbottombrow", pType.RIGHT_BOTTOM_BROW },
        };

        [SerializeField]
        List<DistortionInfo> distortions = new List<DistortionInfo>();

        int distort_count;

        List<Vector4> circles = new List<Vector4>();
        List<Vector4> groups = new List<Vector4>();
        List<float> types = new List<float>();
        List<float> progress = new List<float>();

        Vector4 FaceFactor;
        Vector4 validCR;

        float valid_threshold;

        const float DistFactor = 0.9f;

        Vector2 gridSize;

        void Init(int width, int height, bool isSnap = false)
        {
            if (isSnap) // 사진찍을때 (고해상도)
            {
                gridSize = new Vector2(width / 10.0f, height / 10.0f);
            }
            else
            {
                int longLength = (width > height) ? width : height;
                int shortLength = (width > height) ? height : width;
                int targetShortLength = 72;
                int targetLongLength = (int) ((float) targetShortLength * (float) longLength / (float) shortLength);

                gridSize = (width > height) ? new Vector2(targetLongLength, targetShortLength) : new Vector2(targetShortLength, targetLongLength);
            }

            // grid mesh filter make
            MeshFilter mf = gameObject.AddComponent<MeshFilter>();
            Renderer mr = gameObject.AddComponent<MeshRenderer>();
            if (distort != null)
            {
                mr.material = distort;
            }

            Mesh mesh = new Mesh();
            mf.mesh = mesh;

            CreateGrid(mesh, ref gridSize, new Rect(0, 0, 1, 1));

        }

        void CreateGrid(Mesh mesh, ref Vector2 res, Rect rect)
        {
            const int VERTEX_LIMIT = 65535;

            const float GRID_RATIO_ON_MAX = 0.9f;

            int textureIndicesCount = (2 * (int) res.x + 2) * ((int) res.y - 2) + 2 * (int) res.x;

            // 65535개 넘으면 안됨
            while (textureIndicesCount > VERTEX_LIMIT)
            {
                res.x = Mathf.Ceil(res.x * GRID_RATIO_ON_MAX);
                res.y = Mathf.Ceil(res.y * GRID_RATIO_ON_MAX);
                textureIndicesCount = (2 * (int) res.x + 2) * ((int) res.y - 2) + 2 * (int) res.x;
            }

            float rw = rect.width / (res.x - 1);
            float rh = rect.height / (res.y - 1);

            // Create mesh with texture coordinates
            int textureCoordsCount = (int) res.x * (int) res.y;
            Vector2[] textureCoords = new Vector2[textureCoordsCount];
            Vector3[] vertices = new Vector3[textureCoordsCount];
            //Vector3[] normals = new Vector3[textureCoordsCount];
            int[] tri = new int[((int) res.x - 1) * ((int) res.y - 1) * 6];

            int index = 0;
            int tri_index = 0;
            for (int ys = 0; ys < (int) res.y; ys++)
            {
                for (int xs = 0; xs < (int) res.x; xs++)
                {
                    textureCoords[index].x = rect.x + rw * (float) xs;
                    textureCoords[index].y = rect.y + rh * (float) ys;
                    vertices[index].x = rect.x + rw * (float) xs;
                    vertices[index].y = rect.y + rh * (float) ys;
                    vertices[index].z = 0.0f;
                    //normals[index] = -Vector3.forward;
                    if (0 != ys && 0 != xs)
                    {
                        tri[tri_index++] = index;
                        tri[tri_index++] = index - (int) res.x - 1;
                        tri[tri_index++] = index - 1;
                        tri[tri_index++] = index;
                        tri[tri_index++] = index - (int) res.x;
                        tri[tri_index++] = index - (int) res.x - 1;
                    }
                    index += 1;
                }
            }

            mesh.vertices = vertices;
            mesh.triangles = tri;
            mesh.uv = textureCoords;
            //mesh.normals = normals;
        }

        Vector2Int resolution;

        CameraFrame m_frame;

        FaceData m_face;
        
        bool isInitalized = false;
        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if (ACApp.instance)
            {
                if ( ACApp.instance.GetFaceCount() > 0 )
                {
                    m_face = ACApp.instance.GetFaceByID(ID);
                    m_frame = ACApp.instance.GetFrame();
                
                    if (false == isInitalized)
                    {
                        // 텍스쳐크기로 받지말고, 현재 카메라 뷰로 만들자. 
                        isInitalized = true;
                        #if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                        resolution = new Vector2Int(m_frame.cameraTextureRef.height, m_frame.cameraTextureRef.width);
                        #else
                        resolution = new Vector2Int(m_frame.cameraTextureRef.width, m_frame.cameraTextureRef.height);
                        #endif

                        resolution = new Vector2Int(m_frame.renderResolution.x, m_frame.renderResolution.y);
                        Init(m_frame.renderResolution.x, m_frame.renderResolution.y);
                        // 판을 돌리자

                        // 필요한 것을 만듬.
                        
                        float scale = Camera.main.farClipPlane / (0.5f/Mathf.Tan(Camera.main.fieldOfView / 2.0f * Mathf.Deg2Rad));

                        Vector2 size = new Vector2(scale*m_frame.virtualHeight / (float)resolution.y * (float)resolution.x, scale * m_frame.virtualHeight);

                        transform.localScale = new Vector3(size.x,size.y, 1.0f) * DistFactor;
                        transform.localPosition = new Vector3(-size.x * 0.5f, -size.y * 0.5f, Camera.main.farClipPlane) * DistFactor;
                        if(stickerItem.customData == null || stickerItem.customData.Equals(""))
                        {
#if (!USELESS_DELETE)
                            //dataMakerForDebug2(); //테스트용이고 실제 사용은 기본 파라메터를 넣도록
#endif
                            distort_count = distortions.Count;
                        }
                        else
                        {
                            dataMakerFromJson();
                        }
                        MakeValue();
                        setParameter();
                    }
                    else
                    {
                        MakeValue();
                        setParameter();
                    }

                }
            }
        }
        void setParameter()
        {

            Material material = GetComponent<Renderer>().material;
            if (distortions.Count > 0)
            {
                material.SetVectorArray("circles", circles);
                material.SetVectorArray("groups", groups);
                material.SetFloatArray("types", types);
                material.SetFloatArray("progress", progress);
            }
            material.SetInt("count", distort_count);

            material.SetVector("faceFactors", FaceFactor);
            material.SetVector("validCR", validCR);

            material.SetFloat("valid_threshold", valid_threshold);
            //material.SetInt("u_renderOnlyDistortion", 0);
            //material.SetTexture("u_texture", m_frame.cameraTextureRef);

#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            if(ACApp.instance.isMetal())
            {
                material.SetVector("yFlipCoef", new Vector4(1.0f,-1.0f,1.0f,0.0f));
            }
            else
            {
                material.SetVector("yFlipCoef", new Vector4(1.0f,1.0f,0.0f,0.0f));
            }
#else
            material.SetVector("yFlipCoef", new Vector4(-1.0f,1.0f,0.0f,0.0f));
#endif
            // To do : 얼굴 위치 순서로 변경
            material.renderQueue = 1000 + ID;
        }

        void dataMakerFromJson()
        {
            DistortionJsonContainer readJson = JsonUtils.FromJson<DistortionJsonContainer>(stickerItem.customData);

            foreach(DistortionInfo item in readJson.distortions)
            {
                DistortionInfo info = item;
                pType t;
                anchorStringToType.TryGetValue(item.faceAnchor,out t);
                info.anchor = t;
                distortions.Add(info);
            }
            distort_count = distortions.Count;
        }

#if (!USELESS_DELETE)
        void dataMakerForDebug()
        {
            distort_count = 1;

            DistortionInfo distortion = new DistortionInfo();

            distortion.scale = -204.372197f;
            distortion.angle = 30;
            distortion.offsetY = 0.0f;
            distortion.offsetX = 0.0f;
            distortion.min = 0;
            distortion.anchor = pType.NOSE;
            distortion.type = DistoritionType.bulge;
            distortion.max = 1;
            distortion.radiusY = 7.5f;
            distortion.radiusX = 7.67f;

            distortions.Add(distortion);
        }

        void dataMakerForDebug2()
        {
            distort_count = 9;
            DistortionInfo distortion = new DistortionInfo();

            distortion.angle = 0;
            distortion.scale = -15.0f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.LEFT_EYE;
            distortion.type = DistoritionType.bulge;
            distortion.max = 1;
            distortion.radiusY = 1.5f;
            distortion.radiusX = 1.5f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 0;
            distortion.scale = -15.0f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.RIGHT_EYE;
            distortion.type = DistoritionType.bulge;
            distortion.max = 1;
            distortion.radiusY = 1.5f;
            distortion.radiusX = 1.5f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 0;
            distortion.scale = -2.0f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.LEFT_EYE;
            distortion.type = DistoritionType.bulge;
            distortion.max = 0.9722222f;
            distortion.radiusY = 3.0f;
            distortion.radiusX = 3.0f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 0;
            distortion.scale = -2.0f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.RIGHT_EYE;
            distortion.type = DistoritionType.bulge;
            distortion.max = 1;
            distortion.radiusY = 3.0f;
            distortion.radiusX = 3.0f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 270;
            distortion.scale = 2.024664f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.JAW;
            distortion.type = DistoritionType.shift;
            distortion.max = 1;
            distortion.radiusY = 6.0f;
            distortion.radiusX = 6.0f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 180;
            distortion.scale = 15.0f;
            distortion.offsetY = 0.05747126f;
            distortion.offsetX = 0.0f;
            distortion.min = 0;
            distortion.anchor = pType.MOUTH;
            distortion.type = DistoritionType.bulge;
            distortion.max = 1;
            distortion.radiusY = 5.803571f;
            distortion.radiusX = 5.803571f;
            distortions.Add(distortion);
            
            distortion = new DistortionInfo();
            distortion.angle = 90;
            distortion.scale = 2.132287f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.JAW;
            distortion.type = DistoritionType.shift;
            distortion.max = 1;
            distortion.radiusY = 8.071428f;
            distortion.radiusX = 8.071428f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 180;
            distortion.scale = 1.560538f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.LEFT_CHEEK;
            distortion.type = DistoritionType.shift;
            distortion.max = 1;
            distortion.radiusY = 6.0f;
            distortion.radiusX = 6.0f;
            distortions.Add(distortion);

            distortion = new DistortionInfo();
            distortion.angle = 0;
            distortion.scale = 1.56f;
            distortion.offsetY = 0;
            distortion.offsetX = 0;
            distortion.min = 0;
            distortion.anchor = pType.RIGHT_CHEEK;
            distortion.type = DistoritionType.shift;
            distortion.max = 1;
            distortion.radiusY = 6f;
            distortion.radiusX = 6f;
            distortions.Add(distortion);
        }
#endif
        void MakeValue()
        {
            Vector2 r = new Vector2(resolution.x, resolution.y);
            float aspectRatio = (float)resolution.y / (float)resolution.x;
            float _faceYFactor;
            if (m_frame.shouldRotateBy == TextureRotationCommand.kClockwise90 ||
                m_frame.shouldRotateBy == TextureRotationCommand.kClockwise270)
            {
                _faceYFactor = 1.0f;
            }
            else
            {
                _faceYFactor = 0.75f;
            }

            pType inType;
            anchorTypeStrategy.TryGetValue(pType.LEFT_EYE, out inType);
            Vector2 leftEye = GetNormalizedPointWithScene(inType);
            anchorTypeStrategy.TryGetValue(pType.RIGHT_EYE, out inType);
            Vector2 rightEye = GetNormalizedPointWithScene(inType);
            float dist = Vector2.Distance(leftEye, rightEye);
            dist = dist / r.x / 2.0f * 0.4f;

            Matrix4x4 rotationTransform;
            rotationTransform = Matrix4x4.Rotate(Quaternion.AngleAxis(m_face.relativeRoll, Vector3.forward));

            FaceFactor = new Vector4(_faceYFactor, aspectRatio, dist, -m_face.relativeRoll * Mathf.Deg2Rad);

            circles.Clear();
            groups.Clear();
            types.Clear();
            progress.Clear();
            for (int i = 0; i < distort_count; ++i)

            {

                DistortionInfo current = distortions[i];

                pType anchor;
                anchorTypeStrategy.TryGetValue(current.anchor, out anchor);

                Vector2 center = GetNormalizedPointWithDistortionOffset(anchor);

                Vector2 scaledRadius = new Vector2(dist * current.radiusX, dist * current.radiusY);
                Vector2 scaledOffset = new Vector2(scaledRadius.x * -current.offsetX, scaledRadius.y * -current.offsetY);
                Vector3 rotatedDelta = new Vector3(scaledOffset.x, scaledOffset.y, 0);

                rotatedDelta = rotationTransform.MultiplyVector(rotatedDelta);

                center.x += rotatedDelta.x;
                center.y += rotatedDelta.y / (_faceYFactor * aspectRatio);

                // grab 텍스쳐 위아래 반대
                circles.Add(new Vector4(center.x, 1.0f - center.y, scaledRadius.x, scaledRadius.y));
                groups.Add(new Vector4(distortions[i].scale, (180.0f-distortions[i].angle) * Mathf.Deg2Rad, distortions[i].min, distortions[i].max));
                types.Add((float) distortions[i].type);
                progress.Add(1.0f);
            }

            // Update distortion valid region
            int[] centroidIndices = {
                // Face contour
                0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,
                // Eyebrows
                42,41,40,39,38,
                37,36,35,34,33,
            };
            // unity에서 상하좌우 반대
            Vector2 centroid = CentroidOf(centroidIndices, centroidIndices.Length);
            centroid.y *= -1.0f;
            float faceHeight = FaceHeightOf(1.0f) * 2.0f;
            float faceWidth = FaceWidthOf(1.0f) * 2.0f;

            centroid *= new Vector2(2.0f*aspectRatio,2.0f);

            //centroid.x = centroid.x *2.0f;
            Vector2 validCenter = (centroid + new Vector2(1.0f, 1.0f)) * 0.5f; // should be delivered as uv-coords
            
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            // magin number 3.0f for android 
            Vector2 validRadius = new Vector2(3.0f * faceWidth * 0.4f, 3.0f * faceHeight * 0.7f);
#else
            Vector2 validRadius = new Vector2(faceWidth * 0.4f, faceHeight * 0.7f);
#endif
            validCR = new Vector4(validCenter.x, 1.0f-validCenter.y, validRadius.x, validRadius.y);
            valid_threshold = 0.5f;

        }

        float FaceHeightOf(float aspectRatio)
        {
            Vector2 p37 = m_face.verticesArray[37];
            Vector2 p38 = m_face.verticesArray[38];
            Vector2 p16 = m_face.verticesArray[16];
            Vector2 p43 = m_face.verticesArray[43];
            Vector2 midBrow = IntersectOfLines(p16, p43, p37, p38);

            return distanceWithAspect(midBrow, p16, aspectRatio);
        }

        float FaceWidthOf(float aspectRatio)
        {
            Vector2 p00 = m_face.verticesArray[0];
            Vector2 p32 = m_face.verticesArray[32];

            return distanceWithAspect(p00, p32, aspectRatio);
        }

        float distanceWithAspect(Vector2 pt1, Vector2 pt2, float aspectRatio)
        {
            float dx = pt2.x - pt1.x;
            float dy = pt2.y - pt1.y;
            return Vector2.Distance(Vector2.zero, new Vector2(dx, dy * aspectRatio));
        }
        Vector2 IntersectOfLines(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            float a1 = p2.y - p1.y;
            float b1 = p1.x - p2.x;
            float c1 = a1 * p1.x + b1 * p1.y;

            float a2 = p4.y - p3.y;
            float b2 = p3.x - p4.x;
            float c2 = a2 * p3.x + b2 * p3.y;

            float determinant = a1 * b2 - a2 * b1;
            if (determinant == 0)
            {
                // The lines are parallel.
                return Vector2.zero;
            }

            float x = (b2 * c1 - b1 * c2) / determinant;
            float y = (a1 * c2 - a2 * c1) / determinant;
            return new Vector2(x, y);
        }

        Vector2 CentroidOf(int[] indices, int size)
        {
            Vector2 currentCentroid = Vector2.zero;
            float signedArea = 0.0f;
            Vector2 current = Vector2.zero;
            Vector2 next = Vector2.zero;
            float a = 0.0f;

            int i = 0;
            for (i = 0; i < size - 1; i++)
            {
                current = m_face.verticesArray[indices[i]];
                next = m_face.verticesArray[indices[i + 1]];
                a = current.x * next.y - next.x * current.y;
                signedArea += a;
                currentCentroid.x += (current.x + next.x) * a;
                currentCentroid.y += (current.y + next.y) * a;
            }

            // Do last vertex separately to avoid performing an expensive
            // modulus operation in each iteration.
            next = m_face.verticesArray[indices[0]];
            a = current.x * next.y - next.x * current.y;
            signedArea += a;
            currentCentroid.x += (current.x + next.x) * a;
            currentCentroid.y += (current.y + next.y) * a;

            signedArea *= 0.5f;
            currentCentroid.x /= (6.0f * signedArea);
            currentCentroid.y /= (6.0f * signedArea);

            return currentCentroid;
        }

        Vector2 GetNormalizedPointWithScene(pType type)
        {
            Vector2 p = GetNormalizedPoint(type);

            Vector2 r = new Vector2(resolution.x, resolution.y);
            return new Vector2(p.x * r.x, p.y * r.y);
        }

        Vector2 GetNormalizedPointWithDistortionOffset(pType type)
        {

            Vector2 p = GetNormalizedPoint(type);

            return p;

        }

        Vector2 GetNormalizedPoint(pType type)
        {
            Vector2 p = GetPoint(type);
            //p.x /= m_Data.targetSize.x;
            //p.y /= m_Data.targetSize.y;

            p.x /= 2.0f;
            p.y /= 2.0f;

            p.x += 0.5f;
            p.y += 0.5f;

            p.y = 1.0f - p.y;
            return p;
        }

        Vector2 GetPoint(pType type)
        {
            Vector2[] _points = m_face.ulseeNormalizedVertexShape;
            if (type == pType.LEFT_EYE)
            {
                return (_points[39] + _points[36]) / 2;
            }
            else if (type == pType.RIGHT_EYE)
            {
                return (_points[45] + _points[42]) / 2;
            }
            else if (type == pType.BROW)
            {

                Vector2 center = _points[27];
                Vector2 dest = _points[30];
                Vector2 v = new Vector2(dest.x - center.x, dest.y - center.y);

                Vector2 result = new Vector2(center.x - v.x, center.y - v.y);
                return result;

            }
            else if (type == pType.MOUTH)
            {

                //http://bts.snowcorp.com/browse/SELFIECAM-47
                if (m_frame.shouldRotateBy == TextureRotationCommand.kClockwise90 ||
                    m_frame.shouldRotateBy == TextureRotationCommand.kClockwise270)
                {
                    return new Vector2(((_points[51] + _points[57]) / 2).x,
                        ((_points[54] + _points[48]) / 2).y);
                }
                else
                {
                    return new Vector2(((_points[54] + _points[48]) / 2).x,
                        ((_points[51] + _points[57]) / 2).y);
                }
            }
            else if (type == pType.LEFT_BROW)
            {
                return GetExpandPoint(27, 30, 17, true, 0.7f);
            }
            else if (type == pType.LEFT_BOTTOM_BROW)
            {
                return GetExpandPoint(27, 30, 0, true, 0.6f);
            }
            else if (type == pType.RIGHT_BROW)
            {
                return GetExpandPoint(27, 30, 26, true, 0.7f);
            }
            else if (type == pType.RIGHT_BOTTOM_BROW)
            {
                return GetExpandPoint(27, 30, 16, true, 0.6f);
            }
            else
            {
                int idx;
                anchorTypeToIndex.TryGetValue(type, out idx);
                if (idx < 0)
                {
                    return Vector2.zero;
                }
                return _points[idx];
            }

        }

        Vector2 GetExpandPoint(int begin, int end, int refer, bool useEyeTangent, float basicScale)
        {

            Vector2[] _points = m_face.ulseeNormalizedVertexShape;

            Vector2 trVec = _points[begin] - _points[end];
            Vector2 unit;
            if (useEyeTangent)
            {
                unit = _points[45] - _points[36];
                unit = Vector2.Perpendicular(unit);
                unit = unit.normalized;
            }
            else
            {
                unit = trVec;
                unit = unit.normalized;
            }

            float scale = trVec.magnitude * basicScale;

            Vector2 res = new Vector2(_points[refer].x + unit.x * scale, _points[refer].y + unit.y * scale);
            return res;
        }

    }
}