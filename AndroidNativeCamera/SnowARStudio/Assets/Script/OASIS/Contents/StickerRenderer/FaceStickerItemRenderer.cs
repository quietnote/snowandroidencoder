﻿using UnityEngine;

namespace OASIS.Contents
{
    public class FaceStickerItemRenderer : FaceStickerRendererBase
    {
        [SerializeField]
        protected StickerItem stickerItem;

        public void setItem(StickerItem item)
        {
            stickerItem = item;
        }

        public string Key {
            get {
                return stickerItem.Key;
            }
        }
    }
}
