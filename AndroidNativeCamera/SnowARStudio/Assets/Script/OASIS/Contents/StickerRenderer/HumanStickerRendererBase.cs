﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS.Contents
{
    public class HumanStickerRendererBase : MonoBehaviour
    {
        [SerializeField]
        protected int ID = -1;

        public int getDataID()
        {
            return ID;
        }

        public void setDataID(int id)
        {
            ID = id;
        }

    }
}