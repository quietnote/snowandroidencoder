﻿using UnityEngine;
using System.Collections.Generic;

// Temp test class
namespace OASIS.Contents
{
    using AR;
    using Data;
    public class FaceSkinStickerRenderer : FaceMeshStickerRenderer
    {
        FaceDataUtils.Options lastOptions = FaceDataUtils.Options.None;
        Mesh faceMesh;

        void Update()
        {
            if (lastOptions != options) {
                textureUV = FaceDataUtils.GetTextureUV(options);
                lastOptions = options;
            }

            if (ACApp.instance)
            {
                CameraFrame frame = ACApp.instance.GetFrame();
                FaceData face = ACApp.instance.GetFaceByID(ID);

                GetComponent<MeshFilter>().mesh = SetFaceMesh(faceMesh, UseExtraFeatures ? face.extraFeaturePositions : face.featurePositions, textureUV, options);
                if (transform.position.z != frame.virtualDistance) {
                    transform.position = new Vector3(transform.position.x, transform.position.y, frame.virtualDistance);
                }

            }
        }
    }
}
