﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Temp test class
namespace OASIS.Contents
{
    using Data;
    using AR;
    public class FaceCopyStickerRenderer : FaceMeshStickerRenderer
    {
        public enum FaceFeatureIndex {
            Chin = 8,
            EyeBrowR = 18,
            EyeBrowL = 25,
            foreheadL = 72,
            foreheadR = 73,
        }
        const int maxIndex = 78;
        [SerializeField]
        Material blendMaterial = null;
        [SerializeField]
        RenderTarget[] renderTargets;
        [SerializeField]
        RenderData mask = null;
        [SerializeField]
        RenderData blur = null;
        [SerializeField]
        float blurSize = 5f;
        
        bool isInitialized = false;

        override protected void Start()
        {
            base.Start();
            if (renderTargets == null || renderTargets.Length == 0) {
                renderTargets = new RenderTarget[1];
                renderTargets[0] = new RenderTarget(gameObject);
            }
        }

        void OnDestroy()
        {
            if (mask != null) {
                mask.Destroy();
            }
            if (blur != null) {
                blur.Destroy();
            }
        }

        void Initialize(CameraFrame frame, Texture texture, bool needFlip)
        {
            mask.CreateCamera("Mask Camera", "StickerRoot", texture.width, texture.height, RenderTextureFormat.R8, frame.fov);
            blur.CreateCamera("Blur Camera", "StickerRoot", texture.width, texture.height, RenderTextureFormat.R8, frame.fov);

            blur.material.mainTexture = mask.camera.targetTexture;
            blur.mesh = GetSquareMesh(frame.virtualWidth / 2, frame.virtualHeight / 2, frame.virtualDistance, needFlip);
            
            SetTexelSize(texture);

            foreach (RenderTarget target in renderTargets) {
                target.SetMaterial(blendMaterial);
                target.SetSubTexture(blur.camera.targetTexture);
            }
        }

        void Update()
        {
            if (ACApp.instance)
            {
                CameraFrame frame = ACApp.instance.GetFrame();
#if UNITY_EDITOR
                Texture texture = frame.cameraTextureRef;
#else
                Texture texture = frame.cameraFitTextureRef;
#endif
                if (texture == null || texture.width <= 16 || texture.height <= 16) {
                    return;
                }

                if (!isInitialized) {
#if UNITY_EDITOR
                    Initialize(frame, texture, true);
#else
                    Initialize(frame, texture, false);
#endif
                    isInitialized = true;
                }

#if UNITY_EDITOR
                SetTexelSize(texture);
#endif
                if (ACApp.instance.GetFaceCount() > 0) {
                    FaceData m_face = ACApp.instance.GetFaceByIndex(0);
                    Vector3[] featurePositions = UseExtraFeatures ? m_face.extraFeaturePositions : m_face.featurePositions;

                    //DrawMask
                    mask.mesh = SetFaceMesh(mask.mesh, featurePositions, null, options);
                    Graphics.DrawMesh(mask.mesh, Vector3.forward * frame.virtualDistance, Quaternion.identity, mask.material, mask.layer);

                    //DrawBlur
                    Graphics.DrawMesh(blur.mesh, Vector3.zero, Quaternion.identity, blur.material, blur.layer);
#if UNITY_EDITOR
                    textureUV = GetTextureUV(featurePositions, frame.virtualWidth, frame.virtualHeight, true);
#else
                    textureUV = GetTextureUV(featurePositions, frame.virtualWidth, frame.virtualHeight, false);
#endif
                    foreach (RenderTarget target in renderTargets) {
                        target.SetMainTexture(texture);
                        SetRenderTarget(target, featurePositions, textureUV);
                    }
                }
            }
        }

        void SetRenderTarget(RenderTarget renderTarget, Vector3[] features, Vector2[] uv)
        {
            renderTarget.CheckVerticesLength(features.Length);
            System.Array.Copy(features, renderTarget.vertices, features.Length);
            if (renderTarget.bindingTarget != null) {
                ScaleAndTranslateToCenter(renderTarget.vertices, renderTarget.bindingTarget);
            }
            renderTarget.mesh = SetFaceMesh(renderTarget.mesh, renderTarget.vertices, uv, options);
            renderTarget.Apply();
        }

        void SetTexelSize(Texture texture)
        {
            if (texture != null) {
                blur.material.SetFloat("_Texel_Width", blurSize / texture.width);
                blur.material.SetFloat("_Texel_Height", blurSize / texture.height);
            }
        }

        Mesh GetSquareMesh(float x, float y, float z, bool flip)
        {
            Mesh mesh = new Mesh();

            Vector3[] vertices = new Vector3[] {
                new Vector3(-x, y, z), new Vector3(x, y, z), new Vector3(-x, -y, z),
                new Vector3(-x, -y, z), new Vector3(x, y, z), new Vector3(x, -y, z)
            };
            int[] triangles = new int[] {
                0, 1, 2, 3, 4, 5
            };
            Vector2[] uv = new Vector2[] {
                new Vector2(-1f, 1f), new Vector2(1f, 1f), new Vector2(-1f, -1f),
                new Vector2(-1f, -1f), new Vector2(1f, 1f), new Vector2(1f, -1f)
            };
            if (flip) {
                for (int i = 0; i < uv.Length; ++i) {
                    uv[i].x = 1f - (uv[i].x * 0.5f + 0.5f);
                    uv[i].y = uv[i].y * 0.5f + 0.5f;
                }
            } else {
                for (int i = 0; i < uv.Length; ++i) {
                    uv[i].x = uv[i].x * 0.5f + 0.5f;
                    uv[i].y = uv[i].y * 0.5f + 0.5f;
                }
            }

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uv;
            mesh.RecalculateNormals();

            return mesh;
        }

        void FindIndexOfMinMaxAxisX(Vector3[] vertices, out int min, out int max)
        {
            min = max = 0;
            float minValue = vertices[min].x;
            float maxValue = vertices[max].x;
            int end = (int)Mathf.Min(maxIndex, vertices.Length);

            for (int i = 1; i < end; ++i) {
                if (vertices[i].x > maxValue) {
                    max = i;
                    maxValue = vertices[max].x;
                    continue;
                }
                if (vertices[i].x < minValue) {
                    min = i;
                    minValue = vertices[min].x;
                    continue;
                }
            }
        }

        void ScaleAndTranslateToCenter(Vector3[] vertices, BindingTargetProperty target)
        {
            // Translate to center
            int left = 0, right = 0;
            FindIndexOfMinMaxAxisX(vertices, out left, out right);
            float centerAxisX = (vertices[left].x  + vertices[right].x) * 0.5f;
            float centerAxisY = UseExtraFeatures
                ? ((vertices[(int)FaceFeatureIndex.foreheadL].y + vertices[(int)FaceFeatureIndex.foreheadR].y) * 0.5f + vertices[(int)FaceFeatureIndex.Chin].y) * 0.5f
                : ((vertices[(int)FaceFeatureIndex.EyeBrowL].y + vertices[(int)FaceFeatureIndex.EyeBrowR].y) * 0.5f + vertices[(int)FaceFeatureIndex.Chin].y) * 0.5f;
            Vector3 centerOfFace = new Vector2(centerAxisX, centerAxisY);
            Translate(vertices, Vector3.zero - centerOfFace);

            // Scale
            float deltaWidth = Vector3.Distance(vertices[right], vertices[left]);
            float scale = target.GetWidth() / deltaWidth;
            ScaleVertices(vertices, scale);
        }

        void ScaleVertices(Vector3[] verticies, float scale)
        {
            for (int i = 0; i < verticies.Length; ++i) {
                verticies[i] *= scale;
            }
        }

        void Translate(Vector3[] verticies, Vector3 targetPosition)
        {
            for (int i = 0; i < verticies.Length; ++i) {
                verticies[i] += targetPosition;
            }
        }

        Vector2[] GetTextureUV(Vector3[] vertices, float width, float height, bool needFilpHorizontal)
        {
            if (textureUV == null || textureUV.Length != vertices.Length) {
                textureUV = new Vector2[vertices.Length];
            }
            if (needFilpHorizontal) {
                for (int i = 0; i < textureUV.Length; ++i) {
                    textureUV[i].x = 0.5f - (vertices[i].x / width);
                    textureUV[i].y = vertices[i].y / height + 0.5f;
                }
            } else {
               for (int i = 0; i < textureUV.Length; ++i) {
                    textureUV[i].x = vertices[i].x / width + 0.5f;
                    textureUV[i].y = vertices[i].y / height + 0.5f;
                }
            }
            return textureUV;
        }

        [System.Serializable]
        class RenderData {
            public Material material = null;
            public int layer = 0;
            [HideInInspector]
            public Camera camera = null;
            [HideInInspector]
            public Mesh mesh = null;
            RenderTexture renderTexture = null;

            public void CreateCamera(string cameraName, string parentTag, int width, int height, RenderTextureFormat format, float fov)
            {
                GameObject obj = new GameObject(cameraName);
                obj.transform.SetParent(GameObject.FindGameObjectWithTag(parentTag).transform);
                obj.transform.localPosition = Vector3.zero;
                obj.AddComponent<Camera>();
                Camera cam = obj.GetComponent<Camera>();
                cam.clearFlags = CameraClearFlags.SolidColor;
                cam.backgroundColor = Color.black;
                cam.targetTexture = CreateTexture(width, height, format);
                cam.fieldOfView = fov;
                cam.cullingMask = 2 << (layer - 1);
                camera = cam;
            }

            public void Destroy()
            {
                if (renderTexture != null) {
                    renderTexture.Release();
                    renderTexture = null;
                }
                if (camera != null) {
                    GameObject.Destroy(camera.gameObject);
                }
            }

            RenderTexture CreateTexture(int width, int height, RenderTextureFormat format)
            {
                return new RenderTexture(width, height, 0, format);
            }
        }

        [System.Serializable]
        class RenderTarget {
            public GameObject renderTarget = null;
            public BindingTargetProperty bindingTarget = null;
            public bool trakingRotation = false;
            [HideInInspector]
            public Mesh mesh = null;
            [HideInInspector]
            public Vector3[] vertices = null;

            public Texture MainTexture {
                get { 
                    return renderTarget.GetComponent<MeshRenderer>().material.mainTexture;
                }
            }

            public RenderTarget(GameObject renderTarget)
            {
                this.renderTarget = renderTarget;
            }

            public void SetMaterial(Material material)
            {
                renderTarget.GetComponent<MeshRenderer>().material = material;
            }

            public void SetMainTexture(Texture texture)
            {
                renderTarget.GetComponent<MeshRenderer>().material.mainTexture = texture;
            }

            public void SetSubTexture(Texture texture)
            {
                renderTarget.GetComponent<MeshRenderer>().material.SetTexture("_BlurTex", texture);
            }

            public void CheckVerticesLength(int length)
            {
                if (vertices == null || vertices.Length != length) {
                    vertices = new Vector3[length];
                }
            }

            public void Apply()
            {
                renderTarget.GetComponent<MeshFilter>().mesh = mesh;

                if (bindingTarget != null) {
                    renderTarget.transform.position = bindingTarget.transform.position;
                    if (trakingRotation) {
                        renderTarget.transform.rotation = bindingTarget.transform.rotation;
                    }
                }
            }
        }
    }
}