﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Temp test class
namespace OASIS.Contents
{
    using Data;
    public class ARFaceAttacher : FaceStickerRendererBase
    {
        public bool constraintsX;
        public bool constraintsY;
        public bool constraintsZ;
        Vector3 position = Vector3.zero;

        void Update()
        {
            if(ID == -1)
            {
                // 초기화가 안됨
                return;
            }

            if (ACApp.instance)
            {
                if (ACApp.instance.GetFaceCount() > 0)
                {
                    FaceData face = ACApp.instance.GetFaceByID(ID);
                    if (face == null) {
                        face = ACApp.instance.GetFaceByID(++ID);
                        return;
                    }
                    position = face.faceLocation;
                    if (constraintsX) {
                        position.x = transform.localPosition.x;
                    }
                    if (constraintsY) {
                        position.y = transform.localPosition.y;
                    }
                    if (constraintsZ) {
                        position.z = transform.localPosition.z;
                    }
                    transform.localPosition = position;
                    transform.localRotation = face.faceRotation;
                }
            }
        }
    }
}
