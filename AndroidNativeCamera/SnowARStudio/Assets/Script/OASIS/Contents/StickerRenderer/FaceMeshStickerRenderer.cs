﻿using UnityEngine;

// Temp test class
namespace OASIS.Contents
{
    public abstract class FaceMeshStickerRenderer : FaceStickerItemRenderer
    {
        [SerializeField]
        protected FaceDataUtils.Options options = FaceDataUtils.Options.Face;
        protected Vector2[] textureUV;

        virtual protected void Start()
        {
            if (stickerItem.faceVertices == null || stickerItem.faceVertices.Equals("")) {
                textureUV = FaceDataUtils.GetTextureUV(options);
            } else {
                textureUV = FaceDataUtils.ConvertCoordsToVector2(stickerItem.faceVertices.Split(','));
            }
        }

        protected bool UseExtraFeatures {
            get {
                return (((int)options) & ((int)FaceDataUtils.Options.ExtraFull)) > 0;
            }
        }

        virtual protected Mesh SetFaceMesh(Mesh mesh, Vector3[] vertices, Vector2[] uv, FaceDataUtils.Options options)
        {
            if (vertices == null) {
                return null;
            }
            if (mesh == null) {
                mesh = new Mesh();
            }

            /* 4 points in the list for the square made of two triangles:
            0 *--* 1
            |   /  |
            |  /   |
            3 *--* 2
            */
            // clock wise
            mesh.vertices = vertices;
            int[] triangles;
            FaceDataUtils.SetFaceIndexTriangles(out triangles, options);
            mesh.triangles = triangles;
            if (uv != null) {
                mesh.uv = uv;
            }

            mesh.RecalculateNormals();
            return mesh;
        }
    }
}