﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using XLua;
using System;

namespace OASIS.Contents
{    
    [RequireComponent(typeof(FaceStickerRendererBase))]
    public class LuaFaceBehaviour : Lua.LuaBehaviour
    {
        FaceStickerRendererBase faceRenderer;

        struct TriggerLuaFunctions
        {
            public LuaFunction onInitialState;
            public LuaFunction onTriggerOn;
            public LuaFunction onTriggerOff;
        }

        TriggerLuaFunctions eyeBlinkLuaFuncs;
        TriggerLuaFunctions mouthAhLuaFuncs;
        TriggerLuaFunctions headShakeLuaFuncs;
        TriggerLuaFunctions headNodLuaFuncs;
        TriggerLuaFunctions browJumpLuaFuncs;

        override protected void BindLuaFunctions(LuaTable scriptEnv)
        {
            scriptEnv.Get("eyeBlinkInitialState", out eyeBlinkLuaFuncs.onInitialState);
            scriptEnv.Get("eyeBlinkTriggerOn", out eyeBlinkLuaFuncs.onTriggerOn);
            scriptEnv.Get("eyeBlinkTriggerOff", out eyeBlinkLuaFuncs.onTriggerOff);

            scriptEnv.Get("mouthAhInitialState", out mouthAhLuaFuncs.onInitialState);
            scriptEnv.Get("mouthAhTriggerOn", out mouthAhLuaFuncs.onTriggerOn);
            scriptEnv.Get("mouthAhTriggerOff", out mouthAhLuaFuncs.onTriggerOff);

            scriptEnv.Get("headShakeInitialState", out headShakeLuaFuncs.onInitialState);
            scriptEnv.Get("headShakeTriggerOn", out headShakeLuaFuncs.onTriggerOn);
            scriptEnv.Get("headShakeTriggerOff", out headShakeLuaFuncs.onTriggerOff);

            scriptEnv.Get("headNodInitialState", out headNodLuaFuncs.onInitialState);
            scriptEnv.Get("headNodTriggerOn", out headNodLuaFuncs.onTriggerOn);
            scriptEnv.Get("headNodTriggerOff", out headNodLuaFuncs.onTriggerOff);

            scriptEnv.Get("browJumpInitialState", out browJumpLuaFuncs.onInitialState);
            scriptEnv.Get("browJumpTriggerOn", out browJumpLuaFuncs.onTriggerOn);
            scriptEnv.Get("browJumpTriggerOff", out browJumpLuaFuncs.onTriggerOff);
        }

        new void Awake()
        {
            faceRenderer = GetComponent<FaceStickerRendererBase>();

            faceRenderer.eyeBlinkEvents.initialValueSet += OnEyeBlinkInitialState;
            faceRenderer.eyeBlinkEvents.onTriggerOn += OnEyeBlinkTriggerOn;
            faceRenderer.eyeBlinkEvents.onTriggerOff += OnEyeBlinkTriggerOff;

            faceRenderer.mouthAhEvents.initialValueSet += OnMouthAhInitialState;
            faceRenderer.mouthAhEvents.onTriggerOn += OnMouthAhTriggerOn;
            faceRenderer.mouthAhEvents.onTriggerOff += OnMouthAhTriggerOff;

            faceRenderer.headYawEvents.initialValueSet += OnHeadShakeInitialState;
            faceRenderer.headYawEvents.onTriggerOn += OnHeadShakeTriggerOn;
            faceRenderer.headYawEvents.onTriggerOff += OnHeadShakeTriggerOff;

            faceRenderer.headPitchEvents.initialValueSet += OnHeadNodInitialState;
            faceRenderer.headPitchEvents.onTriggerOn += OnHeadNodTriggerOn;
            faceRenderer.headPitchEvents.onTriggerOff += OnHeadNodTriggerOff;

            faceRenderer.browJumpEvents.initialValueSet += OnBrowJumpInitialState;
            faceRenderer.browJumpEvents.onTriggerOn += OnBrowJumpTriggerOn;
            faceRenderer.browJumpEvents.onTriggerOff += OnBrowJumpTriggerOff;

            base.Awake();
        }

        new void OnDestroy()
        {
            base.OnDestroy();

            faceRenderer.eyeBlinkEvents.initialValueSet -= OnEyeBlinkInitialState;
            faceRenderer.eyeBlinkEvents.onTriggerOn -= OnEyeBlinkTriggerOn;
            faceRenderer.eyeBlinkEvents.onTriggerOff -= OnEyeBlinkTriggerOff;

            faceRenderer.mouthAhEvents.initialValueSet -= OnMouthAhInitialState;
            faceRenderer.mouthAhEvents.onTriggerOn -= OnMouthAhTriggerOn;
            faceRenderer.mouthAhEvents.onTriggerOff -= OnMouthAhTriggerOff;

            faceRenderer.headYawEvents.initialValueSet -= OnHeadShakeInitialState;
            faceRenderer.headYawEvents.onTriggerOn -= OnHeadShakeTriggerOn;
            faceRenderer.headYawEvents.onTriggerOff -= OnHeadShakeTriggerOff;

            faceRenderer.headPitchEvents.initialValueSet -= OnHeadNodInitialState;
            faceRenderer.headPitchEvents.onTriggerOn -= OnHeadNodTriggerOn;
            faceRenderer.headPitchEvents.onTriggerOff -= OnHeadNodTriggerOff;

            faceRenderer.browJumpEvents.initialValueSet -= OnBrowJumpInitialState;
            faceRenderer.browJumpEvents.onTriggerOn -= OnBrowJumpTriggerOn;
            faceRenderer.browJumpEvents.onTriggerOff -= OnBrowJumpTriggerOff;
        }

        void OnEyeBlinkInitialState(bool value)
        {
            eyeBlinkLuaFuncs.onInitialState?.Action<bool>(value);
        }

        void OnEyeBlinkTriggerOn()
        {
            eyeBlinkLuaFuncs.onTriggerOn?.Call();
        }

        void OnEyeBlinkTriggerOff()
        {
            eyeBlinkLuaFuncs.onTriggerOff?.Call();
        }

        void OnMouthAhInitialState(bool value)
        {
            mouthAhLuaFuncs.onInitialState?.Action<bool>(value);
        }

        void OnMouthAhTriggerOn()
        {
            mouthAhLuaFuncs.onTriggerOn?.Call();
        }

        void OnMouthAhTriggerOff()
        {
            mouthAhLuaFuncs.onTriggerOff?.Call();
        }

        void OnHeadShakeInitialState(bool value)
        {
            headShakeLuaFuncs.onInitialState?.Action<bool>(value);
        }

        void OnHeadShakeTriggerOn()
        {
            headShakeLuaFuncs.onTriggerOn?.Call();
        }

        void OnHeadShakeTriggerOff()
        {
            headShakeLuaFuncs.onTriggerOff?.Call();
        }

        void OnHeadNodInitialState(bool value)
        {
            headNodLuaFuncs.onInitialState?.Action<bool>(value);
        }

        void OnHeadNodTriggerOn()
        {
            headNodLuaFuncs.onTriggerOn?.Call();
        }

        void OnHeadNodTriggerOff()
        {
            headNodLuaFuncs.onTriggerOff?.Call();
        }

        void OnBrowJumpInitialState(bool value)
        {
            browJumpLuaFuncs.onInitialState?.Action<bool>(value);
        }

        void OnBrowJumpTriggerOn()
        {
            browJumpLuaFuncs.onTriggerOn?.Call();
        }

        void OnBrowJumpTriggerOff()
        {
            browJumpLuaFuncs.onTriggerOff?.Call();
        }
    }
}