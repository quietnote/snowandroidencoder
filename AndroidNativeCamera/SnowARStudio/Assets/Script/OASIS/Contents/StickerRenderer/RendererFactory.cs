﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Temp test class
namespace OASIS.Contents
{
    public class RendererFactory : MonoBehaviour
    {
        [SerializeField]
        GameObject[] stickerRenderer = null;
        static Dictionary<string, GameObject> stickerRendererDictionary = new Dictionary<string, GameObject>();
        static Transform parent;

        public void Init(Transform p)
        {
            foreach (GameObject renderer in stickerRenderer) {
                stickerRendererDictionary.Add(renderer.GetComponent<FaceStickerItemRenderer>().Key, renderer);
            }
            parent = p;
        }

        public static GameObject Create(StickerItem item)
        {
            GameObject obj = Create(item.Key);
            if(obj != null)
            {
                obj.GetComponent<FaceStickerItemRenderer>().setItem(item);
            }
            return obj;
        }

        static GameObject Create(string key)
        {
            GameObject obj;
            GameObject outObj = null;
            if (stickerRendererDictionary.TryGetValue(key, out obj)) {
                outObj = Instantiate(obj, Vector3.zero, Quaternion.identity, parent);
            }
            return outObj;
        }

        void OnDestroy()
        {
            stickerRendererDictionary.Clear();
        }
    }
}
