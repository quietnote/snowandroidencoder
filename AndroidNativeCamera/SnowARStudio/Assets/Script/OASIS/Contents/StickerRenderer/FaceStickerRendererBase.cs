﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS.Contents
{
    using ST;
    public struct FaceActionEvents
    {
        public System.Action<bool> initialValueSet;
        public System.Action onTriggerOn;
        public System.Action onTriggerOff;
    }

    public class FaceStickerRendererBase : HumanStickerRendererBase
    {
        public FaceActionEvents eyeBlinkEvents;
        public FaceActionEvents mouthAhEvents;
        public FaceActionEvents headYawEvents;
        public FaceActionEvents headPitchEvents;
        public FaceActionEvents browJumpEvents;

        public FaceActionEvents GetFaceActionEvents(SenseTimeLib.FaceAction action)
        {
            switch (action)
            {
                case SenseTimeLib.FaceAction.kEyeBlink:
                    return eyeBlinkEvents;

                case SenseTimeLib.FaceAction.kMouthAh:
                    return mouthAhEvents;

                case SenseTimeLib.FaceAction.kHeadYaw:
                    return headYawEvents;

                case SenseTimeLib.FaceAction.kHeadPitch:
                    return headPitchEvents;

                case SenseTimeLib.FaceAction.kBrowJump:
                    return browJumpEvents;
            }

            return new FaceActionEvents();
        }
    }
}