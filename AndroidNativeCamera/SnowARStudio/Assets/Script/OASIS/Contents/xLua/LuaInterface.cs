﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OASIS.Contents
{
    // Library to be used from Lua, wrapper of existing C Sharp code
    // Key concern of this abstraction is to:
    // 1. Minimize unnessesary marshaling between Lua/C# memory
    // 2. Provide limited and simple API surface to Lua

    // Some Principles of this API, subject to change as we profile:
    // - It is better to have out than ref parameter unless necessary
    // - Prefer static function more than singleton object pattern
    // - Minimize number of structure we use and mark [GCOptimize] for all
    // - Use LuaFunction and LuaTable instead of delegate and C# containers
    // - Table we return follows Lua convention of starting from index 1

    public static class LuaInterface
    {
        // -------------------------------
        // Resource Loading
        // -------------------------------

        public static bool GetAsset(string resourceName, out UnityEngine.Object asset)
        {
            asset = null;

            if (StickerResourceMananger.instance == null)
            {
                return false;
            }

            asset = StickerResourceMananger.instance.GetAsset(resourceName);
            return asset != null;
        }

        public static bool InstantiatePrefab(string name, Vector3 position, Quaternion rotation, Transform parent, out GameObject instance)
        {
            instance = null;

            if (StickerResourceMananger.instance == null)
            {
                return false;
            }

            instance = StickerResourceMananger.instance.InstantiatePrefab(name, position, rotation, parent);
            return instance != null;
        }
    }
}