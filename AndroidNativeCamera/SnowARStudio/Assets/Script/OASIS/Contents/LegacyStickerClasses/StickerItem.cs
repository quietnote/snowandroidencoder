﻿namespace OASIS.Contents
{
    [System.Serializable]
    public class StickerItem
    {
        public string id = "";
        public DrawType drawType;
        public FaceLocationType faceLocationType;
        public float translateX = 0f;
        public float translateY = 0f;
        public int frameCount = 0;
        public float scale = 0f;
        public string resourceName;
        public string faceVertices;
        public string customData;

        public string Key
        {
            get
            {
                return string.Format("{0}_{1}", drawType.ToString(), faceLocationType.ToString());
            }
        }
    }
}