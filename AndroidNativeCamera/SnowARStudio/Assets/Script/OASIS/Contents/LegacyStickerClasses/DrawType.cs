﻿namespace OASIS.Contents
{
    public enum DrawType
    {
        NULL,
        FACE,
        BACKGROUND,
        BUILT_IN,
        FACE_3D,
        FACE_SKIN,
        FACE_SKIN_EX,
        FACE_DISTORTION,
        MORPH_SWAP,
        PARTICLE,
        PREVIEW,
        PREVIEW_EX,
        SCRIPT,
        FACE_TEXT,
        BG_TEXT,
        SNOW,
        //-- snow migration 지원
        SNOW_B,
        SNOW_P,
        SNOW_T,
        SNOW_F,
        SNOW_D,
        SNOW_V,
        SNOW_K,
        FACE_MASK,
        FACE_MASK_INPUT,
        SEGMENTATION,
        SEGMENTATION_SRC,
        COLOR_LENS,
        WORLD_LENS,
        AR_3D,
        MESH_DISTORTION
    }
}