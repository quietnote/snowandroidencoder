﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace OASIS.Contents
{
    public class DownloadedSticker
    {
        public string name = "";
        public string filterName = "";
        public bool noFilterOnSticker;
        public List<SoundItem> soundItem = new List<SoundItem>();
        public List<StickerItem> items = new List<StickerItem>();
        public long stickerId;
        public long modifiedDate;

        // disableSmooth = d.disableSmooth;
        // noFilterOnSticker = d.noFilterOnSticker;
        // moveToConfirm = d.moveToConfirm;
        // filterResourceName = d.filterResourceName;
        // customizedTooltip = d.customizedTooltip;
        // customizedTooltipImgResourceName = d.customizedTooltipImgResourceName;
        // customizedTooltips = d.customizedTooltips;
        // floatingTextBanners = d.floatingTextBanners;
        // bgmTooltip = d.bgmTooltip;
        // maxFrameCount = d.maxFrameCount;
        // minFps = d.minFps;
        // sceneConfig = d.sceneConfig;
        // guidePopups = d.guidePopups;
        // collageId = d.collageId;
        // adUrl = d.adUrl;
        // cameraPositionType = d.cameraPositionType;
        // aiMeta = d.aiMeta;
        // aiModel = d.aiModel;
        // captureByScript = d.captureByScript;
        // resetOnVideoRecoding = d.resetOnVideoRecoding;
        // guidePopupMaxViewCount = d.guidePopupMaxViewCount;
        // contentType = d.contentType;
    }
}