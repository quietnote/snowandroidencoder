﻿namespace OASIS.Contents
{
    public enum FaceLocationType
    {
        //-- FACE의 경우 translateY 값에 따라서 27, 29 중심이 바뀐다.
        FACE,
        FACE_CENTER,
        EYES_CENTER,
        EYE_LT,
        EYE_RT,
        EYE_LB,
        EYE_RB,
        NOSE,
        NOSE_B,
        NOSE_L,
        NOSE_R,
        MOUTH,
        EYEBROW_L,
        EYEBROW_R,
        MOUTH_T,
        MOUTH_B,
        CHIN
    }
}