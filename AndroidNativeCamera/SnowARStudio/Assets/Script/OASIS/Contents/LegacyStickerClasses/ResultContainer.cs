﻿using UnityEngine;
using System.IO;

namespace OASIS.Contents
{
    public class ResultContainer<T>
    {
        public T result;
        // public ServerError error;

        public bool IsEmpty() {
            return result == null;
        }
    }
}