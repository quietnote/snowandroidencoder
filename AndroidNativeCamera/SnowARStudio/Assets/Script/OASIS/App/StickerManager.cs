﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS
{
    using ST;
    using Contents;
    using Data;
    // ACAPP 다음으로 실행되는 instance
    public class StickerManager : MonoBehaviour
    {
        enum StickerStyle
        {
            LegacyCamera, //RenderFactory

            SnowSticker
        }

        [SerializeField]
        StickerStyle style = StickerStyle.SnowSticker;

        [SerializeField]
        JsonParser JsonNode = null;
        [SerializeField]
        GameObject FaceTargetGO;

        Dictionary<int, GameObject> m_faceObject = new Dictionary<int, GameObject>();
        Dictionary<int, FaceStickerRendererBase[]> m_faceRenderers = new Dictionary<int, FaceStickerRendererBase[]>();
        Dictionary<int, SenseTimeLib.FaceActionData> m_faceAction = new Dictionary<int, SenseTimeLib.FaceActionData>();

        HashSet<int> m_lastFace = new HashSet<int>();
        HashSet<int> m_currentFace = new HashSet<int>();

        [SerializeField]
        private SenseTimeLib.Config m_configData = new SenseTimeLib.Config();

        GameObject FaceRootGO;

        public SenseTimeLib.Config GetConfigData()
        {
            return m_configData;
        }

        void Awake()
        {
#if UNITY_EDITOR
            if(!Camera.main)
            {
                ACApp.GLog.LogError("Please set Tag MainCamera");
            }
#endif

            if (m_configData.maxFaceCount > 0 && FaceTargetGO != null)
            {

                switch (style)
                {
                    case StickerStyle.SnowSticker:
                        {
                            FaceTargetGO.SetActive(false);
                        }
                        break;
                        // 레거시 방식은 Json에서 여러가지의 item을 생성하는 형태로 되어있다.
                    case StickerStyle.LegacyCamera:
                        {
                            GameObject FaceOriginGO = new GameObject("FaceOrigin");
                            FaceOriginGO.transform.SetParent(FaceTargetGO.transform);
                            FaceOriginGO.SetActive(false);
                            // Renderer 붙이기까지 완성
                            FaceTargetGO.GetComponent<RendererFactory>().Init(FaceOriginGO.transform);
                            // Json 읽기
                            JsonNode.ReadJson();

                            FaceTargetGO = FaceOriginGO;
                            // FaceTargetGO를 생성된것들로 assign
                        }
                        break;
                }
                // 메인카메라가 얼굴 스티커들의 parent
                FaceRootGO = Camera.main.gameObject;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (m_configData.maxFaceCount > 0 && FaceTargetGO != null)
            {
                if (ACApp.instance.GetFaceCount() > 0)
                {
                    m_currentFace.Clear();
                    for (int i = 0; i < ACApp.instance.GetFaceCount(); ++i)
                    {
                        FaceData face = ACApp.instance.GetFaceByIndex(i);

                        GameObject curFaceSticker;
                        int id = face.getDataID();
                        if (m_faceObject.TryGetValue(id, out curFaceSticker))
                        {
                            curFaceSticker.SetActive(true);

                            SenseTimeLib.FaceActionData faceAction = m_faceAction[id];
                            FaceStickerRendererBase[] renderers = m_faceRenderers[id];

                            for (int action = 0; action < (int)SenseTimeLib.FaceAction.kCount; ++action)
                            {
                                if (faceAction.GetState((SenseTimeLib.FaceAction)action) == face.sourceData.faceActionData.GetState((SenseTimeLib.FaceAction)action))
                                {
                                    // No event happened
                                }
                                else if (face.sourceData.faceActionData.GetState((SenseTimeLib.FaceAction)action))
                                {
                                    foreach (FaceStickerRendererBase renderer in renderers)
                                    {
                                        renderer.GetFaceActionEvents((SenseTimeLib.FaceAction)action).onTriggerOn?.Invoke();
                                    }
                                }
                                else
                                {
                                    foreach (FaceStickerRendererBase renderer in renderers)
                                    {
                                        renderer.GetFaceActionEvents((SenseTimeLib.FaceAction)action).onTriggerOff?.Invoke();
                                    }
                                }
                            }

                            m_faceAction[id] = face.sourceData.faceActionData;
                        }
                        else
                        {
                            // 얼굴노드가 없었으면 생성
                            curFaceSticker = GameObject.Instantiate(FaceTargetGO, FaceRootGO.transform);

                            FaceStickerRendererBase [] stickerRenderer = curFaceSticker.GetComponentsInChildren<FaceStickerRendererBase>();
                            for(int rendererIter = 0; rendererIter < stickerRenderer.Length;++rendererIter )
                            {
                                stickerRenderer[rendererIter].setDataID(id);
                                for (int action = 0; action < (int)SenseTimeLib.FaceAction.kCount; ++action)
                                {
                                    stickerRenderer[rendererIter].GetFaceActionEvents((SenseTimeLib.FaceAction)action).initialValueSet?.Invoke(
                                        face.sourceData.faceActionData.GetState((SenseTimeLib.FaceAction)action));
                                }
                            }

                            m_faceObject.Add(id, curFaceSticker);
                            m_faceRenderers.Add(id, stickerRenderer);
                            m_faceAction.Add(id, face.sourceData.faceActionData);
                        }
                        m_currentFace.Add(id);
                    }
                    // 안쓰는 얼굴 노드 삭제
                    foreach (int id in m_lastFace)
                    {
                        if (!m_currentFace.Contains(id))
                        {
                            GameObject willDeleteGO;
                            m_faceObject.TryGetValue(id, out willDeleteGO);
                            Destroy(willDeleteGO);
                            m_faceObject.Remove(id);
                            m_faceRenderers.Remove(id);
                            m_faceAction.Remove(id);
                        }
                    }
                    // current -> last 카피
                    m_lastFace.Clear();
                    foreach (int id in m_currentFace)
                    {
                        m_lastFace.Add(id);
                    }
                }
                else
                {
                    m_currentFace.Clear();
                    m_lastFace.Clear();
                    foreach (KeyValuePair<int, GameObject> entry in m_faceObject)
                    {
                        Destroy(entry.Value);
                    }
                    m_faceObject.Clear();
                    m_faceRenderers.Clear();
                    m_faceAction.Clear();
                }
            }
        }

        void OnDestroy()
        {
            m_currentFace.Clear();
            m_lastFace.Clear();
            foreach (KeyValuePair<int, GameObject> entry in m_faceObject)
            {
                Destroy(entry.Value);
            }
            m_faceObject.Clear();
            m_faceRenderers.Clear();
            m_faceAction.Clear();
        }
    }
}