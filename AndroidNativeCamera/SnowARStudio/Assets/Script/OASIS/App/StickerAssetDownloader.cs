﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.S3;
using Amazon.S3.Model;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace OASIS
{
    using CS; //contents server
    public class StickerAssetDownloader : MonoBehaviour // : MonoSingletonBase<StickerAssetDownloader>
    {

#pragma warning disable CS0649 // Private SerializeField set by Unity Editor

        [SerializeField]
        ContentsServerSettings m_ServerSettings;

#pragma warning restore CS0649

        UnityMainThreadDispatcher m_AWSMainThreadDispatcher;
        AWSCredentials m_Credentials;
        IAmazonS3 m_S3Client;

        string m_StickerCachePath;
        public string stickerCachePath
        {
            get { return m_StickerCachePath; }
        }

        void Start()
        {
            m_AWSMainThreadDispatcher = UnityInitializer.StartAWSSession(gameObject);
            m_Credentials = new CognitoAWSCredentials(m_ServerSettings.identityPoolId, m_ServerSettings.cognitoIdentityRegion);
            m_S3Client = new AmazonS3Client(m_Credentials, m_ServerSettings.s3Region);

            m_StickerCachePath = Path.Combine(Application.persistentDataPath, "StickerCache");
        }

        void OnDestroy()
        {
            m_S3Client.Dispose();
            m_S3Client = null;
            m_Credentials = null;

            UnityInitializer.EndAWSSession(m_AWSMainThreadDispatcher);
            m_AWSMainThreadDispatcher = null;
        }

        public event System.Action stickerStateSynced;
        public event System.Action<string> stickerDownloading;
        public event System.Action<string> stickerDownloaded;

        public enum StickerAssetState
        {
            kToBeDownloaded,
            kDownloading,
            kDownloaded
        }
        Dictionary<string, StickerAssetState> m_StickerStates = new Dictionary<string, StickerAssetState>();
        public Dictionary<string, StickerAssetState> stickerStates
        {
            get { return m_StickerStates; }
        }

        public void SyncStickerState(bool clearCache)
        {
            if (clearCache)
            {
                if (Directory.Exists(m_StickerCachePath))
                {
                    Directory.Delete(m_StickerCachePath, true);
                    Directory.CreateDirectory(m_StickerCachePath);
                }
            }

            string folderName = ARStickerRemoteStoragePolicy.GetFolderForRuntimePlatform(Application.platform);
            var request = new ListObjectsRequest()
            {
                BucketName = m_ServerSettings.bucketName,
                Prefix = folderName
            };

            if (m_HandleStickerListed == null)
            {
                m_HandleStickerListed = HandleStickerListed;
            }
            m_S3Client.ListObjectsAsync(request, m_HandleStickerListed);
        }

        List<string> m_DownloadingStickerCache = new List<string>();
        AmazonServiceCallback<ListObjectsRequest, ListObjectsResponse> m_HandleStickerListed;
        void HandleStickerListed(AmazonServiceResult<ListObjectsRequest, ListObjectsResponse> responseObj)
        {
            if (responseObj.Exception != null || responseObj.Response == null)
            {
                Debug.LogError("ListObjectsRequest fails");
                if (responseObj.Exception != null)
                {
                    Debug.LogError("Exception: " + responseObj.Exception.ToString());
                }
                return;
            }

            // Remove all element from m_StickerStates except those currently downloading
            {
                m_DownloadingStickerCache.Clear();
                foreach (KeyValuePair<string, StickerAssetState> pair in m_StickerStates)
                {
                    if (pair.Value == StickerAssetState.kDownloading)
                    {
                        m_DownloadingStickerCache.Add(pair.Key);
                    }
                }
                m_StickerStates.Clear();
                foreach (string name in m_DownloadingStickerCache)
                {
                    m_StickerStates[name] = StickerAssetState.kDownloading;
                }
            }

            List<S3Object> s3Objects = responseObj.Response.S3Objects;
            for (int i = 0; i < s3Objects.Count; ++i)
            {
                int prefixLength = responseObj.Request.Prefix.Length;

                string stickerName = s3Objects[i].Key.Substring(prefixLength + 1);

                if (!m_StickerStates.ContainsKey(stickerName))
                {
                    m_StickerStates[stickerName] = StickerAssetState.kToBeDownloaded;
                }
            }

            // In case our cache directory does not already exist
            Directory.CreateDirectory(m_StickerCachePath);
            try
            {
                foreach (string path in Directory.GetFiles(m_StickerCachePath))
                {
                    string localFileName = Path.GetFileName(path);

#if UNITY_EDITOR_OSX

                if (localFileName == ".DS_Store")
                {
                    continue;
                }

#endif
                    StickerAssetState state;
                    bool hasState = m_StickerStates.TryGetValue(localFileName, out state);
                    if (hasState && state == StickerAssetState.kToBeDownloaded)
                    {
                        m_StickerStates[localFileName] = StickerAssetState.kDownloaded;
                    }
                }
            }
            catch (IOException ex)
            {
                Debug.Log(ex.ToString());
            }

            stickerStateSynced.Invoke();
        }

        public void DownloadSticker(string stickerName)
        {
            StickerAssetState state;
            bool hasState = m_StickerStates.TryGetValue(stickerName, out state);
            if (hasState && state != StickerAssetState.kToBeDownloaded)
            {
                // According to info from when we synced last time
                // Sticker is already downloading/downloaded
                return;
            }

            string folderName = ARStickerRemoteStoragePolicy.GetFolderForRuntimePlatform(Application.platform);
            GetObjectRequest request = new GetObjectRequest
            {
                BucketName = m_ServerSettings.bucketName,
                Key = folderName + "/" + stickerName
            };

            if (m_HandleStickerDownloaded == null)
            {
                m_HandleStickerDownloaded = HandleStickerDownloaded;
            }
            m_S3Client.GetObjectAsync(request, m_HandleStickerDownloaded);

            m_StickerStates[stickerName] = StickerAssetState.kDownloading;

            stickerDownloading.Invoke(stickerName);
        }

        AmazonServiceCallback<GetObjectRequest, GetObjectResponse> m_HandleStickerDownloaded;

        void HandleStickerDownloaded(AmazonServiceResult<GetObjectRequest, GetObjectResponse> responseObj)
        {
            string stickerName = Path.GetFileName(responseObj.Request.Key);

            if (responseObj.Exception != null || responseObj.Response == null)
            {
                m_StickerStates[stickerName] = StickerAssetState.kToBeDownloaded;

                Debug.LogError("GetObjectRequest fails");
                if (responseObj.Exception != null)
                {
                    Debug.LogError("Exception: " + responseObj.Exception.ToString());
                }
                return;
            }

            string localStickerPath = Path.Combine(m_StickerCachePath, stickerName);
            FileStream fileStream = File.Create(localStickerPath);
            responseObj.Response.ResponseStream.CopyTo(fileStream);
            fileStream.Close();

            m_StickerStates[stickerName] = StickerAssetState.kDownloaded;
            stickerDownloaded.Invoke(stickerName);
        }
    }

}