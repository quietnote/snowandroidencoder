﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Snow.G;
namespace OASIS
{
    public class StickerResourceMananger : MonoSingletonBase<StickerResourceMananger>
    {
        new void Awake()
        {
            base.Awake();

            if (Application.isPlaying)
                InitResourceLookupTable();
        }

        public List<string> referencedAssetNames = new List<string>();

        public List<Object> referencedAssets = new List<Object>();

        // Read-only hash table used for (resource name -> resource file pointer) lookup
        Dictionary<string, Object> nameToObjectLookupTable = new Dictionary<string, Object>();

        void InitResourceLookupTable()
        {
            Debug.Assert(referencedAssetNames.Count == referencedAssets.Count);
            nameToObjectLookupTable.Clear();
            for (int i = 0; i < referencedAssetNames.Count; ++i)
            {
                nameToObjectLookupTable[referencedAssetNames[i]] = referencedAssets[i];
            }
        }

        public Object GetAsset(string name)
        {
            Object rtn = null;
            if (nameToObjectLookupTable.TryGetValue(name, out rtn))
            {
                return rtn;
            }

            Debug.LogError("No resource with name: " + name);
            return null;
        }

        public GameObject InstantiatePrefab(string name, Vector3 position, Quaternion rotation, Transform parent)
        {
            Object asset = GetAsset(name);
            if (asset == null)
            {
                return null;
            }

            GameObject prefab = asset as GameObject;
            if (prefab == null)
            {
                Debug.LogError("Resource with name: " + name + " is not a prefab");
                return null;
            }

            return Instantiate(prefab, position, rotation, parent);
        }
    }
}