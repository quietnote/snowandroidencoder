﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif
using Snow.G;

namespace OASIS
{
    using Data;
    using AR;
    using ST;
    // 앱이 뜰때 단일.
    // 기본 매니저들은 이클래스를 통해서 호출
    public class ACApp : MonoBehaviour
    {
        // 생성 삭제 컨트롤 위해 mono singleton base 상속받지않음
        public static ACApp instance { get; private set; }

        // 각 매니저들을 넣으면서 싱글톤을 제거. 
        // app instance를 싱글톤으로 두고 app instance를 통해 manager들을 사용한다. 
        // 단 infra 용의 매니저들은 싱글톤으로 생성하고 직접접근.

        public static Snow.G.LogSystem GLog { get; private set; }

        private LocalStickerAssetManager m_localStickeAssetManager = null;
        public LocalStickerAssetManager localStickerAssetManager { get { return m_localStickeAssetManager; } }

        private StickerAssetDownloader m_stickerAssetDownloader = null;
        public StickerAssetDownloader stickerAssetDownloader { get { return m_stickerAssetDownloader; } }

        private StickerAssetLoader m_stickerAssetLoader = null;
        public StickerAssetLoader stickerAssetLoader { get { return m_stickerAssetLoader; } }

        private ACData m_data = null;
        public ACData data { get { return m_data; } }

        private IARSession m_session = null;

        private StickerManager m_stickerManager = null;

        public StickerManager stickerManager { get { return m_stickerManager; } }

        private bool isMainSceneLoaded = false;


#if UNITY_EDITOR
        [SerializeField]
        public GameObject m_knot;
#endif

        private bool Create()
        {
            GLog = new LogSystem();
            GLog.Create(true);
            GLog.FileLog("=== App Created! ===");
            return CreateGlobalInstances();
        }

        private bool CreateGlobalInstances()
        {
            // 여러 매니저들 생성
            // 일단 붙여져있는것들 연결
            m_data = GetComponent<ACData>();
            // 앱에서만 필요.
            m_localStickeAssetManager = GetComponent<LocalStickerAssetManager>();
            // 앱에서만 필요
            m_stickerAssetDownloader = GetComponent<StickerAssetDownloader>();
            // 앱에서만 핊요
            m_stickerAssetLoader = GetComponent<StickerAssetLoader>();

            // 상황에 맞게 프로바이더 붙이는것 분기를 해야.
            m_session = new WebCamProvider(
                () => m_data.gravityVector,
                () => m_data.rawGravityVector,
                () => m_data.rawUserAcceleration,
                () => m_data.rawRotationRate,
                () => m_data.rawAttitude);
            m_session.SessionBegin();

#if UNITY_EDITOR
            GameObject debugGO = new GameObject("Debug");
            debugGO.transform.SetParent(transform);
            ACDebug debug = debugGO.AddComponent<ACDebug>();
            debug.m_knot = m_knot;


            // heejun temp code - 우선 에디터에서 On/off만 지원한다..
            if(EditorPrefs.GetBool("SceneAutoLoader.Landmark", false)) 
            {
                debug.landMarkType = ACDebug.LandMarkType.SENSETIME;
            } else
            {
                debug.landMarkType = ACDebug.LandMarkType.None;
            }
        
            
#endif

            return true;
        }
        private void DestroyGlobalInstances()
        {
            m_session.SessionEnd();
            m_session.Dispose();
        }

        private void Awake()
        {
            // 처음 생성
            if (null == instance)
            {
                instance = this;
                if (false == Create())
                {
                    Destroy(gameObject);
                    return;
                }
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Debug.LogWarning("ACAppMonobehaviour already Exist");
                // 이미 인스턴스가 있는상태 (두개 배치 혹은 파괴되지 않고 다시 로드) => 종료
                Destroy(gameObject);
            }
        }
        void Start()
        {
            DeviceSensor.EnsureSensorIsOn();

#if UNITY_EDITOR
            if (UnityEditor.EditorPrefs.GetBool("SceneAutoLoader.PlayForStudio", false))
            {
                SceneManager.UnloadSceneAsync(ACConst.Path.SCENE + ACConst.SceneName.STARTER + ACConst.Extention.SCENE);

                // sticker 가 이미 있으니 받아온다
                LoadStickerManager();

                isMainSceneLoaded = true;
            }
            else
            {
                // app mode
                StartCoroutine(LoadScene(ACConst.Path.SCENE + ACConst.SceneName.CAMERA_APP + ACConst.Extention.SCENE));
            }
#else
            // 다음 씬 로딩 일단 앱일경우
            StartCoroutine(LoadScene(ACConst.Path.SCENE + ACConst.SceneName.CAMERA_APP + ACConst.Extention.SCENE));
#endif
        }

        IEnumerator LoadScene(string name)
        {
            // Just calling SceneManager.LoadSceneAsync(name) cause WebCamTexture to freeze, this is work-around
            yield return SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
            yield return SceneManager.UnloadSceneAsync(ACConst.Path.SCENE + ACConst.SceneName.STARTER + ACConst.Extention.SCENE);

            // 로딩후 처리

            isMainSceneLoaded = true;

            // UI 들 연결처리 (필요하다면)
        }

        private void OnDestroy()
        {
            if (instance == this)
            {
                DestroyGlobalInstances();

                GLog.FileLog("=== App Destroyed! ===");
                GLog.Destroy();
                GLog = null;
                instance = null;
            }
        }

        public void SetNextCamera()
        {
            m_session.SetNextCamera();
        }

        public int GetFaceCount()
        {
            return m_data.faceCount;
        }

        public FaceData GetFaceByIndex(int index)
        {
            return m_data.GetFace(index);
        }

        public FaceData GetFaceByID(int id)
        {
            for (int i = 0; i < m_data.faceCount; ++i)
            {
                if (id == m_data.GetFace(i).getDataID())
                {
                    return m_data.GetFace(i);
                }
            }
            return null;
        }

        public CameraFrame GetFrame()
        {
            return m_data.frame;
        }

        public Vector3 GetDevicePosition()
        {
            return m_data.devicePosition;
        }

        public Quaternion GetDeviceRotation()
        {
            return m_data.deticeRotation;
        }

        public void UnLoadStickerManager()
        {
            // If we do not pause computer vision analysis when loading scene, android may crash
            m_session.SetAnalysisPaused(true);

            m_stickerManager = null;
        }

        public void LoadStickerManager()
        {
            GameObject root = GameObject.FindGameObjectWithTag("StickerRoot");
            if( root == null)
            {
                m_stickerManager = null;
                return;
            }
            m_stickerManager = root.GetComponent<StickerManager>();

            m_session.SetAnalysisPaused(false);
            m_session.UpdateAnalysisConfig(m_stickerManager.GetConfigData());
        }

        void Update()
        {
            if (!isMainSceneLoaded)
            {
                return;
            }

            // On mobile platform we get raw sensor data
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                if (!DeviceSensor.GetRawGravityVector(ref m_data.rawGravityVector))
                    return;

                if (!DeviceSensor.GetRawUserAcceleration(ref m_data.rawUserAcceleration))
                    return;

                if (!DeviceSensor.GetRawRotationRate(ref m_data.rawRotationRate))
                    return;

                if (!DeviceSensor.GetRawAttitude(ref m_data.rawAttitude))
                    return;
            }

            if (!DeviceSensor.GetGravityVector(ref m_data.gravityVector))
            {
                // error
                return;
            }

            m_session.Tick();

            if (!m_session.GetFrame(ref m_data.frame))
            {
                // error
                return;
            }

            if (m_stickerManager == null)
            {
                return;
            }

            // sticker manager 에서 config 값을 세팅을 할것인가, 여기서 sticker manager 의 값을 읽을 것인가.
            SenseTimeLib.Config flags = m_stickerManager.GetConfigData();

            // 세션이 필요하면
            if (flags.maxFaceCount > 0 || flags.maxHandCount > 0 || flags.maxBodyCount > 0 || flags.useSLAM)
            {
                if (flags.maxFaceCount > 0)
                {
                    m_data.faceCount = m_session.GetFaceCount();
                    for (int i = 0; i < m_data.faceCount; ++i)
                    {
                        m_session.GetFaceAt(i, ref m_data.faceDatas[i].sourceData);
                        m_data.faceDatas[i].setDataID(m_data.faceDatas[i].sourceData.ID);
                    }

                    for (int i = 0; i < m_data.faceCount; ++i)
                    {
                        FaceBuilder.Build(ref m_data.faceDatas[i], ref m_data.frame);
                        TransformBuilder.Build(m_data.frame, ref m_data.faceDatas[i]);
                        TransformBuilder.MakeFeaturePoints(ref m_data.faceDatas[i]);
                        ExtraTransformBuilder.Build(ref m_data.faceDatas[i],
                                                    m_data.frame.virtualHeight / m_data.frame.virtualWidth,
                                                    ACConst.extraScale);
                    }
                }

                if (flags.maxHandCount > 0)
                {
                    //m_Session.GetHandData();
                }

                if (flags.maxBodyCount > 0)
                {
                    //m_Session.GetBodyData();
                }

                // Regardless if SLAM is used, we always get some pose value
                // But if SLAM is used, we need to analyze first, then get pose value second
                m_session.GetPose(ref m_data.devicePosition, ref m_data.deticeRotation);
            }
            else
            {
                // Get default pose without computer vision analysis
                m_session.GetPose(ref m_data.devicePosition, ref m_data.deticeRotation);
            }
        }

        /// <summary>
        /// GraphicsRenderType이 Metal인지 확인
        /// </summary>
        /// <returns></returns>
        public bool isMetal()
        {
            return (SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Metal);
        }
    }
}