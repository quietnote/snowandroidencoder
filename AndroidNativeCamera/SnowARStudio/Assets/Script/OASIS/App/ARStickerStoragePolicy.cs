﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace OASIS
{

    // to do : merge ACConst ??
    public class ARStickerRemoteStoragePolicy
    {

#if UNITY_EDITOR
        public static string GetFolderForBuildTarget(BuildTarget target)
        {
            switch (target)
            {
                case (BuildTarget.Android):
                    return "Android";

                case (BuildTarget.iOS):
                    return "iOS";

                case (BuildTarget.StandaloneWindows):
                case (BuildTarget.StandaloneWindows64):
                    return "Windows";

                case (BuildTarget.StandaloneOSX):
                    return "OSX";

                default:
                    return "Default";
            }
        }
#endif

        public static string GetFolderForRuntimePlatform(RuntimePlatform platform)
        {
            switch (platform)
            {
                case (RuntimePlatform.Android):
                    return "Android";

                case (RuntimePlatform.IPhonePlayer):
                    return "iOS";

                case (RuntimePlatform.WindowsEditor):
                case (RuntimePlatform.WindowsPlayer):
                    return "Windows";

                case (RuntimePlatform.OSXEditor):
                case (RuntimePlatform.OSXPlayer):
                    return "OSX";

                default:
                    return "Default";
            }
        }
    }
}