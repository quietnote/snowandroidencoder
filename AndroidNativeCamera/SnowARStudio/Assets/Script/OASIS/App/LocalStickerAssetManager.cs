﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

using Snow.G;
namespace OASIS
{
    public class LocalStickerAssetManager : MonoBehaviour // : MonoSingletonBase<LocalStickerAssetManager>
    {
        string m_LocalStickerDatabasePath;
        public string localStickerDatabasePath
        {
            get { return m_LocalStickerDatabasePath; }
        }

        void Start()
        {
#if UNITY_EDITOR
            m_LocalStickerDatabasePath = PlatformDepStreamingAssetsHandler.PlatformDepStreamingAssetsPathFor(Application.platform);
#else
            m_LocalStickerDatabasePath = Application.streamingAssetsPath;
#endif
        }

#pragma warning disable CS0649 // Private SerializeField set by Unity Editor

        [SerializeField]
        List<string> m_LocalStickerNames;

        [SerializeField]
        string m_DefaultStickerName;

#pragma warning restore CS0649

        public List<string> localStickerNames
        {
            get { return m_LocalStickerNames; }
        }

        public string defaultStickerName
        {
            get { return m_DefaultStickerName;  }
        }

    }
}
