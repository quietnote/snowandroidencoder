﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS
{
    using Data;
    using AR;
    // To do : release 에서 포함되지 않도록 define 처리 가 필요.
    public class ACDebug : MonoBehaviour
    {
        public float extraScale = 2.5f;

        public enum LandMarkType
        {
            None,
            ULSEE,
            ULSEE_EXTRA,
            SENSETIME
        }
        public LandMarkType landMarkType = LandMarkType.None;
        [SerializeField]
        public GameObject m_knot;
        private Transform[] knotParent;

        private GameObject[] m_knots;

        // Start is called before the first frame update
        void Start()
        {
            knotParent = new Transform[3];
            GameObject KnotOriginGO = new GameObject("Red");
            KnotOriginGO.transform.SetParent(transform);
            knotParent[0] = KnotOriginGO.transform;
            KnotOriginGO = new GameObject("Green");
            KnotOriginGO.transform.SetParent(transform);
            knotParent[1] = KnotOriginGO.transform;
            KnotOriginGO = new GameObject("Blue");
            KnotOriginGO.transform.SetParent(transform);
            knotParent[2] = KnotOriginGO.transform;
        }

        // Update is called once per frame
        void Update()
        {
            if (ACApp.instance && ACApp.instance.GetFaceCount() > 0)
            {
                for (int i = 0; i < ACApp.instance.GetFaceCount(); ++i)
                {
                    FaceData faceData = ACApp.instance.GetFaceByIndex(i);
                    CameraFrame frame = ACApp.instance.GetFrame();

                    if (m_knot != null)
                    {
                        SetKnotsForDebugging(ref faceData, frame.virtualHeight / frame.virtualWidth, frame.virtualDistance);
                    }
                }

                if (knotParent != null)
                {
                    for (int i = 0; i < knotParent.Length; ++i)
                    {
                        knotParent[i].gameObject.SetActive((i + 1) == (int) (landMarkType));
                    }
                }
            }
        }

        void SetKnotsForDebugging(ref FaceData fd, float aspecRatio, float distance)
        {
            if (knotParent == null || knotParent.Length < 2)
            {
                return;
            }
            if (landMarkType == LandMarkType.ULSEE)
            {
                int length = ACConst.FACE_ULSEE_SIZE;
                if (m_knots == null || m_knots.Length != length)
                {
                    DestroyGameObjects(m_knots);
                    m_knots = GenerateFeatures(knotParent[0], length, Color.red);
                }
                for (int k = 0; k < m_knots.Length; ++k)
                {
                    m_knots[k].transform.localPosition = new Vector3(fd.featurePositions[k].x, fd.featurePositions[k].y, distance);
                }
            }

            if (landMarkType == LandMarkType.ULSEE_EXTRA)
            {
                int length = ACConst.FACE_EXTRA_FEATURE_SIZE;
                if (m_knots == null || m_knots.Length != length)
                {
                    DestroyGameObjects(m_knots);
                    m_knots = GenerateFeatures(knotParent[1], length, Color.green);
                }
                for (int k = 0; k < m_knots.Length; ++k)
                {
                    m_knots[k].transform.localPosition = new Vector3(fd.extraFeaturePositions[k].x, fd.extraFeaturePositions[k].y, distance);
                }
            }

            if (landMarkType == LandMarkType.SENSETIME)
            {
                int length = ACConst.FACE_SENSETIME_SIZE;
                if (m_knots == null || m_knots.Length != length)
                {
                    DestroyGameObjects(m_knots);
                    m_knots = GenerateFeatures(knotParent[2], length, Color.blue);
                }
                for (int k = 0; k < m_knots.Length; ++k)
                {
                    m_knots[k].transform.localPosition = new Vector3(fd.verticesArray[k].x, fd.verticesArray[k].y, distance);
                }
            }
        }

        void DestroyGameObjects(GameObject[] obj)
        {
            if (obj != null)
            {
                for (int i = 0; i < m_knots.Length; ++i)
                {
                    DestroyImmediate(m_knots[i]);
                }
            }
        }

        GameObject[] GenerateFeatures(Transform parent, int count, Color color)
        {
            GameObject[] outputVertices = new GameObject[count];
            for (int i = 0; i < count; ++i)
            {
                outputVertices[i] = parent == null ? Instantiate(m_knot, Vector3.zero, Quaternion.identity) : Instantiate(m_knot, Vector3.zero, Quaternion.identity, parent);
                outputVertices[i].name = string.Format("knot{0}", i);
                outputVertices[i].GetComponentInChildren<UnityEngine.UI.Text>().text = i.ToString();
                outputVertices[i].GetComponent<Renderer>().material.SetColor("_Color", color);
            }
            return outputVertices;
        }
    }
}