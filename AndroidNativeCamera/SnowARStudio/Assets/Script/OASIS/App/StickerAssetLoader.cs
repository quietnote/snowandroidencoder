﻿using Amazon;
using Amazon.CognitoIdentity;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.S3;
using Amazon.S3.Model;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace OASIS
{
    using AR;
    // to do : TrackedPoseDriver 위치가 Contents가 맞는가?
    using Contents;
    public enum StickerLoadSource
    {
        kFromLocal,
        kFromCache
    }

    public class StickerAssetLoader : MonoBehaviour //: MonoSingletonBase<StickerAssetLoader>
    {
        Camera m_CameraAppARCamera;
        TrackedPoseDriver m_CameraAppPoseDriver;
        ARCameraBackground m_CameraAppCamBackground;

        void Start()
        {
            GameObject originGO = new GameObject("CameraApp AR Session Origin");
            originGO.transform.SetParent(transform);
            originGO.AddComponent<ARSessionOrigin>();

            GameObject cameraGO = new GameObject("CameraApp AR Camera");
            cameraGO.transform.SetParent(originGO.transform);
            m_CameraAppARCamera = cameraGO.AddComponent<Camera>();
            m_CameraAppPoseDriver = cameraGO.AddComponent<TrackedPoseDriver>();
            m_CameraAppCamBackground = cameraGO.AddComponent<ARCameraBackground>();

            // We make sure m_CameraAppARCamera will be rendered on top when enabled
            // Camera in stickers must have depth less than 100
            m_CameraAppARCamera.enabled = false;
            m_CameraAppARCamera.depth = 100; 
            m_CameraAppARCamera.clearFlags = CameraClearFlags.Color;
            m_CameraAppARCamera.backgroundColor = Color.black;
            m_CameraAppARCamera.nearClipPlane = 0.1f;
            m_CameraAppARCamera.farClipPlane = 20f;
            m_CameraAppARCamera.cullingMask = 0; // Renders nothing from Unity, just do background rendering with ARCameraBackground
        }

        public void LoadSticker(string stickerName, StickerLoadSource source)
        {
            m_StickersToLoad.Enqueue(new StickerLoadCommand(stickerName, source));
            StartCoroutine(LoadPendingStickers());
        }

        struct StickerLoadCommand
        {
            public StickerLoadCommand(string inStickerName, StickerLoadSource inSource)
            {
                stickerName = inStickerName;
                source = inSource;
            }

            public string stickerName;
            public StickerLoadSource source;
        }
        Queue<StickerLoadCommand> m_StickersToLoad = new Queue<StickerLoadCommand>();

        bool m_IsLoadingSticker = false;

        IEnumerator LoadPendingStickers()
        {
            if (!m_IsLoadingSticker)
            {
                m_IsLoadingSticker = true;

                while (m_StickersToLoad.Count >= 1)
                {
                    StickerLoadCommand loadCommand = m_StickersToLoad.Dequeue();
                    yield return DoLoadSticker(loadCommand.stickerName, loadCommand.source);
                }

                m_IsLoadingSticker = false;
            }
        }

        bool m_HasActiveSticker = false;
        Scene m_CurrentStickerScene = new Scene();
        AssetBundle m_CurrentAssetBundle = null;

        IEnumerator DoLoadSticker(string stickerName, StickerLoadSource loadSource)
        {
            string stickerPath;

            if (loadSource == StickerLoadSource.kFromLocal)
            {
                stickerPath = Path.Combine(ACApp.instance.localStickerAssetManager.localStickerDatabasePath, stickerName);
            }
            else // if (loadSource == StickerLoadSource.kFromCache)
            {
                stickerPath = Path.Combine(ACApp.instance.stickerAssetDownloader.stickerCachePath, stickerName);
            }

            // Turn on an AR camera to cover up additive scene loading freeze
            m_CameraAppARCamera.enabled = true;
            m_CameraAppCamBackground.enabled = true;
            m_CameraAppPoseDriver.enabled = true;

            if (m_HasActiveSticker)
            {
                ACApp.instance.UnLoadStickerManager();
                yield return SceneManager.UnloadSceneAsync(m_CurrentStickerScene);
                m_CurrentAssetBundle.Unload(true);
            }

            AssetBundle assetBundle;
            AssetBundleCreateRequest sceneBundleLoadRequest = AssetBundle.LoadFromFileAsync(stickerPath);
            yield return sceneBundleLoadRequest;
            Debug.Assert(sceneBundleLoadRequest != null && sceneBundleLoadRequest.assetBundle != null, 
                "We assume asset bundle loading will always succeed here, need to improve exception handling");

            assetBundle = sceneBundleLoadRequest.assetBundle;

            string[] scenePaths = assetBundle.GetAllScenePaths();
            string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);

            Scene currentScene = SceneManager.GetSceneByName(sceneName);
            SceneManager.SetActiveScene(currentScene);
            ACApp.instance.LoadStickerManager();

            // Wait a bit until the new camera's AR background renderer is set up
            yield return new WaitForSeconds(0.2f);
            m_CameraAppPoseDriver.enabled = false;
            m_CameraAppCamBackground.enabled = false;
            m_CameraAppARCamera.enabled = false;

            // Currently we may run start up script while covered up by m_CameraAppARCamera
            // But ideally scripts from new scene start running their script here... TODO

            m_HasActiveSticker = true;
            m_CurrentStickerScene = currentScene;
            m_CurrentAssetBundle = assetBundle;
        }
    }
}