﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace OASIS
{
    public class UIEventHandler : MonoBehaviour
    {
#pragma warning disable CS0649 // Private SerializeField set by Unity Editor

        [SerializeField]
        List<Button> m_RemoteStickerButtons;

        [SerializeField]
        List<Button> m_LocalStickerButtons;

#pragma warning restore CS0649

        List<Text> m_RemoteStickerTextUIs = new List<Text>();
        Dictionary<string, int> m_RemoteStickerNameToUIIdx = new Dictionary<string, int>();
        Dictionary<int, string> m_RemoteUIIdxToStickerName = new Dictionary<int, string>();

        List<Text> m_LocalStickerTextUIs = new List<Text>();
        Dictionary<int, string> m_LocalUIIdxToStickerName = new Dictionary<int, string>();

        void Start()
        {
            m_RemoteStickerTextUIs.Clear();
            for (int i = 0; i < m_RemoteStickerButtons.Count; ++i)
            {
                Text textComponent = m_RemoteStickerButtons[i].transform.GetComponentInChildren<Text>(false);
                Debug.Assert(textComponent != null, "Please assign a child with text component to the buttons managed by this class");
                m_RemoteStickerTextUIs.Add(textComponent);
            }

            m_LocalStickerTextUIs.Clear();
            for (int i = 0; i < m_LocalStickerButtons.Count; ++i)
            {
                Text textComponent = m_LocalStickerButtons[i].transform.GetComponentInChildren<Text>(false);
                Debug.Assert(textComponent != null, "Please assign a child with text component to the buttons managed by this class");
                m_LocalStickerTextUIs.Add(textComponent);
            }

            InitLocalStickers();

            ACApp.instance.stickerAssetDownloader.SyncStickerState(true);
            ACApp.instance.stickerAssetDownloader.stickerStateSynced += OnRemoteStickerStateSynced;
            ACApp.instance.stickerAssetDownloader.stickerDownloading += OnRemoteStickerDownloading;
            ACApp.instance.stickerAssetDownloader.stickerDownloaded += OnRemoteStickerDownloaded;

            ClearSticker();
        }

        void InitLocalStickers()
        {
            m_LocalUIIdxToStickerName.Clear();

            int buttonIdxToFill = 0;
            foreach (var value in ACApp.instance.localStickerAssetManager.localStickerNames)
            {
                if (buttonIdxToFill >= m_LocalStickerTextUIs.Count)
                {
                    break;
                }

                m_LocalStickerTextUIs[buttonIdxToFill].text = value;
                m_LocalStickerTextUIs[buttonIdxToFill].color = Color.black;

                m_LocalUIIdxToStickerName[buttonIdxToFill] = value;

                buttonIdxToFill++;
            }

            while (buttonIdxToFill < m_LocalStickerTextUIs.Count)
            {
                m_LocalStickerTextUIs[buttonIdxToFill].text = "";
                m_LocalStickerTextUIs[buttonIdxToFill].color = Color.black;

                buttonIdxToFill++;
            }
        }

        void OnRemoteStickerStateSynced()
        {
            m_RemoteStickerNameToUIIdx.Clear();
            m_RemoteUIIdxToStickerName.Clear();

            int buttonIdxToFill = 0; 
            foreach (var value in ACApp.instance.stickerAssetDownloader.stickerStates)
            {
                if (buttonIdxToFill >= m_RemoteStickerTextUIs.Count)
                {
                    break;
                }

                m_RemoteStickerTextUIs[buttonIdxToFill].text = value.Key;
                switch (value.Value)
                {
                    case StickerAssetDownloader.StickerAssetState.kToBeDownloaded:
                        m_RemoteStickerTextUIs[buttonIdxToFill].color = Color.gray / 2.0f;
                        break;

                    case StickerAssetDownloader.StickerAssetState.kDownloading:
                        m_RemoteStickerTextUIs[buttonIdxToFill].color = Color.gray;
                        break;

                    case StickerAssetDownloader.StickerAssetState.kDownloaded:
                        m_RemoteStickerTextUIs[buttonIdxToFill].color = Color.black;
                        break;
                }

                m_RemoteStickerNameToUIIdx[value.Key] = buttonIdxToFill;
                m_RemoteUIIdxToStickerName[buttonIdxToFill] = value.Key;

                buttonIdxToFill++;
            }

            while (buttonIdxToFill < m_RemoteStickerTextUIs.Count)
            {
                m_RemoteStickerTextUIs[buttonIdxToFill].text = "";
                m_RemoteStickerTextUIs[buttonIdxToFill].color = Color.black;

                buttonIdxToFill++;
            }
        }

        void OnRemoteStickerDownloading(string stickerName)
        {
            int idx = m_RemoteStickerNameToUIIdx[stickerName];
            Debug.Assert(m_RemoteStickerTextUIs[idx].text == stickerName);
            m_RemoteStickerTextUIs[idx].color = Color.gray;
        }

        void OnRemoteStickerDownloaded(string stickerName)
        {
            int idx = m_RemoteStickerNameToUIIdx[stickerName];
            Debug.Assert(m_RemoteStickerTextUIs[idx].text == stickerName);
            m_RemoteStickerTextUIs[idx].color = Color.black;
        }

        public void SyncWithServer()
        {
            ACApp.instance.stickerAssetDownloader.SyncStickerState(true);
        }

        public void RemoteStickerButtonClicked(int idx)
        {
            string stickerName;
            bool found = m_RemoteUIIdxToStickerName.TryGetValue(idx, out stickerName);
            if (!found)
                return;

            var state = ACApp.instance.stickerAssetDownloader.stickerStates[stickerName];

            if (state == StickerAssetDownloader.StickerAssetState.kToBeDownloaded)
            {
                ACApp.instance.stickerAssetDownloader.DownloadSticker(stickerName);
            }
            else if (state == StickerAssetDownloader.StickerAssetState.kDownloaded)
            {
                ACApp.instance.stickerAssetLoader.LoadSticker(stickerName, StickerLoadSource.kFromCache);
            }
        }

        public void LocalStickerButtonClicked(int idx)
        {
            string stickerName;
            bool found = m_LocalUIIdxToStickerName.TryGetValue(idx, out stickerName);
            if (!found)
                return;

            ACApp.instance.stickerAssetLoader.LoadSticker(stickerName, StickerLoadSource.kFromLocal);
        }

        public void ClearSticker()
        {
            ACApp.instance.stickerAssetLoader.LoadSticker(ACApp.instance.localStickerAssetManager.defaultStickerName, StickerLoadSource.kFromLocal);
        }

        public void SwapCamera()
        {
            ACApp.instance.SetNextCamera();
        }

    }
}