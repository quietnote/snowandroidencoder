﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace OASIS
{
    using Data;
    using AR;
    // 세션에서 받아온 데이터와
    // 빌더로 생성된 데이터들을 다 넣는곳

    [Serializable]
    public class ACData : MonoBehaviour
    {
        #region device
        public Vector3 rawGravityVector;
        public Vector3 rawUserAcceleration;
        public Vector3 rawRotationRate;
        public Quaternion rawAttitude;

        public Vector3 gravityVector;

        public Vector3 devicePosition;
        public Quaternion deticeRotation;
        #endregion

        #region frame
        public CameraFrame frame = new CameraFrame();
        #endregion

        #region face
        public int faceCount;
        public int maxFaceCount;
        public FaceData [] faceDatas;

        public void FaceDataClear()
        {
            faceDatas = new FaceData[ACConst.APP_MAX_FACE];
            for(int i=0 ; i < ACConst.APP_MAX_FACE;++i)
            {
                faceDatas[i] = new FaceData();
            }
        }

        public FaceData GetFace(int index)
        {
            return faceDatas[index];
        }

        #endregion
        

        // Start is called before the first frame update
        void Start()
        {
            maxFaceCount = ACConst.APP_MAX_FACE;

            faceCount = 0;

            faceDatas = new FaceData[ACConst.APP_MAX_FACE];
            for(int i=0 ; i < ACConst.APP_MAX_FACE;++i)
            {
                faceDatas[i] = new FaceData();
            }
        }      
    }
}