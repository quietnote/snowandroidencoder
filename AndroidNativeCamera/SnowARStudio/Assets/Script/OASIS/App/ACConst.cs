using System.Collections.Generic;
// 앱에서 쓸 static한 것들 여기 다 모아보자
namespace OASIS
{
    public static class ACConst
    {
        public enum AppMode
        {
            NOTDEFINED,
            APP,
            STUDIO,
        }

        public const int FACE_ULSEE_SIZE = 66;
        public const int FACE_SENSETIME_SIZE = 106;
        public const int FACE_ULSEE_POSE_SIZE = 6;

        public const int FACE_EXTRA_FEATURE_SIZE = FACE_ULSEE_SIZE + 42;

        public const int APP_MAX_FACE = 10;
        public const int APP_MAX_BODY = 10;

        public const float extraScale = 2.5f;

        public static class Path
        {
            public static string SCENE = "Assets/Scenes/Release/";
        }

        public static class SceneName
        {
            public static string STARTER = "Starter";
            public static string CAMERA_APP = "CameraApp";
        }

        public static class Extention
        {
            public static string SCENE = ".unity";
            public static string INFO = ".info";
            public static string PREFAB = ".prefab";
            public static string FBX = ".fbx";
            public static string ASSET = ".asset";
            public static string STRINGS = ".strings";
        }
    }
}