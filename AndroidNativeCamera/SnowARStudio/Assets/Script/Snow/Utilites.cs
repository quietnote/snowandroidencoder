﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Snow.G
{
    public static class Utilites
    {
        public static TEnum ToEnum<TEnum>(this int val)
        {
            return (TEnum) System.Enum.ToObject(typeof(TEnum), val);
        }

        public static TEnum ToEnum<TEnum>(this string val)
        {
            return (TEnum) System.Enum.Parse(typeof(TEnum), val);
        }

        public static string GetGameObjectFullPath(this GameObject obj)
        {
            string path = "/" + obj.name;
            while (obj.transform.parent != null)
            {
                obj = obj.transform.parent.gameObject;
                path = "/" + obj.name + path;
            }
            return path;
        }

        public static string GetGameObjectFullPath(this Transform transform)
        {
            string path = transform.name;
            while (transform.parent != null)
            {
                transform = transform.parent;
                path = transform.name + "/" + path;
            }
            return path;
        }

        public static T GetOrAddComponent<T>(this GameObject obj)
        where T : Component
        {
            var component = obj.GetComponent<T>();
            if (component == null)
            {
                component = obj.AddComponent<T>();
            }

            return component;
        }

        static public T GetOrAddComponent<T>(this Component obj)
        where T : Component
        {
            var component = obj.GetComponent<T>();
            if (component == null)
            {
                component = obj.gameObject.AddComponent<T>();
            }

            return component;
        }

        static public T GetOrAddComponent<T>(this Transform obj)
        where T : Transform
        {
            var component = obj.GetComponent<T>();
            if (component == null)
            {
                component = obj.gameObject.AddComponent<T>();
            }

            return component;
        }
    }
}