﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
#endif
using UnityEngine;

namespace Snow.G
{
    public class PlatformDepStreamingAssetsHandler
    {
        public static string PlatformDepStreamingAssetsPathFor(RuntimePlatform platform)
        {
            switch (platform)
            {
                case (RuntimePlatform.Android):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/Android");
                    }

                case (RuntimePlatform.IPhonePlayer):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/iOS");
                    }

                case (RuntimePlatform.WindowsEditor):
                case (RuntimePlatform.WindowsPlayer):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/Windows");
                    }

                case (RuntimePlatform.OSXEditor):
                case (RuntimePlatform.OSXPlayer):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/OSX");
                    }

                default:
                    {
                        Debug.LogError("Currently only support Android/iOS/Windows/OSX");
                        break;
                    }
            }

            return "";
        }

#if UNITY_EDITOR
        public static string PlatformDepStreamingAssetsPathFor(BuildTarget platform)
        {
            switch (platform)
            {
                case (BuildTarget.Android):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/Android");
                    }

                case (BuildTarget.iOS):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/iOS");
                    }

                case (BuildTarget.StandaloneWindows):
                case (BuildTarget.StandaloneWindows64):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/Windows");
                    }

                case (BuildTarget.StandaloneOSX):
                    {
                        return Path.Combine(Application.dataPath, "StreamingAssetsPerPlatform/OSX");
                    }

                default:
                    {
                        Debug.LogError("Currently only support Android/iOS/Windows/OSX");
                        break;
                    }
            }

            return "";
        }

        struct FileMoveOperation
        {
            public string fromPath;
            public string toPath;
        }
        static readonly List<FileMoveOperation> nameOfFileCopied = new List<FileMoveOperation>();

        class PrebuildHandler : IPreprocessBuildWithReport
        {
            public int callbackOrder { get { return 0; } }

            public void OnPreprocessBuild(BuildReport report)
            {
                BuildTarget platform = report.summary.platform;
                string sourceDir = PlatformDepStreamingAssetsPathFor(platform);

                if (nameOfFileCopied.Count > 0)
                {
                    MoveFilesBack();
                }

                foreach (string fromPath in System.IO.Directory.GetFiles(sourceDir))
                {
                    string name = Path.GetFileName(fromPath);
                    if (name == ".DS_Store")
                    {
                        // Do not copy system file on OSX
                        continue;
                    }

                    string toPath = Path.Combine(Application.streamingAssetsPath, name);
                    try
                    {
                        FileUtil.MoveFileOrDirectory(fromPath, toPath);
                    }
                    catch (Exception e)
                    {
                        if (e.GetType() == typeof(IOException))
                        {
                            // Target file may already exist, avoid over-writing the existing file and do nothing
                            Debug.LogWarning("IOException from PrePlatformDepStreamingAssetsHandler.OnPreprocessBuild: " + e);
                            continue;
                        }
                        else
                        {
                            // Unknown exception, worth throwing
                            Debug.Log(e);
                            throw;
                        }
                    }

                    nameOfFileCopied.Add(new PlatformDepStreamingAssetsHandler.FileMoveOperation { fromPath = fromPath, toPath = toPath });
                }

                EditorApplication.update += MoveFilesBackWhenBuildDone;
            }

            static void MoveFilesBackWhenBuildDone()
            {
                if (!BuildPipeline.isBuildingPlayer)
                {
                    MoveFilesBack();
                    EditorApplication.update -= MoveFilesBackWhenBuildDone;
                }
            }

            static void MoveFilesBack()
            {
                foreach (FileMoveOperation op in nameOfFileCopied)
                {
                    FileUtil.MoveFileOrDirectory(op.toPath, op.fromPath);
                }
                nameOfFileCopied.Clear();
            }
        }
#endif
    }
}