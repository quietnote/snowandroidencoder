﻿using System.IO;

namespace Snow.G
{
    public class LogSystem
    {
        public delegate void LogFuncType(object message);
		public LogFuncType Log = null;
		public LogFuncType LogWarning = null;
		public LogFuncType LogError = null;
		public LogFuncType LogDev = null;


		private const string LOG_DIR = "Logs";
		private const string LOG_FILE_NAME = "Snow_PN_Log";
		private const string LOG_FILE_EXT = ".log";

		private StreamWriter logStream = null;

		private string logFilePath = null;

        #region Global Log System
        // For global access
		private static LogSystem appLog = null;
		public static LogSystem GLog { get { return appLog; } }

        #endregion

        // App 에서 life cycle 관리
        public void Create(bool isGlobalLog)
		{
			InitInterface();
			StartLogFile();
            StartServerLog();
			if (isGlobalLog)
			{
				appLog = this;
			}
		}

		public void Destroy()
		{
			if (this == appLog)
			{
				appLog = null;
			}
            CloseServerLog();
			CloseLogFile();
		}

        private void InitInterface()
		{
#if UNITY_EDITOR
			Log = UnityEngine.Debug.Log;
			LogWarning = UnityEngine.Debug.LogWarning;
			LogError = UnityEngine.Debug.LogError;
			LogDev = UnityEngine.Debug.Log;
#else
			Log = SnowLog;
			LogWarning = SnowLogWarning;
			LogError = SnowLogError;
			LogDev = SnowLogDev;
#endif

			UnityEngine.Application.logMessageReceived += UnityLogHandler;
		}

        #region Unity Log Handler
		private void UnityLogHandler(string logString, string stackTrace, UnityEngine.LogType type)
		{
			switch (type)
			{
				case UnityEngine.LogType.Error:
				case UnityEngine.LogType.Assert:
				case UnityEngine.LogType.Exception:
					{
						if (string.IsNullOrEmpty(stackTrace))
						{
							try
							{
								stackTrace = System.Environment.StackTrace;
							}
							catch (System.Exception e)
							{
								// Can't get stackTrace;
								stackTrace = string.Format(e.Message);
							}
						}
						SnowLogError(logString + "\n" + stackTrace);
					}
					break;
				case UnityEngine.LogType.Warning:
					{
						SnowLogWarning(logString);
					}
					break;
				case UnityEngine.LogType.Log:
					{
						SnowLog(logString);
					}
					break;
				default:
					{
						SnowLog(logStream);
					}
					break;
			}
		}
		#endregion // Unity Log Handler

        #region Snow Log Interface
		private void SnowLog(object message)
		{
			FileLog(message.ToString());
			ServerLog(message.ToString());
		}

		private void SnowLogWarning(object message)
		{
			FileLog("[Warning]" + message.ToString());
			ServerLog("[Warning]" + message.ToString());
		}

		private void SnowLogError(object message)
		{
			FileLog("[Error]" + message.ToString());
			ServerLog("[Error]" + message.ToString());
			// 에러 받으면 종료절차.
			Destroy();
		}
#if !UNITY_EDITOR 
		private void SnowLogDev(object message)
		{
			if(UnityEngine.Debug.isDebugBuild)
			{
				FileLog("[Dev]" + message.ToString());
				ServerLog("[Dev]" + message.ToString());
			}
		}
#endif

		#endregion // Snow Log Interface

		#region File Log

		private void StartLogFile()
		{
			if (null == logStream)
			{
				string curDir = System.IO.Directory.GetCurrentDirectory();

#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
				string logForder = curDir + "/" + LOG_DIR;
#elif UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN 
				string logForder = curDir + "\\" + LOG_DIR;
#else
				string logForder = UnityEngine.Application.persistentDataPath + "/" + LOG_DIR;
#endif
				if (!System.IO.Directory.Exists(logForder))
				{
					System.IO.Directory.CreateDirectory(logForder);
				}
#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
				logFilePath = logForder + "/" + LOG_FILE_NAME + LOG_FILE_EXT;
#elif UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN 
				logFilePath = logForder + "\\" + LOG_FILE_NAME + LOG_FILE_EXT;
#else
				logFilePath = logForder + "/" + LOG_FILE_NAME + LOG_FILE_EXT;
#endif

				if (!string.IsNullOrEmpty(logFilePath))
				{
					StreamWriter fileStream = null;
					try
					{
						fileStream = new StreamWriter(logFilePath, false);
					}
					catch
					{
						if (null != fileStream)
						{
							fileStream.Close();
							fileStream.Dispose();
							fileStream = null;
						}
					}
					logStream = fileStream;
				}
				else
				{
					logStream = null;
				}

			}
		}

		private void CloseLogFile()
		{
			if (null != logStream)
			{
				FileLog("EOF");
				logStream.Close();
				logStream.Dispose();
				logStream = null;
			}
		}
		// 파일에만 쓰고싶을때 direct access를 위해 public
		public void FileLog(string message)
		{
			if (null != logStream)
			{
				logStream.WriteLine(System.DateTime.Now.ToString("[HH:mm:ss.ff]") + " - " + message);
				logStream.Flush();
			}
		}
		#endregion // File Log

		#region Server Log

		private void StartServerLog()
		{
			// to do : Server Connect, Init;
		}

		private void CloseServerLog()
		{
			// to do : Server Disconnect, destroy;
		}
		// 서버에만 보내고 싶을때 direct access 를 위해 public
		public void ServerLog(string message)
		{
			// to do : sent Server Log;
		}

		#endregion // Server Log

    }
}
