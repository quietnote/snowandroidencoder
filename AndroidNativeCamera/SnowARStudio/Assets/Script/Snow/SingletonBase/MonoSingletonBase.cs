﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Snow.G
{
    public class MonoSingletonBase<T> : MonoBehaviour where T : MonoBehaviour
    {
        static T s_Instance = null;

        public static T instance
        {
            get
            {
                return s_Instance;
            }
        }

        protected void Awake()
        {
            if (s_Instance != null)
            {
                System.Type type = typeof(T);
                Debug.LogWarning("Two instances of singleton " + type.Name + ", this instance cannot be accessed via " + type.Name + ".instance");
            }
            else
            {
                s_Instance = this as T;
            }
        }

        protected void OnDestroy()
        {
            if (s_Instance == this as T)
            {
                s_Instance = null;
            }
        }
    }
}