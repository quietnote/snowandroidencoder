namespace Snow.G
{
    public abstract class SingletonBase<T> where T : class, new()
    {
        private static T _instance = new T();
        public static T instance { get { return _instance; } }
    }
}