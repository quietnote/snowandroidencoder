﻿using UnityEngine;
using System.IO;

namespace Snow.G
{
    public static class FileUtils
    {
        static string directory = Application.persistentDataPath;

        public static void WriteStringInLocal(string directory, string filename, string str)
        {
            WriteString(string.Format("{0}/{1}", Application.dataPath, directory), filename, str);
        }

        public static void WriteString(string filename, string str)
        {
            WriteString(directory, filename, str);
        }

        public static void WriteString(string directory, string filename, string str)
        {
            bool exists = System.IO.Directory.Exists(directory);
            if (!exists)
                System.IO.Directory.CreateDirectory(directory);

            string path = string.Format("{0}/{1}", directory, filename);
            StreamWriter writer = new StreamWriter(path, false);
            writer.WriteLine(str);
            writer.Close();
            Debug.Log("WriteString() at : " + path);
        }

        public static string ReadStringInLocal(string directory, string filename)
        {
            return ReadString(string.Format("{0}/{1}", Application.dataPath, directory), filename);
        }

        public static string ReadString(string filename)
        {
            return ReadString(directory, filename);
        }

        public static string ReadString(string directory, string filename)
        {
            string path = string.Format("{0}/{1}", directory, filename);
            if (!File.Exists(path))
            {
                Debug.Log("Not found file : " + path);
                return "";
            }

            StreamReader reader = new StreamReader(path);
            string str = reader.ReadToEnd();
            reader.Close();

            return str;
        }
    }
}