﻿using UnityEngine;
using JsonFx;

namespace Snow.G
{
    public static class JsonUtils
    {
        public static string ToJson(object obj)
        {
            return JsonFx.Json.JsonWriter.Serialize(obj);
        }

        public static T FromJson<T>(string json)
        {
            return JsonFx.Json.JsonReader.Deserialize<T>(json);
        }
    }
}