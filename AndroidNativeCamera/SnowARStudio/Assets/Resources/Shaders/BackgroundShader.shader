﻿Shader "Hidden/BackgroundShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}

	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			uniform vector _UVTransform;
			uniform float _UOffset;
			uniform float _VOffset;

			fixed4 frag (v2f i) : SV_Target
			{
				float2x2 trans = float2x2(_UVTransform.x, _UVTransform.y, _UVTransform.z, _UVTransform.w);
				i.uv = mul(trans, i.uv) + float2(_UOffset, _VOffset);

				fixed4 col = tex2D(_MainTex, i.uv);
				return col;
			}
			ENDCG
		}
	}
}
